<script type="text/javascript">

$('#sysoverviewbanner').remove();
if (typeof String.prototype.cStartsWith != 'function') {
  String.prototype.cStartsWith = function (str){
    return this.indexOf(str) === 0;
  };
}

$( document ).ajaxComplete(function( event, xhr, settings ) {
	$('#sysoverviewbanner').hide();
	if ((settings.data.cStartsWith('getsystemoverview')) && (xhr.status === 200) ){
		$('#sysoverviewbanner a').each(function() {
		   var split = $(this).text().split(' ');
            if ($(this).attr("href") == "supporttickets.php"){
				$('#tickets-sbox-value').html(split[0]);
			}else if($(this).attr("href") == "cancelrequests.php"){
				$('#cancel-sbox-value').html(split[0]);
			}else if($(this).attr("href") == "todolist.php"){
				$('#todo-sbox-value').html(split[0]);
			}else if($(this).attr("href") == "networkissues.php"){
				$('#network-sbox-value').html(split[0]);
			}
		});		
	}
});

</script>

<div id="sysoverviewbanner" style="display:none;"></div>


<!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
				<h3 id="tickets-sbox-value"><i class="fa fa-spinner fa-pulse"></i></h3>
                  <p>{$_ADMINLANG.stats.ticketsawaitingreply}</p>
                </div>
                <div class="icon">
                  <i class="fa fa-ticket"></i>
                </div>
                <a href="supporttickets.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
				<h3 id="cancel-sbox-value"><i class="fa fa-spinner fa-pulse"></i></h3>
                  <p>{$_ADMINLANG.stats.pendingcancellations}</p>
                </div>
                <div class="icon">
                  <i class="fa fa-ban"></i>
                </div>
                <a href="cancelrequests.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
				<h3 id="todo-sbox-value"><i class="fa fa-spinner fa-pulse"></i></h3>
                  <p>{$_ADMINLANG.stats.todoitemsdue}</p>				  
                </div>
                <div class="icon">
                  <i class="fa fa-list-ol"></i>
                </div>
                <a href="todolist.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
				<h3 id="network-sbox-value"><i class="fa fa-spinner fa-pulse"></i></h3>
                  <p>{$_ADMINLANG.stats.opennetworkissues}</p>
                </div>
                <div class="icon">
                  <i class="fa  fa-sitemap"></i>
                </div>
                <a href="networkissues.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
          </div><!-- /.row -->