          <div class="tab-pane active" id="control-sidebar-browser-tab">
		  
            <ul class="control-sidebar-menu">
              <li>
                <a href="#">
                  <i class="menu-icon fa fa-globe bg-red "></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">{$_ADMINLANG.browser.bookmarks}</h4>
                    <p><div class="progress progress-xxs"><div class="progress-bar progress-bar-danger" style="width: 100%"></div></div></p>
                  </div>
                </a>
              </li>
            </ul>
			
			<ul class="fa-ul">
				<li><i class="fa-li fa  fa-square"></i><a href="http://www.whmcs.com/" target="brwsrwnd">WHMCS Homepage</a></li>
				<li><i class="fa-li fa  fa-square"></i><a href="https://www.whmcs.com/clients/" target="brwsrwnd">WHMCS Client Area</a></li>
				{foreach from=$browserlinks item=link}
				<li><i class="fa-li fa  fa-minus-square" onclick="doDelete('{$link.id}')" style="cursor: pointer; cursor: hand;"></i><a href="{$link.url}" target="brwsrwnd">{$link.name}</a></li>
				{/foreach}
			</ul>

            <ul class="control-sidebar-menu">
              <li>
                <a href="#">
                  <i class="menu-icon fa fa-plus bg-red "></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">{$_ADMINLANG.browser.addnew}</h4>
                    <p><div class="progress progress-xxs"><div class="progress-bar progress-bar-danger" style="width: 100%"></div></div></p>
                  </div>
                </a>
              </li>
            </ul>
			
			<form method="post" action="browser.php?action=add">
				<input type="hidden" name="token" value="{$csrfToken}" />

				<div class="form-group">
					<label>{$_ADMINLANG.browser.sitename}</label>
					<input type="text" name="sitename" class="form-control input-sm">
				</div>				

				<div class="form-group">
					<label>{$_ADMINLANG.browser.url}</label>
					<input type="text" name="siteurl" placeholder="http://" class="form-control input-sm">
				</div>				

				<div class="form-group">
					<input type="submit" value="{$_ADMINLANG.browser.add}" class="btn btn-warning btn-block" />
				</div>

			</form>
		</div>
