function mascarar(field){
	try {
		$(field).unmask();
	} catch (e) { }
	var size=$(field).val().length;

	if(size==11){
		$(field).mask("999.999.999-99");
	} else if(size==14) {
		$(field).mask("99.999.999/9999-99");
	} else {
		try {
			$(field).unmask();
		} catch (e) { }
	}
}
function desmascarar(field){
	try {
		$(field).val($(field).val().replace(/[\.]|[\-]|[\/]/g,''));
		$(field).unmask();
	} catch (e) { }
}
$(document).ready(function(){
	var $cpfcnpj=$("td.fieldlabel:contains('CNPJ ou CPF')").next().find('input');
	$cpfcnpj.blur(function(){mascarar(this);});
	$cpfcnpj.focus(function(){desmascarar(this);});
	$cpfcnpj.keypress(function(event){
		if(event.keyCode==10||event.keyCode==13){
			event.preventDefault();
			mascarar(this);
			$('form[action="clients.php"]').submit();
		}
	});
});
