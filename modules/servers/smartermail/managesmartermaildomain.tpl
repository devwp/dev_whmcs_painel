<link rel="stylesheet" type="text/css" href="https://controle.webplus.com.br/assets/css/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.responsive.css">
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.responsive.min.js"></script>
{if $message != ""}
<div class="alert alert-success text-center" id="alertModuleCustomButtonSuccess">{$message} </div>
<br clear="both" />
{/if} {if $errormessage != ""}
<div class="alert alert-danger text-center" id="alertModuleCustomButtonFailed">{$errormessage} </div>
<br clear="both" />
{/if} 
<script type="text/javascript">
jQuery(document).ready(function () { 
    var table = jQuery("#tableUser").DataTable({ "dom": '<"listtable"fit>pl', "responsive": true, "oLanguage": { "sEmptyTable": "{$LANG.smglobalNothing}", "sInfo": "", "sInfoEmpty": "", "sInfoFiltered": "({$LANG.smglobalshowing} _MAX_ total)", "sInfoPostFix": "", "sInfoThousands": ",", "sLengthMenu": "{$LANG.smglobalshow} _MENU_", "sLoadingRecords": "{$LANG.smgloballoading}", "sProcessing": "{$LANG.smglobalProcess}", "sSearch": "", "sZeroRecords": "{$LANG.smglobalNothing}", "oPaginate": { "sFirst": "", "sLast": "", "sNext": "{$LANG.smglobalNext}", "sPrevious": "{$LANG.smglobalPrev}" } }, "pageLength": 10, "columnDefs": [{ "targets": 'no-sort', "orderable": false, }], "order": [[0, "asc"]], "stateSave": true }); jQuery('#tableUser').removeClass('hidden').DataTable(); table.draw(); jQuery("#tableUser_filter input").attr("placeholder", "{$LANG.smglobalsearch}");
    var tablealias = jQuery("#tablealias").DataTable({ "dom": '<"listtable"fit>pl', "responsive": true, "oLanguage": { "sEmptyTable": "{$LANG.smglobalNothing}", "sInfo": "", "sInfoEmpty": "", "sInfoFiltered": "({$LANG.smglobalshowing} _MAX_ total)", "sInfoPostFix": "", "sInfoThousands": ",", "sLengthMenu": "{$LANG.smglobalshow} _MENU_", "sLoadingRecords": "{$LANG.smgloballoading}", "sProcessing": "{$LANG.smglobalProcess}", "sSearch": "", "sZeroRecords": "{$LANG.smglobalNothing}", "oPaginate": { "sFirst": "", "sLast": "", "sNext": "{$LANG.smglobalNext}", "sPrevious": "{$LANG.smglobalPrev}" } }, "pageLength": 10, "columnDefs": [{ "targets": 'no-sort', "orderable": false, }], "order": [[0, "asc"]], "stateSave": true }); jQuery('#tablealias').removeClass('hidden').DataTable(); tablealias.draw(); jQuery("#tablealias_filter input").attr("placeholder", "{$LANG.smglobalsearch}"); 
    var tablemailinglist = jQuery("#tablemailinglist").DataTable({ "dom": '<"listtable"fit>pl', "responsive": true, "oLanguage": { "sEmptyTable": "{$LANG.smglobalNothing}", "sInfo": "", "sInfoEmpty": "", "sInfoFiltered": "({$LANG.smglobalshowing} _MAX_ total)", "sInfoPostFix": "", "sInfoThousands": ",", "sLengthMenu": "{$LANG.smglobalshow} _MENU_", "sLoadingRecords": "{$LANG.smgloballoading}", "sProcessing": "{$LANG.smglobalProcess}", "sSearch": "", "sZeroRecords": "{$LANG.smglobalNothing}", "oPaginate": { "sFirst": "", "sLast": "", "sNext": "{$LANG.smglobalNext}", "sPrevious": "{$LANG.smglobalPrev}" } }, "pageLength": 10, "columnDefs": [{ "targets": 'no-sort', "orderable": false, }], "order": [[0, "asc"]], "stateSave": true }); jQuery('#tablemailinglist').removeClass('hidden').DataTable(); tablemailinglist.draw(); jQuery("#tablemailinglist_filter input").attr("placeholder", "{$LANG.smglobalsearch}"); 
     var tablemailinglistsign = jQuery("#tablemailinglistsign").DataTable({ "dom": '<"listtable"fit>pl', "responsive": true, "oLanguage": { "sEmptyTable": "{$LANG.smglobalNothing}", "sInfo": "", "sInfoEmpty": "", "sInfoFiltered": "({$LANG.smglobalshowing} _MAX_ total)", "sInfoPostFix": "", "sInfoThousands": ",", "sLengthMenu": "{$LANG.smglobalshow} _MENU_", "sLoadingRecords": "{$LANG.smgloballoading}", "sProcessing": "{$LANG.smglobalProcess}", "sSearch": "", "sZeroRecords": "{$LANG.smglobalNothing}", "oPaginate": { "sFirst": "", "sLast": "", "sNext": "{$LANG.smglobalNext}", "sPrevious": "{$LANG.smglobalPrev}" } }, "pageLength": 10, "columnDefs": [{ "targets": 'no-sort', "orderable": false, }], "order": [[0, "asc"]], "stateSave": true }); jQuery('#tablemailinglistsign').removeClass('hidden').DataTable(); tablemailinglist.draw(); jQuery("#tablemailinglistsign_filter input").attr("placeholder", "{$LANG.smglobalsearch}"); 
 });
</script>

 <script type="text/javascript">
    $(".sidebar-primary").hide();
    $("#internal-content").attr("style","width:100% !important");
     $(function () {
        // Javascript to enable link to tab
        var hash = document.location.hash;
        if (hash) {
            console.log(hash);
            $('.nav-tabs a[href=' + hash + ']').tab('show');
        }
        // Change hash for page-reload
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
            window.location.hash = e.target.hash;
        });
    });
 </script> 
 
<ul class="nav nav-tabs">
  <li class="active"><a href="/clientarea.php?action=productdetails&id={$id}&modop=custom& a=managesmartermaildomains&d={$domainid}">{$LANG.smmanageusers}</a></li>
  <li><a data-toggle="tab" href="#alias">{$LANG.smmanageAliasDom}</a></li>
   {if ($IsDedicated == "on")}
  <li><a data-toggle="tab" href="#mailinglist">{$LANG.smmanagemailinglist}</a></li>
  {/if}
</ul>

<div class="tab-content">
   <div id="users" class="tab-pane fade in active">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">{$LANG.smmanageusers}</h3>
            </div>
            <div class="panel-body">
    
    <div id="musers" class="tab-pane fade in active">
        <br clear="both"/>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalAddUser"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;{$LANG.smaddusers}</button>
        <br clear="both" />
        <div id="myModalAddUser" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{$LANG.smaddusers}</h4>
                </div>
                <div class="modal-body">
                    <form method="post" class="form-horizontal using-password-strength" action="/clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$domainid}">
                        <div class="form-group">
                            <label for="newusername" class="col-sm-3 control-label">{$LANG.smusersemail}</label>
                            <div class="col-sm-6  input-group">
                                <input type="text" name="newusername" id="Text1" style="" class="form-control" /><div class="input-group-addon">@{$DomainName}</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="newfirstname" class="col-sm-3 control-label">{$LANG.smusersname}</label>
                            <div class="col-sm-6 input-group">
                                <input type="text" name="newfirstname" id="Text2" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="newpassword" class="col-sm-3 control-label">{$LANG.smuserspass}</label>
                            <div class="col-sm-6 input-group">
                                <input type="password" name="newpassword" id="Text3" style="" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="text-center">
                                <input type="hidden" name="adddomain" value="{$DomainName}" />
                                <input type="hidden" name="managea" value="adduser" />
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                    &nbsp;&nbsp;{$LANG.smaddusers}</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
        </div>
        <table id="tableUser" class="table table-bordered table-hover table-list dataTable no-footer dtr-inline">
        <thead>
            <tr>
                <th>{$LANG.smusersname }</th>
                <th>{$LANG.contactemail}</th>
                <th>{$LANG.clientareaused}</th>
                <th class="no-sort"></th>
                <th class="no-sort"></th>
            </tr>
        </thead>
        <tbody>
            {foreach $wsGetDomainUsersResultArray as $key => $val }
            <tr>
                <td><a href="/clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$domainid}&managea=EditUsuario&email={$val.Email}"> {$val.Name} {$val.LastName}</a></td>
                <td><span style="display: none;">{$val.Email}</span>  <a href="/clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$domainid}&managea=EditUsuario&email={$val.Email}">{$val.Email} </a></td>
                <td data-order="{$val.PercentUsed}">{$val.PercentUsed}% de {$val.MaxSpace} GB</td>
                <td style="width: 150px;" data-order="{$val.PercentUsed}">
                    <div class="progress" style="margin-bottom: 0px;">
                        <div class="progress-bar" role="progressbar" aria-valuenow="{$val.PercentUsed}" aria-valuemin="0" aria-valuemax="100" style="width: {$val.PercentUsed}%"><span class="sr-only">{$val.PercentUsed}%</span> </div>
                    </div>
                </td>
                <td style="width: 45px;" data-order="{$val.PercentUsed}">
                 
                        <form method="post" action ="/clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$domainid}" class="form-horizontal using-password-strength">
                            <input type="hidden" name="username" value="{$val.User}" />
                            <input type="hidden" name="managed" value="deluser" />
                               <a href="/clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$domainid}&managea=EditUsuario&email={$val.Email}"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                            <!--<a href="/clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$domainid}&managea=EditUsuario&email={$val.Email}"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;&nbsp; --><a href="#" onclick="if(confirm('Esta ação irá apagar todos os emails nesta caixa de entrada.\nDeseja continuar?')){ parentNode.submit(); } else { return false; }" title="{$LANG.smglobalDelete} {$val.Email}"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </form>
                    </div>
                </td>
            </tr>
            {/foreach}
        </tbody>
        </table>
    </div>

        </div>
    </div>

  </div>
  <div id="alias" class="tab-pane fade">
          <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">{$LANG.smmanageAliasDom}</h3>
            </div>
            <div class="panel-body">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModalAddAlias"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;{$LANG.smaddAlias}</button>
    <br clear="both"/>
    <div id="ModalAddAlias" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{$LANG.smaddAliasDom}</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action ="/clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$domainid}#alias" class="form-horizontal using-password-strength">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="newdomainalias">{$LANG.smAliasDom}</label>
                            <div class="col-sm-6 input-group">
                                <input class="form-control" type="text" name="newdomainalias" />
                            </div>
                        </div>
                        <div class="text-center">
                            <div class="form-group">
                                <div class="text-center">
                                    <button class="btn btn-primary" type="submit" name="managed" value="adddomainalias">{$LANG.smaddAlias}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div> 
    </div>
    <form method="post" class="form-horizontal using-password-strength" action="/clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$domainid}">
            <table id="tablealias" class="table table-bordered table-hover table-list dataTable no-footer dtr-inline"> 
                <thead>
                    <tr>
                        <th>{$LANG.smAliasDom }</th>
                        <th class="no-sort"></th>
                    </tr>
                </thead> 
                <tbody>
                    {foreach $DomainEliases as $key => $val}
                        <tr>
                            <td>{$val}</td>
                            <td style="width: 45px;">
                                <div>
                                    <form method="post" action ="/clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$domainid}#alias" class="form-horizontal using-password-strength">
                                        <button type="submit" class="bnt btn-link" name="deldomainalias" value="{$val}" onclick="if(confirm('Esta ação irá apagar este apelido.\nDeseja continuar?')){ parentNode.submit(); } else { return false; }"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    {/foreach}
                </tbody>
            </table>
    </form>
          </div>
    </div>

  </div>
  <div id="mailinglist" class="tab-pane fade">
            <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">{$LANG.smmanagemailinglist}</h3>
            </div>
            <div class="panel-body">
    <a href="#" title="{$LANG.smaddmailinglist}" class="btn btn-primary" data-toggle="modal" data-target="#ModalAddmailinglist">{$LANG.smaddmailinglist}</a>
    <br clear="both"/>
     <div id="ModalAddmailinglist" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{$LANG.smaddmailinglist}</h4>
                </div>
                <div class="modal-body">
                  <form method="post" action ="/clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$domainid}#mailinglist" class="form-horizontal using-password-strength">
                    <div class="form-group">
                        <label for="newusername" class="col-sm-3 control-label">{$LANG.smmailinglistsname}</label>
                        <div class="col-sm-6  input-group">
                            <input type="text" name="newlistname" style="width: 100%;" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="newusername" class="col-sm-3 control-label">{$LANG.smmailinglistsModerator}</label>
                        <div class="col-sm-6  input-group">
                            <select name="newmoderatoradd" class="form-control">
                                {foreach $wsGetDomainUsersResultArray as $domain}
                                <option value="{$domain.Email}">{$domain.Email}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="newfirstname" class="col-sm-3 control-label">{$LANG.smmailinglistssize}</label>
                        <div class="col-sm-6 input-group">
                            <select name="maxmessagesize" class="form-control">
                                <option value="2048" selected>2MB</option>
                                <option value="5120">5MB</option>
                                <option value="10240">10MB</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="newpassword" class="col-sm-3 control-label">{$LANG.smmailinglistsperm}</label>
                        <div class="col-sm-6 input-group">
                            <select name="whocanpost" class="form-control">
                                <option value="Moderator">{$LANG.smmailinglistsmod}</option>
                                <option value="Subscribers">{$LANG.smmailinglistsass}</option>
                                <option value="anyone">{$LANG.smmailinglistsother}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="text-center">
                            <input type="hidden" name="domain" value="{$DomainName}" />
                            <input type="hidden" name="modop" value="custom" />
                            <input type="hidden" name="managea" value="addList" />
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                &nbsp;&nbsp;{$LANG.smaddmailinglist}</button>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div> 
    </div>
    <table id="tablemailinglist" class="table table-bordered table-hover table-list dataTable no-footer dtr-inline">
        <thead>
            <tr>
                <th>{$LANG.smmailinglistsname}</th>
                <th class="no-sort"></th>
            </tr>
        </thead>
        <tbody>
            {foreach $GetMailingList as $domain}
            <tr>
                <td><a href="/clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$domainid}&listname={$domain}&managesign=GetEditSign#mailinglist">{$domain}</a> </td>
                <td width="45">
                    <form method="post" action ="/clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$domainid}#mailinglist" class="form-horizontal using-password-strength">
                        <a href="/clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$domainid}&listname={$domain}&managesign=GetEditSign#mailinglist" title="{$LANG.smeditusers}" ><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        <input type="hidden" name="listname" value="{$domain}" />
                        <input type="hidden" name="managea" value="delList" />
                        <button type="submit" class="bnt btn-link" name="delList" value="{$domain}" onclick="if(confirm('Esta ação irá apagar esta lista.\nDeseja continuar?')){ parentNode.submit(); } else { return false; }"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                    </form>
                </td>
            </tr>
            {/foreach}
        </tbody> 
    </table>
  </div>
</div>
          </div>
    </div>




  <div id="ModalEditUser" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{$LANG.smeditusers}</h4>
                </div>
                <div class="modal-body">
                   <ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#user">{$LANG.smeditusers}</a></li>
  <li><a data-toggle="tab" href="#aliasuser">{$LANG.smmanageAlias}</a></li>
  <li><a data-toggle="tab" href="#config">Configurações</a></li>
</ul>

<div class="tab-content">
   <br clear="both"/>
    <div id="user" class="tab-pane fade in active">
        <form method="post" id="deleteForm3" name="deleteForm" action ="/clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$domainid}" style="float: right;">
            <input type="hidden" name="username" value="{$UserName}">
            <input type="hidden" name="id" value="{$id}" />
            <input type="hidden" name="modop" value="custom" />
            <input type="hidden" name="domain" value="{$DomainName}">
            <input type="hidden" name="a" value="deletesmartermailuser"/>
        </form>

        <form method="post" action ="/clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$domainid}" class="form-horizontal using-password-strength" >
        <h3>{$LANG.smeditusers}</h3>
        <div class="form-group">
        <label for="inputalias" class="col-sm-5 control-label">{$LANG.smusersemail}</label>
        <div class="col-sm-6" style="text-align: left;line-height: 35px;">
        {$UserName}
        </div>
        </div>
        <div class="form-group">
        <label for="inputalias" class="col-sm-5 control-label">{$LANG.smuserspass}</label>
        <div class="col-sm-6">
        <input type="password" name= "password"  value="{$Password}" style="width: 200px; background: #EEE; cursor: not-allowed;" class="form-control" readonly/>
        </div>
        </div> 
        <div class="form-group">
        <label for="inputalias" class="col-sm-5 control-label">Nova Senha</label>
        <div class="col-sm-6">
        <input type="password" name= "newpassword" style="width: 200px;" class="form-control" placeholder="******"/>
        </div> 
        </div> 
        <div class="form-group"> 
        <label for="inputalias" class="col-sm-5 control-label">{$LANG.smusersname}</label>
        <div class="col-sm-6">
        <input type="text" name= "firstname" value="{$FirstName} {$LastName}" style="width: 200px;" class="form-control"/>
        </div>
        </div> 

        {if ($IsDedicated == "on")}
        <div class="form-group">
            <label for="inputalias" class="col-sm-5 control-label">{$LANG.smuserssize}</label>
            <div class="col-sm-6">
            <input type="text" name= "mailboxsize" value="{$maxMailboxSize}" style="width: 200px;float:left;line-height: 34px;" class="form-control"/><span style="float:left">MB</span> 
            </div>
        </div> 
        {else}
        <input type="hidden" name="mailboxsize" value="{$maxMailboxSize}">
        {/if} 
        <br clear="both" />
        <div class="form-group">
        <div class="text-center">

            <input type="hidden" name="domaincheckbox" value="{$isdomainadmin}">
            <input type="hidden" name="lastname" value="{$LastName}"> 
            <input type="hidden" name="username" value="{$UserName}">
            <input type="hidden" name="domain" value="{$DomainName}">
            <input type="hidden" name="id" value="{$id}" />
            <input type="hidden" name="modop" value="custom" />
            <input type="hidden" name="managee" value="edituser" /> 
            {if $isdomainadmin == "True"}{else}<input type="submit" value="{$LANG.smglobalreflesh}" class="btn btn-primary" />  {/if}  
            </div> 
        </div>
        </form>

        <br clear="both" />
    </div>
    <div id="aliasuser" class="tab-pane fade">
        <form method="post" action="/clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$DomainName}">
            <div class="form-group">
            <label class="col-sm-3 control-label" for="newalias">{$LANG.smAlias}</label>
            <div class="col-sm-6 input-group">
            <input type="text"  name="aliasname" style="" class="form-control" /><div class="input-group-addon">@{$DomainName}</div>
            <input type="hidden"  name="newaliasemailaddress" id="username"  style="" class="form-control" />
            <input type="hidden"  name="email" id="email" value="{$UserName}"  style="" class="form-control" />
            </div>
            </div> 
            <div class="text-center"> 
            <button type="submit" name="managed" value="addAlias" class="btn btn-primary">
            {$LANG.smaddAlias}</buton> 
            </div> 
        </form>
        <table id="tablealiasuser" class="table table-list">
                <thead>
                    <tr> 

                        <th>{$LANG.smmanageAlias}</th>
                        <th></th>
                    </tr>
                </thead> 
                <tbody>
                    {foreach $GetAliases as $domain}     
                    <tr>
                        <td style="font-size:16px;">
                            {$domain}
                        </td>
                        <td>
                        <form method="post" class="form-horizontal using-password-strength" action="/clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$domainid}">
                        <input type="hidden" name="aliasname" value="{$domain}" /> 
                         <input type="hidden" name="managed" value="delAlias" /> 
                            <button type"submit" class="bnt btn-link" value="{$domain}"  onclick="if(confirm('Esta ação irá apagar este apelido.\nDeseja continuar?')){ parentNode.submit(); } else { return false; }" ><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                        </form>
                        </td>
                    </tr>
                    {/foreach}
                </tbody>
         
            </table>
    </div>
            <div id="config" class="tab-pane fade">
            <fieldset>
            <h3>Resposta Automática</h3>
            <form method="post" class="form-horizontal using-password-strength" action ="/clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$domainid}">
			<input type='hidden' name="username" value="{$UserName}" />
            <input type="hidden"  name="isenableda" id="isenableda" value="{$GetUserAutoResponseInfoenabled}" class="isenableda" style="" class="form-control" />
            <input type="hidden"  name="isenabledf" id="isenabledf" class="isenabledf" style="" class="form-control" />
            <button class="btnra" type="button" class="btn btn-primary">
            Habilitar resposta automática</button > 
            <button class="btnrad" type="button" class="btn btn-primary" > 
            Desabilitar  resposta automática</button> 
            <div class="form-group ra">
            <label class="col-sm-3 control-label" for="newalias">Assunto</label>
            <div class="col-sm-6 input-group">

            <input type="text"  name="Subject" style="" value="{$GetUserAutoResponseInfosubject}" class="form-control" id="subject"/>
          
            </div>
            </div><br clear="both" />
            <div class="form-group ra">
            <label class="col-sm-3 control-label" for="newalias">Mensagem</label>
            <div class="col-sm-6 input-group">
            <textarea name="Body" style="" class="form-control" id="body">{$GetUserAutoResponseInfobody}</textarea>                                    
            </div>
            </div> <br clear="both" />
            <h3>Redirecionamento</h3>
            <button  class="btnr" type="button" class="btn btn-primary"> 
            Habilitar redirecionamento</button> 
            <button  class="btnrd" type="button" class="btn btn-primary">
            Desabilitar redirecionamento</button>  
            <div class="form-group r">
            <label class="col-sm-3 control-label" for="newalias">Redirecionameto para</label>
            <div class="col-sm-6 input-group">
            <input type="text"  name="forwardaddress" style="" class="form-control" id="forwardaddress" value="{$GetUserForwardingInfo2}"/>                                    
            </div>
            </div><br clear="both" />
            <div class="text-center"> 
            <button type="submit" name="managea" value="UserConfig" class="btn btn-primary">
            Atualizar</buton> <br clear="both" />
            </div>
            </form>
            </fieldset>
            </div>
            </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div> 
    </div>
    {if $editSign == 'on'}
        <script type="text/javascript">
        jQuery(document).ready(function () { 
        $('#ModalEditmailinglistsign').modal('show');
         });
    </script>
    {/if}
    {if $editUser == 'on'}
    <script type="text/javascript">
    jQuery(document).ready(function () { 
    $('#ModalEditUser').modal('show');
    $(".rad").hide();
    $(".ra").hide();
    $(".r").hide();
    $(".btnrad").hide();
    $(".btnrd").hide();
	if ($("#isenableda").val()=='1'){
	            $(".ra").show();
            $(".btnra").hide();
            $(".btnrad").show();
            $("#isenableda").val(true);
	}
		if ($("#forwardaddress").val()!=''){ 
        $(".r").show();
        $(".btnr").hide();
        $(".btnrd").show();
        $("#isenabledf").val(true);
    }
    if($(this).attr('isenableda')== '1'){
            $(".ra").show();
            $(".btnra").hide();
            $(".btnrad").show();
            $("#isenableda").val(true);

    } 
    if($(this).attr('isenabledf') == '1'){
        $(".r").show();
        $(".btnr").hide();
        $(".btnrd").show();
        $("#isenabledf").val(true);
    }

    $('.btnra').on('click',function() {
            $(".ra").show();
            $(".btnra").hide();
            $(".btnrad").show();
            $("#isenableda").val(true);
    });
    $('.btnr').on('click',function() {
            $(".r").show();
            $(".btnr").hide();
            $(".btnrd").show();
             $("#isenabledf").val(true);
    });

    $('.btnrad').on('click',function() {
            $(".ra").hide();
            $(".btnra").show();
            $(".btnrad").hide();
            $("#isenableda").val(false);

    });
    $('.btnrd').on('click',function() {
            $(".r").hide();
            $(".btnr").show();
            $(".btnrd").hide();
            
             $("#forwardaddress").val("");
            $("#isenabledf").val(false);
    });
     });

    </script>
{/if} 








<div id="ModalEditmailinglistsign" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{$LANG.smaddmailinglist}</h4>
            </div>
            <div class="modal-body">
                    <form method="post" action="/clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$domainid}&listname={$ListName}&managesign=GetEditSign#mailinglist" class="form-horizontal using-password-strength" >
                        <div class="form-group">
                        <label for="inputalias" class="col-sm-5 control-label">{$LANG.smmailinglistsASS}</label>
                        <div class="col-sm-6">
                        <textarea cols="36" rows="4" name="newSub" style="overflow: auto; resize: none; width: 100%"  class="form-control"></textarea>
                        </div>
                        </div>
                        <input type="hidden" name="listname" value="{$ListName}" />
                        <div class="form-group"> 
                        <div class="text-center">
                        <input type="hidden" name="id" value="{$id}" />
                        <input type="hidden" name="modop" value="custom" />
                        <input type="hidden" name="domain" value="{$DomainName}" />
                        <input type="hidden" name="managea" value="addListSign" />
                        <input type="submit" value="{$LANG.smglobalsave}" class="btn btn-primary" /> 
                        </div>
                        </div>
                    </form>


                <br clear="both"/>
                <table id="tablemailinglistsign" class="table table-bordered table-hover table-list dataTable no-footer dtr-inline">
                <thead>
                    <tr>
                        <th>Assinaturas</th>
                        <th class="no-sort"></th>
                    </tr>
                </thead>
                <tbody>
                {foreach $GetListSign as $domain}
                <tr>
                    <td>{$domain}</td>
                    <td width="45">
                        <form method="post" action ="/clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$domainid}&listname={$ListName}&managesign=GetEditSign#mailinglist" class="form-horizontal using-password-strength">
                        
                        <input type="hidden" name="sign" value="{$domain}" />
                        <input type="hidden" name="managea" value="delListSign" />
                        <button type="submit" class="bnt btn-link" name="delList" value="{$domain}" onclick="if(confirm('Esta ação irá apagar esta lista.\nDeseja continuar?')){ parentNode.submit(); } else { return false; }"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                        </form>
                    </td>
                </tr>
                {/foreach}
                </tbody> 
                </table><br clear="both" /> 
            </div>
        </div>
    </div>
</div>
<!--
<link rel="stylesheet" type="text/css" href="https://controle.webplus.com.br/assets/css/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.responsive.css">
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.responsive.min.js"></script>
<script type="text/javascript">
    $(".sidebar-primary").hide();
    $("#internal-content").attr("style","width:100% !important");
    
    $(function () {
        // Javascript to enable link to tab
        var hash = document.location.hash;
        if (hash) {
            console.log(hash);
            $('.nav-tabs a[href=' + hash + ']').tab('show');
        }
        // Change hash for page-reload
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
            window.location.hash = e.target.hash;
        });
    });
</script>
<script type="text/javascript"> jQuery(document).ready(function () { var table = jQuery("#tableDomainsList2").DataTable({ "dom": '<"listtable"fit>pl', "responsive": true, "oLanguage": { "sEmptyTable": "{$LANG.smglobalNothing}", "sInfo": "", "sInfoEmpty": "", "sInfoFiltered": "({$LANG.smglobalshowing} _MAX_ total)", "sInfoPostFix": "", "sInfoThousands": ",", "sLengthMenu": "{$LANG.smglobalshow} _MENU_", "sLoadingRecords": "{$LANG.smgloballoading}", "sProcessing": "{$LANG.smglobalProcess}", "sSearch": "", "sZeroRecords": "{$LANG.smglobalNothing}", "oPaginate": { "sFirst": "", "sLast": "", "sNext": "{$LANG.smglobalNext}", "sPrevious": "{$LANG.smglobalPrev}" } }, "pageLength": 10, "columnDefs": [{ "targets": 'no-sort', "orderable": false, }], "order": [[0, "asc"]], "stateSave": true }); jQuery('#tableDomainsList2').removeClass('hidden').DataTable(); table.draw(); jQuery("#tableDomainsList2_filter input").attr("placeholder", "{$LANG.smglobalsearch}"); });
</script>
<script type="text/javascript"> jQuery(document).ready(function () { var table = jQuery("#tableUser").DataTable({ "dom": '<"listtable"fit>pl', "responsive": true, "oLanguage": { "sEmptyTable": "{$LANG.smglobalNothing}", "sInfo": "", "sInfoEmpty": "", "sInfoFiltered": "({$LANG.smglobalshowing} _MAX_ total)", "sInfoPostFix": "", "sInfoThousands": ",", "sLengthMenu": "{$LANG.smglobalshow} _MENU_", "sLoadingRecords": "{$LANG.smgloballoading}", "sProcessing": "{$LANG.smglobalProcess}", "sSearch": "", "sZeroRecords": "{$LANG.smglobalNothing}", "oPaginate": { "sFirst": "", "sLast": "", "sNext": "{$LANG.smglobalNext}", "sPrevious": "{$LANG.smglobalPrev}" } }, "pageLength": 10, "columnDefs": [{ "targets": 'no-sort', "orderable": false, }], "order": [[0, "asc"]], "stateSave": true }); jQuery('#tableUser').removeClass('hidden').DataTable(); table.draw(); jQuery("#tableUser_filter input").attr("placeholder", "{$LANG.smglobalsearch}"); });
</script>
<script type="text/javascript"> jQuery(document).ready(function () { var table = jQuery("#tablealias").DataTable({ "dom": '<"listtable"fit>pl', "responsive": true, "oLanguage": { "sEmptyTable": "{$LANG.smglobalNothing}", "sInfo": "", "sInfoEmpty": "", "sInfoFiltered": "({$LANG.smglobalshowing} _MAX_ total)", "sInfoPostFix": "", "sInfoThousands": ",", "sLengthMenu": "{$LANG.smglobalshow} _MENU_", "sLoadingRecords": "{$LANG.smgloballoading}", "sProcessing": "{$LANG.smglobalProcess}", "sSearch": "", "sZeroRecords": "{$LANG.smglobalNothing}", "oPaginate": { "sFirst": "", "sLast": "", "sNext": "{$LANG.smglobalNext}", "sPrevious": "{$LANG.smglobalPrev}" } }, "pageLength": 10, "columnDefs": [{ "targets": 'no-sort', "orderable": false, }], "order": [[0, "asc"]], "stateSave": true }); jQuery('#tablealias').removeClass('hidden').DataTable(); table.draw(); jQuery("#tablealias_filter input").attr("placeholder", "{$LANG.smglobalsearch}"); });
</script>
{if $message != ""}
<div class="alert alert-success text-center" id="alertModuleCustomButtonSuccess">{$message} </div>
<br clear="both" />
{/if} {if $errormessage != ""}
<div class="alert alert-danger text-center" id="alertModuleCustomButtonFailed">{$errormessage} </div>
<br clear="both" />
{/if} 
<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li><a href="clientarea.php?action=productdetails&id={$id}">{$LANG.yourdetails} </a></li>
            {if $IsDedicated == 'on'}
            <li><a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains">{$LANG.smmanageDomain}</a></li>
            {/if}
            <li class="active"><a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermailuserdomain">{$LANG.smmanageusers}{if $IsDedicated == 'on'} ({$DomainName}){/if}   </a></li>
            {if $IsDedicated != 'on'}
            <li><a href="upgrade.php?type=package&id={$id}">{$LANG.upgradedowngradepackage}</a></li>
            {/if}
        </ul>
    </div>
</div>
<br />
<div class="tabbable">
    {if ($IsDedicated == "on")} 
    <div class="row clearfix">
        <div class="col-xs-12">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#domain2">{$LANG.smmanageusers}</a></li>
                <li><a data-toggle="tab" href="#alias">{$LANG.smmanageAlias}</a></li>
                <li><a data-toggle="tab" href="#resourceusage2">{$LANG.smmanagemailinglist}</a></li>
            </ul>
        </div>
    </div>
    {/if}
</div>
<div class="tab-content">
    <div id="domain2" class="tab-pane active">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <ul class="nav nav-pills">
                            {if $canCreateUser}
                            <li>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalAdd"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;{$LANG.smaddusers}</button></li>
                            {/if}
                        </ul>
                        <div class="tab-content">
                            <div>
                                <br clear="both" />
                                <table id="tableUser" class="table table-bordered table-hover table-list dataTable no-footer dtr-inline">
                                    <thead>
                                        <tr>
                                            <th>{$LANG.smusersname }</th>
                                            <th>{$LANG.contactemail}</th>
                                            <th>{$LANG.clientareaused}</th>
                                            <th class="no-sort"></th>
                                            <th class="no-sort"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {foreach $wsGetDomainUsersResultArray as $key => $val }
                                        <tr>
                                            <td>{$val.Name} {$val.LastName}</td>
                                            <td><span style="display: none;">{$val.Email}</span> {$val.Email} </td>
                                            <td data-order="{$val.PercentUsed}">{$val.PercentUsed}% de {$val.MaxSpace} GB</td>
                                            <td style="width: 150px;" data-order="{$val.PercentUsed}">
                                                <div class="progress" style="margin-bottom: 0px;">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="{$val.PercentUsed}" aria-valuemin="0" aria-valuemax="100" style="width: {$val.PercentUsed}%"><span class="sr-only">{$val.PercentUsed}%</span> </div>
                                                </div>
                                            </td>
                                            <td style="width: 45px;" data-order="{$val.PercentUsed}">
                                                <div>
                                                    <form method="post">
                                                        <input type="hidden" name="username" value="{$val.User}" />
                                                        <input type="hidden" name="managed" value="deluser" />
                                                        <a href="#" title="{$LANG.smeditusers}" class="editmodal" data-toggle="modal" data-target="#myModalEdit" data-user="{$val.User}" subject="{$val.Subject}" body="{$val.Body}" forwardaddress="{$val.forwardaddress}" isenableda="{$val.isenabledAutoResponseInfo}" isenabledf="{$val.isenabledforwardaddress}" data-name="{$val.Name} {$val.LastName}"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;&nbsp; <a href="#" onclick="if(confirm('Esta ação irá apagar todos os emails nesta caixa de entrada.\nDeseja continuar?')){ parentNode.submit(); } else { return false; }" title="{$LANG.smglobalDelete} {$val.Email}"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                        {/foreach}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="alias" class="tab-pane fade">
        <div class="panel panel-default">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-body">
                        <div class="tab-content">
                            <ul class="nav nav-pills">
                                <li>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModalAddAlias"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;{$LANG.smaddAlias}</button></li>
                            </ul>
                            <div>
                                <form method="post" class="form-horizontal using-password-strength">
                                    <ul class="list-group">
                                        <table id="tablealias" class="table table-bordered table-hover table-list dataTable no-footer dtr-inline">
                                            <thead>
                                                <tr>
                                                    <th>{$LANG.smusersname }</th>
                                                    <th class="no-sort"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {foreach $DomainEliases as $key => $val}
                                                    <tr>
                                                        <td>{$val}</td>
                                                        <td style="width: 45px;">
                                                            <div>
                                                                <form method="post">
                                                                    <button type="submit" class="bnt btn-link" name="deldomainalias" value="{$val}" onclick="if(confirm('Esta ação irá apagar este apelido.\nDeseja continuar?')){ parentNode.submit(); } else { return false; }"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                                </form>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                {/foreach}
                                            </tbody>
                                        </table>
                                    </ul>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="resourceusage2" class="tab-pane fade">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="tab-content">
                            <div style="float: left; width: 123px;"><a href="#" title="{$LANG.smaddmailinglist}" class="btn btn-primary" data-toggle="modal" data-target="#myModalAddList">{$LANG.smaddmailinglist}</a> </div>
                            {if $warnings} {include file="$template/includes/alert.tpl" type="warning" msg=$warnings textcenter=true} {/if} 
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tabOverview">
                                    <br />
                                    <div class="table-container clearfix">
                                        <br />
                                        <table id="tableDomainsList2" class="table table-bordered table-hover table-list dataTable no-footer dtr-inline">
                                            <thead>
                                                <tr>
                                                    <th>{$LANG.smmailinglistsname}</th>
                                                    <th class="no-sort"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {foreach $wsGetDomainUsersListResultArray as $domain}
                                                <tr>
                                                    <td><a href="#" title="{$LANG.smeditusers}" class="editlistmodal" data-toggle="modal" data-target="#myModalEditList" listname="{$domain.listname}" whocanpost="{$domain.whocanpost}" whocanpostname="{$domain.whocanpostname}" moderator="{$domain.Moderator}" maxmessagesize="{$domain.maxmessagesize}" maxmessagesizemb="{$domain.maxmessagesizeMB}" listsign=" {http_build_query($domain.listsign)}">{$domain.listname}</a> </td>
                                                    <td width="45">
                                                        <form method="post" class="form-horizontal using-password-strength">
                                                            <a href="#" title="{$LANG.smeditusers}" class="editlistmodal" data-toggle="modal" data-target="#myModalEditList" listname="{$domain.listname}" whocanpost="{$domain.whocanpost}" whocanpostname="{$domain.whocanpostname}" moderator="{$domain.Moderator}" maxmessagesize="{$domain.maxmessagesize}" maxmessagesizemb="{$domain.maxmessagesizeMB}" listsign="{http_build_query($domain.listsign)}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                            <input type="hidden" name="listname" value="{$domain.listname}" />
                                                            <input type="hidden" name="managea" value="delList" />
                                                            <button type="submit" class="bnt btn-link" name="delList" value="{$domain.listname}" onclick="if(confirm('Esta ação irá apagar esta lista.\nDeseja continuar?')){ parentNode.submit(); } else { return false; }"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                                {/foreach}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModalEdit" class="modal large fade" role="dialog">
    <div class="modal-dialog">
      
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title1">{$LANG.smeditusers}</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form method="post" class="form-horizontal using-password-strength">
                        <div class="form-group">
                            <label for="username" class="col-sm-3 control-label">{$LANG.smusersemail}</label>
                            <div class="col-sm-6  input-group">
                                <input type="text" disabled="disabled" name="username" id="username" style="" class="form-control" /><div class="input-group-addon">@{$DomainName}</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="editfirstname" class="col-sm-3 control-label">{$LANG.smusersname}</label>
                            <div class="col-sm-6 input-group">
                                <input type="text" name="editfirstname" id="editfirstname" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="editpassword" class="col-sm-3 control-label">{$LANG.smuserspass}</label>
                            <div class="col-sm-6 input-group">
                                <input type="text" name="editpassword" id="editpassword" style="" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="text-center">
                                {*ifdedicated*}
                                    <input type="hidden" name="editdomain" value="{$DomainName}" />
                                {*ifdedicated*}
                                    <input type="hidden" name="editusername" id="editusername" value="" />
                                <input type="hidden" name="managee" value="edituser" />
                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;&nbsp;{$LANG.smglobalsave}</button>
                            </div>
                        </div>
                    </form>
             
                <div class="col-md-12">
                    <ul class="nav nav-tabs">
                        <li><a data-toggle="tab" href="#menu1">Apelidos</a></li>
                        <li><a data-toggle="tab" href="#menu2">Customizar</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="menu1" class="tab-pane fade in active">
                            <form method="post" class="form-horizontal using-password-strength">
                                <ul class="list-group">
                                    {foreach $wsGetUserAliasResultArray as  $val}<li class="list-group-item">
                                        <input type="hidden" name="managed" value="delAlias" />
                                        <input type="hidden" name="aliasname" value="{$val}" />
                                        <button type="submit" class="bnt btn-link" name="delAlias" value="{$val}" onclick="if(confirm('Esta ação irá apagar este apelido.\nDeseja continuar?')){ parentNode.submit(); } else { return false; }"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                        {$val}</li>
                                    {/foreach}
                                </ul>
                            </form>
                            <form method="post" class="form-horizontal using-password-strength">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="newalias">{$LANG.smAlias}</label>
                                    <div class="col-sm-6 input-group">
                                        <input type="text" name="aliasname" style="" class="form-control" /><div class="input-group-addon">@{$DomainName}</div>
                                        <input type="hidden" name="newaliasemailaddress" id="Hidden1" style="" class="form-control" />
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" name="managed" value="addAlias">{$LANG.smaddAlias}</button>
                                </div>
                            </form>
                        </div>
                        <div id="menu2" class="tab-pane fade">
                            <h3>Resposta Automática</h3>
                            <form method="post" class="form-horizontal using-password-strength">
                                <input type="hidden" name="isenableda" id="isenableda" class="isenableda" style="" class="form-control" />
                                <input type="hidden" name="isenabledf" id="isenabledf" class="isenabledf" style="" class="form-control" />
                                <button class="btnra" type="button">Habilitar resposta automática</button>
                                <button class="btnrad" type="button">Desabilitar  resposta automática</button>
                                <div class="form-group ra">
                                    <label class="col-sm-3 control-label" for="newalias">Assunto</label>
                                    <div class="col-sm-6 input-group">
                                        <input type="text" name="Subject" style="" class="form-control" id="subject" />
                                        <input type="hidden" name="username" id="Hidden2" style="" class="form-control" />
                                    </div>
                                </div>
                                <br clear="both" />
                                <div class="form-group ra">
                                    <label class="col-sm-3 control-label" for="newalias">Mensagem</label>
                                    <div class="col-sm-6 input-group">
                                        <input type="text" name="Body" style="" class="form-control" id="body" />
                                    </div>
                                </div>
                                <br clear="both" />
                                <h3>Redirecionamento</h3>
                                <button class="btnr" type="button">Habilitar redirecionamento</button>
                                <button class="btnrd" type="button">Desabilitar redirecionamento</button>
                                <div class="form-group r">
                                    <label class="col-sm-3 control-label" for="newalias">Redirecionameto para</label>
                                    <div class="col-sm-6 input-group">
                                        <input type="text" name="forwardaddress" style="" class="form-control" id="forwardaddress" />
                                    </div>
                                </div>
                                <br clear="both" />
                                <div class="text-center">
                                <input type="hidden" disabled="disabled" name="username" id="username" style="" class="form-control" />
                                    <button type="submit" name="managea" value="UserConfig">Atualizar</button>
                                    <br clear="both" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
               </div>

            <div class="modal-footer">
                <div class="text-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
</div>
{if $canCreateUser}
<div id="myModalConfig" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{$LANG.smaddusers}</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form method="post" class="form-horizontal using-password-strength">
                        <div class="form-group">
                            <label for="newusername" class="col-sm-3 control-label">{$LANG.smusersemail}</label>
                            <div class="col-sm-6  input-group">
                                <input type="text" name="newusername" id="newusername" style="" class="form-control" /><div class="input-group-addon">@{$DomainName}</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="newfirstname" class="col-sm-3 control-label">{$LANG.smusersname}</label>
                            <div class="col-sm-6 input-group">
                                <input type="text" name="newfirstname" id="newfirstname" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="newpassword" class="col-sm-3 control-label">{$LANG.smuserspass}</label>
                            <div class="col-sm-6 input-group">
                                <input type="text" name="newpassword" id="newpassword" style="" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="text-center">
                                {*ifdedicated*}
                                    <input type="hidden" name="adddomain" value="{$DomainName}" />
                                {*ifdedicated*}
                                    <input type="hidden" name="managea" value="adduser" />
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                    &nbsp;&nbsp;{$LANG.smaddusers}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
</div>
{/if}
<div id="myModalAdd" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{$LANG.smaddusers}</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form method="post" class="form-horizontal using-password-strength">
                        <div class="form-group">
                            <label for="newusername" class="col-sm-3 control-label">{$LANG.smusersemail}</label>
                            <div class="col-sm-6  input-group">
                                <input type="text" name="newusername" id="Text1" style="" class="form-control" /><div class="input-group-addon">@{$DomainName}</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="newfirstname" class="col-sm-3 control-label">{$LANG.smusersname}</label>
                            <div class="col-sm-6 input-group">
                                <input type="text" name="newfirstname" id="Text2" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="newpassword" class="col-sm-3 control-label">{$LANG.smuserspass}</label>
                            <div class="col-sm-6 input-group">
                                <input type="text" name="newpassword" id="Text3" style="" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="text-center">
                                <input type="hidden" name="adddomain" value="{$DomainName}" />
                                <input type="hidden" name="managea" value="adduser" />
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                    &nbsp;&nbsp;{$LANG.smaddusers}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModalAddList" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{$LANG.smaddmailinglist}</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form method="post" class="form-horizontal using-password-strength">
                        <div class="form-group">
                            <label for="newusername" class="col-sm-3 control-label">{$LANG.smmailinglistsname}</label>
                            <div class="col-sm-6  input-group">
                                <input type="text" name="newlistname" style="width: 100%;" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="newusername" class="col-sm-3 control-label">{$LANG.smmailinglistsModerator}</label>
                            <div class="col-sm-6  input-group">
                                <select name="newmoderatoradd" class="form-control">
                                    {foreach $wsGetDomainUsersResultArray as $domain}
                                    <option value="{$domain.Email}">{$domain.Email}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="newfirstname" class="col-sm-3 control-label">{$LANG.smmailinglistssize}</label>
                            <div class="col-sm-6 input-group">
                                <select name="maxmessagesize" class="form-control">
                                    <option value="2048" selected>2MB</option>
                                    <option value="5120">5MB</option>
                                    <option value="10240">10MB</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="newpassword" class="col-sm-3 control-label">{$LANG.smmailinglistsperm}</label>
                            <div class="col-sm-6 input-group">
                                <select name="whocanpost" class="form-control">
                                    <option value="Moderator">{$LANG.smmailinglistsmod}</option>
                                    <option value="Subscribers">{$LANG.smmailinglistsass}</option>
                                    <option value="anyone">{$LANG.smmailinglistsother}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="text-center">
                                <input type="hidden" name="domain" value="{$DomainName}" />
                                <input type="hidden" name="modop" value="custom" />
                                <input type="hidden" name="managea" value="addList" />
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                    &nbsp;&nbsp;{$LANG.smaddusers}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModalEditList" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{$LANG.smeditmailinglist}</h4>
            </div>
            <div class="modal-body">
                <div style="float: left; width: 123px;">
                    <form method="post" id="deleteForm2" action="clientarea.php?action=productdetails" style="float: left; width: 150px;">
                        <input type="hidden" name="listname" value="{$ListName}" />
                        <input type="hidden" name="domain" value="{$DomainName}" />
                        <input type="hidden" name="id" value="{$id}" />
                        <input type="hidden" name="modop" value="custom" />
                        <input type="hidden" name="a" value="deletesmartermailmailinglists" />
                    </form>
                </div>
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#listtab">{$LANG.smeditmailinglist}</a></li>
                    <li><a data-toggle="tab" href="#listsign">{$LANG.smmanagemailinglistASS}</a></li>
                </ul>
                <div class="tab-content">
                    <div id="listtab" class="tab-pane fade in active">
                        <form method="post" class="form-horizontal using-password-strength">
                            <h3>Editar lista para {$DomainName}</h3>
                            <div class="form-group">
                                <label for="inputalias" class="col-sm-5 control-label">{$LANG.smmailinglistsname}</label>
                                <div class="col-sm-6" style="text-align: left; line-height: 35px;">
                                    <input type="text" class="ListNameID" name="ListNameID" disabled="disabled" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputalias" class="col-sm-5 control-label">{$LANG.smmailinglistsModerator}</label>
                                <div class="col-sm-6">
                                    <select name="newmoderator" class="form-control" id="moderatorval">
                                        {foreach $wsGetDomainUsersResultArray as $domain}
                                        <option value="{$domain.Email}">{$domain.Email}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputalias" class="col-sm-5 control-label">{$LANG.smmailinglistssize}</label>
                                <div class="col-sm-6">
                                    <select name="maxmessagesize" class="form-control maxmessagesize">
                                        <option value="{$maxmessagesize}">{$maxmessagesizeMB}</option>
                                        <option value="5120">5MB</option>
                                        <option value="10240">10MB</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputalias" class="col-sm-5 control-label">{$LANG.smmailinglistsperm}</label>
                                <div class="col-sm-6">
                                    <select name="whocanpost" class="form-control whocanpost">
                                        <option value="Moderator">{$LANG.smmailinglistsmod}</option>
                                        <option value="Subscribers">{$LANG.smmailinglistsass}</option>
                                        <option value="anyone">{$LANG.smmailinglistsother}</option>
                                    </select>
                                </div>
                            </div>
                            <br clear="both" />
                            <div class="form-group">
                                <div class="text-center">
                                    <input type="hidden" name="newlistname" value="{$ListName}" />
                                    <input type="hidden" name="listname" value="{$ListName}" />
                                    <input type="hidden" name="domain" value="{$DomainName}" />
                                    <input type="hidden" name="managea" value="editList" />
                                    <input type="submit" value="{$LANG.smglobalreflesh}" class="btn btn-primary" />
                                </div>
                            </div>
                        </form>
                        <br clear="both" />
                    </div>
                    <div id="listsign" class="tab-pane">
                        <input type="hidden" name="listsign" id="Hidden3" class="listsign" />
                        <br />
                        <a href="#" title="{$LANG.smaddmailinglistASS}" class="btn btn-primary" data-toggle="modal" data-target="#myModaladdListSign" listname="{$domain.listname}">{$LANG.smaddmailinglistASS}</a>  {if $warnings} {include file="$template/includes/alert.tpl" type="warning" msg=$warnings textcenter=true} {/if}
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="Div5">
                                <div class="table-container clearfix">
                                    <br />
                                    <table id="tableDomainsList" class="table table-bordered table-hover table-list dataTable no-footer dtr-inline tableDomainsListSgn">
                                        <thead>
                                            <tr>
                                                <th>{$LANG.orderdomain}</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModaladdListSign" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title1">{$LANG.smaddmailinglistASS}</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form method="post" class="form-horizontal using-password-strength">
                        <div class="form-group">
                            <label for="inputalias" class="col-sm-5 control-label">{$LANG.smmailinglistsname}</label>
                            <div class="col-sm-6" style="text-align: left; line-height: 35px;">
                                <input type="text" class="ListNameID" name="ListNameID" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputalias" class="col-sm-5 control-label">{$LANG.smmailinglistsASS}</label>
                            <div class="col-sm-6">
                                <textarea cols="36" rows="15" name="newSub" style="overflow: auto; resize: none; width: 100%" class="form-control"></textarea>
                            </div>
                        </div>
                        <input type="hidden" name="listname" value="{$ListName}" />
                        <div class="form-group">
                            <div class="text-center">
                                <input type="hidden" name="modop" value="custom" />
                                <input type="hidden" name="domain" value="{$DomainName}" />
                                <input type="hidden" name="managea" value="addListSign" />
                                <input type="submit" value="{$LANG.smglobalsave}" class="btn btn-primary" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalAddAlias" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title1">{$LANG.smaddAlias}</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form method="post" class="form-horizontal using-password-strength">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="newdomainalias">{$LANG.smAlias}</label>
                            <div class="col-sm-6 input-group">
                                <input class="form-control" type="text" name="newdomainalias" />
                            </div>
                        </div>
                        <div class="text-center">
                            <div class="form-group">
                                <div class="text-center">
                                    <button class="btn btn-primary" type="submit" name="managed" value="adddomainalias">{$LANG.smaddAlias}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
   <script>
        $(document).on("click", ".editmodal", function () {
             $(".rad").hide();
    $(".ra").hide();
    $(".r").hide();
    $(".btnrad").hide();
    $(".btnrd").hide();
    if($(this).attr('isenableda')== '1'){
            $(".ra").show();
            $(".btnra").hide();
            $(".btnrad").show();
            $("#isenableda").val(true);

    }
    if($(this).attr('isenabledf') == '1'){
        $(".r").show();
        $(".btnr").hide();
        $(".btnrd").show();
        $("#isenabledf").val(true);
    }

    $('.btnra').on('click',function() {
            $(".ra").show();
            $(".btnra").hide();
            $(".btnrad").show();
            $(".enableda").val(true);
    });
    $('.btnr').on('click',function() {
            $(".r").show();
            $(".btnr").hide();
            $(".btnrd").show();
             $("#isenabledf").val(true);
    });

    $('.btnrad').on('click',function() {
            $(".ra").hide();
            $(".btnra").show();
            $(".btnrad").hide();
            $("#isenableda").val(false);

    });
    $('.btnrd').on('click',function() {
            $(".r").hide();
            $(".btnr").show();
            $(".btnrd").hide();
            $("#isenabledf").val(false);
    });
            
            $("#myModalEdit .modal-body #body").val($(this).attr('body'));
            $("#myModalEdit .modal-body #subject").val($(this).attr('subject'));
            $("#myModalEdit .modal-body #forwardaddress").val($(this).attr('forwardaddress'));
            $("#isenabledf").val($(this).attr('isenabledf'));
            $("#isenableda").val($(this).attr('isenableda'));

            $("#myModalEdit .modal-body #username").val($(this).data('user'));
            $("#myModalEdit .modal-body #editusername").val($(this).data('user'));
            $("#myModalEdit .modal-body #editfirstname").val($(this).data('name'));
            $("#myModalEdit .modal-header .modal-title.title1").text('{$LANG.smeditusers} ' + $(this).data('user') + '@{$DomainName}');
            // As pointed out in comments,
            // it is superfluous to have to manually call the modal.
            // $('#addBookDialog').modal('show');
        });
         $(document).on("click", ".editlistmodal", function () {

            if ($(this).attr('whocanpost') == "moderatoronly"){
                $m = "Moderator";
            }else if ($(this).attr('whocanpost') == "Subscribers"){

                $m = "Subscribers"
            }else if ($(this).attr('whocanpost') == "anyone"){

                $m = "anyone"
            }
            //alert($(this).attr('listsign'));


            $("#myModaladdListSign .modal-body .ListNameID").val($(this).attr('listname'));
            $("#myModalEditList .modal-body .ListNameID").val($(this).attr('listname'));
            $("#myModalEditList .modal-body .whocanpost").val($m);
            $("#myModalEditList .modal-body .whocanpostname").val($(this).attr('whocanpostname'));
            $("#myModalEditList .modal-body #moderatorval").val($(this).attr('moderator'));
            $("#myModalEditList .modal-body .maxmessagesize").val($(this).attr('maxmessagesize'));
            $("#myModalEditList .modal-body .maxmessagesizeMB").val($(this).attr('maxmessagesizeMB'));


                    var table = jQuery("#tableDomainsList").DataTable({
                        "dom": '<"listtable"fit>pl', "responsive": true,
                        "oLanguage": {
                            "sEmptyTable": "{$LANG.smglobalNothing}",
                            "sInfo": "{$LANG.smmanagemailinglistASS}",
                            "sInfoEmpty": "{$LANG.smmanagemailinglistASS}",
                            "sInfoFiltered": "{$LANG.smmanagemailinglistASS}",
                            "sInfoPostFix": "",
                            "sInfoThousands": ",",
                            "sLengthMenu": "{$LANG.smglobalshow} _MENU_",
                            "sLoadingRecords": "{$LANG.smgloballoading}",
                            "sProcessing": "{$LANG.smglobalProcess}",
                            "sSearch": "",
                            "sZeroRecords": "{$LANG.smglobalNothing}",
                            "oPaginate": {
                            "sFirst": "",
                            "sLast": "",
                            "sNext": "{$LANG.smglobalNext}",
                            "sPrevious": "{$LANG.smglobalPrev}"
                            }
                            }, 
                            "pageLength": 10,
                            "columnDefs": [{
                            "targets": 'no-sort',
                            "orderable": false,
                            }],
                            "order": [
                            [0, "asc"]
                            ],
                            "stateSave": true
                            });
                    jQuery('#tableDomainsList').removeClass('hidden').DataTable();
             
                    jQuery("#tableDomainsList_filter input").attr("placeholder", "{$LANG.smglobalsearch}");


                     table.draw(); 

            {foreach $wsGetDomainUsersListResultArray as $domain}     
            {foreach $domain.listsign as $sign}  
            if("{$domain.listname}" == $(this).attr('listname')){
                  var c = "<tr><td>{$sign}</td><td><form method=\"post\" class=\"form-horizontal using-password-strength\"><input type=\"hidden\" name=\"sign\" value=\"{$sign}\" /><input type=\"hidden\" name=\"listname\" value=\"{$domain.listname}\" /><input type=\"hidden\" name=\"managea\" value=\"delListSign\" /><button type=\"submit\" class=\"bnt btn-link\" name=\"delList\" value=\"{$sign}\"  onclick=\"if(confirm('Esta ação irá apagar esta lista.\nDeseja continuar?')){ parentNode.submit(); } else { return false; }\" ><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></button></form></td>/tr>";
                  $('#tableDomainsList').append(c);
            }
            {/foreach}
            {/foreach}
                    // As pointed out in comments, 
            // it is superfluous to have to manually call the modal.
            // $('#addBookDialog').modal('show');
        });

    </script>
    {debug}
 -->
