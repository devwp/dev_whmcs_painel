<form method="post" id="deleteForm2" action="clientarea.php?action=productdetails" style="float: left;width:150px;">
<input type="hidden" name="listname" value="{$ListName}" />
<input type="hidden" name="domain" value="{$DomainName}" />
<input type="hidden" name="id" value="{$id}" />
<input type="hidden" name="modop" value="custom" />
<input type="hidden" name="a" value="deletesmartermailmailinglists" />
</form>
</div>

<div class="row clearfix">
	<div class="col-xs-12">
		<ul class="nav nav-tabs">
			<li {$lista}>
				<a href="#domain" data-toggle="tab">{$LANG.smeditmailinglist}</a>
			</li>
			<li {$assinante}> 
				<a href="#resourceusage" data-toggle="tab">{$LANG.smmanagemailinglistASS}</a>
			</li>
		</ul>
	</div>
</div>
<div class="tab-content product-details-tab-container">
<div class="tab-pane {if ($lista == "class='active'")}active{/if}"  id="domain">
<form method="post" action="clientarea.php?action=productdetails" class="form-horizontal using-password-strength">
<h3>Editar lista para {$DomainName}</h3>
<div class="form-group">
	<label for="inputalias" class="col-sm-5 control-label">{$LANG.smmailinglistsname}</label>
	<div class="col-sm-6" style="text-align: left;line-height: 35px;">
	{$ListName}	
	</div>
</div>
<div class="form-group">
	<label for="inputalias" class="col-sm-5 control-label">{$LANG.smmailinglistsModerator}</label>
	<div class="col-sm-6">
	  <select  name="newmoderator" class="form-control" >
		   {foreach $wsGetDomainUsersResultArray as $domain}
			 <option value="{$domain}" {if ($Moderator == {$domain})}selected{/if}>{$domain}</option>
		   {/foreach} </select> 
	  
	</div>
</div>

<div class="form-group">
<label for="inputalias" class="col-sm-5 control-label">{$LANG.smmailinglistssize}</label>
	<div class="col-sm-6">
		<select name="maxmessagesize" class="form-control">
			<option value="{$maxmessagesize}" {if ($maxmessagesizeMB == "2MB")}selected{/if} >{$maxmessagesizeMB}</option> 
			<option value="5120" {if ($maxmessagesizeMB == "5MB")}selected{/if}>5MB</option>
			<option value="10240" {if ($maxmessagesizeMB == "10MB")}selected{/if}>10MB</option>
		</select>
	</div>
</div>
<div class="form-group">
<label for="inputalias" class="col-sm-5 control-label">{$LANG.smmailinglistsperm}</label>
<div class="col-sm-6">
<select name="whocanpost" class="form-control"> 
<option value="Moderator" {if ($whocanpostname == "Moderador")}selected{/if} >{$LANG.smmailinglistsmod}</option> 
<option value="Subscribers" {if ($whocanpostname == "Assinante")}selected{/if} >{$LANG.smmailinglistsass}</option> 
<option value="anyone" {if ($whocanpostname == "Qualquer um")}selected{/if} >{$LANG.smmailinglistsother}</option>   
</select> 
</div>
</div> 
<br clear="both" />
<div class="form-group">
<div class="text-center">
<input type="hidden" name="newlistname" value="{$ListName}" />
<input type="hidden" name="listname" value="{$ListName}" />
<input type="hidden" name="domain" value="{$DomainName}" />
<input type="hidden" name="id" value="{$id}" />
<input type="hidden" name="modop" value="custom" />
<input type="hidden" name="a" value="savechangessmartermailmailinglists" />
<input type="submit" value="{$LANG.smglobalreflesh}" class="btn btn-primary" /> 
<input  type="button" id="deleteButtom" value="{$LANG.smglobalDelete}" class="btn btn-default" onClick="document.getElementById('deleteForm2').submit()" />
</div>
</div>	
</form><br clear="both" />
</div>

<div class="tab-pane {if ($assinante == "class='active'")}active{/if}" id="resourceusage">
<link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.responsive.css">
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.responsive.min.js"></script>
<div style="float:right; width: 155px;">
    <form method="post" action="clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermailmailinglistsASS"> 
        <input type="hidden" name="modop" value="custom" />
		<input type="hidden" name="listname" value="{$ListName}" />
		<input type="hidden" name="domain" value="{$DomainName}" />
        <input type="hidden" name="a" value="addsmartermailmailinglistsSign" />
        <input type="submit" value="{$LANG.smaddmailinglistASS}" class="btn btn-primary" />  
    </form>
</div>
{if $warnings}
    {include file="$template/includes/alert.tpl" type="warning" msg=$warnings textcenter=true}
{/if}
<div class="tab-content">
    <div class="tab-pane fade in active" id="tabOverview">
             <script type="text/javascript">
            jQuery(document).ready(function () {
                var table = jQuery("#tableDomainsList").DataTable({
                    "dom": '<"listtable"fit>pl', "responsive": true,
                         "oLanguage": {
                        "sEmptyTable": "{$LANG.smglobalNothing}",
						"sInfo": "{$LANG.smmanagemailinglistASS}",
                        "sInfoEmpty": "{$LANG.smmanagemailinglistASS}",
                        "sInfoFiltered": "{$LANG.smmanagemailinglistASS}",
                        "sInfoPostFix": "",
                        "sInfoThousands": ",",
                        "sLengthMenu": "{$LANG.smglobalshow} _MENU_",
                        "sLoadingRecords": "{$LANG.smgloballoading}",
                        "sProcessing": "{$LANG.smglobalProcess}",
                        "sSearch": "",
                        "sZeroRecords": "{$LANG.smglobalNothing}",
                        "oPaginate": {
                            "sFirst": "",
                            "sLast": "",
                            "sNext": "{$LANG.smglobalNext}",
                            "sPrevious": "{$LANG.smglobalPrev}"
                        }
                    },
                    "pageLength": 10,
                    "order": [
                        [0, "asc"]
                    ],
                    "lengthMenu": [
                        [10, 25, 50, -1],
                        [10, 25, 50, "All"]
                    ], "stateSave": true
                });
                jQuery('#tableDomainsList').removeClass('hidden').DataTable();
                table.draw();
				jQuery("#tableDomainsList_filter input").attr("placeholder", "{$LANG.smglobalsearch}");

            });
        </script>
</br>
     
        <div class="table-container clearfix" ></br>
            <table id="tableDomainsList" class="table table-list hidden">
                <thead>
                    <tr>

                        <th>{$LANG.orderdomain}</th>
						<th></th>
                    </tr>
                </thead> 
                <tbody>
                    {foreach $wsGetAliasesResultArray as $domain}	 
                    <tr>
                        <td>
							<p style="font-size: 16px;line-height: 1.42857143; color: #333333;">{$domain}</p>
                        </td>
						<td>
							<form method="post" id="deleteForm" action="clientarea.php?action=productdetails" style="float: right;width:80px;">
								<input type="hidden" name="Subscriber" value="{$domain}" />
								<input type="hidden" name="listname" value="{$ListName}" />
								<input type="hidden" name="id" value="{$id}" />
								<input type="hidden" name="modop" value="custom" />
								<input type="hidden" name="domain" value="{$DomainName}">
								<input type="hidden" name= "newlistname" value="{$ListName}" style="width: 100%;"  class="form-control" />
								<input type="hidden" name="a" value="deletesmartermailmailinglistsSign" />
								<input type="submit" value="Excluir" class="btn btn-primary" style="border:0;font-size: 16px;line-height: 1.42857143;color: #333333 !important; background:transparent; padding:0;">
							</form>
						</td>
                    </tr>
                </tbody>
                {/foreach}
            </table>
            
        </div>
    </div> 
</div>

</div>
</div>