<link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.responsive.css">
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.responsive.min.js"></script>
<div style="float:left; width: 123px;">
    <form method="post" action="clientarea.php?action=productdetails">
        <input type="hidden" name="id" value="{$id}" />
		<input type="submit" value="{$LANG.smglobalback}" class="btn btn-primary"/>
    </form>
</div>
<div style="float:right; width: 123px;">
    <form method="post" action="clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermailmailinglistsASS"> 
        <input type="hidden" name="modop" value="custom" />
		<input type="hidden" name="listname" value="{$ListName}" />
        <input type="hidden" name="a" value="addsmartermailmailinglistsASS" />
        <input type="submit" value="{$LANG.smaddmailinglistASS}" class="btn btn-primary" />  
    </form>
</div>
{if $warnings}
    {include file="$template/includes/alert.tpl" type="warning" msg=$warnings textcenter=true}
{/if}
<div class="tab-content">
    <div class="tab-pane fade in active" id="tabOverview">

        <script type="text/javascript">
            jQuery(document).ready(function () {
                var table = jQuery("#tableDomainsList").DataTable({
                    "dom": '<"listtable"fit>pl', "responsive": true,
                    "oLanguage": {
                        "sEmptyTable": "{$LANG.smglobalNothing}",
                        "sInfo": "{$LANG.smmanagemailinglist}",
                        "sInfoEmpty": "{$LANG.smmanagemailinglist}",
                        "sInfoFiltered": "(filtered from _MAX_ total entries)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ",",
                        "sLengthMenu": "Show _MENU_ entries",
                        "sLoadingRecords": "{$LANG.smgloballoading}",
                        "sProcessing": "{$LANG.smglobalProcess}",
                        "sSearch": "",
                        "sZeroRecords": "{$LANG.smglobalNothing}",
                        "oPaginate": {
                            "sFirst": "",
                            "sLast": "",
                            "sNext": "{$LANG.smglobalNext}",
                            "sPrevious": "{$LANG.smglobalPrev}"
                        }
                    },
                    "pageLength": 10,
                    "order": [
                        [0, "asc"]
                    ],
                    "lengthMenu": [
                        [10, 25, 50, -1],
                        [10, 25, 50, "All"]
                    ], "stateSave": true
                });
                jQuery('#tableDomainsList').removeClass('hidden').DataTable();
                table.draw();
                jQuery('#Div1').addClass('hidden');
				jQuery("#tableAlias_filter input").attr("placeholder", "Enter search term...");
				jQuery("#tableDomainsList_filter input").attr("placeholder", "Enter search term...");

            });
        </script>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                var tableAlias = jQuery("#tableAlias").DataTable({
                    "dom": '<"listtable"fit>pl', "responsive": true,
                    "oLanguage": {
                        "sEmptyTable": "{$LANG.smglobalNothing}",
						"sInfo": "{$LANG.smmanagemailinglist}",
                        "sInfoEmpty": "{$LANG.smmanagemailinglist}",
                        "sInfoFiltered": "(filtered from _MAX_ total entries)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ",",
                        "sLengthMenu": "Show _MENU_ entries",
                        "sLoadingRecords": "{$LANG.smgloballoading}",
                        "sProcessing": "{$LANG.smglobalProcess}",
                        "sSearch": "",
                        "sZeroRecords": "{$LANG.smglobalNothing}",
                        "oPaginate": {
                            "sFirst": "",
                            "sLast": "",
                            "sNext": "{$LANG.smglobalNext}",
                            "sPrevious": "{$LANG.smglobalPrev}"
                        }
                    },
                    "pageLength": 10,
                    "order": [
                        [0, "asc"]
                    ],
                    "lengthMenu": [
                        [10, 25, 50, -1],
                        [10, 25, 50, "All"]
                    ], "stateSave": true
                });
                jQuery('#tableAlias').removeClass('hidden').DataTable();
                tableAlias.draw();
                jQuery('#tableLoading').addClass('hidden');

            });
        </script>
</br>
     
        <div class="table-container clearfix" ></br>
            <h3>Listas de Assinantes para {$ListName}</h3>
            <table id="tableAlias" class="table table-list hidden">
                <thead>
                    <tr>

                        <th>{$LANG.orderdomain}</th>

                    </tr>
                </thead> 
                <tbody>
                    {foreach $wsGetAliasesResultArray as $domain}	 
                    <tr>
                        <td>
                           {$domain}
                        </td>
                    </tr>
                </tbody>
                {/foreach}
            </table>
            <div class="text-center" id="Div1">
                <p><i class="fa fa-spinner fa-spin"></i>{$LANG.loading}</p>
            </div>
        </div>
    </div>
</div>
