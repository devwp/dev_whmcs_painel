<?php
function smartermail_ClientArea($params) {

	if($params["customfields"]["IsDedicated"]=='on'){

		/// LISTAR DOMINIOS
		$wsGetDomainsArray = array();
		$wsGetDomains = smartermail_API('svcDomainAdmin.asmx?WSDL', $params, 'GetAllDomains');
		if(is_array($wsGetDomains['GetAllDomainsResult']['DomainNames']['string'])){
				$wsGetDomainsArray = $wsGetDomains['GetAllDomainsResult']['DomainNames']['string'];
		} else {
			$wsGetDomainsArray[] = $wsGetDomains['GetAllDomainsResult']['DomainNames']['string'];
		}
		$wsTotalDomains = count($wsGetDomainsArray);

		//echo'<pre>';print_r($wsGetDomains);echo'</pre>';

		//$wsGetListCommandAddressResultListsNames = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters,"GetAllDomains");

		$pagearray = array(
			'templatefile' => 'clientarea',
			'breadcrumb' => ' > <a href="#">Manage SmarterMail</a>',
			'vars' => array(
				//'message' => $message,
				//'errormessage' => $errormessage,
				'totalDomainsPercent' => ($params['configoption18']!=0)?($wsTotalDomains*100)/$params['configoption18']:0,
				'totalDomains' => $wsTotalDomains,
				'maxDomains' => $params['configoption18'],
				'canCreateDomain' => ($wsTotalDomains>=$params['configoption18'])?false:true,
				'wsGetDomainsResultArray' => $wsGetDomainsArray,
				//'DomainName' => $params['domain'],
				//'wsGetListCommandAddressResultListsNames' => $smAliasesArray,
				//'wsGetAliasesResult' => $wsGetAliasesResult,
				//'domainid' => $params['domain'],
				'IsDedicated' => $params["customfields"]["IsDedicated"],
				'params2' => var_export($params,1),
			),
		);
	}else{
		$wsGetDomainUsersResult = smartermail_API('svcUserAdmin.asmx?WSDL', $params, 'GetUsers');
		$totalUsers=0;
		if(is_array($wsGetDomainUsersResult['GetUsersResult']['Users']['UserInfo'])){
			foreach($wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo'] as $keyvar =>$userStringValue){
				if(is_array($userStringValue)){
					if(substr($userStringValue["UserName"],0,strrpos($userStringValue["UserName"], '@')) == 'postmaster') continue;
					$totalUsers++;
				}else{
					if ($keyvar == "UserName"){
						if(substr($userStringValue,0,strrpos($userStringValue, '@')) == 'postmaster') continue;
						$totalUsers++;
					}
				}
			}
		}
		////////////// END GET LIST
		$pagearray = array(
			'templatefile' => 'clientarea',
			'breadcrumb' => ' > <a href="#">Manage SmarterMail</a>',
			'vars' => array(
				'totalUsers' => $totalUsers,
				'totalUsersPercent' => ($params['configoption3']!=0)?($totalUsers*100)/$params['configoption3']:0,
				'maxUsers' => $params['configoption3'],
				'canCreateUser' => ($totalUsers>=$params['configoption3'])?false:true,
				'DomainName' => $params['domain'],
				//'wsGetListCommandAddressResultListsNames' => $smAliasesArray,
				//'wsGetAliasesResult' => $wsGetAliasesResult,
				'domainid' => $params['domain'],
				'IsDedicated' => '',
				'params2' => var_export($params,1),
			),
		);
	}
	//echo '<pre>'; print_r($pagearray);echo '</pre>';
	return $pagearray;
}

//used to configure options for the SmarterMail settings within the server settings.  Each option is called by 'configoption#'
function smartermail_ConfigOptions(){
    $configarray = array(
		"SmarterMail Data Path" => array("Type"=>"text", "Size"=>"25", "Description"=>"Ex. M:\\SmarterMail\\Domains\\ NOTE: this would need to be consistent across all products"),
		"***Outbound IP Address" => array("Type"=>"text", "Size"=>"25", "Description"=>"IP address used for outbound messages"),
		"Maximum Number of Domain Users" => array("Type"=>"text", "Size"=>"25", "Description"=>""),
		"***Maximum Domain Size" => array("Type"=>"text", "Size"=>"25", "Description"=>"MB"),
		"***Maximum Number of Domain Aliases" => array("Type"=>"text", "Size"=>"25", "Description"=>""),
		"***Maximum Number of User Aliases" => array("Type"=>"text", "Size"=>"25", "Description"=>""),
		"Maximum Message Size in KB" => array("Type"=>"text", "Size"=>"25", "Description"=>"Ex. for 50MB -> 50000 + 53,6% = '76800' = 50MB "),
		"Maximum Number of Recipients" => array("Type"=>"text", "Size"=>"25", "Description"=>""),
		"Maximum Number of Mailing Lists" => array("Type"=>"text", "Size"=>"25", "Description"=>""),
		"***List Command Address" => array("Type"=>"text", "Size"=>"25", "Description"=>""),
		"***Require SMTP Authentication" => array("Type"=>"yesno", "Description"=>"Check to require SMTP Authentication"),
		"***Enable Domain Aliases" => array("Type"=>"yesno", "Description"=>""),
		"Enable Content Filtering" => array("Type"=>"yesno", "Description"=>""),
		"Enable users to modify spam settings" => array("Type"=>"yesno", "Description"=>""),
		"Enable Mailing Lists" => array("Type"=>"yesno", "Description"=>""),
		"***SmarterMail Web Interface URL" => array("Type"=>"text", "Size"=>"25", "Description"=>"Ex. http://mail.example.com"),
		"Max Maibox Size in MB" => array("Type"=>"text", "Size"=>"25", "Description"=>"Ex. '10240' = 10GB "),
		"Maximum Number of Dedicated Users" => array("Type"=>"text", "Size"=>"25", "Description"=>"Apply only for for Dedicated"),
    );
    return $configarray;
}

//Used when a product is created within WHMCS.  This calls 'createdomain' web service on SmarterMail installation.
function smartermail_CreateAccount($params){
	$smUrl = addHttpToHostname($params["serverhostname"]);//$params["configoption16"];
	$paramaters = array(
		'AuthUserName' => $params["serverusername"],
		'AuthPassword' => $params["serverpassword"],
		'DomainName'=> $params["domain"],
		'Path'=> $params["configoption1"].$params["domain"],

		//'PrimaryDomainAdminUserName'=> $params["username"],
		//'PrimaryDomainAdminPassword'=> $params["Password"],
		//'PrimaryDomainAdminLastName'=> $params['clientsdetails']['lastname'],
		//'PrimaryDomainAdminFirstName'=> $params['clientsdetails']['firstname'],

		'PrimaryDomainAdminUserName' => "postmaster",
		'PrimaryDomainAdminPassword' => "iu$2Pks",
		'PrimaryDomainAdminFirstName'=> "Administrador",
		'PrimaryDomainAdminLastName'=> "Sistema",

		'IP'=> $params["configoption2"],
		'ImapPort' => 143,
		'PopPort'=> 110,
		'SmtpPort' => 25,
		'MaxAliases' => $params["configoption6"],
		'MaxDomainSizeInMB' => $params["configoption4"],
		'MaxDomainUsers' => $params["configoption3"]+1,
		'MaxMailboxSizeInMB' => 0,
		'MaxMessageSize' => $params["configoption7"],
		'MaxRecipients' => $params["configoption8"],
		'MaxDomainAliases' => $params["configoption5"],
		'MaxLists'  => $params["configoption9"],
		'ShowDomainAliasMenu'  => (($params["configoption12"] == "on") ? true : false),
		'ShowContentFilteringMenu'  => (($params["configoption13"] == "on") ? true : false),
		'ShowSpamMenu'  => (($params["configoption14"] == "on") ? true : false),
		'ShowStatsMenu'  => true,
		'RequireSmtpAuthentication'  => (($params["configoption11"] == "on") ? true : false),
		'ShowListMenu'  => (($params["configoption15"] == "on") ? true : false),
		'ListCommandAddress'  =>  $params["configoption10"]
	);
	// IF HAS ADITIONAL PACKGE
	foreach ($params['configoptions'] as $key => $value) {
		if($key == 'totalDomainUsers'){
			$paramaters['MaxDomainUsers'] = $paramaters['MaxDomainUsers'] + $value;
		}
	}
	//echo'<pre>';print_r($params);print_r($paramaters);exit();

	$sm_createAccount_ws_url =  $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
	$wsResult = sm_webServiceCall($sm_createAccount_ws_url,$paramaters,"AddDomain");

	if($wsResult['AddDomainResult']['Result']){
		$result = "success";
	} else {
		$result = $wsResult['AddDomainResult']['Message'];
	}


	return $result;
}


function smartermail_CreateDomain($params){
	$smUrl = addHttpToHostname($params["serverhostname"]);
	$arrdomaininfo = array(
		'AuthUserName' => $params["serverusername"],
		'AuthPassword' => $params["serverpassword"],
		'DomainName' => $params["newdomainname"],
		'Path' => "m:\\SmarterMail\\Domains\\".$params["newdomainname"],
		'PrimaryDomainAdminUserName' => "postmaster",
		'PrimaryDomainAdminPassword' => "iu$2Pks",
		'PrimaryDomainAdminFirstName'=> "Administrador",
		'PrimaryDomainAdminLastName'=> "Sistema",
		'IP'=> null
	);
	//Manage Domain
	$paramaters = array(
		'AuthUserName' => $params["serverusername"],
		'AuthPassword' => $params["serverpassword"],
	);

	$domaininfo = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
	$wsResultdomaininfo =  sm_webServiceCall($domaininfo, $arrdomaininfo, "AddDomainEx");
	$retorno = array();
	if($wsResultdomaininfo['AddDomainExResult']['Result']){
		$retorno['sucess'] = true;
    $retorno['message'] = "Domínio ".$params["newdomainname"]." adicionado com sucesso.";
  } else {
		$retorno['sucess'] = false;
    $retorno['message'] = $wsResultdomaininfo['AddDomainExResult']['Message'];
  }
	return $retorno;
}

//Used when a product is terminated within WHMCS.  This calls 'deletedomain' web service on SmarterMail installation.
function smartermail_TerminateAccount($params) {

	$smUrl = addHttpToHostname($params["serverhostname"]);

	$paramaters = array(
	'AuthUserName' => $params["serverusername"],
	'AuthPassword' => $params["serverpassword"],
	'DomainName' => $params["domain"],
	'DeleteFiles' => true
	);

	$sm_TerminateAccount_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
	$wsResult = sm_webServiceCall($sm_TerminateAccount_ws_url,$paramaters,"DeleteDomain");


	if($wsResult['DeleteDomainResult']['Result']){
		$result = "success";
	} else {
		$result = $wsResult['DeleteDomainResult']['Message'];
	}
	return $result;
}

function smartermail_managesmartermaildomains($params) {
	$message='';
	$errormessage='';
	if(isset($_POST['managed']) && $_POST['managed']=='adddomain'
	&& isset($_POST['newdomainname']) && $_POST['newdomainname'] != ''
	) {
		$params["newdomainname"] = $_POST['newdomainname'];
		$create = smartermail_CreateDomain($params);
		//echo'<pre>';print_r($create);echo'</pre>';
		if ($create['sucess']) {
			$message=$create['message'];
		} else {
			$errormessage=$create['message'];
		}
		//$errormessage='ERRO ->>> dominio '.$_POST['newdomainname'].' nao foi criado!!!';
	}


	if(isset($_POST['managed']) && $_POST['managed']=='deldomain'
	&& isset($_POST['deldomainname']) && $_POST['deldomainname'] != ''
	) {
		$params["domain"] = $_POST['deldomainname'];
		$del = smartermail_TerminateAccount($params);
		if ($del == 'success') {
			$message='Domínio '.$_POST['deldomainname'].' excluido com sucesso.';
		} else {
			$errormessage=$del;
		}
		//$errormessage='ERRO ->>> dominio '.$_POST['deldomainname'].' nao foi apagado!!!';
	}

	if($params["customfields"]["IsDedicated"]=='on'
	&& isset($_GET['d']) && $_GET['d']!=''
	) {
		$params['configoption3'] = $params['configoption18'];
		$params['configoption17'] = 10240;
		$params['domain'] = $_GET['d'];
		return smartermail_managesmartermaildomain($params);
	} elseif($params["customfields"]["IsDedicated"]=='on') {
		/// LISTAR DOMINIOS
		$wsGetDomainsArray = array();
		$wsGetDomains = smartermail_API('svcDomainAdmin.asmx?WSDL', $params, 'GetAllDomains');
		if(is_array($wsGetDomains['GetAllDomainsResult']['DomainNames']['string'])){
				$wsGetDomainsArray = $wsGetDomains['GetAllDomainsResult']['DomainNames']['string'];
		} elseif(is_string($wsGetDomains['GetAllDomainsResult']['DomainNames']['string'])){
			$wsGetDomainsArray[] = $wsGetDomains['GetAllDomainsResult']['DomainNames']['string'];
		}
		$wsTotalDomains = count($wsGetDomainsArray);

		//echo'<pre>';print_r($wsGetDomains);echo'</pre>';

		//$wsGetListCommandAddressResultListsNames = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters,"GetAllDomains");

		$pagearray = array(
			'templatefile' => 'managesmartermaildomains',
			'breadcrumb' => ' > <a href="#">Manage SmarterMail</a>',
			'vars' => array(
				'message' => $message,
				'errormessage' => $errormessage,
				'totalDomains' => $wsTotalDomains,
				'maxDomains' => $params['configoption18'],
				'canCreateDomain' => ($wsTotalDomains>=$params['configoption18'])?false:true,
				'wsGetDomainsResultArray' => $wsGetDomainsArray,
				//'DomainName' => $params['domain'],
				//'wsGetListCommandAddressResultListsNames' => $smAliasesArray,
				//'wsGetAliasesResult' => $wsGetAliasesResult,
				//'domainid' => $params['domain'],
				'IsDedicated' => $params["customfields"]["IsDedicated"],
			),
		);
		return $pagearray;
	} else exit('ERRO 5897');

}


function smartermail_managesmartermaildomain($params) {

	////////////// START ADD USER
	if(isset($_POST['managea']) && $_POST['managea']=='adduser'){
		if(isset($_POST['newusername']) && $_POST['newusername']!=''
		&& isset($_POST['newpassword']) && $_POST['newpassword']!=''
		&& isset($_POST['newusername']) && $_POST['newusername']!='') {
		//	echo '<pre>';print_r($_POST);echo '</pre>';

			$paramaters = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'NewUsername' =>  $_POST['newusername'],
				'NewPassword' => $_POST['newpassword'],
				'DomainName' => $params["domain"],
				'FirstName' =>  $_POST['newfirstname'],
				'LastName' =>  '',
				'IsDomainAdmin' => false,

				'maxMailboxSize' => $params["configoption17"],
			);

			$smUrl = addHttpToHostname($params["serverhostname"]);
			$sm_AddUser2_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
			$wsResult = sm_webServiceCall($sm_AddUser2_ws_url,$paramaters,"AddUser2");

			if($wsResult['AddUser2Result']['Result']){
				$message = "Usuário adicionado com sucesso.";
			} else {
				$errormessage = $wsResult['AddUser2Result']['Message'];
			}
		} else {
			$errormessage = "Favor preencha todos os campos corretamente.";
		}
	}
	////////////// END ADD USER

	////////////// START ADD ALIAS
	if(isset($_POST['managed']) && $_POST['managed']=='adddomainalias'){
		if(isset($_POST['newdomainalias']) && $_POST['newdomainalias']!='') {

			$paramaters = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'DomainName' => $params["domain"],
				'DomainAliasName' =>  $_POST['newdomainalias'],
			);

			$smUrl = addHttpToHostname($params["serverhostname"]);
			$sm_AddAlias_ws_url = $smUrl."/Services/svcDomainAliasAdmin.asmx?WSDL";
			$wsResult = sm_webServiceCall($sm_AddAlias_ws_url,$paramaters,"AddDomainAliasWithoutMxCheck");
			//	echo '<pre>';print_r($wsResult);echo '</pre>';

			if($wsResult['AddDomainAliasWithoutMxCheckResult']['Result']){
				$message = "Alias '".$_POST['newdomainalias']."' para '".$params["domain"]."' adicionado com sucesso.";
			} else {
				$errormessage = $wsResult['AddDomainAliasWithoutMxCheckResult']['Message'];
			}
		} else {
			$errormessage = "Favor preencha todos os campos corretamente.";
		}
	}
	////////////// END ADD ALIAS

	////////////// START DEL ALIAS
		if(isset($_POST['deldomainalias']) && $_POST['deldomainalias']!='') {

		$paramaters = array(
			'AuthUserName' => $params["serverusername"],
			'AuthPassword' => $params["serverpassword"],
			'DomainName' => $params["domain"],
			'DomainAliasName' =>  $_POST['deldomainalias'],
		);

		$smUrl = addHttpToHostname($params["serverhostname"]);
		$sm_AddAlias_ws_url = $smUrl."/Services/svcDomainAliasAdmin.asmx?WSDL";
		$wsResult = sm_webServiceCall($sm_AddAlias_ws_url,$paramaters,"DeleteDomainAlias");
		//	echo '<pre>';print_r($wsResult);echo '</pre>';

		if($wsResult['DeleteDomainAliasResult']['Result']){
			$message = "Alias '".$_POST['deldomainalias']."' para '".$params["domain"]."' removido com sucesso.";
		} else {
			$errormessage = $wsResult['DeleteDomainAliasResult']['Message'];
		}
	}
	////////////// END DEL ALIAS


	////////////// START EDIT USER
	if(isset($_POST['managee']) && $_POST['managee']=='edituser'
	&& isset($_POST['editusername']) && $_POST['editusername']!=''
	&& isset($_POST['editfirstname']) && $_POST['editfirstname']!=''
	&& isset($_POST['editpassword'])){
		$error = false;
		$errormessage = '';
		$smUrl = addHttpToHostname($params["serverhostname"]);
		$paramatersUpdateUser2 = array(
			'AuthUserName' => $params["serverusername"],
			'AuthPassword' => $params["serverpassword"],
			'EmailAddress' => $_POST['editusername'].'@'.$_POST['editdomain'],
			//'NewPassword' => $senha,
			'NewFirstName' =>$_POST['editfirstname'],
			'NewLastName' => '',
			'IsDomainAdmin' => 'false',
			'maxMailboxSize' => '10240' ,
		);

		$sm_UpdateUser2_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";

		if (($_POST['editpassword'])!=''){
			$senha =  $_POST['editpassword'];
			$paramatersUpdateUser2['NewPassword'] = $senha;

		} else {
			$paramatersUpdateGetUser2 = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'EmailAddress' => $_POST['editusername'].'@'.$_POST['editdomain'],
			);
			$wsResultGetUser = sm_webServiceCall($sm_UpdateUser2_ws_url, $paramatersUpdateGetUser2, "GetUser");

			if($wsResultGetUser['GetUserResult']['Result']){
				$paramatersUpdateUser2['NewPassword'] = $wsResultGetUser['GetUserResult']['UserInfo']['Password'];
			} else {
				$error = true;
				$errormessage .= ':( Erro número 54';
			}
		}
		//echo '<pre>';print_r($wsResultGetUser);echo '</pre>';

		if(!$error){
			$wsResult1 = sm_webServiceCall($sm_UpdateUser2_ws_url, $paramatersUpdateUser2, "UpdateUser2");
			if($wsResult1['UpdateUser2Result']['Result']){
				$message = "Usuário atualizado com sucesso.";
			} else {
				$errormessage .= $wsResult1['UpdateUser2Result']['Message'];
			}
		}
		//echo '<pre>';
		//print_r($paramatersUpdateUser2);
		//echo '</pre>';



	}

	////////////// END EDIT USER

	////////////// START DELETE USER
	if(isset($_POST['managed']) && $_POST['managed']=='deluser'
	&& isset($_POST['username']) && $_POST['username']!=''){

		$smUrl = addHttpToHostname($params["serverhostname"]);
		$paramaters = array(
			'AuthUserName' => $params["serverusername"],
			'AuthPassword' => $params["serverpassword"],
			'Username' => $_POST['username'],
			'DomainName' => $params["domain"],
		);

		$sm_DeleteUser_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
		$wsResult = sm_webServiceCall($sm_DeleteUser_ws_url,$paramaters,"DeleteUser");
		//echo '<pre>';print_r($wsResult);echo '</pre>';
		if($wsResult['DeleteUserResult']['Result']){
			$message = "Usuário ".$_POST['username']."@".$params["domain"].", removido com sucesso.";
		} else {
			$errormessage = $wsResult['DeleteUserResult']['Message'];
		}
	}

	////////////// END DELETE USER


	////////////// START GET USER LISTTTTTTTTTTT
	//$sm_GetAliases_ws_url = $smUrl."/Services/svcAliasAdmin.asmx?WSDL";
	//$wsGetAliasesResult = sm_webServiceCall($sm_GetAliases_ws_url,$paramaters,"GetAliases");
	$wsGetDomainUsersLists =  smartermail_API('svcMailListAdmin.asmx?WSDL', $params, 'GetMailingListsByDomain');
	$wsGetDomainUsersResult = smartermail_API('svcUserAdmin.asmx?WSDL', $params, 'GetUsers');
	$wsGetDomainUsersQuotas = smartermail_API('svcUserAdmin.asmx?WSDL', $params, 'GetUserQuotas');
	$wsGetDomainAliasResult = smartermail_API('svcDomainAliasAdmin.asmx?WSDL', $params, 'GetAliases');
	
	$smUsersListsArray = array();

	if(is_array($wsGetDomainUsersLists['GetMailingListsByDomainResult']['listNames']['string'])){
		foreach($wsGetDomainUsersLists['GetMailingListsByDomainResult']['listNames']['string'] as $keyvar =>$userStringValue){

					array_push($smUsersListsArray, $userStringValue);

		}
	}else{

		array_push($smUsersListsArray, $wsGetDomainUsersLists['GetMailingListsByDomainResult']['listNames']['string']);
	}


	$wsGetDomainAlias = array();
	if(is_array($wsGetDomainAliasResult['GetAliasesResult']['DomainAliasNames']['string'])){
		$wsGetDomainAlias = $wsGetDomainAliasResult['GetAliasesResult']['DomainAliasNames']['string'];
	} elseif(is_string($wsGetDomainAliasResult['GetAliasesResult']['DomainAliasNames']['string'])){
		$wsGetDomainAlias[] = $wsGetDomainAliasResult['GetAliasesResult']['DomainAliasNames']['string'];
	}
	//echo '<pre>';
	//print_r($wsGetDomainAlias);
  //print_r($wsGetDomainUsersResult);
	//print_r($wsGetDomainUsersQuotas);
	//echo '</pre>';
	//setup user array to be displayed in ManageSmarterMail Template.  Checking to see if listServ name is showing or if they are an alias which shows in a separate section.
	$smUsersArray = array();
	$totalUsers=0;
	//echo '<pre>';print_r($wsGetDomainUsersResult);exit();

	if(is_array($wsGetDomainUsersResult['GetUsersResult']['Users']['UserInfo'])){
		foreach($wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo'] as $keyvar =>$userStringValue){
			if(is_array($userStringValue)){
				if(substr($userStringValue["UserName"],0,strrpos($userStringValue["UserName"], '@')) == 'postmaster') continue;
				$vall = array();
				$keyy = array_search(substr($userStringValue["UserName"],0,strrpos($userStringValue["UserName"], '@')), array_column($wsGetDomainUsersQuotas['GetUserQuotasResult']['Users']['UserQuota'], 'UserName'));
				$vall['Name'] = $userStringValue["FirstName"];
				$vall['LastName'] = $userStringValue["LastName"];
				$vall['Pass'] = $userStringValue["Password"];

				$vall['Email'] = $userStringValue["UserName"];
				$vall['User'] = substr($userStringValue["UserName"],0,strrpos($userStringValue["UserName"], '@'));
				$vall['MaxSpace'] = ((($wsGetDomainUsersQuotas['GetUserQuotasResult']['Users']['UserQuota'][$keyy]['MaxSpace'])/1024)/1024)/1024;
				$vall['CurrentSpace'] = $wsGetDomainUsersQuotas['GetUserQuotasResult']['Users']['UserQuota'][$keyy]['CurrentSpace'];
				$vall['PercentUsed'] = round($vall['CurrentSpace']*100/$wsGetDomainUsersQuotas['GetUserQuotasResult']['Users']['UserQuota'][$keyy]['MaxSpace']);
				array_push($smUsersArray, $vall);
				$totalUsers++;
			}else{
				if ($keyvar == "UserName"){
					if(substr($userStringValue,0,strrpos($userStringValue, '@')) == 'postmaster') continue;
					$vall = array();
					$vall['Name'] = $wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo']["FirstName"];
					$vall['LastName'] = $wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo']["LastName"];
					$vall['Pass'] = $wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo']["Password"];

					$vall['Email'] = $userStringValue;
					$vall['User'] = substr($userStringValue,0,strrpos($userStringValue, '@'));
					$vall['MaxSpace'] = ((($wsGetDomainUsersQuotas['GetUserQuotasResult']['Users']['UserQuota']['MaxSpace'])/1024)/1024)/1024;
					$vall['CurrentSpace'] = $wsGetDomainUsersQuotas['GetUserQuotasResult']['Users']['UserQuota']['CurrentSpace'];
					$vall['PercentUsed'] = round($vall['CurrentSpace']*100/$wsGetDomainUsersQuotas['GetUserQuotasResult']['Users']['UserQuota'][$keyy]['MaxSpace']);
					array_push($smUsersArray, $vall);
					$totalUsers++;
				}
			}
		}
	}



    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramatersList = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName' => $params['domain'],
        'ListName' =>  $_POST['listname'],
        'requestedSettings' => array("moderator", "description","listname","whocanpost","listtoaddress","listfromaddress","listreplytoaddress","maxmessagesize"),
    );
    $paramatersList2 = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName' => $params['domain'],
        'ListName' =>  $_POST['listname'],
     );



	$sm_GetListCommandAddress_ws_url2 = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
	$wsResult = sm_webServiceCall($sm_GetListCommandAddress_ws_url2,$paramatersList,"GetRequestedListSettings");

	if (str_replace("'", "",(str_replace('whocanpost=', '',$wsResult['GetRequestedListSettingsResult']['settingValues']['string'][2]))) === 'Moderator'){
		$whocanpostname= 'Moderador';
		$whocanpost= 'moderatoronly';
	}else if (str_replace("'", "",(str_replace('whocanpost=', '',$wsResult['GetRequestedListSettingsResult']['settingValues']['string'][2]))) === 'Subscribers'){
		$whocanpostname= 'Assinante';
		$whocanpost= 'subscribersonly';
	}else{
		 $whocanpostname= 'Qualquer um';
		 $whocanpost= 'anyone';
	}


	 //Call to get LstServ address
    $sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
	$wsGetListCommandAddressResultListsNames = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramatersList2,"GetSubscriberList");

    $smAliasesArrayForLoop = array();
    $smAliasesArray = array();
	if (is_array($wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string'])) {
	foreach($wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string'] as $key => $value){
		array_push($smAliasesArray,$value);
	}
	}else{
		if($wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string'] != null){
			array_push($smAliasesArray,$wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string']);
		}
	}
	if(isset($_POST['managea']) && $_POST['managea']=='editList' ){
	
	$smUrl = addHttpToHostname($params["serverhostname"]);

    $paramatersList = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName' => $params['domain'],
        'ListName' =>  $_POST['listname'],
        'requestedSettings' => array("moderator", "description","listname","whocanpost","listtoaddress","listfromaddress","listreplytoaddress","maxmessagesize"),
    );
    $paramatersList2 = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName' => $params['domain'],
        'ListName' =>  $_POST['listname'],
     );



	$sm_GetListCommandAddress_ws_url22 = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
	$wsResult = sm_webServiceCall($sm_GetListCommandAddress_ws_url22,$paramatersList,"GetRequestedListSettings");

	if (str_replace("'", "",(str_replace('whocanpost=', '',$wsResult['GetRequestedListSettingsResult']['settingValues']['string'][2]))) === 'Moderator'){
		$whocanpostname= 'Moderador';
		$whocanpost= 'moderatoronly';
	}else if (str_replace("'", "",(str_replace('whocanpost=', '',$wsResult['GetRequestedListSettingsResult']['settingValues']['string'][2]))) === 'Subscribers'){
		$whocanpostname= 'Assinante';
		$whocanpost= 'subscribersonly';
	}else{
		 $whocanpostname= 'Qualquer um';
		 $whocanpost= 'anyone';
	}


	 //Call to get LstServ address
    $sm_GetListCommandAddress_ws_url56 = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
	$wsGetListCommandAddressResultListsNames = sm_webServiceCall($sm_GetListCommandAddress_ws_url56,$paramatersList2,"GetSubscriberList");

    $smAliasesArrayForLoop = array();
    $smAliasesArray = array();
	if (is_array($wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string'])) {
	foreach($wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string'] as $key => $value){
		array_push($smAliasesArray,$value);
	}
	}else{
		if($wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string'] != null){
			array_push($smAliasesArray,$wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string']);
		}
	}

    //Call to get Domain Users
    $sm_getDomainUsers_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
    $wsGetDomainUsersResult = sm_webServiceCall($sm_getDomainUsers_ws_url,$paramaters,"GetUsers");
	$smUsersArray = array();
	if(is_array($wsGetDomainUsersResult['GetUsersResult']['Users']['UserInfo'])){
		foreach($wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo'] as $keyvar =>$userStringValue){

			if(is_array($userStringValue)){
				array_push($smUsersArray, $userStringValue["UserName"]);
			}else{
				if ($keyvar == "UserName"){
					array_push($smUsersArray, $userStringValue);
				}
			}
		}
	}
	
	}
	if(isset($_POST['managea']) && $_POST['managea']=='addList' ){
		$sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
		$paramatersAddList1 = array(
		'AuthUserName' => $params["serverusername"],
		'AuthPassword' => $params["serverpassword"],
	     'DomainName' => $params['domain'],
		'ListName' =>  $_POST['newlistname'],
		'Moderator' => $_POST['newmoderator'],
		'Description' => ' ',
		);
		$wsResultaddlist = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramatersAddList1,"AddList");

		$paramatersaddlist = array(
			'AuthUserName' => $params["serverusername"],
			'AuthPassword' => $params["serverpassword"],
			'DomainName' => $params['domain'],
			'ListName' =>  $_POST['newlistname'], 
			'newSettings' => array("whocanpost=".$_POST["whocanpost"]."","maxmessagesize=".$_POST["maxmessagesize"]."", "listreplytoaddress=LISTADDRESS", "listtoaddress=LISTADDRESS","listfromaddress=POSTERADDRESS","moderator=".$_POST['newmoderator'].""),
		);

		$sm_GetListCommandAddress_ws_url2 = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
		$wsResultaddlist2 = sm_webServiceCall( $sm_GetListCommandAddress_ws_url2,$paramatersaddlist,"SetRequestedListSettings");


		if($wsResultaddlist['AddListResult']['Result']){
		    $message  = "Lista de e-mail adicionada com sucesso";
		} else {
		    $errormessage = $wsResultaddlist['AddListResult']['Message'];
		}


 
	}
	////////////// END GET LIST
	//echo '<pre>';
	//var_export($smUsersArray);
	//var_export($wsGetDomainUsersQuotas);
	//echo '<pre>';
	//print_r($wsGetDomainUsersLists);
	//echo '</pre>'; 
	$pagearray = array(
		'templatefile' => 'managesmartermaildomain',
		'breadcrumb' => ' > <a href="#">Manage SmarterMail</a>',
		'vars' => array(
			'message' => $message,
			'errormessage' => $errormessage,
			'totalUsers' => $totalUsers,
			'maxUsers' => $params['configoption3'],
			'canCreateUser' => ($totalUsers>=$params['configoption3'])?false:true,
			'wsGetDomainUsersResultArray' => $smUsersArray,
			'wsGetDomainUsersListResultArray' => $smUsersListsArray,
			'DomainName' => $params['domain'],
			'DomainEliases' => $wsGetDomainAlias,
  			'Moderator' =>  str_replace("'", "",(str_replace('moderator=', '',$wsResult['GetRequestedListSettingsResult']['settingValues']['string'][0]))),
			'whocanpost' =>  $whocanpost,
			'whocanpostname' =>  $whocanpostname,
        	'maxmessagesize' => str_replace("'", "",(str_replace('maxmessagesize=', '',$wsResult['GetRequestedListSettingsResult']['settingValues']['string'][6]))),
			'maxmessagesizeMB' =>  (((int)(str_replace("'", "",(str_replace('maxmessagesize=', '',$wsResult['GetRequestedListSettingsResult']['settingValues']['string'][6])))))/ 1024)."MB",
			'assinante' => "class",
			'lista' => "class='active'",
			'domainid' => $params['domain'],
			'IsDedicated' => $params["customfields"]["IsDedicated"],
		),
	);
	return $pagearray;
	//End Gerenciar Users e Lista
}

function smartermail_API($ws, $params, $action){
	$paramaters = array(
		'AuthUserName' => $params["serverusername"],
		'AuthPassword' => $params["serverpassword"],
		'DomainName'=> $params['domain']
	);
	$smUrl = addHttpToHostname($params["serverhostname"]);
	$sm_ws_url = $smUrl."/Services/".$ws;
	return sm_webServiceCall($sm_ws_url,$paramaters,$action);
}

//Used when an account is suspended within WHMCS.  This calls 'DisableDomain' web service on SmarterMail installation.
function smartermail_SuspendAccount($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'domainName' => $params["domain"]
    );

    $sm_SuspendAccount_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
    $wsResult = sm_webServiceCall($sm_SuspendAccount_ws_url,$paramaters,"DisableDomain");


    if($wsResult['DisableDomainResult']['Result']){
        $result = "success";
    } else {
        $result = $wsResult['DisableDomainResult']['Message'];
    }
    return $result;
}

//Used when an product is unsuspended within WHMCS.  This calls 'EnableDomain' web service on SmarterMail installation.
function smartermail_UnsuspendAccount($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'domainName' => $params["domain"]
    );

    $sm_UnsuspendAccount_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
    $wsResult = sm_webServiceCall($sm_UnsuspendAccount_ws_url,$paramaters,"EnableDomain");

    if($wsResult['EnableDomainResult']['Result']){
        $result = "success";
    } else {
        $result = $wsResult['EnableDomainResult']['Message'];
    }
    return $result;
}

//Used when the Password is changed in WHMCS.  The new Password must first be saved in WHMCS before the module is called.  This calls 'SetRequestedUserSettings' web service on SmarterMail installation.
function smartermail_ChangePassword($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'EmailAddress' => ($params["username"]."@".$params["domain"]),
        'newSettings' => array("Password = ".$params["Password"])
    );

    $sm_changePassword_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
    $wsResult = sm_webServiceCall($sm_changePassword_ws_url,$paramaters,"SetRequestedUserSettings");

    if ($wsResult['SetRequestedUserSettingsResult']['Result']) {
        $result = "success";
    } else {
        $result = $wsResult['SetRequestedUserSettingsResult']['Message'];
    }
    return $result;

}

//Used when a product is changed within WHMCS.  This calls 'updatedomain' web service on SmarterMail installation.
function smartermail_ChangePackage($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName'=> $params["domain"],
        'ServerIP'=> $params["configoption2"],
        'ImapPort' => 143,
        'PopPort'=> 110,
        'SmtpPort' => 25,
        'MaxAliases' => $params["configoption6"],
        'MaxDomainSizeInMB' => $params["configoption4"],
        'MaxDomainUsers' => $params["configoption3"]+1,
        'MaxMailboxSizeInMB' => 0,
        'MaxMessageSize' => $params["configoption7"],
        'MaxRecipients' => $params["configoption8"],
        'MaxDomainAliases' => $params["configoption5"],
        'MaxLists'  => $params["configoption9"],
        'ShowDomainAliasMenu'  => (($params["configoption12"] == "on") ? true : false),
        'ShowContentFilteringMenu'  => (($params["configoption13"] == "on") ? true : false),
        'ShowSpamMenu'  => (($params["configoption14"] == "on") ? true : false),
        'ShowStatsMenu'  => true,
        'RequireSmtpAuthentication'  => (($params["configoption11"] == "on") ? true : false),
        'ShowListMenu'  => (($params["configoption15"] == "on") ? true : false),
        'ListCommandAddress'  =>  $params["configoption10"]
    );
		// IF HAS ADITIONAL PACKGE
		foreach ($params['configoptions'] as $key => $value) {
			if($key == 'totalDomainUsers'){
				$paramaters['MaxDomainUsers'] = $paramaters['MaxDomainUsers'] + $value;
			}
		}
		//echo'<pre>';print_r($params);print_r($paramaters);exit();


    $sm_changePackage_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
    $wsResult = sm_webServiceCall($sm_changePackage_ws_url,$paramaters,"UpdateDomain");

    if($wsResult['UpdateDomainResult']['Result']){
        $result = "success";
    } else {
        $result = $wsResult['UpdateDomainResult']['Message'];
    }
    return $result;

}

//Provides URL link in management interface for access to the SmarterMail web interface.
function smartermail_LoginLink($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);
    echo "<a href=$smUrl target=\"_blank\" style=\"color:#cc0000\">Access SmarterMail Web Interface</a>";

}

//Custom button array that allows users to interact with specified buttons below in client area.
function smartermail_ClientAreaCustomButtonArray($params) {
	$smUrl = addHttpToHostname($params["serverhostname"]);
	$paramaters3 = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
    );

    $paramaters2 = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName'=> $params["domain"]
    );
    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'domainName'=> $params["domain"]
    );

	$sm_GetUsers_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";

	$sm_GetAliases_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
	$wsGetDomainSettingsResult = sm_webServiceCall($sm_GetAliases_ws_url,$paramaters,"GetDomainSettings");

	if($wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainUsers'] == 0)
		$wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainUsers'] = "∞";
	if($wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainAliases'] == 0)
		$wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainAliases'] = "∞";
	if($wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainSizeInMB'] == 0)
		$wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainSizeInMB'] = "∞";

	$sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
	$wsGetListCommandAddressResultListsNames = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters,"GetAllDomains");

	$smDomainArray = array();
    $IsDedicated = $params["customfields"]["IsDedicated"];

	if ($IsDedicated == 'on'){
		$total = 0;
		if(is_array($wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string'])){
			foreach	($wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string'] as $dom){
				$param = array();
				$param = array(
					'AuthUserName' => $params["serverusername"],
					'AuthPassword' => $params["serverpassword"],
					'DomainName'=> $dom,
				);

				$wsGetUsersforDomainResult = sm_webServiceCall($sm_GetUsers_ws_url, $param,"GetUsers");
				if (count($wsGetUsersforDomainResult['GetUsersResult']['Users']['UserInfo']) != 6){
					$total = $total + count($wsGetUsersforDomainResult['GetUsersResult']['Users']["UserInfo"]);
				}else {
					if (strpos($wsGetUsersforDomainResult['GetUsersResult']['Users']["UserInfo"]['UserName'], '@') !== false) {
						$total = $total + 1;
					} else {
						$total = $total + count($wsGetUsersforDomainResult['GetUsersResult']['Users']["UserInfo"]);
					}
				}
			}
		}else{
			$param = array();
			$param = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'DomainName'=> $wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string'],
			);

			$wsGetUsersforDomainResult = sm_webServiceCall($sm_GetUsers_ws_url, $param,"GetUsers");
			if (count($wsGetUsersforDomainResult['GetUsersResult']['Users']['UserInfo']) != 6){
				$total = $total + count($wsGetUsersforDomainResult['GetUsersResult']['Users']["UserInfo"]);
			}else {
				if (strpos($wsGetUsersforDomainResult['GetUsersResult']['Users']["UserInfo"]['UserName'], '@') !== false) {
					$total = $total + 1;
				} else {
					$total = $total + count($wsGetUsersforDomainResult['GetUsersResult']['Users']["UserInfo"]);
				}
			}

		}
	}else{
		$total = 0;
		$wsGetUsersResult = sm_webServiceCall($sm_GetUsers_ws_url, $paramaters2,"GetUsers");
		if (count($wsGetUsersResult['GetUsersResult']['Users']['UserInfo']) != 6){
			$total = $total + count($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]);
		}else {
			if (strpos($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]['UserName'], '@') !== false) {
				$total = $total + 1;
			} else {
				$total = $total + count($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]);
			}
		}
	}
	//echo count($wsGetUsersResult['GetUsersResult']['Users']);
	if ($params["customfields"]["IsDedicated"] == 'on'){
		$buttonarray = array(
			"Gerenciar Domínios" => "managesmartermaildomain",
			'vars' => array(
			'IsDedicated' => $params["customfields"]["IsDedicated"],
			'maxUsers' =>  $params["customfields"]["AccountLimit"],
			'countUsers' => $total
			),
		);
	}else{
			$buttonarray = array(
			//"Gerenciar Domínios" => "managesmartermaildomain",
			"Gerenciar Usuários" => "managesmartermailuserdomaintpl",
			'vars' => array(
				'IsDedicated' => $params["customfields"]["IsDedicated"],
				 'maxUsers' => $wsGetDomainSettingsResult['GetDomainSettingsResult']["MaxDomainUsers"],
				 'countUsers' => $total
			),
		);
	}

    return $buttonarray;
}

function smartermail_ClientAreaAllowedFunctions(){
	    $functionarray = array(
		 "Users" => "managesmartermailuserdomaintpl",
		 "Add User" => "addsmartermailusernew",
        "Create Alias (API)" => "createsmartermailalias",
        "Edit Alias (API)" => "editsmartermailaliaspage",
        "Delete Alias (API)" => "deletesmartermailalias",
		"Create User (API)" => "createsmartermailuser",
        "Edit User (API)" => "editsmartermailuserpage",
		"Delete User (API)" => "deletesmartermailuser",
        "Save User (API)" => "savechangessmartermailuser",
        "Save Alias (API)" => "savechangessmartermailalias",
		"Manage Alias (API)" => "managesmartermailalias",
		"Manage Lists (API)" => "managesmartermailmailinglists",
		"Manage Sign (API)" => "managesmartermailmailinglistsSign",
        "Save Alias (API)" => "savechangessmartermailalias",
		"ADD User" => "addsmartermailuser",
        "ADD Alias (API)" => "addsmartermailalias",
		"ADD Lists (API)" => "addsmartermailmailinglists",
		"Create Lists (API)" => "createsmartermailmailinglists",
		"Edit Lists (API)" => "editsmartermailmailinglistspage",
		"Save Lists  (API)" => "savechangessmartermailmailinglists",
		"ADD ListsSign (API)" => "addsmartermailmailinglistsSign",
		"Create ListsSign (API)" => "createsmartermailmailinglistsSign",
		"Edit ListsSign (API)" => "editsmartermailmailinglistspageSign",
		 "Delete Sign (API)" => "deletesmartermailmailinglistsSign",
		"Save ListsSign  (API)" => "savechangessmartermailmailinglistsSign",
		 "ADD Domain (API)" => "addsmartermaildomain",
		"Create Domain (API)" => "createsmartermaildomain",
        "Edit Domain (API)" => "editsmartermaildomainpage",
        "Delete Domain (API)" => "deletesmartermaildomain",
		"Manage Domain (API)" => "managesmartermaildomain",
		"Manage Domains (API)" => "managesmartermaildomains",
		"Edit Domain(API)" => "editsmartermaildomainpage",
		"Delete Mailing Lists (API)" => "deletesmartermailmailinglists"
    );
    return $functionarray;
}

function smartermail_managesmartermailalias($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName'=> $_POST["domain"]
    );


    //Call to get Domain Aliases
    $sm_GetAliases_ws_url = $smUrl."/Services/svcAliasAdmin.asmx?WSDL";
    $wsGetAliasesResult = sm_webServiceCall($sm_GetAliases_ws_url,$paramaters,"GetAliases");

    //Call to get LstServ address
    $sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
    $wsGetListCommandAddressResult = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters,"GetListCommandAddress");


//Set alias array for manage smartermail tpl

    $smAliasesArrayForLoop = array();
    $smAliasesArray = array();



    if(is_array($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo'][0])){
        foreach($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo'] as $key => $value)
        {
            foreach($value as $key2 => $value2)
            {
                if($key2 === 'Name')
                {
					foreach($value as $key3 => $value3)
					{
						if($key3 === 'Addresses')
						{
							foreach	($value3 as $ws)
							{
								if($_POST["username"] === $ws){

									array_push($smAliasesArray,$value2);
								}
							}
						}
					}
                }
            }
        }
    }else{
		if(is_array($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo'])){
			foreach($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo'] as $key => $value){
				if($key === 'Name'){
				   foreach($value as $key3 => $value3){
						if($key3 === 'Addresses'){
							foreach	($value3 as $ws) {
								if($_POST["username"] === $ws){
									array_push($smAliasesArray,$value);
								}
							}
						}
					}
				}
			}
		}
	}

    $pagearray = array(
        'templatefile' => 'managesmartermailalias',
        'breadcrumb' => ' > <a href="#">Manage SmarterMail alias</a>',
        'vars' => array(
            'wsGetDomainUsersResultArray' => $smUsersArray,
            'wsGetAliasesResultArray' => $smAliasesArray,
            'domainName' => $params["domain"],
			'UserName' => $_POST["username"],
        ),
    );
    return $pagearray;
}

//Manage SmarterMail button on client area details page.  Calls various web services to get users and aliases to be displayed in managesmartermail.tpl
function smartermail_managesmartermail($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName'=>  $_POST['domain']
    );

    //Call to get Domain Users
    $sm_getDomainUsers_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
    $wsGetDomainUsersResult = sm_webServiceCall($sm_getDomainUsers_ws_url,$paramaters,"GetUsers");

    //Call to get LstServ address
    $sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
	$wsGetListCommandAddressResultListsNames = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters,"GetMailingListsByDomain");

    $smAliasesArrayForLoop = array();
    $smAliasesArray = array();
	if(is_array($wsGetDomainUsersResult['GetUsersResult']['Users']['UserInfo'])){
	foreach($wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo'] as $keyvar =>$userStringValue){
		if ($keyvar == "UserName"){
		array_push($smUsersArray, $userStringValue);
		}
	}
	}else{
		foreach($wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo'] as $userStringValueA){
			array_push($smUsersArray, $userStringValueA["UserName"]);
		}
	}

//setup user array to be displayed in ManageSmarterMail Template.  Checking to see if listServ name is showing or if they are an alias which shows in a separate section.
    $smUsersArray = array();
        foreach($wsGetDomainUsersResult['GetUsersResult']['Users']['UserInfo'] as $userStringValue){
		   if((!in_array( $userStringValue , $smAliasesArray))&& (!in_array( $userStringValue , $smAliasesArray2))){
				array_push($smUsersArray, $userStringValue);
            }
        }

    sort($smUsersArray);


    $pagearray = array(
        'templatefile' => 'managesmartermail',
        'breadcrumb' => ' > <a href="#">Manage SmarterMail</a>',
        'vars' => array(
            'wsGetDomainUsersResultArray' => $smUsersArray,
			'wsGetListCommandAddressResultListsNames' => $smAliasesArray,
			'wsGetAliasesResult' => $wsGetAliasesResult,
			'DomainName'=> $_POST['domain'],
			'IsDedicated' => $params["customfields"]["IsDedicated"]
        ),
    );
    return $pagearray;
}

/*function smartermail_managesmartermailmailinglists($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName'=> $params["domain"]
    );
    echo "<pre>";
    print_r($wsGetListCommandAddressResultListsNames);

    //Call to get LstServ address

	$wsGetListCommandAddressResultListsNames = smartermail_API('svcMailListAdmin.asmx?WSDL', $params, 'GetMailingListsByDomain');
	$smUsersArray = array();
	if(is_array($wsGetListCommandAddressResultListsNames['GetMailingListsByDomainResult']['listNames']['string'])){
		foreach($wsGetListCommandAddressResultListsNames['GetMailingListsByDomainResult']['listNames']['string'] as $keyvar =>$userStringValue){
			if(is_array($userStringValue)){
				array_push($smUsersArray, $userStringValue["UserName"]);
			}else{
				if ($keyvar == "UserName"){
					array_push($smUsersArray, $userStringValue);
				}
			}
		}
	}

	$pagearray = array(
        'templatefile' => 'managesmartermailmailinglists',
        'breadcrumb' => ' > <a href="#">Manage SmarterMail mailinglists</a>',
        'vars' => array(
            'wsGetAliasesResultArray' => $smUsersArray,
            'domainName' => $params["domain"],
        ),
    );

    return $pagearray;
}*/

function smartermail_addsmartermailmailinglists($params) {
    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName'=> $_POST["domain"],
    );

    //Call to get Domain Users
    //$sm_getDomainUsers_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";

    $params["domain"] = $_POST["domain"];
    $wsGetDomainUsersResult = smartermail_API('svcUserAdmin.asmx?WSDL', $params, 'GetUsers'); //sm_webServiceCall($sm_getDomainUsers_ws_url,$paramaters,"GetUsers");

    //Call to get Domain Aliases
   // $sm_GetAliases_ws_url = $smUrl."/Services/svcAliasAdmin.asmx?WSDL";
    $wsGetAliasesResult = smartermail_API('svcAliasAdmin.asmx?WSDL', $params, 'GetAliases');//sm_webServiceCall($sm_GetAliases_ws_url,$paramaters,"GetAliases");

    //Call to get LstServ address
   // $sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
    $wsGetListCommandAddressResult =  smartermail_API('svcMailListAdmin.asmx?WSDL', $params, 'GetListCommandAddress');///sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters,"GetListCommandAddress");
//Set alias array for manage smartermail tpl

    $smAliasesArrayForLoop = array();
    $smAliasesArray = array();



    if(is_array($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo'][0])){
        foreach($wsGetAliasesResult['GetAliasesResult']['AliasInfos']["AliasInfo"] as $key => $value)
        {
            foreach($value as $key2 => $value2)
            {
                if($key2 === 'Name')
                {
                    array_push($smAliasesArray,$value2);
                }
            }
        }
    }else{
		if(is_array($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo'])){
			foreach($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo'] as $key => $value)
			{
			if($key === 'Name')
				{
					array_push($smAliasesArray,$value);
				}
			}
		}else{

		}
    }
		$smUsersArray = array();
//setup user array to be displayed in ManageSmarterMail Template.  Checking to see if listServ name is showing or if they are an alias which shows in a separate section.
		if(is_array($wsGetDomainUsersResult['GetUsersResult']['Users']['UserInfo'])){
			foreach($wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo'] as $keyvar =>$userStringValue){
				if(is_array($userStringValue)){
					array_push($smUsersArray, $userStringValue["UserName"]);
				}else{
					if ($keyvar == "UserName"){
						array_push($smUsersArray, $userStringValue);
					}
				}
			}
		}


    $pagearray = array(
        'templatefile' => 'addsmartermailmailinglists',
        'breadcrumb' => ' > <a href="#">Add SmarterMail User Alias</a>',
		'vars' => array(
			'UserName' => $_POST["username"],
			'wsGetDomainUsersResultArray'=> $smUsersArray,
			'domainName' => $_POST["domain"]
		),
    );
    return $pagearray;
}

//used to create user alias through smartermail web service.  Information is pSigned through the POST action from the addsmartermailalias.tpl
function smartermail_createsmartermailmailinglists($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramaters = array(
		'AuthUserName' => $params["serverusername"],
		'AuthPassword' => $params["serverpassword"],
        'DomainName' => $_POST["domain"],
        'ListName' =>  $_POST['newlistname'],
        'Moderator' => $_POST['newmoderator'],
		'Description' => ' ',
    );

	if(($_POST['newlistname'] == ''))
	{
		$smUrl = addHttpToHostname($params["serverhostname"]);

		$paramatersGetUsers = array(
		'AuthUserName' => $params["serverusername"],
		'AuthPassword' => $params["serverpassword"],
		'DomainName'=> $_POST["domain"],
		);

		//Call to get Domain Users
		$sm_getDomainUsers_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
		$wsGetDomainUsersResult = sm_webServiceCall($sm_getDomainUsers_ws_url,$paramatersGetUsers,"GetUsers");

		//Call to get Domain Aliases
		$sm_GetAliases_ws_url = $smUrl."/Services/svcAliasAdmin.asmx?WSDL";
		$wsGetAliasesResult = sm_webServiceCall($sm_GetAliases_ws_url,$paramatersGetUsers,"GetAliases");

		//Call to get LstServ address
		$sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
		$wsGetListCommandAddressResult = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramatersGetUsers,"GetListCommandAddress");


		//Set alias array for manage smartermail tpl

		$smAliasesArrayForLoop = array();
		$smAliasesArray = array();



		if(is_array($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo'][0])){
		foreach($wsGetAliasesResult['GetAliasesResult']['AliasInfos']["AliasInfo"] as $key => $value)
		{
			foreach($value as $key2 => $value2)
			{
				if($key2 === 'Name')
				{
					array_push($smAliasesArray,$value2);
				}
			}
		}
		}else{
			if(is_array($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo'])){
				foreach($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo'] as $key => $value)
				{
					if($key === 'Name')
					{
						array_push($smAliasesArray,$value);
					}
				}
			}else{

			}
		}
		$smUsersArray = array();
		//setup user array to be displayed in ManageSmarterMail Template.  Checking to see if listServ name is showing or if they are an alias which shows in a separate section.
		if(is_array($wsGetDomainUsersResult['GetUsersResult']['Users']['UserInfo'])){
			foreach($wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo'] as $keyvar =>$userStringValue){
				if(is_array($userStringValue)){
					array_push($smUsersArray, $userStringValue["UserName"]);
				}else{
					if ($keyvar == "UserName"){
						array_push($smUsersArray, $userStringValue);
					}
				}
			}
		}
		$errormessage = "Favor preencha todos os campos corretamente.";
		$pagearray = array(
		'templatefile' => 'addsmartermailmailinglists',
		'breadcrumb' => ' > <a href="#">Add SmarterMail User Alias</a>',
		'vars' => array(
			'UserName' => $_POST["username"],
			'wsGetDomainUsersResultArray'=> $smUsersArray,
			'domainName' => $_POST["domain"],
			'message' => $message,
			'errormessage' => $errormessage,
			'assinante' => "",
			'lista' => "class='active'",
		),
		);

		return $pagearray;
	}

	$sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
	$wsResultaddlist = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters,"AddList");

	$paramatersaddlist = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName' => $_POST["domain"],
		'ListName' =>  $_POST['newlistname'],
		'newSettings' => array("whocanpost=".$_POST["whocanpost"]."","maxmessagesize=".$_POST["maxmessagesize"]."", "listreplytoaddress=LISTADDRESS", "listtoaddress=LISTADDRESS","listfromaddress=POSTERADDRESS","moderator=".$_POST['newmoderator'].""),
    );

	$sm_GetListCommandAddress_ws_url2 = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
   $wsResultaddlist2 = sm_webServiceCall( $sm_GetListCommandAddress_ws_url2,$paramatersaddlist,"SetRequestedListSettings");


	if($wsResultaddlist['AddListResult']['Result']){
        $result = "success";
    } else {
        $result =$wsResultaddlist['AddListResult']['Message'];
    }
   //Gerenciar Usuários e Lista
    $paramaters2 = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName'=> $_POST["domain"]
    );


    $sm_getDomainUsers_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
    $wsGetDomainUsersResult = sm_webServiceCall($sm_getDomainUsers_ws_url,$paramaters2,"GetUsers");

	$sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
	$wsGetListCommandAddressResultListsNames = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters2,"GetMailingListsByDomain");

    $smAliasesArrayForLoop = array();
    $smAliasesArray = array();


	if(is_array($wsGetListCommandAddressResultListsNames['GetMailingListsByDomainResult']['listNames']['string'] )){
		foreach($wsGetListCommandAddressResultListsNames['GetMailingListsByDomainResult']['listNames']['string'] as $key => $value){
			array_push($smAliasesArray,$value);
		}
	}else{
		array_push($smAliasesArray, $wsGetListCommandAddressResultListsNames['GetMailingListsByDomainResult']['listNames']['string']);
	}

//setup user array to be displayed in ManageSmarterMail Template.  Checking to see if listServ name is showing or if they are an alias which shows in a separate section.
    $smUsersArray = array();
			if(is_array($wsGetDomainUsersResult['GetUsersResult']['Users']['UserInfo'])){
				foreach($wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo'] as $keyvar =>$userStringValue){
					if(is_array($userStringValue)){
						array_push($smUsersArray, $userStringValue["UserName"]);
					}else{
						if ($keyvar == "UserName"){
							array_push($smUsersArray, $userStringValue);
						}
					}
				}
			}

	if($wsResult['AddListResult']['Result']){
		$message = "Lista de e-mail adicionada com sucesso.";
	} else {
		$errormessage = $wsResult['AddListResult']['Message'];
	}

    $pagearray = array(
        'templatefile' => 'managesmartermail',
        'breadcrumb' => ' > <a href="#">Manage SmarterMail</a>',
        'vars' => array(
            'wsGetDomainUsersResultArray' => $smUsersArray,
            'DomainName' => $_POST["domain"],
			'wsGetListCommandAddressResultListsNames' => $smAliasesArray,
			'wsGetAliasesResult' => $wsGetAliasesResult,
			'domainid' => $_POST["domain"],
			'IsDedicated' => $params["customfields"]["IsDedicated"],
			'message' => $message,
			'errormessage' => $errormessage,
			'assinante' => "",
			'lista' => "class='active'",
        ),
    );

    return $pagearray;
	//End Gerenciar Users e Lista
}

//called to display editsmartermailaliaspage.tpl, allowing the ability to edit user aliases to the domain.  Function of editing user alias is handled through smartermail_savesmartermailalias
function smartermail_editsmartermailmailinglistspage($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramatersList = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName' => $_POST["domain"],
        'ListName' =>  $_POST['listname'],
        'requestedSettings' => array("moderator", "description","listname","whocanpost","listtoaddress","listfromaddress","listreplytoaddress","maxmessagesize"),
    );
    $paramatersList2 = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName' => $_POST["domain"],
        'ListName' =>  $_POST['listname'],
     );



	$sm_GetListCommandAddress_ws_url2 = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
	$wsResult = sm_webServiceCall($sm_GetListCommandAddress_ws_url2,$paramatersList,"GetRequestedListSettings");

	if (str_replace("'", "",(str_replace('whocanpost=', '',$wsResult['GetRequestedListSettingsResult']['settingValues']['string'][2]))) === 'Moderator'){
		$whocanpostname= 'Moderador';
		$whocanpost= 'moderatoronly';
	}else if (str_replace("'", "",(str_replace('whocanpost=', '',$wsResult['GetRequestedListSettingsResult']['settingValues']['string'][2]))) === 'Subscribers'){
		$whocanpostname= 'Assinante';
		$whocanpost= 'subscribersonly';
	}else{
		 $whocanpostname= 'Qualquer um';
		 $whocanpost= 'anyone';
	}


	 //Call to get LstServ address
    $sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
	$wsGetListCommandAddressResultListsNames = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramatersList2,"GetSubscriberList");

    $smAliasesArrayForLoop = array();
    $smAliasesArray = array();
	if (is_array($wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string'])) {
	foreach($wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string'] as $key => $value){
		array_push($smAliasesArray,$value);
	}
	}else{
		if($wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string'] != null){
			array_push($smAliasesArray,$wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string']);
		}
	}

    //Call to get Domain Users
    $sm_getDomainUsers_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
    $wsGetDomainUsersResult = sm_webServiceCall($sm_getDomainUsers_ws_url,$paramaters,"GetUsers");
	$smUsersArray = array();
	if(is_array($wsGetDomainUsersResult['GetUsersResult']['Users']['UserInfo'])){
		foreach($wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo'] as $keyvar =>$userStringValue){

			if(is_array($userStringValue)){
				array_push($smUsersArray, $userStringValue["UserName"]);
			}else{
				if ($keyvar == "UserName"){
					array_push($smUsersArray, $userStringValue);
				}
			}
		}
	}
    $pagearray = array(
        'templatefile' => 'editsmartermailmailinglists',
        'breadcrumb' => ' > <a href="#">Edit SmarterMail User Alias</a>',
        'vars' => array(
			'wsGetAliasesResultArray' => $smAliasesArray,
			'wsGetDomainUsersResultArray' => $smUsersArray,
            'ListName' => $_POST['listname'],
            'Moderator' =>  str_replace("'", "",(str_replace('moderator=', '',$wsResult['GetRequestedListSettingsResult']['settingValues']['string'][0]))),
			'whocanpost' =>  $whocanpost,
			'DomainName' => $_POST["domain"],
			'whocanpostname' =>  $whocanpostname,
        	'maxmessagesize' => str_replace("'", "",(str_replace('maxmessagesize=', '',$wsResult['GetRequestedListSettingsResult']['settingValues']['string'][6]))),
			'maxmessagesizeMB' =>  (((int)(str_replace("'", "",(str_replace('maxmessagesize=', '',$wsResult['GetRequestedListSettingsResult']['settingValues']['string'][6])))))/ 1024)."MB",
			'assinante' => "class",
			'lista' => "class='active'",
		),
    );
    return $pagearray;
}

//used to edit user alias through smartermail web service.  Information is pSigned through the POST action from the editsmartermailaliasrpage.tpl
function smartermail_savechangessmartermailmailinglists($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);
	if ($_POST["whocanpost"] == 'Moderator'){
		$whocanpostname= 'Moderador';
		$whocanpost= 'moderatoronly';
	}else if ($_POST["whocanpost"] == 'Subscribers'){
		$whocanpostname= 'Assinante';
		$whocanpost= 'subscribersonly';
	}else{
		 $whocanpostname= 'Qualquer um';
		 $whocanpost= 'anyone';
	}

    $paramatersMailListAdmin = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName' => $_POST["domain"],
		'ListName' =>  $_POST['newlistname'],
		'newSettings' => array("whocanpost=".$whocanpost."","maxmessagesize=".$_POST["maxmessagesize"]."", "listreplytoaddress=LISTADDRESS", "listtoaddress=LISTADDRESS","listfromaddress=POSTERADDRESS","moderator=".$_POST['newmoderator'].""),
    );

   $sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
    $wsResult = sm_webServiceCall( $sm_GetListCommandAddress_ws_url,$paramatersMailListAdmin,"SetRequestedListSettings");

//Editar Lista e Assinantes
  $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName' => $_POST["domain"],
        'ListName' =>  $_POST['listname'],
        'requestedSettings' => array("moderator", "description","listname","whocanpost","listtoaddress","listfromaddress","listreplytoaddress","maxmessagesize"),
    );

    $paramaters2 = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName' => $_POST["domain"],
        'ListName' =>  $_POST['listname'],
    );

	$sm_GetListCommandAddress_ws_url2 = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
	$wsResult = sm_webServiceCall($sm_GetListCommandAddress_ws_url2,$paramaters,"GetRequestedListSettings");

	if (str_replace("'", "",(str_replace('whocanpost=', '',$wsResult['GetRequestedListSettingsResult']['settingValues']['string'][2]))) === 'Moderator'){
		$whocanpostname= 'Moderador';
		$whocanpost= 'moderatoronly';
	}else if (str_replace("'", "",(str_replace('whocanpost=', '',$wsResult['GetRequestedListSettingsResult']['settingValues']['string'][2]))) === 'Subscribers'){
		$whocanpostname= 'Assinante';
		$whocanpost= 'subscribersonly';
	}else{
		 $whocanpostname= 'Qualquer um';
		 $whocanpost= 'anyone';
	}

	 //Call to get LstServ address
    $sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
	$wsGetListCommandAddressResultListsNames = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters2,"GetSubscriberList");

    $smAliasesArrayForLoop = array();
    $smAliasesArray = array();
	if (is_array($wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string'])) {
		foreach($wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string'] as $key => $value)
		{
			array_push($smAliasesArray,$value);
		}
	}else{
		if($wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string'] != null){
			array_push($smAliasesArray,$wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string']);
		}
	}

    //Call to get Domain Users
    $sm_getDomainUsers_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
    $wsGetDomainUsersResult = sm_webServiceCall($sm_getDomainUsers_ws_url,$paramaters,"GetUsers");
	$smUsersArray = array();
	if(is_array($wsGetDomainUsersResult['GetUsersResult']['Users']['UserInfo'])){
		foreach($wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo'] as $keyvar =>$userStringValue){

			if(is_array($userStringValue)){
				array_push($smUsersArray, $userStringValue["UserName"]);
			}else{
				if ($keyvar == "UserName"){
					array_push($smUsersArray, $userStringValue);
				}
			}
		}
	}
    $pagearray = array(
        'templatefile' => 'editsmartermailmailinglists',
        'breadcrumb' => ' > <a href="#">Edit SmarterMail User Alias</a>',
        'vars' => array(
			'wsGetAliasesResultArray' => $smAliasesArray,
			'wsGetDomainUsersResultArray' => $smUsersArray,
            'ListName' => $_POST['listname'],
            'Moderator' =>  str_replace("'", "",(str_replace('moderator=', '',$wsResult['GetRequestedListSettingsResult']['settingValues']['string'][0]))),
			'whocanpost' =>  $whocanpost,
			'DomainName' => $_POST["domain"],
			'whocanpostname' =>  $whocanpostname,
        	'maxmessagesize' => str_replace("'", "",(str_replace('maxmessagesize=', '',$wsResult['GetRequestedListSettingsResult']['settingValues']['string'][6]))),
			'maxmessagesizeMB' =>  (((int)(str_replace("'", "",(str_replace('maxmessagesize=', '',$wsResult['GetRequestedListSettingsResult']['settingValues']['string'][6])))))/ 1024)."MB",
			'assinante' => "class",
			'lista' => "class='active'",
		),
    );

    return $pagearray;
//End
}

//called to delete alias through smartermail web services.  This is initiated from the editsmartermailalias.tpl
function smartermail_deletesmartermailmailinglists($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName' => $_POST["domain"],
        'ListName' =>  $_POST['listname']
    );


    $sm_DeleteAlias_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
    $wsResult = sm_webServiceCall($sm_DeleteAlias_ws_url,$paramaters,"DeleteList");


	 $result =  $wsResult ;
  //Gerenciar Usuários e Lista
    $paramaters2 = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName'=> $_POST["domain"]
    );


    $sm_getDomainUsers_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
    $wsGetDomainUsersResult = sm_webServiceCall($sm_getDomainUsers_ws_url,$paramaters2,"GetUsers");

	$sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
	$wsGetListCommandAddressResultListsNames = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters2,"GetMailingListsByDomain");

    $smAliasesArrayForLoop = array();
    $smAliasesArray = array();


	if(is_array($wsGetListCommandAddressResultListsNames['GetMailingListsByDomainResult']['listNames']['string'] )){
		foreach($wsGetListCommandAddressResultListsNames['GetMailingListsByDomainResult']['listNames']['string'] as $key => $value){
			array_push($smAliasesArray,$value);
		}
	}else{
		array_push($smAliasesArray, $wsGetListCommandAddressResultListsNames['GetMailingListsByDomainResult']['listNames']['string']);
	}

//setup user array to be displayed in ManageSmarterMail Template.  Checking to see if listServ name is showing or if they are an alias which shows in a separate section.
    $smUsersArray = array();
			if(is_array($wsGetDomainUsersResult['GetUsersResult']['Users']['UserInfo'])){
				foreach($wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo'] as $keyvar =>$userStringValue){
					if(is_array($userStringValue)){
						array_push($smUsersArray, $userStringValue["UserName"]);
					}else{
						if ($keyvar == "UserName"){
							array_push($smUsersArray, $userStringValue);
						}
					}
				}
			}

	if($wsResult['DeleteListResult']['Result']){
		$message = "Lista de e-mail removida com sucesso.";
	} else {
		$errormessage = $wsResult['DeleteListResult']['Message'];
	}
    $pagearray = array(
        'templatefile' => 'managesmartermail',
        'breadcrumb' => ' > <a href="#">Manage SmarterMail</a>',
        'vars' => array(
            'wsGetDomainUsersResultArray' => $smUsersArray,
            'DomainName' => $_POST["domain"],
			'wsGetListCommandAddressResultListsNames' => $smAliasesArray,
			'wsGetAliasesResult' => $wsGetAliasesResult,
			'domainid' => $_POST["domain"],
			'IsDedicated' => $params["customfields"]["IsDedicated"],
			'message' => $message,
			'errormessage' => $errormessage,
			'lista' => "class='active'",
			'usuarios' => "",
        ),
    );

    return $pagearray;
	//End Gerenciar Users e Lista
}

function smartermail_addsmartermailmailinglistsSign($params) {
    $pagearray = array(
        'templatefile' => 'addsmartermailmailinglistsSign',
        'breadcrumb' => ' > <a href="#">Add SmarterMail mailmailinglistsSign</a>',
		'vars' => array(
			'DomainName' => $_POST["domain"],
			'ListName' => $_POST["listname"],
		),
    );
    return $pagearray;
}

//used to create user alias through smartermail web service.  Information is pSigned through the POST action from the addsmartermailalias.tpl
function smartermail_createsmartermailmailinglistsSign($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramatersGetRequestedListSettings = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName' => $_POST["domain"],
        'ListName' =>  $_POST['listname'],
        'Subscribers' =>  explode("\r\n", $_POST['newSub']),
    );
       $paramaters = array(
			'AuthUserName' => $params["serverusername"],
			'AuthPassword' => $params["serverpassword"],
			'DomainName' => $_POST["domain"],
			'ListName' =>  $_POST['listname'],
			'requestedSettings' => array("moderator", "description","listname","whocanpost","listtoaddress","listfromaddress","listreplytoaddress","maxmessagesize"),
		);
		$paramaters2 = array(
			'AuthUserName' => $params["serverusername"],
			'AuthPassword' => $params["serverpassword"],
			'DomainName' => $_POST["domain"],
			'ListName' =>  $_POST['listname'],
		 );

		$paramatersdomain = array(
			'AuthUserName' => $params["serverusername"],
			'AuthPassword' => $params["serverpassword"],
			'DomainName' => $_POST["domain"],
		 );

		$sm_GetListCommandAddress_ws_url2 = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
		$wsResultGetRequestedListSettings = sm_webServiceCall($sm_GetListCommandAddress_ws_url2, $paramaters,"GetRequestedListSettings");

		if (str_replace("'", "",(str_replace('whocanpost=', '',$wsResultGetRequestedListSettings['GetRequestedListSettingsResult']['settingValues']['string'][2]))) === 'Moderator'){
			$whocanpostname= 'Somente Moderador';
			$whocanpost= 'moderatoronly';
		}else if (str_replace("'", "",(str_replace('whocanpost=', '',$wsResultGetRequestedListSettings['GetRequestedListSettingsResult']['settingValues']['string'][2]))) === 'Subscribers'){
			$whocanpostname= 'Somente Signinante';
			$whocanpost= 'subscribersonly';
		}else{
			 $whocanpostname= 'Qualquer um';
			 $whocanpost= 'anyone';
		}


	if($_POST['newSub'] != '')
	{
		$sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
		$wsResult = sm_webServiceCall($sm_GetListCommandAddress_ws_url, $paramatersGetRequestedListSettings,"AddSubscriber2");
		if($wsResult['AddSubscriber2Result']['Result']){
			 $message = "Assinante adicionado com sucesso";
		} else {
			$errormessage = $wsResult['AddSubscriber2Result']['Message'];
		}
	} else{
	    $errormessage = "Favor preencha todos os campos corretamente.";
		$pagearray = array(
		'templatefile' => 'addsmartermailmailinglistsSign',
		'breadcrumb' => ' > <a href="#">Add SmarterMail mailmailinglistsSign</a>',
		'vars' => array(
		'DomainName' => $_POST["domain"],
		'ListName' => $_POST["listname"],
		'message' => $message,
		'errormessage' => $errormessage,
		'assinante' => "class='active'",
		'lista' => "",
		),
		);
		return $pagearray;

	}

	 //Call to get LstServ address
	$sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
	$wsGetListCommandAddressResultListsNames = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters2,"GetSubscriberList");

	$smAliasesArrayForLoop = array();
	$smAliasesArray = array();
	if (is_array($wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string'])) {
		foreach($wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string'] as $key => $value)
		{
			array_push($smAliasesArray,$value);
		}
	}else{
		if($wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string'] != null){
			array_push($smAliasesArray,$wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string']);
		}
	}


	//Call to get Domain Users
	$sm_getDomainUsers_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
	$wsGetDomainUsersResult = sm_webServiceCall($sm_getDomainUsers_ws_url,$paramatersdomain,"GetUsers");
		$smUsersArray = array();
		if(is_array($wsGetDomainUsersResult['GetUsersResult']['Users']['UserInfo'])){
			foreach($wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo'] as $keyvar =>$userStringValue){

				if(is_array($userStringValue)){
					array_push($smUsersArray, $userStringValue["UserName"]);
				}else{
					if ($keyvar == "UserName"){
						array_push($smUsersArray, $userStringValue);
					}
				}
			}
		}
	$pagearray = array(
		'templatefile' => 'editsmartermailmailinglists',
		'breadcrumb' => ' > <a href="#">Edit SmarterMail User Alias</a>',
		'vars' => array(
			'wsGetAliasesResultArray' => $smAliasesArray,
			'wsGetDomainUsersResultArray' => $smUsersArray,
			'ListName' => $_POST['listname'],
			'Moderator' =>  str_replace("'", "",(str_replace('moderator=', '',$wsResultGetRequestedListSettings['GetRequestedListSettingsResult']['settingValues']['string'][0]))),
			'whocanpost' =>  $whocanpost,
			'DomainName' => $_POST["domain"],
			'whocanpostname' =>  $whocanpostname,
			'maxmessagesize' => str_replace("'", "",(str_replace('maxmessagesize=', '',$wsResultGetRequestedListSettings['GetRequestedListSettingsResult']['settingValues']['string'][6]))),
			'maxmessagesizeMB' =>  (((int)(str_replace("'", "",(str_replace('maxmessagesize=', '',$wsResultGetRequestedListSettings['GetRequestedListSettingsResult']['settingValues']['string'][6])))))/ 1024)."MB",
			'message' => $message,
			'errormessage' => $errormessage,
			'assinante' => "class='active'",
			'lista' => "",
		),
	);

	return $pagearray;
}

//called to display editsmartermailaliaspage.tpl, allowing the ability to edit user aliases to the domain.  Function of editing user alias is handled through smartermail_savesmartermailalias
function smartermail_editsmartermailmailinglistspageSign($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName' => $params["domain"],
        'ListName' =>  $_POST['listname'],
        'requestedSettings' => array("moderator", "description","listname"),
    );

	if(is_null($_POST['listname']))
		return "Don't run API Calls. (try running the FORM instead)";

	$sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
	$wsResult = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters,"GetRequestedListSettings");


    $pagearray = array(
        'templatefile' => 'editsmartermailmailinglists',
        'breadcrumb' => ' > <a href="#">Edit SmarterMail User Alias</a>',
        'vars' => array(
            'ListName' => $_POST['listname'],
            'Moderator' =>  str_replace("'", "",(str_replace('moderator=', '',$wsResult['GetRequestedListSettingsResult']['settingValues']['string'][0]))),
			'Description' =>  str_replace("'", "",(str_replace('description=', '',$wsResult['GetRequestedListSettingsResult']['settingValues']['string'][1]))),
        ),
    );
    return $pagearray;
}

//used to edit user alias through smartermail web service.  Information is pSigned through the POST action from the editsmartermailaliasrpage.tpl
function smartermail_savechangessmartermailmailinglistsSign($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName' => $params["domain"],
		'ListName' =>  $_POST['newlistname'],
		'newSettings' => array("moderator='".$_POST['newmoderator']."'","description='".$_POST['newdescription']."'"),
    );

	if(is_null($_POST['newlistname']))
		return "Don't run API Calls. (try running the FORM instead)";

   $sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
    $wsResult = sm_webServiceCall( $sm_GetListCommandAddress_ws_url,$paramaters,"SetRequestedListSettings");

	if($wsResult['SetRequestedListSettingsResult']['Result']){
        $result = "success";
    } else {
        $result = $wsResult['SetRequestedListSettingsResult']['Message'];
    }
    return $result;
}

//called to delete alias through smartermail web services.  This is initiated from the editsmartermailalias.tpl
function smartermail_deletesmartermailmailinglistsSign($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);
	$paramatersSign = array(
		'AuthUserName' => $params["serverusername"],
		'AuthPassword' => $params["serverpassword"],
		'DomainName' => $_POST["domain"],
		'ListName' =>  $_POST['listname'],
		'requestedSettings' => array("moderator", "description","listname","whocanpost","listtoaddress","listfromaddress","listreplytoaddress","maxmessagesize"),
	);
    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName' =>  $_POST["domain"],
        'ListName' =>  $_POST['listname'],
		'Subscriber' =>  $_POST['Subscriber'],
    );
	$paramaters2 = array(
		'AuthUserName' => $params["serverusername"],
		'AuthPassword' => $params["serverpassword"],
		'DomainName' => $_POST["domain"],
		'ListName' =>  $_POST['listname'],
	 );

	$paramatersdomain = array(
		'AuthUserName' => $params["serverusername"],
		'AuthPassword' => $params["serverpassword"],
		'DomainName' => $_POST["domain"],
	 );

    $sm_DeleteAlias_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
    $wsResult = sm_webServiceCall($sm_DeleteAlias_ws_url,$paramaters,"RemoveSubscriber");
	if($wsResult['RemoveSubscriberResult']['Result']){
       	 $message = "Assinante removido com sucesso";
    } else {
		$errormessage = $wsResult['RemoveSubscriberResult']['Message'];
	}
	$sm_GetListCommandAddress_ws_url2 = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
	$wsResultGetRequestedListSettings = sm_webServiceCall($sm_GetListCommandAddress_ws_url2, $paramatersSign,"GetRequestedListSettings");

	if (str_replace("'", "",(str_replace('whocanpost=', '',$wsResultGetRequestedListSettings['GetRequestedListSettingsResult']['settingValues']['string'][2]))) === 'Moderator'){
		$whocanpostname= 'Somente Moderador';
		$whocanpost= 'moderatoronly';
	}else if (str_replace("'", "",(str_replace('whocanpost=', '',$wsResultGetRequestedListSettings['GetRequestedListSettingsResult']['settingValues']['string'][2]))) === 'Subscribers'){
		$whocanpostname= 'Somente Signinante';
		$whocanpost= 'subscribersonly';
	}else{
		 $whocanpostname= 'Qualquer um';
		 $whocanpost= 'anyone';
	}

		 //Call to get LstServ address
	$sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
	$wsGetListCommandAddressResultListsNames = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters2,"GetSubscriberList");



	$smAliasesArrayForLoop = array();
	$smAliasesArray = array();
	if (is_array($wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string'])) {
		foreach($wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string'] as $key => $value)
		{
			array_push($smAliasesArray,$value);
		}
	}else{
		if($wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string'] != null){
			array_push($smAliasesArray,$wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string']);
		}
	}


	//Call to get Domain Users
	$sm_getDomainUsers_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
	$wsGetDomainUsersResult = sm_webServiceCall($sm_getDomainUsers_ws_url,$paramatersdomain,"GetUsers");

		$smUsersArray = array();
		if(is_array($wsGetDomainUsersResult['GetUsersResult']['Users']['UserInfo'])){
			foreach($wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo'] as $keyvar =>$userStringValue){

				if(is_array($userStringValue)){
					array_push($smUsersArray, $userStringValue["UserName"]);
				}else{
					if ($keyvar == "UserName"){
						array_push($smUsersArray, $userStringValue);
					}
				}
			}
		}
	$pagearray = array(
		'templatefile' => 'editsmartermailmailinglists',
		'breadcrumb' => ' > <a href="#">Edit SmarterMail User Alias</a>',
		'vars' => array(
			'wsGetAliasesResultArray' => $smAliasesArray,
			'wsGetDomainUsersResultArray' => $smUsersArray,
			'ListName' => $_POST['listname'],
			'Moderator' =>  str_replace("'", "",(str_replace('moderator=', '',$wsResultGetRequestedListSettings['GetRequestedListSettingsResult']['settingValues']['string'][0]))),
			'whocanpost' =>  $whocanpost,
			'DomainName' => $_POST["domain"],
			'whocanpostname' =>  $whocanpostname,
			'maxmessagesize' => str_replace("'", "",(str_replace('maxmessagesize=', '',$wsResultGetRequestedListSettings['GetRequestedListSettingsResult']['settingValues']['string'][6]))),
			'maxmessagesizeMB' =>  (((int)(str_replace("'", "",(str_replace('maxmessagesize=', '',$wsResultGetRequestedListSettings['GetRequestedListSettingsResult']['settingValues']['string'][6])))))/ 1024)."MB",
		'message' => $message,
		'errormessage' => $errormessage,
		'assinante' => "class='active'",
		'lista' => "",
		),
	);

	return $pagearray;
}

//called to display addsmartermailuser.tpl, allowing the ability to add new users to the domain.  Function of adding user is handled through smartermail_createsmartermailuser
function smartermail_addsmartermailuser($params) {
	// TRAVA SOMENTE DEDICADO GERENCIA DOMINIO VIA POST
	if($params['customfields']['IsDedicated'] != 'on'){
		$_POST["domain"] = $params["domain"];
	}
	//echo'<pre>'; print_r($params);
    $pagearray = array(
        'templatefile' => 'addsmartermailuser',
        'breadcrumb' => ' > <a href="#">Add SmarterMail User</a>',
        'vars' => array(
            'DomainName' =>  $_POST["domain"],
            'DomainName' =>  $_POST["domain"],
        ),
    );

    return $pagearray;
}

//used to create user through smartermail web service.  Information is pSigned through the POST action from the addsmartermailuser.tpl
function smartermail_createsmartermailuser($params) {
    $smUrl = addHttpToHostname($params["serverhostname"]);
    $arrdomaininfo = array(
            'AuthUserName' => $params["serverusername"],
            'AuthPassword' => $params["serverpassword"],
			'requestedSettings' => array('defaultmaxmailboxsize'),
    );

	$domaininfo = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
	$wsResultdomaininfo = sm_webServiceCall($domaininfo, $arrdomaininfo, "GetRequestedDomainDefaults");
	$sizemailbox = explode("=", $wsResultdomaininfo['GetRequestedDomainDefaultsResult']['settingValues']['string']);


	$paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'NewUsername' =>  $_POST['newusername'],
        'NewPassword' => $_POST['newpassword'],
        'DomainName' => $_POST["domain"],
        'FirstName' =>  $_POST['newfirstname'],
        'LastName' =>  '',
        'IsDomainAdmin' => false,
        //'maxMailboxSize' => $sizemailbox[1],
				'maxMailboxSize' => $params["configoption17"],
    );

	if($_POST['newusername'] ==''){
			$errormessage = "Favor preencha todos os campos corretamente.";
			$pagearray = array(
				'templatefile' => 'addsmartermailuser',
				'breadcrumb' => ' > <a href="#">Add SmarterMail User</a>',
				'vars' => array(
				'DomainName' =>  $_POST["domain"],
				'message' => $message,
				'errormessage' => $errormessage,
				),
			);

		return $pagearray;

	}
    $sm_AddUser2_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
    $wsResult = sm_webServiceCall($sm_AddUser2_ws_url,$paramaters,"AddUser2");

	//Gerenciar Usuários e Lista
    $paramaters2 = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName'=> $_POST["domain"]
    );


    $sm_getDomainUsers_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
    $wsGetDomainUsersResult = sm_webServiceCall($sm_getDomainUsers_ws_url,$paramaters2,"GetUsers");

	$sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
	$wsGetListCommandAddressResultListsNames = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters2,"GetMailingListsByDomain");

    $smAliasesArrayForLoop = array();
    $smAliasesArray = array();


	if(is_array($wsGetListCommandAddressResultListsNames['GetMailingListsByDomainResult']['listNames']['string'] )){
		foreach($wsGetListCommandAddressResultListsNames['GetMailingListsByDomainResult']['listNames']['string'] as $key => $value)
		{
			array_push($smAliasesArray,$value);
		}
	}else{
		array_push($smAliasesArray, $wsGetListCommandAddressResultListsNames['GetMailingListsByDomainResult']['listNames']['string']);
	}

//setup user array to be displayed in ManageSmarterMail Template.  Checking to see if listServ name is showing or if they are an alias which shows in a separate section.
    $smUsersArray = array();
			if(is_array($wsGetDomainUsersResult['GetUsersResult']['Users']['UserInfo'])){
				foreach($wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo'] as $keyvar =>$userStringValue){
					if(is_array($userStringValue)){
						array_push($smUsersArray, $userStringValue["UserName"]);
					}else{
						if ($keyvar == "UserName"){
							array_push($smUsersArray, $userStringValue);
						}
					}
				}
			}

	if($wsResult['AddUser2Result']['Result']){
		$message = "Usuário adicionado com sucesso.";
	} else {
		$errormessage = $wsResult['AddUser2Result']['Message'];
	}

    $pagearray = array(
        'templatefile' => 'managesmartermail',
        'breadcrumb' => ' > <a href="#">Manage SmarterMail</a>',
        'vars' => array(
			'wsGetDomainUsersResultArray' => $smUsersArray,
			'DomainName' => $_POST["domain"],
			'wsGetListCommandAddressResultListsNames' => $smAliasesArray,
			'wsGetAliasesResult' => $wsGetAliasesResult,
			'domainid' => $_POST["domain"],
			'IsDedicated' => $params["customfields"]["IsDedicated"],
			'message' => $message,
			'errormessage' => $errormessage,
        ),
    );

    return $pagearray;
	//End Gerenciar Users e Lista
}

//called to display editsmartermailuserpage.tpl, allowing the ability to edit users to the domain.  Function of editng user is handled through smartermail_savesmartermailuser
function smartermail_editsmartermailuserpage($params) {
    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramatersForGetUser = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'EmailAddress' => ($_POST['selectuser']),
    );

    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName'=> $_POST["domain"]
    );

    $sm_GetUser_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
    $wsResult = sm_webServiceCall($sm_GetUser_ws_url,$paramatersForGetUser,"GetUser");


    //Get MAX mailbox size

    $paramatersForReqSettings = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'EmailAddress' => ($_POST['selectuser']),
        'requestedSettings' => array('maxsize', 'enabledstatus', 'isdomainadmin'),
    );

    $sm_GetRequestedUserSettings_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
    $wsResult2 = sm_webServiceCall($sm_GetRequestedUserSettings_ws_url,$paramatersForReqSettings,"GetRequestedUserSettings");

    //Call to get Domain Aliases
    $sm_GetAliases_ws_url = $smUrl."/Services/svcAliasAdmin.asmx?WSDL";
    $wsGetAliasesResult = sm_webServiceCall($sm_GetAliases_ws_url,$paramaters,"GetAliases");

    //Call to get LstServ address
    $sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
    $wsGetListCommandAddressResult = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters,"GetListCommandAddress");


//Set alias array for manage smartermail tpl

   			$smAliasesArrayForLoop = array();
			$smAliasesArray = array();
			if(is_array($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo'])){
				foreach($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo'] as $keyvar =>$userStringValue){
					if(is_array($userStringValue)){
							if ($userStringValue["Addresses"]["string"] == $_POST["username"]){
								array_push($smAliasesArray, $userStringValue["Name"]);
							}
					}else{
						if($userStringValue ==  $_POST["username"]){
							if ($keyvar == "Name"){
								array_push($smAliasesArray, $userStringValue);
							}

						}else{
							if ($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo']["Addresses"]["string"]== $_POST["username"]){
								array_push($smAliasesArray,$wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo']["Name"]);
							}
						}
					}
				}
			}


    //Get Primary Admin to prevent account delete

    $paramatersForGetPrimaryAdmin = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName'=> $_POST["domain"],
    );

    $sm_GetPrimaryDomainAdmin_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
    $wsResult3 = sm_webServiceCall($sm_GetPrimaryDomainAdmin_ws_url,$paramatersForGetPrimaryAdmin,"GetPrimaryDomainAdmin");

    $mailboxStatus = "Enabled";
    switch(ltrim(strrchr(($wsResult2['GetRequestedUserSettingsResult']['settingValues']['string'][1]), '='),'=')){
        case "Enabled":
            $mailboxStatus = 1;
            break;

        case "DisabledWMail":
            $mailboxStatus = 2;
            break;
        case "Disabled":
            $mailboxStatus = 3;
    }


    $pagearray = array(
        'templatefile' => 'editsmartermailuser',
        'breadcrumb' => ' > <a href="#">Edit SmarterMail User</a>',
        'vars' => array(
		    'wsGetAliasesResultArray' => $smAliasesArray,
            'FirstName' => $wsResult['GetUserResult']['UserInfo']['FirstName'],
            'isDomainAdmin' => $wsResult['GetUserResult']['UserInfo']['IsDomainAdmin'],
            'LastName' => $wsResult['GetUserResult']['UserInfo']['LastName'],
            'Password' => $wsResult['GetUserResult']['UserInfo']['Password'],
            'UserName' => $wsResult['GetUserResult']['UserInfo']['UserName'],
            'maxMailboxSize' => ltrim(strrchr(($wsResult2['GetRequestedUserSettingsResult']['settingValues']['string'][0]), '='),'='),
            'mailboxStatus' => $mailboxStatus,
			'DomainName'=> $_POST["domain"],
			'IsDedicated' => $params["customfields"]["IsDedicated"],
            'isdomainadmin' => ltrim(strrchr(($wsResult2['GetRequestedUserSettingsResult']['settingValues']['string'][2]), '='),'='),
        ),
    );

    return $pagearray;
}

//used to edit user through smartermail web service.  Information is pSigned through the POST action from the editsmartermailuserpage.tpl
function smartermail_savechangessmartermailuser($params) {
  $smUrl = addHttpToHostname($params["serverhostname"]);
  if (($_POST['newpassword'])!=''){
		$senha =  $_POST['newpassword'];
	}else{
		$senha =  $_POST['password'];
	}


    $paramatersUpdateUser2 = array(
            'AuthUserName' => $params["serverusername"],
            'AuthPassword' => $params["serverpassword"],
            'EmailAddress' => $_POST['username'],
            'NewPassword' =>$senha,
            'NewFirstName' =>$_POST['firstname'],
            'NewLastName' => $_POST['lastname'],
            'IsDomainAdmin' => false,
            'maxMailboxSize' => $_POST['mailboxsize'] ,
    );



    $sm_UpdateUser2_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
    $wsResult1 = sm_webServiceCall($sm_UpdateUser2_ws_url, $paramatersUpdateUser2, "UpdateUser2");

    $mailboxStatusArray = array();
    switch($_POST['mailboxstatus']){

        case "1":
            $mailboxStatusArray = array(
                'enabledstatus=Enabled'
            );
            break;

        case "2":
            $mailboxStatusArray = array(
                'enabledstatus=DisabledWMail'
            );
            break;
        case "3":
            $mailboxStatusArray = array(
                'enabledstatus=Disabled'
            );
            break;
    }

    $paramaters2 = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'EmailAddress' => $_POST['username'],
        'newSettings' => $mailboxStatusArray,
    );

	$sm_SetRequestedUserSettings_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
	$wsResultSetRequestedUserSettings = sm_webServiceCall($sm_SetRequestedUserSettings_ws_url, $paramaters2, "SetRequestedUserSettings");

	if($wsResult1['UpdateUser2Result']['Result']){
		$message = "Usuário atualizado com sucesso.";
	} else {
		$errormessage = $wsResult1['UpdateUser2Result']['Message'];
	}


	//Edit User e gerecia alias
    $paramatersForGetUser = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'EmailAddress' =>  $_POST['username'],
    );

    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName'=> $_POST["domain"]
    );

    $sm_GetUser_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
    $wsResult = sm_webServiceCall($sm_GetUser_ws_url,$paramatersForGetUser,"GetUser");


    //Get MAX mailbox size

    $paramatersForReqSettings = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'EmailAddress' =>  $_POST['username'],
        'requestedSettings' => array('maxsize', 'enabledstatus', 'isdomainadmin'),
    );

    $sm_GetRequestedUserSettings_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
    $wsResult2 = sm_webServiceCall($sm_GetRequestedUserSettings_ws_url,$paramatersForReqSettings,"GetRequestedUserSettings");

    //Call to get Domain Aliases
    $sm_GetAliases_ws_url = $smUrl."/Services/svcAliasAdmin.asmx?WSDL";
    $wsGetAliasesResult = sm_webServiceCall($sm_GetAliases_ws_url,$paramaters,"GetAliases");

    //Call to get LstServ address
    $sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
    $wsGetListCommandAddressResult = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters,"GetListCommandAddress");


//Set alias array for manage smartermail tpl

   			$smAliasesArrayForLoop = array();
			$smAliasesArray = array();
			if(is_array($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo'])){
				foreach($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo'] as $keyvar =>$userStringValue){
					if(is_array($userStringValue)){
							if ($userStringValue["Addresses"]["string"] == $_POST["username"]){
								array_push($smAliasesArray, $userStringValue["Name"]);
							}
					}else{
						if($userStringValue ==  $_POST["username"]){
							if ($keyvar == "Name"){
								array_push($smAliasesArray, $userStringValue);
							}

						}else{
							if ($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo']["Addresses"]["string"]== $_POST["username"]){
								array_push($smAliasesArray,$wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo']["Name"]);
							}
						}
					}
				}
			}


    //Get Primary Admin to prevent account delete

    $paramatersForGetPrimaryAdmin = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName'=> $_POST["domain"],
    );

    $sm_GetPrimaryDomainAdmin_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
    $wsResult3 = sm_webServiceCall($sm_GetPrimaryDomainAdmin_ws_url,$paramatersForGetPrimaryAdmin,"GetPrimaryDomainAdmin");

    $mailboxStatus = "Enabled";
    switch(ltrim(strrchr(($wsResult2['GetRequestedUserSettingsResult']['settingValues']['string'][1]), '='),'=')){
        case "Enabled":
            $mailboxStatus = 1;
            break;

        case "DisabledWMail":
            $mailboxStatus = 2;
            break;
        case "Disabled":
            $mailboxStatus = 3;
    }


    $pagearray = array(
        'templatefile' => 'editsmartermailuser',
        'breadcrumb' => ' > <a href="#">Edit SmarterMail User</a>',
        'vars' => array(
		    'wsGetAliasesResultArray' => $smAliasesArray,
        'FirstName' => $wsResult['GetUserResult']['UserInfo']['FirstName'],
        'isDomainAdmin' => $wsResult['GetUserResult']['UserInfo']['IsDomainAdmin'],
        'LastName' => $wsResult['GetUserResult']['UserInfo']['LastName'],
        'Password' => $wsResult['GetUserResult']['UserInfo']['Password'],
        'UserName' => $wsResult['GetUserResult']['UserInfo']['UserName'],
        'maxMailboxSize' => ltrim(strrchr(($wsResult2['GetRequestedUserSettingsResult']['settingValues']['string'][0]), '='),'='),
        'mailboxStatus' => $mailboxStatus,
				'DomainName'=> $_POST["domain"],
				'IsDedicated' => $params["customfields"]["IsDedicated"],
        'isdomainadmin' => ltrim(strrchr(($wsResult2['GetRequestedUserSettingsResult']['settingValues']['string'][2]), '='),'='),
				'message' => $message,
				'errormessage' => $errormessage,
		),
    );

    return $pagearray;
	//END
}

//called to delete user through smartermail web services.  This is initiated from the editsmartermailuser.tpl
function smartermail_deletesmartermailuser($params) {
  $smUrl = addHttpToHostname($params["serverhostname"]);

  $paramaters = array(
      'AuthUserName' => $params["serverusername"],
      'AuthPassword' => $params["serverpassword"],
      'Username' => str_replace("@".$_POST["domain"], "", $_POST['username']),
      'DomainName' => $_POST["domain"],
  );

	if(is_null($_POST['username']))
		return "Don't run API Calls. (try running the FORM instead)";

  $sm_DeleteUser_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
  $wsResult = sm_webServiceCall($sm_DeleteUser_ws_url,$paramaters,"DeleteUser");


//Gerenciar Usuários e Lista
  $paramaters2 = array(
      'AuthUserName' => $params["serverusername"],
      'AuthPassword' => $params["serverpassword"],
      'DomainName'=> $_POST["domain"]
  );


  $sm_getDomainUsers_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
  $wsGetDomainUsersResult = sm_webServiceCall($sm_getDomainUsers_ws_url,$paramaters2,"GetUsers");

	$sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
	$wsGetListCommandAddressResultListsNames = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters2,"GetMailingListsByDomain");

  $smAliasesArrayForLoop = array();
  $smAliasesArray = array();


	if(is_array($wsGetListCommandAddressResultListsNames['GetMailingListsByDomainResult']['listNames']['string'] )){
		foreach($wsGetListCommandAddressResultListsNames['GetMailingListsByDomainResult']['listNames']['string'] as $key => $value)
		{
			array_push($smAliasesArray,$value);
		}
	}else{

		array_push($smAliasesArray, $wsGetListCommandAddressResultListsNames['GetMailingListsByDomainResult']['listNames']['string']);
	}

//setup user array to be displayed in ManageSmarterMail Template.  Checking to see if listServ name is showing or if they are an alias which shows in a separate section.
	$smUsersArray = array();

	if(is_array($wsGetDomainUsersResult['GetUsersResult']['Users']['UserInfo'])){
		foreach($wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo'] as $keyvar =>$userStringValue){
			if(is_array($userStringValue)){
				array_push($smUsersArray, $userStringValue["UserName"]);
			}else{
				if ($keyvar == "UserName"){
					array_push($smUsersArray, $userStringValue);
				}
			}
		}
	}

	if($wsResult['DeleteUserResult']['Result']){
		$message = "Usuário removido com sucesso.";
	} else {
		$errormessage = $wsResult['DeleteUserResult']['Message'];
	}

  $pagearray = array(
    'templatefile' => 'managesmartermail',
    'breadcrumb' => ' > <a href="#">Manage SmarterMail</a>',
    'vars' => array(
      'wsGetDomainUsersResultArray' => $smUsersArray,
      'DomainName' => $_POST["domain"],
			'wsGetListCommandAddressResultListsNames' => $smAliasesArray,
			'wsGetAliasesResult' => $wsGetAliasesResult,
			'domainid' => $_POST["domain"],
			'IsDedicated' => $params["customfields"]["IsDedicated"],
			'message' => $message,
			'errormessage' => $errormessage,
    ),
  );

  return $pagearray;
}

//called to display addsmartermailalias.tpl, allowing the ability to add new user aliases to the domain.  Function of adding alias is handled through smartermail_createsmartermailalias
function smartermail_addsmartermailalias($params) {
	$pagearray = array(
	  'templatefile' => 'addsmartermailalias',
	  'breadcrumb' => ' > <a href="#">Add SmarterMail User Alias</a>',
		'vars' => array(
			'UserName' => $_POST["username"],
			'DomainName' => $_POST["domain"],
		),
	);
	return $pagearray;
}

//used to create user alias through smartermail web service.  Information is pSigned through the POST action from the addsmartermailalias.tpl
function smartermail_createsmartermailalias($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);

	if(is_null($_POST['newaliasname']))
	{
		$pagearray = array(
			'templatefile' => 'addsmartermailalias',
			'breadcrumb' => ' > <a href="#">Add SmarterMail User Alias</a>',
			'vars' => array(
			'UserName' => $_POST["username"],
			  'DomainName' => $_POST["domain"],
			),
		);
		return $pagearray;

	}else{
		if($_POST['newaliasname'] != ""){
			$paramaters2 = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'DomainName' => $_POST["domain"],
				'AliasName' =>  $_POST['newaliasname'],
				'Addresses' => explode("\r\n", $_POST['newaliasemailaddress']),
			);

			$sm_AddAlias_ws_url = $smUrl."/Services/svcAliasAdmin.asmx?WSDL";
			$wsResultAlias = sm_webServiceCall($sm_AddAlias_ws_url,$paramaters2,"AddAlias");

			if($wsResultAlias['AddAliasResult']['Result']){
				$message = "Apelido de usuário adicionado com sucesso.";
			} else {
				$errormessage = $wsResultAlias['AddAliasResult']['Message'];
			}

			$smUrl = addHttpToHostname($params["serverhostname"]);

			$paramatersForGetUser = array(
			'AuthUserName' => $params["serverusername"],
			'AuthPassword' => $params["serverpassword"],
			'EmailAddress' => $_POST["username"],
			);

			$paramaters = array(
			'AuthUserName' => $params["serverusername"],
			'AuthPassword' => $params["serverpassword"],
			'DomainName'=> $_POST["domain"]
			);


			$sm_GetUser_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
			$wsResult = sm_webServiceCall($sm_GetUser_ws_url,$paramatersForGetUser,"GetUser");


			//Get MAX mailbox size

			$paramatersForReqSettings = array(
			'AuthUserName' => $params["serverusername"],
			'AuthPassword' => $params["serverpassword"],
			'EmailAddress' => $_POST["username"],
			'requestedSettings' => array('maxsize', 'enabledstatus', 'isdomainadmin'),
			);

			$sm_GetRequestedUserSettings_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
			$wsResult2 = sm_webServiceCall($sm_GetRequestedUserSettings_ws_url,$paramatersForReqSettings,"GetRequestedUserSettings");

			//Call to get Domain Aliases
			$sm_GetAliases_ws_url = $smUrl."/Services/svcAliasAdmin.asmx?WSDL";
			$wsGetAliasesResult = sm_webServiceCall($sm_GetAliases_ws_url,$paramaters,"GetAliases");

			//Call to get LstServ address
			$sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
			$wsGetListCommandAddressResult = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters,"GetListCommandAddress");

			//Set alias array for manage smartermail tpl
//Set alias array for manage smartermail tpl

			$smAliasesArrayForLoop = array();
			$smAliasesArray = array();
			if(is_array($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo'])){
				foreach($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo'] as $keyvar =>$userStringValue){
					if(is_array($userStringValue)){
							if ($userStringValue["Addresses"]["string"] == $_POST["username"]){
								array_push($smAliasesArray, $userStringValue["Name"]);
							}
					}else{
						if($userStringValue ==  $_POST["username"]){
							if ($keyvar == "Name"){
								array_push($smAliasesArray, $userStringValue);
							}

						}else{
							if ($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo']["Addresses"]["string"]== $_POST["username"]){
								array_push($smAliasesArray,$wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo']["Name"]);
							}
						}
					}
				}
			}


			//Get Primary Admin to prevent account delete

			$paramatersForGetPrimaryAdmin = array(
			'AuthUserName' => $params["serverusername"],
			'AuthPassword' => $params["serverpassword"],
			'DomainName'=> $_POST["domain"],
			);

			$sm_GetPrimaryDomainAdmin_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
			$wsResult3 = sm_webServiceCall($sm_GetPrimaryDomainAdmin_ws_url,$paramatersForGetPrimaryAdmin,"GetPrimaryDomainAdmin");

			$mailboxStatus = "Enabled";
			switch(ltrim(strrchr(($wsResult2['GetRequestedUserSettingsResult']['settingValues']['string'][1]), '='),'=')){
			case "Enabled":
				$mailboxStatus = 1;
				break;

			case "DisabledWMail":
				$mailboxStatus = 2;
				break;
			case "Disabled":
				$mailboxStatus = 3;
			}


			$pagearray = array(
			'templatefile' => 'editsmartermailuser',
			'breadcrumb' => ' > <a href="#">Edit SmarterMail User</a>',
			'vars' => array(
				'wsGetAliasesResultArray' => $smAliasesArray,
				'FirstName' => $wsResult['GetUserResult']['UserInfo']['FirstName'],
				'isDomainAdmin' => $wsResult['GetUserResult']['UserInfo']['IsDomainAdmin'],
				'LastName' => $wsResult['GetUserResult']['UserInfo']['LastName'],
				'Password' => $wsResult['GetUserResult']['UserInfo']['Password'],
				'UserName' => $wsResult['GetUserResult']['UserInfo']['UserName'],
				'maxMailboxSize' => ltrim(strrchr(($wsResult2['GetRequestedUserSettingsResult']['settingValues']['string'][0]), '='),'='),
				'mailboxStatus' => $mailboxStatus,
				'DomainName'=> $_POST["domain"],
				'IsDedicated' => $params["customfields"]["IsDedicated"],
				'isdomainadmin' => ltrim(strrchr(($wsResult2['GetRequestedUserSettingsResult']['settingValues']['string'][2]), '='),'='),
				'message' => $message,
				'errormessage' => $errormessage,
				'aliastab' => "class='active'",
			),
			);

			return $pagearray;
		}else{
			$errormessage = "Favor preencha todos os campos corretamente.";
			$pagearray = array(
				'templatefile' => 'addsmartermailalias',
				'breadcrumb' => ' > <a href="#">Add SmarterMail User Alias</a>',
				'vars' => array(
				'UserName' => $_POST["username"],
				'DomainName' => $_POST["domain"],
				'message' => $message,
				'errormessage' => $errormessage
				),
			);
			return $pagearray;
		}
	}



}

//called to display editsmartermailaliaspage.tpl, allowing the ability to edit user aliases to the domain.  Function of editing user alias is handled through smartermail_savesmartermailalias
function smartermail_editsmartermailaliaspage($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramaters = array(
	    'AuthUserName' => $params["serverusername"],
	    'AuthPassword' => $params["serverpassword"],
	    'DomainName' => $params["domain"],
	    'AliasName' =>  $_POST['selectalias'],
    );

	if(is_null($_POST['selectalias']))
		return "Don't run API Calls. (try running the FORM instead)";

  $sm_GetAlias_ws_url = $smUrl."/Services/svcAliasAdmin.asmx?WSDL";
  $wsResult = sm_webServiceCall($sm_GetAlias_ws_url,$paramaters,"GetAlias");

  $pagearray = array(
    'templatefile' => 'editsmartermailalias',
    'breadcrumb' => ' > <a href="#">Edit SmarterMail User Alias</a>',
    'vars' => array(
      'name' => $wsResult['GetAliasResult']['AliasInfo']['Name'],
      'addresses' => $wsResult["GetAliasResult"]["AliasInfo"]["Addresses"]['string'],
			'UserName' => $_POST["username"],
    ),
  );
  return $pagearray;
}

//called to delete alias through smartermail web services.  This is initiated from the editsmartermailalias.tpl
function smartermail_deletesmartermailalias($params) {

	$smUrl = addHttpToHostname($params["serverhostname"]);

	$paramatersDeleteAlias = array(
		'AuthUserName' => $params["serverusername"],
		'AuthPassword' => $params["serverpassword"],
		'DomainName' =>  $_POST["domain"],
		'AliasName' =>  $_POST['aliasname'],
	);

	$sm_DeleteAlias_ws_url = $smUrl."/Services/svcAliasAdmin.asmx?WSDL";
	$wsResult2 = sm_webServiceCall($sm_DeleteAlias_ws_url,$paramatersDeleteAlias,"DeleteAlias");

	if($wsResult2['DeleteAliasResult']['Result']){
		$message = "Apelido de usuário removido com sucesso.";
	} else {
		$errormessage =  $wsResult2['DeleteAliasResult']['Message'];
	}

	$paramatersForGetUser = array(
		'AuthUserName' => $params["serverusername"],
		'AuthPassword' => $params["serverpassword"],
		'EmailAddress' => $_POST["username"],
	);

	$paramaters = array(
		'AuthUserName' => $params["serverusername"],
		'AuthPassword' => $params["serverpassword"],
		'DomainName'=> $_POST["domain"]
	);


	$sm_GetUser_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
	$wsResult = sm_webServiceCall($sm_GetUser_ws_url,$paramatersForGetUser,"GetUser");


	//Get MAX mailbox size

	$paramatersForReqSettings = array(
		'AuthUserName' => $params["serverusername"],
		'AuthPassword' => $params["serverpassword"],
		'EmailAddress' => $_POST["username"],
		'requestedSettings' => array('maxsize', 'enabledstatus', 'isdomainadmin'),
	);

	$sm_GetRequestedUserSettings_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
	$wsResult2 = sm_webServiceCall($sm_GetRequestedUserSettings_ws_url,$paramatersForReqSettings,"GetRequestedUserSettings");

	//Call to get Domain Aliases
	$sm_GetAliases_ws_url = $smUrl."/Services/svcAliasAdmin.asmx?WSDL";
	$wsGetAliasesResult = sm_webServiceCall($sm_GetAliases_ws_url,$paramaters,"GetAliases");

	//Call to get LstServ address
	$sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
	$wsGetListCommandAddressResult = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters,"GetListCommandAddress");


	//Set alias array for manage smartermail tpl

	$smAliasesArrayForLoop = array();
	$smAliasesArray = array();
	if(is_array($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo'])){
		foreach($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo'] as $keyvar =>$userStringValue){
			if(is_array($userStringValue)){
					if ($userStringValue["Addresses"]["string"] == $_POST["username"]){
						array_push($smAliasesArray, $userStringValue["Name"]);
					}
			}else{
				if($userStringValue ==  $_POST["username"]){
					if ($keyvar == "Name"){
						array_push($smAliasesArray, $userStringValue);
					}

				}else{
					if ($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo']["Addresses"]["string"]== $_POST["username"]){
						array_push($smAliasesArray,$wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo']["Name"]);
					}
				}
			}
		}
	}


	//Get Primary Admin to prevent account delete

	$paramatersForGetPrimaryAdmin = array(
		'AuthUserName' => $params["serverusername"],
		'AuthPassword' => $params["serverpassword"],
		'DomainName'=> $_POST["domain"],
	);

	$sm_GetPrimaryDomainAdmin_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
	$wsResult3 = sm_webServiceCall($sm_GetPrimaryDomainAdmin_ws_url,$paramatersForGetPrimaryAdmin,"GetPrimaryDomainAdmin");

	$mailboxStatus = "Enabled";
	switch(ltrim(strrchr(($wsResult2['GetRequestedUserSettingsResult']['settingValues']['string'][1]), '='),'=')){
		case "Enabled":
			$mailboxStatus = 1;
			break;

		case "DisabledWMail":
			$mailboxStatus = 2;
			break;
		case "Disabled":
			$mailboxStatus = 3;
	}


	$pagearray = array(
		'templatefile' => 'editsmartermailuser',
		'breadcrumb' => ' > <a href="#">Edit SmarterMail User</a>',
		'vars' => array(
			'wsGetAliasesResultArray' => $smAliasesArray,
			'FirstName' => $wsResult['GetUserResult']['UserInfo']['FirstName'],
			'isDomainAdmin' => $wsResult['GetUserResult']['UserInfo']['IsDomainAdmin'],
			'LastName' => $wsResult['GetUserResult']['UserInfo']['LastName'],
			'Password' => $wsResult['GetUserResult']['UserInfo']['Password'],
			'UserName' => $wsResult['GetUserResult']['UserInfo']['UserName'],
			'maxMailboxSize' => ltrim(strrchr(($wsResult2['GetRequestedUserSettingsResult']['settingValues']['string'][0]), '='),'='),
			'mailboxStatus' => $mailboxStatus,
			'DomainName'=> $_POST["domain"],
			'IsDedicated' => $params["customfields"]["IsDedicated"],
			'isdomainadmin' => ltrim(strrchr(($wsResult2['GetRequestedUserSettingsResult']['settingValues']['string'][2]), '='),'='),
			'message' => $message,
			'errormessage' => $errormessage,
			'aliastab' => "class='active'",
		),
	);

	return $pagearray;
}

///////////////////////////////////// - SM Functions created for common use - /////////////////////////////////////
//  Function to call web service
function sm_webServiceCall($url, $paramaters, $ws_name){
  $soap_client = new SoapClient($url);
  $result = $soap_client->__SoapCall($ws_name, array('paramaters' => $paramaters));
//echo "res: ".print_r(sm_toArray($result));
  return sm_toArray($result);
}

function sm_webServiceCall2($url, $paramaters, $ws_name){
  $soap_client = new SoapClient($url);
  $result = $soap_client->__SoapCall($ws_name, array('paramaters' => $paramaters));
//echo "res: ".print_r($result);
  return sm_toArray($result);
}

//Converts JSON Data Structure from .NET Web Service to Array
function sm_toArray($obj){
  $return=array();
  if (is_array($obj)) {
    $keys=array_keys($obj);
  } elseif (is_object($obj)) {
    $keys=array_keys(get_object_vars($obj));
  } else {return $obj;}
  foreach ($keys as $key) {
    $value=$obj->$key;
    if (is_array($obj)) {
      $return[$key]=sm_toArray($obj[$key]);
    } else {
      $return[$key]=sm_toArray($obj->$key);
    }
  }
  return $return;
}

function addHttpToHostname($url) {
	if(substr($url, 0, 7) != 'http://') {
	  $url = 'http://' . $url . "/";
	}
	return $url;
}

function smartermail_managesmartermaildomain__________($params) {

  $smUrl = addHttpToHostname($params["serverhostname"]);

  $paramaters = array(
    'AuthUserName' => $params["serverusername"],
    'AuthPassword' => $params["serverpassword"],
  );


  //Call to get LstServ address
  $sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
  $wsGetListCommandAddressResultListsNames = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters,"GetAllDomains");
  $smAliasesArrayForLoop = array();
  $smAliasesArray = array();
  $IsDedicated = $params["customfields"]["IsDedicated"];


  if ($IsDedicated == 'on'){
    if (is_array($wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string'])) {
			foreach($wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string'] as $key){
				$total = 0;
				$paramaters2 = array(
					'AuthUserName' => $params["serverusername"],
					'AuthPassword' => $params["serverpassword"],
					'DomainName'=> $key
				);

				$sm_GetUsers_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
				$wsGetUsersResult = sm_webServiceCall($sm_GetUsers_ws_url, $paramaters2,"GetUsers");

				if (count($wsGetUsersResult['GetUsersResult']['Users']['UserInfo']) != 6){
					$total = $total + count($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]);
				} else {
					if (strpos($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]['UserName'], '@') !== false) {
					  $total = $total + 1;
					} else {
					  $total = $total + count($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]);
					}
				}

				$param = array(
					'AuthUserName' => $params["serverusername"],
					'AuthPassword' => $params["serverpassword"],
					'domainName'=> $key
				);

				$sm_GetAliases_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
				$wsGetDomainSettingsResult = sm_webServiceCall($sm_GetAliases_ws_url, $param,"GetDomainSettings");

				if($wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainUsers'] == 0)
				$wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainUsers'] = "∞";
				$domainslist = array(
					'domain' => $key,
					'contas' => $total,
					'total' => $wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainUsers'],
				);
				if($key != ""){
					array_push($smAliasesArray, $domainslist);
				}

			}
    } else {
			$total = 0;
			$paramaters2 = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'DomainName'=> $wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string']
			);
			$sm_GetUsers_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
			$wsGetUsersResult = sm_webServiceCall($sm_GetUsers_ws_url, $paramaters2,"GetUsers");

			if (count($wsGetUsersResult['GetUsersResult']['Users']['UserInfo']) != 6){
				$total = $total + count($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]);
			} else {
				if (strpos($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]['UserName'], '@') !== false) {
					$total = $total + 1;
				} else {
					$total = $total + count($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]);
				}
			}
			$param = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'domainName'=> $wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string']
			);

			$sm_GetAliases_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
			$wsGetDomainSettingsResult = sm_webServiceCall($sm_GetAliases_ws_url, $param,"GetDomainSettings");
			if($wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainUsers'] == 0)
				$wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainUsers'] = "∞";

			$domainslist = array(
				'domain' => $wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string'],
				'contas' => $total,
				'total' => $wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainUsers'],
			);
			if($wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string'] != ""){
				array_push($smAliasesArray, $domainslist);
			}
		}
  } else {
	  $domainslist = array(
	    'domain' => $params["domain"],
	  );
	  if($params["domain"] != ""){
	    array_push($smAliasesArray, $domainslist);
	  }
  }

  $pagearray = array(
    'templatefile' => 'managesmartermaildomain',
    'breadcrumb' => ' > <a href="#">Manage SmarterMail Domain</a>',
    'vars' => array(
      'wsGetAliasesResultArray' => $smAliasesArray,
      'domainName' => $_POST["domain"],
      'IsDedicated' => $params["customfields"]["IsDedicated"]
    ),
  );
  return $pagearray;
}

//called to display addsmartermailuser.tpl, allowing the ability to add new users to the domain.  Function of adding user is handled through smartermail_createsmartermailuser
function smartermail_addsmartermaildomain($params) {
    $pagearray = array(
        'templatefile' => 'addsmartermaildomain',
        'breadcrumb' => ' > <a href="#">Add SmarterMail Domain</a>',
    );
    return $pagearray;
}

//used to create user through smartermail web service.  Information is pSigned through the POST action from the addsmartermailuser.tpl
function smartermail_createsmartermaildomain($params) {



	if ($_POST['domain'] == ""){
		$params["newdomainname"] = $_POST['domain'];
		$create = smartermail_CreateDomain($params);
		//echo'<pre>';print_r($create);echo'</pre>';
		if ($create['sucess']) {
			$message=$create['message'];
		} else {
			$errormessage=$create['message'];

		}
		//$errormessage='ERRO ->>> dominio '.$_POST['newdomainname'].' nao foi criado!!!';
	} else {
		$message="";
		$errormessage = "Favor preencha todos os campos corretamente.";
		$pagearray = array(
			'templatefile' => 'addsmartermaildomain',
			'breadcrumb' => ' > <a href="#">Add SmarterMail Domain</a>',
			'vars' => array(
				'message' => $message,
				'errormessage' => $errormessage,
			)
		);
		return $pagearray;
	}




  $sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
	$wsGetListCommandAddressResultListsNames = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters,"GetAllDomains");

	$smAliasesArrayForLoop = array();
	$smAliasesArray = array();
    $IsDedicated = $params["customfields"]["IsDedicated"];






	if ($IsDedicated == 'on'){
		if (is_array($wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string'])) {
			foreach($wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string'] as $key){
				$total = 0;
				$paramaters2 = array(
					'AuthUserName' => $params["serverusername"],
					'AuthPassword' => $params["serverpassword"],
					'DomainName'=> $key
				);

				$sm_GetUsers_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
				$wsGetUsersResult = sm_webServiceCall($sm_GetUsers_ws_url, $paramaters2,"GetUsers");

				if (count($wsGetUsersResult['GetUsersResult']['Users']['UserInfo']) != 6){
					$total = $total + count($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]);
				}else {
					if (strpos($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]['UserName'], '@') !== false) {
						$total = $total + 1;
					} else {
						$total = $total + count($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]);
					}
				}

				$param = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'domainName'=> $key
				);

				$sm_GetAliases_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
				$wsGetDomainSettingsResult = sm_webServiceCall($sm_GetAliases_ws_url, $param,"GetDomainSettings");

				if($wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainUsers'] == 0)
				$wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainUsers'] = "∞";
				$domainslist = array(
					'domain' => $key,
					'contas' => $total,
					'total' => $wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainUsers'],
				);
				if($key != ""){
					array_push($smAliasesArray, $domainslist);
				}

			}
		}else{
			$total = 0;
			$paramaters2 = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'DomainName'=> $wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string']
			);
			$sm_GetUsers_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
			$wsGetUsersResult = sm_webServiceCall($sm_GetUsers_ws_url, $paramaters2,"GetUsers");

			if (count($wsGetUsersResult['GetUsersResult']['Users']['UserInfo']) != 6){
				$total = $total + count($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]);
			}else {
				if (strpos($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]['UserName'], '@') !== false) {
					$total = $total + 1;
				} else {
					$total = $total + count($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]);
				}
			}
			$param = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'domainName'=> $wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string']
				);

			$sm_GetAliases_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
			$wsGetDomainSettingsResult = sm_webServiceCall($sm_GetAliases_ws_url, $param,"GetDomainSettings");

			$domainslist = array(
				'domain' => $wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string'],
				'contas' => $total,
				'total' => $wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainUsers'],
			);
			if($wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string'] != ""){
				array_push($smAliasesArray, $domainslist);
			}
		}
	}else{
		$domainslist = array(
			'domain' => $params["domain"],
		);
		if($params["domain"] != ""){
			array_push($smAliasesArray, $domainslist);
		}

	}

    $pagearray = array(
        'templatefile' => 'managesmartermaildomain',
        'breadcrumb' => ' > <a href="#">Manage SmarterMail Domain</a>',
        'vars' => array(
            'wsGetAliasesResultArray' => $smAliasesArray,
            'domainName' => $_POST["domain"],
			'IsDedicated' => $params["customfields"]["IsDedicated"]
        ),
    );
    return $pagearray;
}


function smartermail_editsmartermaildomainpage($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);

   //Gerenciar Usuários e Lista
    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName'=> $_POST['domain']
    );


    $sm_getDomainUsers_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
    $wsGetDomainUsersResult = sm_webServiceCall($sm_getDomainUsers_ws_url,$paramaters,"GetUsers");

	$sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
	$wsGetListCommandAddressResultListsNames = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters,"GetMailingListsByDomain");

    $smAliasesArrayForLoop = array();
    $smAliasesArray = array();


	if(is_array($wsGetListCommandAddressResultListsNames['GetMailingListsByDomainResult']['listNames']['string'] )){
		foreach($wsGetListCommandAddressResultListsNames['GetMailingListsByDomainResult']['listNames']['string'] as $key => $value)
		{


			array_push($smAliasesArray,$value);
		}
	}else{
		array_push($smAliasesArray, $wsGetListCommandAddressResultListsNames['GetMailingListsByDomainResult']['listNames']['string']);
	}

//setup user array to be displayed in ManageSmarterMail Template.  Checking to see if listServ name is showing or if they are an alias which shows in a separate section.
    $smUsersArray = array();
			if(is_array($wsGetDomainUsersResult['GetUsersResult']['Users']['UserInfo'])){

;
				foreach($wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo'] as $keyvar =>$userStringValue){

					if(is_array($userStringValue)){
						array_push($smUsersArray, $userStringValue["UserName"]);
					}else{
						if ($keyvar == "UserName"){
							array_push($smUsersArray, $userStringValue);
						}
					}
				}
			}


    $pagearray = array(
        'templatefile' => 'managesmartermail',
        'breadcrumb' => ' > <a href="#">Manage SmarterMail</a>',
        'vars' => array(
            'wsGetDomainUsersResultArray' => $smUsersArray,
            'DomainName' => $_POST['domain'],
			'wsGetListCommandAddressResultListsNames' => $smAliasesArray,
			'wsGetAliasesResult' => $wsGetAliasesResult,
			'domainid' => $_POST['domain'],
			'IsDedicated' => $params["customfields"]["IsDedicated"],

        ),
    );

    return $pagearray;
	//End Gerenciar Users e Lista
}
//used to edit user through smartermail web service.  Information is pSigned through the POST action from the editsmartermailuserpage.tpl

function smartermail_deletesmartermaildomain($params) {
    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName' => $_POST["domain"],
		'DeleteFiles' => true
    );

	$sm_getDomainUsers_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
    $wsGetDomainUsersResult = sm_webServiceCall($sm_getDomainUsers_ws_url,$paramaters,"GetUsers");



	$smUsersArray = array();
	if(is_array($wsGetDomainUsersResult['GetUsersResult']['Users']['UserInfo'])){
		foreach($wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo'] as $keyvar =>$userStringValue){
			if(is_array($userStringValue)){
				array_push($smUsersArray, $userStringValue["UserName"]);
			}else{
				if ($keyvar == "UserName"){
					array_push($smUsersArray, $userStringValue);
				}
			}
		}
	}

		foreach($smUsersArray as $user){
			$paramaters2 = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'Username' => str_replace("@".$user, "", $user),
				'DomainName' => $_POST["domain"],
			);

			if(is_null($user))
				return "Don't run API Calls. (try running the FORM instead)";

			$sm_DeleteUser_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
			$wsResult2 = sm_webServiceCall($sm_DeleteUser_ws_url,$paramaters2,"DeleteUser");
		}
		$sm_DeleteUser_ws_url2 = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
		$wsResult = sm_webServiceCall($sm_DeleteUser_ws_url2,$paramaters,"DeleteDomain");



	//Manage Domain
	$paramatersDomain = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
    );

    $sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
	$wsGetListCommandAddressResultListsNames = sm_webServiceCall($sm_GetListCommandAddress_ws_url, $paramatersDomain,"GetAllDomains");


    $IsDedicated = $params["customfields"]["IsDedicated"];
	$smAliasesArray = array();
		if ($IsDedicated == 'on'){
		if (is_array($wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string'])) {
			foreach($wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string'] as $key){
				$total = 0;
				$paramaters2 = array(
					'AuthUserName' => $params["serverusername"],
					'AuthPassword' => $params["serverpassword"],
					'DomainName'=> $key
				);

				$sm_GetUsers_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
				$wsGetUsersResult = sm_webServiceCall($sm_GetUsers_ws_url, $paramaters2,"GetUsers");

				if (count($wsGetUsersResult['GetUsersResult']['Users']['UserInfo']) != 6){
					$total = $total + count($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]);
				}else {
					if (strpos($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]['UserName'], '@') !== false) {
						$total = $total + 1;
					} else {
						$total = $total + count($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]);
					}
				}

				$param = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'domainName'=> $key
				);

				$sm_GetAliases_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
				$wsGetDomainSettingsResult = sm_webServiceCall($sm_GetAliases_ws_url, $param,"GetDomainSettings");

				if($wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainUsers'] == 0)
				$wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainUsers'] = "∞";
				$domainslist = array(
					'domain' => $key,
					'contas' => $total,
					'total' => $wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainUsers'],
				);
				if($key != ""){
					array_push($smAliasesArray, $domainslist);
				}

			}
		}else{
			$total = 0;
			$paramaters2 = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'DomainName'=> $wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string']
			);
			$sm_GetUsers_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
			$wsGetUsersResult = sm_webServiceCall($sm_GetUsers_ws_url, $paramaters2,"GetUsers");

			if (count($wsGetUsersResult['GetUsersResult']['Users']['UserInfo']) != 6){
				$total = $total + count($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]);
			}else {
				if (strpos($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]['UserName'], '@') !== false) {
					$total = $total + 1;
				} else {
					$total = $total + count($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]);
				}
			}
			$param = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'domainName'=> $wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string']
				);

			$sm_GetAliases_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
			$wsGetDomainSettingsResult = sm_webServiceCall($sm_GetAliases_ws_url, $param,"GetDomainSettings");

			$domainslist = array(
				'domain' => $wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string'],
				'contas' => $total,
				'total' => $wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainUsers'],
			);
			if($wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string'] != ""){
				array_push($smAliasesArray, $domainslist);
			}
		}
	}else{
		$domainslist = array(
			'domain' => $params["domain"],
		);
		if($params["domain"] != ""){
			array_push($smAliasesArray, $domainslist);
		}

	}

	if($wsResult['DeleteDomainResult']['Result']){
		$message = "Domínio removido com sucesso.";
	} else {
		$errormessage = $wsResult['DeleteDomainResult']['Message'];
	}

	$pagearray = array(
        'templatefile' => 'managesmartermaildomain',
        'breadcrumb' => ' > <a href="#">Manage SmarterMail Domain</a>',
        'vars' => array(
            'wsGetAliasesResultArray' => $smAliasesArray,
            'domainName' => $_POST["domain"],
			'IsDedicated' => $params["customfields"]["IsDedicated"],
			'message' => $message,
			'errormessage' => $errormessage,
        ),
    );
	//End Manage Domain

    return $pagearray;
}


//called to display editsmartermailaliaspage.tpl, allowing the ability to edit user aliases to the domain.  Function of editing user alias is handled through smartermail_savesmartermailalias

//called to display addsmartermailuser.tpl, allowing the ability to add new users to the domain.  Function of adding user is handled through smartermail_createsmartermailuser
//function smartermail_addsmartermailusernew($params) {
     //$pagearray = array(
       // 'templatefile' => 'addsmartermailusernew',
       // 'breadcrumb' => ' > <a href="#">Add SmarterMail User</a>',
       // 'vars' => array(
          //  'DomainName' =>  $_POST["domain"],
      //  ),
   // );
   // return $pagearray;
//}
//function smartermail_managesmartermailmailinglistsSign($params) {
    //$smUrl = addHttpToHostname($params["serverhostname"]);

    //$paramaters = array(
        //'AuthUserName' => $params["serverusername"],
        //'AuthPassword' => $params["serverpassword"],
       // 'DomainName'=> $params["domain"],
	   // 'ListName' =>  $_POST['listname'],
   // );


    //Call to get LstServ address
   // $sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
    //$wsGetListCommandAddressResult = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters,"GetListCommandAddress");
	//$wsGetListCommandAddressResultListsNames = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters,"GetSubscriberList");

//Set alias array for manage smartermail tpl

    //$smAliasesArrayForLoop = array();
    //$smAliasesArray = array();
	//if (is_array($wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string'])) {
	//foreach($wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string'] as $key => $value)
	//{
	//	array_push($smAliasesArray,$value);
	//}
	//}else{
		//if($wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string'] != null){
			///array_push($smAliasesArray,$wsGetListCommandAddressResultListsNames['GetSubscriberListResult']['Subscribers']['string']);
		//}
	//}

	//if($wsResult['AddListResult']['Result']){
		//$message = "Lista de e-mail adicionada com sucesso.";
	//} else {
		//$errormessage = $wsResult['AddListResult']['Message'];
	//}

    ///$pagearray = array(
      // 'templatefile' => 'managesmartermailmailinglistsSign',
      //  'breadcrumb' => ' > <a href="#">Manage SmarterMail mailinglistsSign</a>',
      // 'vars' => array(
            //'wsGetAliasesResultArray' => $smAliasesArray,
           // 'DomainName' => $params["domain"],
			//'ListName' =>  $_POST['listname'],
			//'message' => $message,
			//'errormessage' => $errormessage
       /// ),
    //);
    //return $pagearray;
//}
//function smartermail_savechangessmartermailalias($params) {
   // $smUrl = addHttpToHostname($params["serverhostname"]);

  //  $paramaters = array(
   //     'AuthUserName' => $params["serverusername"],
   //     'AuthPassword' => $params["serverpassword"],
    //    'DomainName' => $params["domain"],
    //    'AliasName' =>  $_POST['aliasname'],
    //    'Addresses' => explode("\r\n", $_POST['aliasemailaddress']),
	//	'UserName' => $_POST["username"],
    //);


  //  $sm_UpdateAlias_ws_url = $smUrl."/Services/svcAliasAdmin.asmx?WSDL";
  //  $wsResult = sm_webServiceCall($sm_UpdateAlias_ws_url,$paramaters,"UpdateAlias");
	//	if($wsResult['UpdateAliasResult']['Result']){
	//	   $message = "Apelido de usuário atualizado com sucesso.";
	//	} else {
	//		$errormessage = $wsResult['UpdateAliasResult']['Message'];
	//	}


		//	$pagearray = array(
			//	'templatefile' => 'managesmartermail',
			//	'breadcrumb' => ' > <a href="#">Manage SmarterMail</a>',
			//	'vars' => array(
				//	'wsGetDomainUsersResultArray' => $smUsersArray,
					///'DomainName' => $params["domain"],
					//'wsGetListCommandAddressResultListsNames' => $smAliasesArray,
				///	'wsGetAliasesResult' => $wsGetAliasesResult,
					//'domainid' => $params["domain"],
					//'IsDedicated' => $params["customfields"]["IsDedicated"],
					//'message' => $message,
					///'errormessage' => $errormessage
				//),
			//);

			///return $pagearray;
//}

?>
