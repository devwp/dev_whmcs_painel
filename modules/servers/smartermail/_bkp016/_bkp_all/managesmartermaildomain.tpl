

				{if $message != ""}
					<div class="alert alert-success text-center" id="alertModuleCustomButtonSuccess">
						{$message}
					</div>
					<br clear="both" />
				{/if}
				{if $errormessage != ""}
					<div class="alert alert-danger text-center" id="alertModuleCustomButtonFailed">
						{$errormessage}
					</div>
					<br clear="both" />
				{/if}

				<div class="row">
				<div class="col-md-12">
				<ul class="nav nav-tabs">
				  <li><a href="clientarea.php?action=productdetails&id={$id}">{$LANG.yourdetails} </a></li>
				  <!--<li><a data-toggle="tab" href="#tabDownloads">{$LANG.downloadsfiles}</a></li>
					<li><a data-toggle="tab" href="#tabChangepw">{$LANG.serverchangepassword}</a></li>-->
				{if $IsDedicated == 'on'}
				  <li><a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains">{$LANG.smmanageDomain}</a></li>
				{/if}
				  <li class="active"><a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermailuserdomain">{$LANG.smmanageusers}{if $IsDedicated == 'on'} ({$DomainName}){/if}	</a></li>
				{if $IsDedicated != 'on'}
				  <li><a href="upgrade.php?type=package&id={$id}">{$LANG.upgradedowngradepackage}</a></li>
				{/if}
				</ul>
				</div>
				</div>

				<link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.bootstrap.css">
				<link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.responsive.css">
				<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/jquery.dataTables.min.js"></script>
				<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.bootstrap.min.js"></script>
				<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.responsive.min.js"></script>
								<script src="/assets/js/bootstrap-tabdrop.js"></script>

				{if $IsDedicated == 'on'}
				<div style="float:left; width: 123px;">
					<a menuitemname="Custom Module Button Gerenciar Domínio" style="background: #337ab7 !important; color:#ffffff !important" href="clientarea.php?action=productdetails&amp;id={$id}&amp;modop=custom&amp;a=managesmartermaildomain" class="list-group-item btn btn-primary" id="Primary_Sidebar-Service_Details_Actions-Custom_Module_Button_Gerenciar_Domínio"> {$LANG.smglobalback}</a>
				</div><br clear="both" />
				{else}
				<!--div style="float:left; width: 123px;">
					<a menuitemname="Custom Module Button Gerenciar Domínio" style="background: #337ab7 !important; color:#ffffff !important" href="clientarea.php?action=productdetails&amp;id={$id}" class="list-group-item btn btn-primary" id="Primary_Sidebar-Service_Details_Actions-Custom_Module_Button_Gerenciar_Domínio"> {$LANG.smglobalback}</a>
				</div><br clear="both" /-->
				{/if}
					<script type="text/javascript">
					    jQuery(document).ready(function () {
					        var table = jQuery("#tableMailList").DataTable({
					            "dom": '<"listtable"fit>pl', "responsive": true,
					            "oLanguage": {
					                "sEmptyTable": "{$LANG.smglobalNothing}",
					                "sInfo": "{$LANG.smmanageusers}, {$totalUsers} de {$maxUsers}",
					                "sInfoEmpty": "{$LANG.smmanageusers}, {$totalUsers} de {$maxUsers}",
					                "sInfoFiltered": "({$LANG.smglobalshowing} _MAX_ total)",
					                "sInfoPostFix": "",
					                "sInfoThousands": ",",
					                "sLengthMenu": "{$LANG.smglobalshow} _MENU_",
					                "sLoadingRecords": "{$LANG.smgloballoading}",
					                "sProcessing": "{$LANG.smglobalProcess}",
					                "sSearch": "",
					                "sZeroRecords": "{$LANG.smglobalNothing}",
					                "oPaginate": {
					                    "sFirst": "",
					                    "sLast": "",
					                    "sNext": "{$LANG.smglobalNext}",
					                    "sPrevious": "{$LANG.smglobalPrev}"
					                }
					            },
					            "pageLength": 10,
					            "columnDefs": [{
					                "targets": 'no-sort',
					                "orderable": false,
					            }],
					            "order": [
	                                [0, "asc"]
					            ],
					            "stateSave": true
					        });
					        jQuery('#tableMailList').removeClass('hidden').DataTable();
					        table.draw();
					        jQuery("#tableMailList_filter input").attr("placeholder", "{$LANG.smglobalsearch}");
					    });
							</script>

					<script type="text/javascript">
					    jQuery('.nav-tabs').tabdrop();

					</script>
				<div class="tabbable">
				{if ($IsDedicated == "on")}
				
					<div class="row clearfix">
						<div class="col-xs-12">
								<ul class="nav nav-tabs">
									<li class="active"><a data-toggle="tab" href="#domain2">{$LANG.smmanageusers}</a></li>
									<li><a data-toggle="tab" href="#resourceusage2">{$LANG.smmanagemailinglist}</a></li>
								</ul>
								
						</div>
					</div>

				{/if}
				</div>
				{debug}
				<div class="tab-content">
				<div id="domain2" class="tab-pane active">
					<div class="row">
					    <div class="col-md-12">
					    <div class="panel panel-default">
						<div class="panel-heading" >
							<h3 class="panel-title" >{$LANG.smmanageusers}</h3>
						</div>
						<div class="panel-body">
						<ul class="nav nav-pills">
							{if $canCreateUser}
							<li><button type="button"  class="btn btn-primary" data-toggle="modal" data-target="#myModalAdd" ><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;{$LANG.smaddusers}</button></li>
							{/if}
						</ul>
						<div class="tab-content">
							<div><br clear="both"/>
								<table id="tableMailList" class="table table-bordered table-hover table-list" >
									<thead>
										<tr>
											<th>{$LANG.smusersname }</th>
											<th>{$LANG.contactemail}</th>
											<th>{$LANG.clientareaused}</th>
											<th class="no-sort"></th>
											<th class="no-sort"></th>
										</tr>
									</thead>
									<tbody>
										{foreach $wsGetDomainUsersResultArray as $key => $val }
											<tr>
												<td>{$val.Name} {$val.LastName}</td>
												<td><span style="display:none;">{$val.Email}</span>
													<!--form method="post" action="clientarea.php?action=productdetails&id={$id}">
													<input type="hidden" name="id" value="{$id}" />
													<input type="hidden" name="modop" value="custom" />
													<input type="hidden" name="domain" value="{$DomainName}" />
													<input type="hidden" name="a" value="editsmartermailuserpage" />
													<input type="hidden" name="selectuser" value="{$val.Email}" />
													<input type="submit" value="{$val.Email}" style="border: 0; background: none; color:#3598DB; font-weight:bold;" />
												</form-->
													{$val.Email}
												</td>
												<td  data-order="{$val.PercentUsed}">{$val.PercentUsed}% de {$val.MaxSpace} GB</td>
												<td style="width:150px;" data-order="{$val.PercentUsed}" >
													<div class="progress" style="margin-bottom: 0px;">
														<div class="progress-bar" role="progressbar" aria-valuenow="{$val.PercentUsed}" aria-valuemin="0" aria-valuemax="100" style="width:{$val.PercentUsed}% ">
															<span class="sr-only">{$val.PercentUsed}%</span>
														</div>
													</div>
												</td>
												<td style="width:45px;" data-order="{$val.PercentUsed}" >
													<div>
														<form method="post">
															<input type="hidden" name="username" value="{$val.User}"/>
															<input type="hidden" name="managed" value="deluser"/>
															<a href="#" title="{$LANG.smeditusers}" class="editmodal" data-toggle="modal" data-target="#myModalEdit" data-user="{$val.User}" data-name="{$val.Name} {$val.LastName}" ><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;&nbsp;
															<a href="#" onclick="if(confirm('Esta ação irá apagar todos os emails nesta caixa de entrada.\nDeseja continuar?')){ parentNode.submit(); } else { return false; }" title="{$LANG.smglobalDelete} {$val.Email}"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
													</form>

													</div>
												</td>

											</tr>
										{/foreach}
									</tbody>
								</table>
							</div>
						</div>
					</div>
								<br clear="both"/><br clear="both"/>


								<!--div class="panel-group"-->
								<div class="panel panel-default">
								  <div class="panel-heading">
									<a data-toggle="collapse" href="#collapse1">
												<h4 class="panel-title">
										{$LANG.cartconfigurationoptions}&nbsp;&nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i>
										</h4>
											</a>
								  </div>
								  <div id="collapse1" class="panel-collapse collapse">
									<div class="panel-body">
												<fieldset><legend>{$LANG.smmanageAlias}</legend>
													<form method="post" class="form-horizontal using-password-strength" >
														<ul class="list-group">
															{foreach $DomainEliases as $key => $val}
															<li class="list-group-item"><button type"submit" class="bnt btn-link" name="deldomainalias" value="{$val}"  onclick="if(confirm('Esta ação irá apagar este apelido.\nDeseja continuar?')){ parentNode.submit(); } else { return false; }" ><i class="fa fa-trash-o" aria-hidden="true"></i></button> {$val} </li>
															{/foreach}
														</ul>
													</form>

													<form method="post" class="form-horizontal using-password-strength" >
														<div class="form-group">
															<label class="col-sm-3 control-label" for="newdomainalias">{$LANG.smAlias}</label>
															<div class="col-sm-6 input-group">
																<input  class="form-control" type="text" name="newdomainalias"/>
															</div>
														</div>
														<div class="text-center">
															<button type="submit" name="managed" value="adddomainalias">{$LANG.smaddAlias}</buton>
														</div>
													</form>
												</fieldset>
											</div>
									<div class="panel-footer"></div>
								  </div>



							</div>
						</div>
					</div>
				    </div>
	            </div>
				<div id="resourceusage2" class="tab-pane fade">

	                                <div class="row">
	                                    <div class="col-md-12">
	                                        <div class="panel panel-default">
	                                            <div class="panel-heading">
	                                                <h3 class="panel-title">{$LANG.smmanageuserslist}</h3>
	                                            </div>
	                                            <div class="panel-body">
	                                                <div class="tab-content">
	                                                    <br clear="both" />
	                                                    {if $message != ""}
											    <div class="alert alert-success text-center" id="Div1">
	                                                {$message}
	                                            </div>
	                                                    {/if} 
										    {if $errormessage != ""}
											    <div class="alert alert-danger text-center" id="Div2">
	                                                {$errormessage}
	                                            </div>
	                                                    {/if}<br clear="both" />
	                                                    <link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.bootstrap.css">
	                                                    <link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.responsive.css">
	                                                    <script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/jquery.dataTables.min.js"></script>
	                                                    <script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.bootstrap.min.js"></script>
	                                                    <script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.responsive.min.js"></script>
	                                                    <div style="float: left; width: 123px;">
	                                                       <!--     <form method="post" action="clientarea.php?action=productdetails">
	                                                            <input type="hidden" name="id" value="{$id}" />
	                                                            <input type="submit" value="{$LANG.smglobalback}" class="btn btn-primary" />
	                                                        </form>
	                                                    </div>
	                                                    <div style="float: right; width: 184px;">
	                                                     <form method="post" action="clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermailmailinglists">
																//<input type="hidden" name="modop" value="custom" />
																<input type="hidden" name="a" value="addsmartermailmailinglists" />
																<input type="hidden" name="domain" value="{$DomainName}" /> 

	                                                            <input type="submit" value="{$LANG.smaddmailinglist}" class="btn btn-primary"   data-toggle="modal" data-target="#myModalAddList" />
	                                                        </form>-->
	                                                     <a href="#" title="{$LANG.smaddmailinglist}" class="btn btn-primary"  data-toggle="modal" data-target="#myModalAddList">{$LANG.smaddmailinglist}</a>
	                                                    </div>
	                                                    {if $warnings}
	                                                        {include file="$template/includes/alert.tpl" type="warning" msg=$warnings textcenter=true}
	                                                    {/if}
										
										    <div class="tab-content">
	                                            <div class="tab-pane fade in active" id="tabOverview">
	                                                <script type="text/javascript">
	                                                    jQuery(document).ready(function () {
	                                                        var table = jQuery("#tableDomainsList2").DataTable({
	                                                            "dom": '<"listtable"fit>pl', "responsive": true,
	                                                            "oLanguage": {
	                                                                "sEmptyTable": "{$LANG.smglobalNothing}",
	                                                                "sInfo": "{$LANG.smmanagemailinglist}",
	                                                                "sInfoEmpty": "{$LANG.smmanagemailinglist}",
	                                                                "sInfoFiltered": "({$LANG.smglobalshowing} _MAX_ total)",
	                                                                "sInfoPostFix": "",
	                                                                "sInfoThousands": ",",
	                                                                "sLengthMenu": "{$LANG.smglobalshow} _MENU_",
	                                                                "sLoadingRecords": "{$LANG.smgloballoading}",
	                                                                "sProcessing": "{$LANG.smglobalProcess}",
	                                                                "sSearch": "",
	                                                                "sZeroRecords": "{$LANG.smglobalNothing}",
	                                                                "oPaginate": {
	                                                                    "sFirst": "",
	                                                                    "sLast": "",
	                                                                    "sNext": "{$LANG.smglobalNext}",
	                                                                    "sPrevious": "{$LANG.smglobalPrev}"
	                                                                }
	                                                            },
	                                                            "pageLength": 10,
	                                                            "order": [
	                                                                [0, "asc"]
	                                                            ],
	                                                            "lengthMenu": [
	                                                                [10, 25, 50, -1],
	                                                                [10, 25, 50, "All"]
	                                                            ], "stateSave": true
	                                                        });
	                                                        jQuery('#tableDomainsList2').removeClass('hidden').DataTable();
	                                                        table.draw();
	                                                        jQuery("#tableDomainsList2_filter input").attr("placeholder", "{$LANG.smglobalsearch}");
	                                                    });
	                                                </script>
	                                                </br>
									        	    <div class="table-container clearfix">
	                                                    </br>
	                                                    <table id="tableDomainsList2" class="table table-list hidden">
	                                                        <thead>
	                                                            <tr>
	                                                                <th>{$LANG.smmailinglistsname}</th>
	                                                            </tr>
	                                                        </thead>
	                                                        <tbody>
	                                                            {foreach $wsGetDomainUsersListResultArray as $domain}	 
														    <tr>
	                                                            <td>
	                                                                <form method="post" action="clientarea.php?action=productdetails">
	                                                                    <input type="hidden" name="id" value="{$id}" />
	                                                                    <input type="hidden" name="modop" value="custom" />
	                                                                    <input type="hidden" name="listname" value="{$domain}"/>
	                                                                    <input type="hidden" name="managea" value="editList" />
																		
	                                                                   <a href="#" title="{$LANG.smeditusers}" class="editmodal" data-toggle="modal" data-target="#myModalEditList?managea=editList&listname={$domain}" data-user="{$val.User}" data-name="{$val.Name} {$val.LastName}" ><i class="fa fa-pencil" aria-hidden="true"></i></a>
	                                                                </form>
	                                                            </td>
	                                                        </tr>
	                                                            {/foreach}
	                                                        </tbody>
	                                                    </table>
	                                                </div>
	                                            </div>
	                                        </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	              </div>
				</div>
				<!-- Modal -->
				<script>
				    $(document).on("click", ".editmodal", function () {
				        //var myBookId = $(this).data('user');
				        //alert($(this).data('user'));
				        $("#myModalEdit .modal-body #username").val($(this).data('user'));
				        $("#myModalEdit .modal-body #editusername").val($(this).data('user'));
				        $("#myModalEdit .modal-body #editfirstname").val($(this).data('name'));
				        $("#myModalEdit .modal-header .modal-title.title1").text('{$LANG.smeditusers} ' + $(this).data('user') + '@{$DomainName}');
				        // As pointed out in comments,
				        // it is superfluous to have to manually call the modal.
				        // $('#addBookDialog').modal('show');
				    });
				</script>
				<div id="myModalEdit" class="modal large fade" role="dialog">
				  <div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title title1">{$LANG.smeditusers}</h4>
					  </div>
					  <div class="modal-body">
								<!--div class="panel-heading" >
									<h3 class="panel-title" >{$LANG.smaddusers}</h3>
								</div-->
								<div class="panel-body">
									<fieldset>
									<form method="post" class="form-horizontal using-password-strength" >
										<!--h3>{$LANG.smaddusers}</h3-->
										<div class="form-group">
											<label for="username" class="col-sm-3 control-label">{$LANG.smusersemail}</label>
											<div class="col-sm-6  input-group">
															<input type="text" disabled="disabled" name="username" id="username" style="" class="form-control"/><div  class="input-group-addon">@{$DomainName}</div>
											</div>
										</div>
											<div class="form-group">
												<label for="editfirstname" class="col-sm-3 control-label">{$LANG.smusersname}</label>
												<div class="col-sm-6 input-group">
													<input type="text" name="editfirstname" id="editfirstname" class="form-control"/>
												</div>
											</div>
										<div class="form-group">
									<label for="editpassword" class="col-sm-3 control-label">{$LANG.smuserspass}</label>
									<div class="col-sm-6 input-group">
												<input type="text" name="editpassword" id="editpassword" style="" class="form-control"/>
									</div>
									</div>

										<div class="form-group">
											<div class="text-center">
												{*ifdedicated*}
												<input type="hidden" name="editdomain" value="{$DomainName}" />
												{*ifdedicated*}
												<input type="hidden" name="editusername" id="editusername" value="" />
												<!--input type="hidden" name="id" value="{$id}" />
												<input type="hidden" name="modop" value="custom" />
												<input type="hidden" name="a" value="createsmartermailuser" /-->
												<input type="hidden" name="managee" value="edituser" />
												<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;&nbsp;{$LANG.smglobalsave}</buton>
											</div>
										</div>
									</form>
									</fieldset>
								</div>


										<fieldset><legend>Opções Avançadas</legend>
											<div class="col-md-12">

											<ul class="nav nav-tabs">
												<li class="active"><a data-toggle="tab" href="#home">Customizar</a></li>
												<li><a data-toggle="tab" href="#menu1">Apelidos</a></li>
												<li><a data-toggle="tab" href="#menu2">Redirecionamento</a></li>
												<li><a data-toggle="tab" href="#menu3">Resposta Automática</a></li>
											</ul>
											<div class="tab-content">
											  <div id="home" class="tab-pane fade in active">
												<h3>HOME</h3>
												<p>Some content.</p>
											  </div>
											  <div id="menu1" class="tab-pane fade">
												<h3>Menu 1</h3>
												<p>Some content in menu 1.</p>
											  </div>
												<div id="menu2" class="tab-pane fade">
												<h3>Menu 2</h3>
												<p>Some content in menu 2.</p>
											  </div>
												<div id="menu3" class="tab-pane fade">
												<h3>Menu 3</h3>
												<p>Some content in menu 3.</p>
											  </div>
											</div>
											<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />



											</div>
										</fieldset>
							</div>

					  <div class="modal-footer">
								<div class="text-right">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
					  </div>
					</div>

				  </div>
				</div>









				{if $canCreateUser}


				<div id="myModalConfig" class="modal fade" role="dialog">
				  <div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">{$LANG.smaddusers}</h4>
					  </div>
					  <div class="modal-body">
									<div class="panel-body">


											<fieldset>
									<form method="post" class="form-horizontal using-password-strength" >
										<!--h3>{$LANG.smaddusers}</h3-->
										<div class="form-group">
											<label for="newusername" class="col-sm-3 control-label">{$LANG.smusersemail}</label>
											<div class="col-sm-6  input-group">
															<input type="text" name="newusername" id="newusername" style="" class="form-control"/><div  class="input-group-addon">@{$DomainName}</div>
											</div>
										</div>
											<div class="form-group">
												<label for="newfirstname" class="col-sm-3 control-label">{$LANG.smusersname}</label>
												<div class="col-sm-6 input-group">
													<input type="text" name="newfirstname" id="newfirstname" class="form-control"/>
												</div>
											</div>
										<div class="form-group">
									<label for="newpassword" class="col-sm-3 control-label">{$LANG.smuserspass}</label>
									<div class="col-sm-6 input-group">
												<input type="text" name="newpassword" id="newpassword" style="" class="form-control"/>
									</div>
									</div>

										<div class="form-group">
											<div class="text-center">
												{*ifdedicated*}
												<input type="hidden" name="adddomain" value="{$DomainName}" />
												{*ifdedicated*}
												<!--input type="hidden" name="id" value="{$id}" />
												<input type="hidden" name="modop" value="custom" />
												<input type="hidden" name="a" value="createsmartermailuser" /-->
												<input type="hidden" name="managea" value="adduser" />
												<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;&nbsp;{$LANG.smaddusers}</buton>
											</div>
										</div>
									</form>
							</div>




					  </div>
					  <div class="modal-footer">
								<div class="text-right">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
					  </div>
					</div>

				  </div>
				</div>
				{/if}
				<div id="myModalAdd" class="modal fade" role="dialog">
				  <div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">{$LANG.smaddusers}</h4>
					  </div>
					  <div class="modal-body">
											<div class="panel-body">
											<form method="post" class="form-horizontal using-password-strength" >
												<!--h3>{$LANG.smaddusers}</h3-->
												<div class="form-group">
													<label for="newusername" class="col-sm-3 control-label">{$LANG.smusersemail}</label>
													<div class="col-sm-6  input-group">
																	<input type="text" name="newusername" id="Text1" style="" class="form-control"/><div  class="input-group-addon">@{$DomainName}</div>
													</div>
												</div>
													<div class="form-group">
														<label for="newfirstname" class="col-sm-3 control-label">{$LANG.smusersname}</label>
														<div class="col-sm-6 input-group">
															<input type="text" name="newfirstname" id="Text2" class="form-control"/>
														</div>
													</div>
												<div class="form-group">
											<label for="newpassword" class="col-sm-3 control-label">{$LANG.smuserspass}</label>
											<div class="col-sm-6 input-group">
												<input type="text" name="newpassword" id="Text3" style="" class="form-control"/>
											</div>
											</div>

												<div class="form-group">
													<div class="text-center">
														<input type="hidden" name="adddomain" value="{$DomainName}" />
														<input type="hidden" name="managea" value="adduser" />
														<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;&nbsp;{$LANG.smaddusers}</buton>
													</div>
												</div>
											</form>
									</div>




					  </div>
					  <div class="modal-footer">
								<div class="text-right">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
					  </div>
					</div>

				  </div>
				</div>



				<div id="myModalAddList" class="modal fade" role="dialog">
				  <div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">{$LANG.smaddusers}</h4>
					  </div>
					  <div class="modal-body">
											<div class="panel-body">
											<form method="post" class="form-horizontal using-password-strength" >
												<div class="form-group">
												<label for="newusername" class="col-sm-3 control-label">{$LANG.smmailinglistsname}</label>
													<div class="col-sm-6  input-group">
														 <input type="text" name= "newlistname"  style="width: 100%;"  class="form-control" />
													</div>
												</div>
												<!--h3>{$LANG.smaddusers}</h3-->
												<div class="form-group">
													<label for="newusername" class="col-sm-3 control-label">{$LANG.smmailinglistsModerator}</label>
													<div class="col-sm-6  input-group">
														<select  name="newmoderator" class="form-control" >
															{foreach $wsGetDomainUsersResultArray as $domain}
																<option value="{$domain.Email}">{$domain.Email}</option>
															{/foreach} 
														</select> 
													</div>
												</div>
													<div class="form-group">
														<label for="newfirstname" class="col-sm-3 control-label">{$LANG.smmailinglistssize}</label>
														<div class="col-sm-6 input-group">
															<select name="maxmessagesize" class="form-control">
															<option value="2048" selected>2MB</option> 
															<option value="5120" >5MB</option>
															<option value="10240">10MB</option> 
															</select>
														</div>
													</div>
												<div class="form-group">
											<label for="newpassword" class="col-sm-3 control-label">{$LANG.smmailinglistsperm}</label>
											<div class="col-sm-6 input-group">
												<select name="whocanpost" class="form-control">
													<option value="Moderator">{$LANG.smmailinglistsmod}</option> 
													<option value="Subscribers">{$LANG.smmailinglistsass}</option> 
													<option value="anyone">{$LANG.smmailinglistsother}</option>  
												</select>
											</div>
											</div>

												<div class="form-group">
													<div class="text-center">
														<input type="hidden" name="domain" value="{$DomainName}" />
														<input type="hidden" name="modop" value="custom" />
																<input type="hidden" name="managea" value="addList" /> 
													
														<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;&nbsp;{$LANG.smaddusers}</buton>
													</div>
												</div>
											</form>
									</div>




					  </div>
					  <div class="modal-footer">
								<div class="text-right">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
					  </div>
					</div>

				  </div>
				</div>

    <div id="myModalEditList" class="modal fade" role="dialog">
				  <div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">{$LANG.smaddusers}</h4>
						</div>
				 <div class="modal-body">
	{if $message != ""}
		<div class="alert alert-success text-center" id="alertModuleCustomButtonSuccess">
			{$message}
		</div>
	{/if}
	{if $errormessage != ""}
		<div class="alert alert-danger text-center" id="alertModuleCustomButtonFailed">
			{$errormessage}
		</div>
	{/if}<br clear="both" /> 

	<div style="float:left; width: 123px;">
		<form method="post" action="clientarea.php?action=productdetails">
			<input type="hidden" name="id" value="{$id}" />
			<input type="hidden" name="modop" value="custom" />
			<input type="hidden" name="domain" value="{$DomainName}">
			<input type="hidden" name="a" value="editsmartermaildomainpage" />

			<select style="width: 0px; height: 0; border:0; line-height: 0" size="0" name="selectalias">
			<option value="{$DomainName}">{$DomainName}</option>
			</select>
			<input type="submit" value="{$LANG.smglobalback}" class="btn btn-primary" style="width:123px" />
		</form>
	<br clear="both" />
	<form method="post" id="deleteForm2" action="clientarea.php?action=productdetails" style="float: left;width:150px;">
	<input type="hidden" name="listname" value="{$ListName}" />
	<input type="hidden" name="domain" value="{$DomainName}" />
	<input type="hidden" name="id" value="{$id}" />
	<input type="hidden" name="modop" value="custom" />
	<input type="hidden" name="a" value="deletesmartermailmailinglists" />
	</form>
	</div>

	<div class="row clearfix">
		<div class="col-xs-12">
			<ul class="nav nav-tabs">
				<li {$lista}>
					<a href="#domain" data-toggle="tab">{$LANG.smeditmailinglist}</a>
				</li>
				<li {$assinante}> 
					<a href="#resourceusage" data-toggle="tab">{$LANG.smmanagemailinglistASS}</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="tab-content product-details-tab-container">
	<div class="tab-pane {if ($lista == "class='active'")}active{/if}"  id="domain">
	<form method="post" action="clientarea.php?action=productdetails" class="form-horizontal using-password-strength">
	<h3>Editar lista para {$DomainName}</h3>
	<div class="form-group">
		<label for="inputalias" class="col-sm-5 control-label">{$LANG.smmailinglistsname}</label>
		<div class="col-sm-6" style="text-align: left;line-height: 35px;">
		{$ListName}	
		</div>
	</div>
	<div class="form-group">
		<label for="inputalias" class="col-sm-5 control-label">{$LANG.smmailinglistsModerator}</label>
		<div class="col-sm-6">
		  <select  name="newmoderator" class="form-control" >
			   {foreach $wsGetDomainUsersResultArray as $domain}
				 <option value="{$domain.Email}" {if ($Moderator == {$domain.Email})}selected{/if}>{$domain.Email}</option>
			   {/foreach} </select> 
		  
		</div>
	</div>

	<div class="form-group">
	<label for="inputalias" class="col-sm-5 control-label">{$LANG.smmailinglistssize}</label>
		<div class="col-sm-6">
			<select name="maxmessagesize" class="form-control">
				<option value="{$maxmessagesize}" {if ($maxmessagesizeMB == "2MB")}selected{/if} >{$maxmessagesizeMB}</option> 
				<option value="5120" {if ($maxmessagesizeMB == "5MB")}selected{/if}>5MB</option>
				<option value="10240" {if ($maxmessagesizeMB == "10MB")}selected{/if}>10MB</option>
			</select>
		</div>
	</div>
	<div class="form-group">
	<label for="inputalias" class="col-sm-5 control-label">{$LANG.smmailinglistsperm}</label>
	<div class="col-sm-6">
	<select name="whocanpost" class="form-control"> 
	<option value="Moderator" {if ($whocanpostname == "Moderador")}selected{/if} >{$LANG.smmailinglistsmod}</option> 
	<option value="Subscribers" {if ($whocanpostname == "Assinante")}selected{/if} >{$LANG.smmailinglistsass}</option> 
	<option value="anyone" {if ($whocanpostname == "Qualquer um")}selected{/if} >{$LANG.smmailinglistsother}</option>   
	</select> 
	</div>
	</div> 
	<br clear="both" />
	<div class="form-group">
	<div class="text-center">
	<input type="hidden" name="newlistname" value="{$ListName}" />
	<input type="hidden" name="listname" value="{$ListName}" />
	<input type="hidden" name="domain" value="{$DomainName}" />
	<input type="hidden" name="id" value="{$id}" />
	<input type="hidden" name="modop" value="custom" />
	<input type="hidden" name="a" value="savechangessmartermailmailinglists" />
	<input type="submit" value="{$LANG.smglobalreflesh}" class="btn btn-primary" /> 
	<input  type="button" id="deleteButtom" value="{$LANG.smglobalDelete}" class="btn btn-default" onClick="document.getElementById('deleteForm2').submit()" />
	</div>
	</div>	
	</form><br clear="both" />
	</div>

	<div class="tab-pane {if ($assinante == "class='active'")}active{/if}" id="resourceusage">
	<link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.responsive.css">
	<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/jquery.dataTables.min.js"></script>
	<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.responsive.min.js"></script>
	<div style="float:right; width: 155px;">
	    <form method="post" action="clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermailmailinglistsASS"> 
	        <input type="hidden" name="modop" value="custom" />
			<input type="hidden" name="listname" value="{$ListName}" />
			<input type="hidden" name="domain" value="{$DomainName}" />
	        <input type="hidden" name="a" value="addsmartermailmailinglistsSign" />
	        <input type="submit" value="{$LANG.smaddmailinglistASS}" class="btn btn-primary" />  
	    </form>
	</div>
	{if $warnings}
	    {include file="$template/includes/alert.tpl" type="warning" msg=$warnings textcenter=true}
	{/if}
	<div class="tab-content">
	    <div class="tab-pane fade in active" id="tabOverview">
	             <script type="text/javascript">
	            jQuery(document).ready(function () {
	                var table = jQuery("#tableDomainsList").DataTable({
	                    "dom": '<"listtable"fit>pl', "responsive": true,
	                         "oLanguage": {
	                        "sEmptyTable": "{$LANG.smglobalNothing}",
							"sInfo": "{$LANG.smmanagemailinglistASS}",
	                        "sInfoEmpty": "{$LANG.smmanagemailinglistASS}",
	                        "sInfoFiltered": "{$LANG.smmanagemailinglistASS}",
	                        "sInfoPostFix": "",
	                        "sInfoThousands": ",",
	                        "sLengthMenu": "{$LANG.smglobalshow} _MENU_",
	                        "sLoadingRecords": "{$LANG.smgloballoading}",
	                        "sProcessing": "{$LANG.smglobalProcess}",
	                        "sSearch": "",
	                        "sZeroRecords": "{$LANG.smglobalNothing}",
	                        "oPaginate": {
	                            "sFirst": "",
	                            "sLast": "",
	                            "sNext": "{$LANG.smglobalNext}",
	                            "sPrevious": "{$LANG.smglobalPrev}"
	                        }
	                    },
	                    "pageLength": 10,
	                    "order": [
	                        [0, "asc"]
	                    ],
	                    "lengthMenu": [
	                        [10, 25, 50, -1],
	                        [10, 25, 50, "All"]
	                    ], "stateSave": true
	                });
	                jQuery('#tableDomainsList').removeClass('hidden').DataTable();
	                table.draw();
					jQuery("#tableDomainsList_filter input").attr("placeholder", "{$LANG.smglobalsearch}");

	            });
	        </script>
	</br>
	     
	        <div class="table-container clearfix" ></br>
	            <table id="tableDomainsList" class="table table-list hidden">
	                <thead>
	                    <tr>

	                        <th>{$LANG.orderdomain}</th>
							<th></th>
	                    </tr>
	                </thead> 
	                <tbody>
	                    {foreach $wsGetAliasesResultArray as $domain}	 
	                    <tr>
	                        <td>
								<p style="font-size: 16px;line-height: 1.42857143; color: #333333;">{$domain}</p>
	                        </td>
							<td>
								<form method="post" id="deleteForm" action="clientarea.php?action=productdetails" style="float: right;width:80px;">
									<input type="hidden" name="Subscriber" value="{$domain}" />
									<input type="hidden" name="listname" value="{$ListName}" />
									<input type="hidden" name="id" value="{$id}" />
									<input type="hidden" name="modop" value="custom" />
									<input type="hidden" name="domain" value="{$DomainName}">
									<input type="hidden" name= "newlistname" value="{$ListName}" style="width: 100%;"  class="form-control" />
									<input type="hidden" name="a" value="deletesmartermailmailinglistsSign" />
									<input type="submit" value="Excluir" class="btn btn-primary" style="border:0;font-size: 16px;line-height: 1.42857143;color: #333333 !important; background:transparent; padding:0;">
								</form>
							</td>
	                    </tr>
	                </tbody>
	                {/foreach}
	            </table>
	            
	        </div>
	    </div> 
	</div>

	</div>
	</div>



					  </div>
					  <div class="modal-footer">
								<div class="text-right">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
					  </div>
					</div>

				  </div>
				</div>