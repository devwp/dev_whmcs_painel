<link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.responsive.css">
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.responsive.min.js"></script>
<div style="float:left; width: 123px;">
    <form method="post" action="clientarea.php?action=productdetails">
        <input type="hidden" name="id" value="{$id}" />
		<input type="submit" value="{$LANG.smglobalback}" class="btn btn-primary"/>
    </form>
</div>
<div style="float:right; width: 123px;">
    <form method="post" action="clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermailalias"> 
		<input type="hidden" name="username" value="{$UserName}">
        <input type="hidden" name="modop" value="custom" />
        <input type="hidden" name="a" value="addsmartermailalias" />
        <input type="submit" value="{$LANG.smaddAlias}" class="btn btn-primary" />    
    </form>
</div>

{if $warnings}
    {include file="$template/includes/alert.tpl" type="warning" msg=$warnings textcenter=true}
{/if}
<div class="tab-content">
    <div class="tab-pane fade in active" id="tabOverview">
             <script type="text/javascript">
            jQuery(document).ready(function () {
                var table = jQuery("#tableDomainsList").DataTable({
                    "dom": '<"listtable"fit>pl', "responsive": true,
                         "oLanguage": {
                        "sEmptyTable": "{$LANG.smglobalNothing}",
                        "sInfo": "{$LANG.smmanageAlias} | {$UserName}", 
                        "sInfoFiltered": "({$LANG.smglobalshowing} _MAX_ total)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ",",
                        "sLengthMenu": "{$LANG.smglobalshow} _MENU_",
                        "sLoadingRecords": "{$LANG.smgloballoading}",
                        "sProcessing": "{$LANG.smglobalProcess}",
                        "sSearch": "",
                        "sZeroRecords": "{$LANG.smglobalNothing}",
                        "oPaginate": {
                            "sFirst": "",
                            "sLast": "",
                            "sNext": "{$LANG.smglobalNext}",
                            "sPrevious": "{$LANG.smglobalPrev}"
                        }
                    },
                    "pageLength": 10,
                    "order": [
                        [0, "asc"]
                    ],
                    "lengthMenu": [
                        [10, 25, 50, -1],
                        [10, 25, 50, "All"]
                    ], "stateSave": true
                });
                jQuery('#tableDomainsList').removeClass('hidden').DataTable();
                table.draw();
				jQuery("#tableDomainsList_filter input").attr("placeholder", "{$LANG.smglobalsearch}");

            });
        </script>
</br>
     
        <div class="table-container clearfix" ></br>
            <table id="tableDomainsList" class="table table-list hidden">
                <thead>
                    <tr>

                        <th>{$LANG.orderdomain}</th>
						<th></th>
                    </tr>
                </thead> 
                <tbody>
                    {foreach $wsGetAliasesResultArray as $domain}	 
                    <tr>
                        <td>
                            <form method="post" action="clientarea.php?action=productdetails">
                                <input type="hidden" name="id" value="{$id}" />
                                <input type="hidden" name="modop" value="custom" />
								<input type="hidden" name="username" value="{$UserName}">
                                <input type="hidden" name="a" value="editsmartermailaliaspage" />
                                <input type="submit" value="{$domain}" style="border: 0; background: none;" />
                                <select style="width: 0px; height: 0; border:0; line-height: 0" size="0" name="selectalias">
                                    <option value="{$domain}">{$domain}</option>
                                </select>
                            </form>
                        </td>
						<td>
							<form method="post" id="deleteForm" action="clientarea.php?action=productdetails" style="float: right;width:80px;">
							<input type="hidden" name="aliasname" value="{$domain}" />
							<input type="hidden" name="id" value="{$id}" />
							<input type="hidden" name="modop" value="custom" />
							<input type="hidden" name="a" value="deletesmartermailalias" />
							<input type="submit" value="Excluir" class="btn btn-primary" style="border:0;font-size: 14px;line-height: 1.42857143;color: #333333 !important; background:transparent; padding:0;">
							</form>
						</td>
                    </tr>
                </tbody>
                {/foreach}
            </table>
            
        </div>
    </div>
</div>
