

	{if $message != ""}
	<div class="alert alert-success text-center" id="alertModuleCustomButtonSuccess">
		{$message}
	</div>
	<br clear="both" />
{/if}
{if $errormessage != ""}
	<div class="alert alert-danger text-center" id="alertModuleCustomButtonFailed">
		{$errormessage}
	</div>
	<br clear="both" />
{/if}

<div class="row">
<div class="col-md-12">
<ul class="nav nav-tabs">

  <li><a href="clientarea.php?action=productdetails&id={$id}">{$LANG.yourdetails} </a></li>
  <!--<li><a data-toggle="tab" href="#tabDownloads">{$LANG.downloadsfiles}</a></li>
	<li><a data-toggle="tab" href="#tabChangepw">{$LANG.serverchangepassword}</a></li>-->
{if $IsDedicated == 'on'}
  <li><a href="">{$LANG.smmanageDomain}</a></li>
{/if}
  <li class="active"><a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermailuserdomain">{$LANG.smmanageusers }</a></li>
  <li><a href="upgrade.php?type=package&id={$id}">{$LANG.upgradedowngradepackage}</a></li>
</ul>
</div>
</div>

<link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.responsive.css">
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.responsive.min.js"></script>

{if $IsDedicated == 'on'}
<div style="float:left; width: 123px;">
	<a menuitemname="Custom Module Button Gerenciar Domínio" style="background: #337ab7 !important; color:#ffffff !important" href="clientarea.php?action=productdetails&amp;id={$id}&amp;modop=custom&amp;a=managesmartermaildomain" class="list-group-item btn btn-primary" id="Primary_Sidebar-Service_Details_Actions-Custom_Module_Button_Gerenciar_Domínio"> {$LANG.smglobalback}</a>
</div><br clear="both" />
{else}
<!--div style="float:left; width: 123px;">
	<a menuitemname="Custom Module Button Gerenciar Domínio" style="background: #337ab7 !important; color:#ffffff !important" href="clientarea.php?action=productdetails&amp;id={$id}" class="list-group-item btn btn-primary" id="Primary_Sidebar-Service_Details_Actions-Custom_Module_Button_Gerenciar_Domínio"> {$LANG.smglobalback}</a>
</div><br clear="both" /-->
{/if}
	<script type="text/javascript">
  			jQuery(document).ready(function () {
					var table = jQuery("#tableMailList").DataTable({
						"dom": '<"listtable"fit>pl', "responsive": true,
							"oLanguage": {
							"sEmptyTable": "{$LANG.smglobalNothing}",
							"sInfo": "{$LANG.smmanageusers}, {$totalUsers} de {$maxUsers}",
							"sInfoEmpty": "{$LANG.smmanageusers}, {$totalUsers} de {$maxUsers}",
							"sInfoFiltered": "({$LANG.smglobalshowing} _MAX_ total)",
							"sInfoPostFix": "",
							"sInfoThousands": ",",
							"sLengthMenu": "{$LANG.smglobalshow} _MENU_",
							"sLoadingRecords": "{$LANG.smgloballoading}",
							"sProcessing": "{$LANG.smglobalProcess}",
							"sSearch": "",
							"sZeroRecords": "{$LANG.smglobalNothing}",
							"oPaginate": {
								"sFirst": "",
								"sLast": "",
								"sNext": "{$LANG.smglobalNext}",
								"sPrevious": "{$LANG.smglobalPrev}"
							}
						},
						"pageLength": 10,
						"columnDefs": [ {
		          "targets": 'no-sort',
		          "orderable": false,
				    } ],
						"order": [
							[0, "asc"]
						],
						 "stateSave": true
					});
					jQuery('#tableMailList').removeClass('hidden').DataTable();
					table.draw();
					jQuery("#tableMailList_filter input").attr("placeholder", "{$LANG.smglobalsearch}");
				});
			</script>

	<script src="/assets/js/bootstrap-tabdrop.js"></script>
	<script type="text/javascript">
	jQuery('.nav-tabs').tabdrop();
	</script>
{if ($IsDedicated == "on")}
	<div class="tabbable">
	<div class="row clearfix">
		<div class="col-xs-12">
			<ul class="nav nav-tabs">
				<li {if ($lista == '')}class="active"{/if}>
					<a href="#domain" data-toggle="tab">{$LANG.smmanageusers}</a>
				</li>

				<li {$lista}>
					<a href="#resourceusage" data-toggle="tab">{$LANG.smmanagemailinglist}</a>
				</li>

			</ul>
		</div>
	</div>
{/if}

{debug}
<div class="row">
<div class="col-md-12">
	<div class="panel panel-default">
	<div class="panel-heading" >
		<h3 class="panel-title" >{$LANG.smmanageusers}</h3>
	</div>
	<div class="panel-body">
		<div style="float:left; width: 180px;">
			{if $canCreateUser}
			<a href="#addsmartermailuser"  class="editmodal btn btn-primary" data-toggle="modal" data-target="#myModalAdd" ><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;{$LANG.smaddusers}</a>
			<!--a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=addsmartermailuser"  class="btn btn-primary"><i class="fa fa-plus-circle" aria-hidden="true"></i> {$LANG.smaddusers}</a-->
			<!--a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=addsmartermailuser"  class="btn btn-primary"><i class="fa fa-plus-circle" aria-hidden="true"></i> {$LANG.smaddusers}</a-->
			<!--form method="post" action="clientarea.php?action=productdetails&id={$id}">
				<input type="hidden" name="modop" value="custom" />
				<input type="hidden" name="a" value="addsmartermailuser" />
				<input type="hidden" name="domain" value="{$DomainName}" />
				<button type="submit" class="btn btn-primary"><i class="fa fa-plus-circle" aria-hidden="true"></i> {$LANG.smaddusers}</button>
			</form-->
			{/if}
		</div>
		<div class="tab-content">
		    <div><br clear="both"/>

	            <table id="tableMailList" class="table table-bordered table-hover table-list" >
	                <thead>
										<tr>
												<th>{$LANG.smusersname }</th>
												<th>{$LANG.contactemail}</th>
												<th>{$LANG.clientareaused}</th>
												<th class="no-sort"></th>
												<th class="no-sort"></th>
										</tr>
	                </thead>
	                <tbody>
						{foreach $wsGetDomainUsersResultArray as $key => $val }
							<tr>
								<td>{$val.Name} {$val.LastName}</td>
								<td><span style="display:none;">{$val.Email}</span>
									<!--form method="post" action="clientarea.php?action=productdetails&id={$id}">
									<input type="hidden" name="id" value="{$id}" />
									<input type="hidden" name="modop" value="custom" />
									<input type="hidden" name="domain" value="{$DomainName}" />
									<input type="hidden" name="a" value="editsmartermailuserpage" />
									<input type="hidden" name="selectuser" value="{$val.Email}" />
									<input type="submit" value="{$val.Email}" style="border: 0; background: none; color:#3598DB; font-weight:bold;" />
								</form-->
									{$val.Email}
								</td>
								<td  data-order="{$val.PercentUsed}">{$val.PercentUsed}% de {$val.MaxSpace} GB</td>
								<td style="width:150px;" data-order="{$val.PercentUsed}" >
									<div class="progress" style="margin-bottom: 0px;">
										<div class="progress-bar" role="progressbar" aria-valuenow="{$val.PercentUsed}" aria-valuemin="0" aria-valuemax="100" style="width:{$val.PercentUsed}% ">
											<span class="sr-only">{$val.PercentUsed}%</span>
										</div>
									</div>
								</td>
								<td style="width:45px;" data-order="{$val.PercentUsed}" >
									<div>
										<form method="post">
											<input type="hidden" name="username" value="{$val.User}"/>
											<input type="hidden" name="managed" value="deluser"/>
											<a href="#" title="{$LANG.smeditusers}" class="editmodal" data-toggle="modal" data-target="#myModal" data-user="{$val.User}" data-name="{$val.Name} {$val.LastName}" ><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;&nbsp;
											<a href="#" onclick="if(confirm('Esta ação irá apagar todos os emails nesta caixa de entrada.\nDeseja continuar?')){ parentNode.submit(); } else { return false; }" title="{$LANG.smglobalDelete} {$val.Email}"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
									</form>

									</div>
								</td>

							</tr>
						{/foreach}
					</tbody>
	            </table>
						</div>
					</div>
				</div>



				<br clear="both"/><br clear="both"/>

			</div>
		</div>
	</div>
	</div>
</div>
</div>
</div>

<!-- Modal -->
<script>
$(document).on("click", ".editmodal", function () {
     //var myBookId = $(this).data('user');
		 //alert($(this).data('user'));
     $(".modal-body #username").val( $(this).data('user') );
     $(".modal-body #editusername").val( $(this).data('user') );
     $(".modal-body #editfirstname").val( $(this).data('name') );
     // As pointed out in comments,
     // it is superfluous to have to manually call the modal.
     // $('#addBookDialog').modal('show');
});
</script>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{$LANG.smeditusers}</h4>
      </div>
      <div class="modal-body">



											<!--div class="panel-heading" >
												<h3 class="panel-title" >{$LANG.smaddusers}</h3>
											</div-->
											<div class="panel-body">


													<fieldset>
											<form method="post" class="form-horizontal using-password-strength" >
											    <!--h3>{$LANG.smaddusers}</h3-->
											    <div class="form-group">
											        <label for="username" class="col-sm-3 control-label">{$LANG.smusersemail}</label>
											        <div class="col-sm-6  input-group">
																	<input type="text" disabled="disabled" name="username" id="username" style="" class="form-control"/><div  class="input-group-addon">@{$DomainName}</div>
											        </div>
											    </div>
													<div class="form-group">
														<label for="editfirstname" class="col-sm-3 control-label">{$LANG.smusersname}</label>
														<div class="col-sm-6 input-group">
															<input type="text" name="editfirstname" id="editfirstname" class="form-control"/>
														</div>
													</div>
												<div class="form-group">
									        <label for="editpassword" class="col-sm-3 control-label">{$LANG.smuserspass}</label>
									        <div class="col-sm-6 input-group">
														<input type="text" name="editpassword" id="editpassword" style="" class="form-control"/>
									        </div>
										    </div>

											    <div class="form-group">
											        <div class="text-center">
														{*ifdedicated*}
														<input type="hidden" name="editdomain" value="{$DomainName}" />
														{*ifdedicated*}
														<input type="hidden" name="editusername" id="editusername" value="" />
														<!--input type="hidden" name="id" value="{$id}" />
														<input type="hidden" name="modop" value="custom" />
														<input type="hidden" name="a" value="createsmartermailuser" /-->
														<input type="hidden" name="managee" value="edituser" />
														<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;&nbsp;{$LANG.smglobalsave}</buton>
											        </div>
											    </div>
											</form>
									</div>




      </div>
      <div class="modal-footer">
				<div class="text-right">
        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
      </div>
    </div>

  </div>
</div>









{if $canCreateUser}
<div id="myModalAdd" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{$LANG.smaddusers}</h4>
      </div>
      <div class="modal-body">
											<div class="panel-body">


													<fieldset>
											<form method="post" class="form-horizontal using-password-strength" >
											    <!--h3>{$LANG.smaddusers}</h3-->
											    <div class="form-group">
											        <label for="newusername" class="col-sm-3 control-label">{$LANG.smusersemail}</label>
											        <div class="col-sm-6  input-group">
																	<input type="text" name="newusername" id="newusername" style="" class="form-control"/><div  class="input-group-addon">@{$DomainName}</div>
											        </div>
											    </div>
													<div class="form-group">
														<label for="newfirstname" class="col-sm-3 control-label">{$LANG.smusersname}</label>
														<div class="col-sm-6 input-group">
															<input type="text" name="newfirstname" id="newfirstname" class="form-control"/>
														</div>
													</div>
												<div class="form-group">
									        <label for="newpassword" class="col-sm-3 control-label">{$LANG.smuserspass}</label>
									        <div class="col-sm-6 input-group">
														<input type="text" name="newpassword" id="newpassword" style="" class="form-control"/>
									        </div>
										    </div>

											    <div class="form-group">
											        <div class="text-center">
														{*ifdedicated*}
														<input type="hidden" name="adddomain" value="{$DomainName}" />
														{*ifdedicated*}
														<!--input type="hidden" name="id" value="{$id}" />
														<input type="hidden" name="modop" value="custom" />
														<input type="hidden" name="a" value="createsmartermailuser" /-->
														<input type="hidden" name="managea" value="adduser" />
														<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;&nbsp;{$LANG.smaddusers}</buton>
											        </div>
											    </div>
											</form>
									</div>




      </div>
      <div class="modal-footer">
				<div class="text-right">
        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
      </div>
    </div>

  </div>
</div>
{/if}
