﻿Customizable WHMCS SmarterMail Server Provisioning Module

* Package Description *
The WHMCS SmarterMail module is an open source module developed in PHP that integrates SmarterMail as a Product\Service into WHMCS.  This allows the ability to create packages based off of custom settings available within the SmarterMail software that can then be used by administrators to offer varying levels of SmarterMail access to end users. For example, the ability to create packages based off of mailbox counts, disk space allocations, etc.  Additionally in the Client Area of WHMCS, the ability to manage SmarterMail has been added as well for end users. 

* Package Goals *
The primary goal of the WHMCS server module is to give server administrators more flexibility in regards to how SmarterMail is offered as a service.  With the use of this SmarterMail Server module, Product\Service items can be created specific to the SmarterMail settings.  This means that Parallels users can create custom packages, unique to users across the Parallels install versus standard packages that are applied to all users. These customizable features include but not limited to: specifying the number of domain users, domain size limits, number of aliases, etc.


The package also provides the following services

Admin Area Features
 - Create Domain
 - Suspend Domain
 - Unsuspend Domain
 - Terminate Domain
 - Change Package
 - Change Password

Client Area Features
 - Manage Mailboxes
 -  - Add Mailboxes
 -  - Delete Mailboxes
 -  - Modify mailbox settings
 -  - Change mailbox password
 - Manage User Aliases
 -  - Add Aliases
 -  - Delete Aliases
 - Change Password


* Prerequisites *
 - Existing installation of WHMCS (version 5.0 and above)
 - Existing installation of WHM/CPanel
 - Licensed installation of SmarterMail


*Installing the SmarterMail Server Module *
First the package will need to be installed in the WHMCS Control Panel.  To do so, use the following instructions:

1.) download ZIP package
2.) navigate to ../modules/servers/ and create a folder called ‘smartermail’ all lower case
3.) extract and upload contents of zipped SmarterMail folder into that created folder

* Configure WHMCS to Access and Group Smartermail Servers *
Once the SmarterMail package has been added, you now have the ability to configure the SmarterMail servers to be used.  To configure the SmarterMail servers to be used:

1.) Login to WHMCS as a system administrator
2.) Click Setup → Products\Services → Servers
3.) Click add new server across the top
4.) Enter Server information for the SmarterMail server to be added
5.) Under Server Details, select the Type of ‘smartermail’ and enter system administrator credentials
6.) save changes
7.) Once the server has been added, you then have the ability to create a group of SmarterMail servers.  Click Create New Group across the top
8.) Enter a name for the group of SmarterMail servers, select and add your server that was recently added
9.) Save changes


* Configure Product\Service *
Now that the package has been added and the SmarterMail servers have been configured, you now have the ability to configure individual Product\Service packages within WHMCS.  To do so:

1.) Click Setup → Products\Services → Products\Services
2.) You have the option of creating a new product group if you see fit, otherwise click Create a new product
3.) Select product type, product group, and give product name 
4.) Click continue
5.) Edit the various product tabs as you see fit such as details and pricing
6.) For the Module Settings Tab - Select Smartermail as the Module Name, and select the appropriate Server Group that was created earlier
7.) This will then allow you to configure data specific to the packages being created.

* WHMCS Client Area *
The WHMCS SmarterMail module also allows WHMCS users to interact with their SmarterMail domain.  Abilities include actions such as adding and editing both users as well as user aliases.  To access this feature as a WHMCS end user:

- Client Area
To access SmarterMail through the client area:

1.) Click My Services → Product details
2.) Click Manage SmarterMail Users and Aliases
3.) You will find the option to add both users as well as aliases.  Also the option to edit each of the corresponding items

- Add User
To add a user:

1.) Click Add SmarterMail User
2.) Enter requested information
3.) Click Create SmarterMail User

- Edit User
To edit a user:

1.) From the Manage SmarterMail Users page, select the user to edit, and click Edit SmarterMail User
2.) Change or update any of the specified fields for each user.
3.) Also note the ability to change the status of an account or delete an account.  NOTE: deleting the account is permanent. 

- Add Alias
To add an alias:

1.) Click Add SmarterMail Alias
2.) Enter name for alias
3.) Enter accounts the alias will send too
4.) Click Create SmarterMail Alias

- Edit Alias
To edit an alias:

1.) From the Manage SmarterMail Aliases page, select the alias to edit, and click Edit SmarterMail Alias
2.) Add or remove any email addresses that you’d like
3.) Also note the ability to delete an alias.  NOTE: deleting the alias is permanent.
