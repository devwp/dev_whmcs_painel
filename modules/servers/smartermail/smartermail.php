<?php function smartermail_ClientArea($params) {

	if($params["customfields"]["IsDedicated"]=='on'){

		/// LISTAR DOMINIOS
		$wsGetDomainsArray = array();
		$wsGetDomains = smartermail_API('svcDomainAdmin.asmx?WSDL', $params, 'GetAllDomains');
		foreach ($wsGetDomains['GetAllDomainsResult']['DomainNames']['string'] as $value) {
			$smUrl = addHttpToHostname($params["serverhostname"]);
			$sm_UpdateUser2_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
			$paramaters = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'DomainName' =>  $value
			); 
			$wsGetinfo = sm_webServiceCall(	$sm_UpdateUsFdeeer2_ws_url, $paramaters, 'GetDomainCounts');
			$wsTotalDomains += $wsGetinfo["GetDomainCountsResult"]['Users'];
		}
		$pagearray = array(
			'templatefile' => 'clientarea',
			'breadcrumb' => ' > <a href="#">Manage SmarterMail</a>',
			'vars' => array(
				'totalDomains' => $wsTotalDomains,
				'maxDomains' => $params['configoption18'],
				'canCreateDomain' => ($wsTotalDomains>=$params['configoption18'])?false:true,
				'wsGetDomainsResultArray' => $wsGetDomainsArray,
				'IsDedicated' => $params["customfields"]["IsDedicated"],
				'params2' => var_export($params,1),
			),
		);
	}else{
		$wsGetDomainUsersResult = smartermail_API('svcUserAdmin.asmx?WSDL', $params, 'GetUsers');
		$totalUsers=0;
		if(is_array($wsGetDomainUsersResult['GetUsersResult']['Users']['UserInfo'])){
			foreach($wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo'] as $keyvar =>$userStringValue){
				if(is_array($userStringValue)){
					if(substr($userStringValue["UserName"],0,strrpos($userStringValue["UserName"], '@')) == 'postmaster') continue;
					$totalUsers++;
				}else{
					if ($keyvar == "UserName"){
						if(substr($userStringValue,0,strrpos($userStringValue, '@')) == 'postmaster') continue;
						$totalUsers++;
					}
				}
			}
		}
		////////////// END GET LIST
		$pagearray = array(
			'templatefile' => 'clientarea',
			'breadcrumb' => ' > <a href="#">Manage SmarterMail</a>',
			'vars' => array(
				'totalUsers' => $totalUsers,
				'totalUsersPercent' => ($params['configoption3']!=0)?($totalUsers*100)/$params['configoption3']:0,
				'maxUsers' => $params['configoption3'],
				'canCreateUser' => ($totalUsers>=$params['configoption3'])?false:true,
				'DomainName' => $params['domain'],
				'domainid' => $params['domain'],
				'IsDedicated' => '',
				'params2' => var_export($params,1),
			),
		);
	}
	//echo '<pre>'; print_r($pagearray);echo '</pre>';
	return $pagearray;
}

//used to configure options for the SmarterMail settings within the server settings.  Each option is called by 'configoption#'
function smartermail_ConfigOptions(){
    $configarray = array(
		"SmarterMail Data Path" => array("Type"=>"text", "Size"=>"25", "Description"=>"Ex. M:\\SmarterMail\\Domains\\ NOTE: this would need to be consistent across all products"),
		"***Outbound IP Address" => array("Type"=>"text", "Size"=>"25", "Description"=>"IP address used for outbound messages"),
		"Maximum Number of Domain Users" => array("Type"=>"text", "Size"=>"25", "Description"=>""),
		"***Maximum Domain Size" => array("Type"=>"text", "Size"=>"25", "Description"=>"MB"),
		"***Maximum Number of Domain Aliases" => array("Type"=>"text", "Size"=>"25", "Description"=>""),
		"***Maximum Number of User Aliases" => array("Type"=>"text", "Size"=>"25", "Description"=>""),
		"Maximum Message Size in KB" => array("Type"=>"text", "Size"=>"25", "Description"=>"Ex. for 50MB -> 50000 + 53,6% = '76800' = 50MB "),
		"Maximum Number of Recipients" => array("Type"=>"text", "Size"=>"25", "Description"=>""),
		"Maximum Number of Mailing Lists" => array("Type"=>"text", "Size"=>"25", "Description"=>""),
		"***List Command Address" => array("Type"=>"text", "Size"=>"25", "Description"=>""),
		"***Require SMTP Authentication" => array("Type"=>"yesno", "Description"=>"Check to require SMTP Authentication"),
		"***Enable Domain Aliases" => array("Type"=>"yesno", "Description"=>""),
		"Enable Content Filtering" => array("Type"=>"yesno", "Description"=>""),
		"Enable users to modify spam settings" => array("Type"=>"yesno", "Description"=>""),
		"Enable Mailing Lists" => array("Type"=>"yesno", "Description"=>""),
		"***SmarterMail Web Interface URL" => array("Type"=>"text", "Size"=>"25", "Description"=>"Ex. http://mail.example.com"),
		"Max Maibox Size in MB" => array("Type"=>"text", "Size"=>"25", "Description"=>"Ex. '10240' = 10GB "),
		"Maximum Number of Dedicated Users" => array("Type"=>"text", "Size"=>"25", "Description"=>"Apply only for for Dedicated"),
    );
    return $configarray;
}

//Used when a product is created within WHMCS.  This calls 'createdomain' web service on SmarterMail installation.
function smartermail_CreateAccount($params){
	$smUrl = addHttpToHostname($params["serverhostname"]);//$params["configoption16"];
	$paramaters = array(
		'AuthUserName' => $params["serverusername"],
		'AuthPassword' => $params["serverpassword"],
		'DomainName'=> $params["domain"],
		'Path'=> $params["configoption1"].$params["domain"],
		'PrimaryDomainAdminUserName' => "postmaster",
		'PrimaryDomainAdminPassword' => "iu$2Pks",
		'PrimaryDomainAdminFirstName'=> "Administrador",
		'PrimaryDomainAdminLastName'=> "Sistema",

		'IP'=> $params["configoption2"],
		'ImapPort' => 143,
		'PopPort'=> 110,
		'SmtpPort' => 25,
		'MaxAliases' => $params["configoption6"],
		'MaxDomainSizeInMB' => $params["configoption4"],
		'MaxDomainUsers' => $params["configoption3"]+1,
		'MaxMailboxSizeInMB' => 0,
		'MaxMessageSize' => $params["configoption7"],
		'MaxRecipients' => $params["configoption8"],
		'MaxDomainAliases' => $params["configoption5"],
		'MaxLists'  => $params["configoption9"],
		'ShowDomainAliasMenu'  => (($params["configoption12"] == "on") ? true : false),
		'ShowContentFilteringMenu'  => (($params["configoption13"] == "on") ? true : false),
		'ShowSpamMenu'  => (($params["configoption14"] == "on") ? true : false),
		'ShowStatsMenu'  => true,
		'RequireSmtpAuthentication'  => (($params["configoption11"] == "on") ? true : false),
		'ShowListMenu'  => (($params["configoption15"] == "on") ? true : false),
		'ListCommandAddress'  =>  $params["configoption10"]
	);
	// IF HAS ADITIONAL PACKGE
	foreach ($params['configoptions'] as $key => $value) {
		if($key == 'totalDomainUsers'){
			$paramaters['MaxDomainUsers'] = $paramaters['MaxDomainUsers'] + $value;
		}
	}
	$sm_createAccount_ws_url =  $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
	$wsResult = sm_webServiceCall($sm_createAccount_ws_url,$paramaters,"AddDomain");
	if($wsResult['AddDomainResult']['Result']){
		$result = "success";
	} else {
		$result = $wsResult['AddDomainResult']['Message'];
	}
	return $result;
}


function smartermail_CreateDomain($params){
	$smUrl = addHttpToHostname($params["serverhostname"]);
	$arrdomaininfo = array(
		'AuthUserName' => $params["serverusername"],
		'AuthPassword' => $params["serverpassword"],
		'DomainName' => $params["newdomainname"],
		'Path' => "m:\\SmarterMail\\Domains\\".$params["newdomainname"],
		'PrimaryDomainAdminUserName' => "postmaster",
		'PrimaryDomainAdminPassword' => "iu$2Pks",
		'PrimaryDomainAdminFirstName'=> "Administrador",
		'PrimaryDomainAdminLastName'=> "Sistema",
		'IP'=> null
	);
	//Manage Domain
	$paramaters = array(
		'AuthUserName' => $params["serverusername"],
		'AuthPassword' => $params["serverpassword"],
	);

	$domaininfo = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
	$wsResultdomaininfo =  sm_webServiceCall($domaininfo, $arrdomaininfo, "AddDomainEx");
	$retorno = array();
	if($wsResultdomaininfo['AddDomainExResult']['Result']){
		$retorno['sucess'] = true;
    $retorno['message'] = "Domínio ".$params["newdomainname"]." adicionado com sucesso.";
  } else {
		$retorno['sucess'] = false;
    $retorno['message'] = $wsResultdomaininfo['AddDomainExResult']['Message'];
  }
	return $retorno;
}

//Used when a product is terminated within WHMCS.  This calls 'deletedomain' web service on SmarterMail installation.
function smartermail_TerminateAccount($params) {
	$smUrl = addHttpToHostname($params["serverhostname"]);
	$paramaters = array(
	'AuthUserName' => $params["serverusername"],
	'AuthPassword' => $params["serverpassword"],
	'DomainName' => $params["domain"],
	'DeleteFiles' => true
	);
	$sm_TerminateAccount_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
	$wsResult = sm_webServiceCall($sm_TerminateAccount_ws_url,$paramaters,"DeleteDomain");
	if($wsResult['DeleteDomainResult']['Result']){
		$result = "success";
	} else {
		$result = $wsResult['DeleteDomainResult']['Message'];
	}
	return $result;
}
 
function smartermail_managesmartermaildomains($params) {
	$message='';
	$errormessage='';
	if(isset($_POST['managed']) && $_POST['managed']=='adddomain'
	&& isset($_POST['newdomainname']) && $_POST['newdomainname'] != ''
	) {
		$params["newdomainname"] = $_POST['newdomainname'];
		$create = smartermail_CreateDomain($params);
		//echo'<pre>';print_r($create);echo'</pre>';
		if ($create['sucess']) {
			$message=$create['message'];
		} else {
			$errormessage=$create['message'];
		}
		//$errormessage='ERRO ->>> dominio '.$_POST['newdomainname'].' nao foi criado!!!';
	}


	if(isset($_POST['managed']) && $_POST['managed']=='deldomain'
	&& isset($_POST['deldomainname']) && $_POST['deldomainname'] != ''
	) {
		$params["domain"] = $_POST['deldomainname'];
		$del = smartermail_TerminateAccount($params);
		if ($del == 'success') {
			$message='Domínio '.$_POST['deldomainname'].' excluido com sucesso.';
		} else {
			$errormessage=$del;
		}
		//$errormessage='ERRO ->>> dominio '.$_POST['deldomainname'].' nao foi apagado!!!';
	}

	if($params["customfields"]["IsDedicated"]=='on'
	&& isset($_GET['d']) && $_GET['d']!=''
	) {
		$params['configoption3'] = $params['configoption18'];
		$params['configoption17'] = 10240;
		$params['domain'] = $_GET['d'];
		return smartermail_managesmartermaildomain($params);
	} elseif($params["customfields"]["IsDedicated"]=='on') {
		/// LISTAR DOMINIOS
		$wsGetDomainsArray = array();
		$wsGetDomains = smartermail_API('svcDomainAdmin.asmx?WSDL', $params, 'GetAllDomains');
		if(is_array($wsGetDomains['GetAllDomainsResult']['DomainNames']['string'])){
				$wsGetDomainsArray = $wsGetDomains['GetAllDomainsResult']['DomainNames']['string'];
		} elseif(is_string($wsGetDomains['GetAllDomainsResult']['DomainNames']['string'])){
			$wsGetDomainsArray[] = $wsGetDomains['GetAllDomainsResult']['DomainNames']['string'];
		}
		$wsTotalDomains = count($wsGetDomainsArray);

		//echo'<pre>';print_r($wsGetDomains);echo'</pre>';

		//$wsGetListCommandAddressResultListsNames = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters,"GetAllDomains");

		$pagearray = array(
			'templatefile' => 'managesmartermaildomains',
			'breadcrumb' => ' > <a href="#">Manage SmarterMail</a>',
			'vars' => array(
				'message' => $message,
				'errormessage' => $errormessage,
				'totalDomains' => $wsTotalDomains,
				'maxDomains' => $params['configoption18'],
				'canCreateDomain' => ($wsTotalDomains>=$params['configoption18'])?false:true,
				'wsGetDomainsResultArray' => $wsGetDomainsArray,
				//'DomainName' => $params['domain'],
				//'wsGetListCommandAddressResultListsNames' => $smAliasesArray,
				//'wsGetAliasesResult' => $wsGetAliasesResult,
				//'domainid' => $params['domain'],
				'IsDedicated' => $params["customfields"]["IsDedicated"],
			),
		);
		return $pagearray;
	} elseif($params["customfields"]["IsDedicated"]!='on') {
		$params['configoption3'] = $params['configoption18'];
		$params['configoption17'] = 10240;
		$params['domain'] = $_GET['d'];
		return smartermail_managesmartermaildomain($params);
	}
}


function smartermail_managesmartermaildomain($params) {
	$editUser = 'off';
	$editSign = 'off';
	$smUrl = addHttpToHostname($params["serverhostname"]);
	$sm_UpdateUser2_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
   if ($_POST["isenableda"] == 'false'){
   		$isenabledA = 0;
   }else{
   		$isenabledA = 1;
   }
	if(isset($_POST['managea']) && $_POST['managea']=='UserConfig'){

			$paramaters = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'EmailAddress' =>  $_POST['username'],
				'Enabled' => $isenabledA,
				'Subject' => $_POST['Subject'],
				'Body' =>  $_POST['Body'], 
			); 
			//echo ;
			$paramatersMailListAdmin = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'EmailAddress' =>  $_POST['username'],
				'newSettings' => array("forwardaddress=".$_POST["forwardaddress"].""),
			);

			$wsResult = sm_webServiceCall($sm_UpdateUser2_ws_url,$paramaters,"UpdateUserAutoResponseInfo");
			$wsResult2222 = sm_webServiceCall($sm_UpdateUser2_ws_url, $paramatersMailListAdmin,"SetRequestedUserSettings");
			if($wsResult['UpdateUserAutoResponseInfoResult']['Result']){
				$message = "Configurações de usuário atualizada com sucesso.";
			} else {
				$errormessage = $wsResult['UpdateUserAutoResponseInfoResult']['Message']; 
			}  

	 }

		if(isset($_POST['managed']) && $_POST['managed']=='addAlias'){
			$sm_AddAlias_ws_url = $smUrl."/Services/svcAliasAdmin.asmx?WSDL";
			$paramaters3 = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'DomainName' => $params["domain"],
				'AliasName' =>  $_POST['aliasname'],
				'Addresses' => Array($_POST['email'])
			);
			//echo "<pre>";
			//print_r($paramaters3);
			$wsResultAlias = sm_webServiceCall($sm_AddAlias_ws_url,$paramaters3,"AddAlias");
			if($wsResultAlias['AddAliasResult']['Result']){
				$message = "Apelido de usuário adicionado com sucesso.";
			} else {
				$errormessage = $wsResultAlias['AddAliasResult']['Message'];
			}
		}
	////////////// START DELETE Alias
		if(isset($_POST['managed']) && $_POST['managed']=='delAlias'){
			$paramatersDeleteAlias = array(
			'AuthUserName' => $params["serverusername"],
			'AuthPassword' => $params["serverpassword"],
			'DomainName' => $params["domain"],
			'AliasName' =>  $_POST['aliasname'],
			);

			$sm_DeleteAlias_ws_url = $smUrl."/Services/svcAliasAdmin.asmx?WSDL";
			$wsResult2 = sm_webServiceCall($sm_DeleteAlias_ws_url,$paramatersDeleteAlias,"DeleteAlias");

			if($wsResult2['DeleteAliasResult']['Result']){
				$message = "Apelido de usuário removido com sucesso.";
			} else {
				$errormessage =  $wsResult2['DeleteAliasResult']['Message'];
			}

	

		}
	////////////// START DELETE LIST
	//print_r($wsGetListCommandAddressResultListsNames);
	if(isset($_POST['managea']) && $_POST['managea']=='delList' ){
		$paramatersMailListAdmin = array(
			'AuthUserName' => $params["serverusername"],
			'AuthPassword' => $params["serverpassword"],
			'DomainName' => $params['domain'],
			'ListName' =>  $_POST['listname'],
			);

		$sm_DeleteAlias_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
		$wsResult = sm_webServiceCall($sm_DeleteAlias_ws_url, $paramatersMailListAdmin,"DeleteList");
			if($wsResult['DeleteListResult']['Result']){
			   	 $message = "Lista removida com sucesso";
			} else { 
				$errormessage = $wsResult['DeleteListResult']['Message']; 
			}
	}
	////////////// END DELETE LIST

	////////////// START DELETE LIST SIGN
	if(isset($_POST['managea']) && $_POST['managea']=='delListSign' ){
			$paramatersMailListSign = array(
			'AuthUserName' => $params["serverusername"],
			'AuthPassword' => $params["serverpassword"],
			'DomainName' => $params['domain'],
			'ListName' =>  $_GET['listname'],
			'Subscriber' =>  $_POST['sign'],
			); 
			
			$sm_DeleteAlias_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
			$wsResult = sm_webServiceCall($sm_DeleteAlias_ws_url,$paramatersMailListSign,"RemoveSubscriber");
			if($wsResult['RemoveSubscriberResult']['Result']){
			   	 $message = "Assinante removido com sucesso";
			} else {
				$errormessage = $wsResult['RemoveSubscriberResult']['Message'];
			} 

	}
	////////////// END DELETE LIST SIGN
	////////////// START EDIT LIST 
	if(isset($_POST['managea']) && $_POST['managea']=='editList' ){
			$paramatersMailListAdmin = array(
			'AuthUserName' => $params["serverusername"],
			'AuthPassword' => $params["serverpassword"],
			'DomainName' => $params['domain'],
			'ListName' =>  $_POST['ListNameID'],
			'newSettings' => array("whocanpost=".$_POST["whocanpost"]."","maxmessagesize=".$_POST["maxmessagesize"]."", "listreplytoaddress=LISTADDRESS", "listtoaddress=LISTADDRESS","listfromaddress=POSTERADDRESS","moderator=".$_POST['newmoderator'].""),
			);

			$sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
			$wsResult = sm_webServiceCall( $sm_GetListCommandAddress_ws_url,$paramatersMailListAdmin,"SetRequestedListSettings");
			if($wsResult['SetRequestedListSettingsResult']['Result']){
					 $message = "Lista atualizada com sucesso";
				} else {
					$errormessage = $wsResult['SetRequestedListSettingsResult']['Message'];
				}
		
	 }

	////////////// END EDIT LIST 

////////////// START ADD LIST
		if(isset($_POST['managea']) && $_POST['managea']=='addList' ){
			$sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
			$paramatersAddList1 = array(
			'AuthUserName' => $params["serverusername"],
			'AuthPassword' => $params["serverpassword"],
		     'DomainName' => $params['domain'],
			'ListName' =>  $_POST['newlistname'],
			'Moderator' => $_POST['newmoderatoradd'],
			'Description' => ' ',
			);
			$wsResultaddlist = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramatersAddList1,"AddList");

			$paramatersaddlist = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'DomainName' => $params['domain'],
				'ListName' =>  $_POST['newlistname'], 
				'newSettings' => array("whocanpost=".$_POST["whocanpost"]."","maxmessagesize=".$_POST["maxmessagesize"]."", "listreplytoaddress=LISTADDRESS", "listtoaddress=LISTADDRESS","listfromaddress=POSTERADDRESS","moderator=".$_POST['newmoderatoradd'].""), 
			);

			$sm_GetListCommandAddress_ws_url2 = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
			$wsResultaddlist2 = sm_webServiceCall( $sm_GetListCommandAddress_ws_url2,$paramatersaddlist,"SetRequestedListSettings");


			if($wsResultaddlist['AddListResult']['Result']){
			    $message  = "Lista de e-mail adicionada com sucesso";
			} else {
			    $errormessage = $wsResultaddlist['AddListResult']['Message'];
			} 
	}
	////////////// END ADD LIST 

	////////////// START ADD USER
	if(isset($_POST['managea']) && $_POST['managea']=='adduser'){
		if(isset($_POST['newusername']) && $_POST['newusername']!=''
		&& isset($_POST['newpassword']) && $_POST['newpassword']!=''
		&& isset($_POST['newusername']) && $_POST['newusername']!='') {
		//	echo '<pre>';print_r($_POST);echo '</pre>';

			$paramaters = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'NewUsername' =>  $_POST['newusername'],
				'NewPassword' => $_POST['newpassword'],
				'DomainName' => $params["domain"],
				'FirstName' =>  $_POST['newfirstname'],
				'LastName' =>  '',
				'IsDomainAdmin' => false,

				'maxMailboxSize' => $params["configoption17"],
			);


			$sm_AddUser2_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
			$wsResult = sm_webServiceCall($sm_AddUser2_ws_url,$paramaters,"AddUser2");

			if($wsResult['AddUser2Result']['Result']){
				$message = "Usuário adicionado com sucesso.";
			} else {
				$errormessage = $wsResult['AddUser2Result']['Message'];
			}
		} else {
			$errormessage = "Favor preencha todos os campos corretamente.";
		}
	}
	////////////// END ADD USER

	////////////// START ADD ALIAS
	if(isset($_POST['managed']) && $_POST['managed']=='adddomainalias'){
		$editUser = 'off';
		if(isset($_POST['newdomainalias']) && $_POST['newdomainalias']!='') { 

			$paramaters = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'DomainName' => $params["domain"],
				'DomainAliasName' =>  $_POST['newdomainalias'],
			);

			$smUrl = addHttpToHostname($params["serverhostname"]);
			$sm_AddAlias_ws_url = $smUrl."/Services/svcDomainAliasAdmin.asmx?WSDL";
			$wsResult = sm_webServiceCall($sm_AddAlias_ws_url,$paramaters,"AddDomainAliasWithoutMxCheck");
			//	echo '<pre>';print_r($wsResult);echo '</pre>';

			if($wsResult['AddDomainAliasWithoutMxCheckResult']['Result']){
				$message = "Alias '".$_POST['newdomainalias']."' para '".$params["domain"]."' adicionado com sucesso.";
			} else {
				$errormessage = $wsResult['AddDomainAliasWithoutMxCheckResult']['Message'];
			}
		} else {
			$errormessage = "Favor preencha todos os campos corretamente.";
		}
	}
	////////////// END ADD ALIAS

	////////////// START DEL ALIAS
		if(isset($_POST['deldomainalias']) && $_POST['deldomainalias']!='') {

		$paramaters = array(
			'AuthUserName' => $params["serverusername"],
			'AuthPassword' => $params["serverpassword"],
			'DomainName' => $params["domain"],
			'DomainAliasName' =>  $_POST['deldomainalias'],
		);

		$smUrl = addHttpToHostname($params["serverhostname"]);
		$sm_AddAlias_ws_url = $smUrl."/Services/svcDomainAliasAdmin.asmx?WSDL";
		$wsResult = sm_webServiceCall($sm_AddAlias_ws_url,$paramaters,"DeleteDomainAlias");
		//	echo '<pre>';print_r($wsResult);echo '</pre>';

		if($wsResult['DeleteDomainAliasResult']['Result']){
			$message = "Alias '".$_POST['deldomainalias']."' para '".$params["domain"]."' removido com sucesso.";
		} else {
			$errormessage = $wsResult['DeleteDomainAliasResult']['Message'];
		}
	}
	////////////// END DEL ALIAS


	////////////// START EDIT USER
	if(isset($_POST['managee']) && $_POST['managee']=='edituser'){
		$error = false;
		$errormessage = '';
		$smUrl = addHttpToHostname($params["serverhostname"]);
		$paramatersUpdateUser2 = array(
			'AuthUserName' => $params["serverusername"],
			'AuthPassword' => $params["serverpassword"],
			'EmailAddress' => $_POST['username'],
			'NewFirstName' =>$_POST['firstname'],
			'NewLastName' => '',
			'IsDomainAdmin' => 'false',
			'maxMailboxSize' => $_POST['mailboxsize'],
		);

		$sm_UpdateUser2_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";

		if (($_POST['newpassword'])!=''){
			$senha =  $_POST['newpassword'];
			$paramatersUpdateUser2['NewPassword'] = $senha;

		} else {
			$paramatersUpdateGetUser2 = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'EmailAddress' => $_POST['username'],
			);
			$wsResultGetUser = sm_webServiceCall($sm_UpdateUser2_ws_url, $paramatersUpdateGetUser2, "GetUser");

			if($wsResultGetUser['GetUserResult']['Result']){
				$paramatersUpdateUser2['NewPassword'] = $wsResultGetUser['GetUserResult']['UserInfo']['Password'];
			} else {
				$error = true;
				$errormessage .= ':( Erro número 54';
			}
		}
		//echo '<pre>';print_r($wsResultGetUser);echo '</pre>';

		if(!$error){
			$wsResult1 = sm_webServiceCall($sm_UpdateUser2_ws_url, $paramatersUpdateUser2, "UpdateUser2");
			if($wsResult1['UpdateUser2Result']['Result']){
				$message = "Usuário atualizado com sucesso.";
			} else {
				$errormessage .= $wsResult1['UpdateUser2Result']['Message'];
			}
		}
	}

	////////////// END EDIT USER

	////////////// START DELETE USER
	if(isset($_POST['managed']) && $_POST['managed']=='deluser'
	&& isset($_POST['username']) && $_POST['username']!=''){

		$smUrl = addHttpToHostname($params["serverhostname"]);
		$paramaters = array(
			'AuthUserName' => $params["serverusername"],
			'AuthPassword' => $params["serverpassword"],
			'Username' => $_POST['username'],
			'DomainName' => $params["domain"],
		);

		$sm_DeleteUser_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
		$wsResult = sm_webServiceCall($sm_DeleteUser_ws_url,$paramaters,"DeleteUser");
		//echo '<pre>';print_r($wsResult);echo '</pre>';
		if($wsResult['DeleteUserResult']['Result']){
			$message = "Usuário ".$_POST['username']."@".$params["domain"].", removido com sucesso.";
		} else {
			$errormessage = $wsResult['DeleteUserResult']['Message'];
		}
	}

	////////////// END DELETE USER


	////////////// START GET LISTS
	//$sm_GetAliases_ws_url = $smUrl."/Services/svcAliasAdmin.asmx?WSDL";
	//$wsGetAliasesResult = sm_webServiceCall($sm_GetAliases_ws_url,$paramaters,"GetAliases");
	$wsGetDomainUsersLists =  smartermail_API('svcMailListAdmin.asmx?WSDL', $params, 'GetMailingListsByDomain');
	$wsGetDomainUsersResult = smartermail_API('svcUserAdmin.asmx?WSDL', $params, 'GetUsers');
	$wsGetDomainUsersQuotas = smartermail_API('svcUserAdmin.asmx?WSDL', $params, 'GetUserQuotas');
	$wsGetDomainAliasResult = smartermail_API('svcDomainAliasAdmin.asmx?WSDL', $params, 'GetAliases');


	$GetAliasesDomain = array();
	foreach ($wsGetDomainAliasResult["GetAliasesResult"]["DomainAliasNames"] as $key => $value) {
		foreach ($value as $alias) {
					array_push($GetAliasesDomain, $alias);
		}
	}

	if(isset($_GET['managea']) || $_POST['managea']=='GetEditUser'){
    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramatersForGetUser = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'EmailAddress' => ($_GET["email"]),
    );

    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName'=>  ($_GET["d"]),
    );

 
	$sm_GetUser_ws_url = $smUrl."/Services/svcAliasAdmin.asmx?WSDL";
  	$wsGetAliasesResult = sm_webServiceCall($sm_GetUser_ws_url, $paramaters,"GetAliases");
	$GetAliases = array(); 


    if(is_array($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo'][0])){
        foreach($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo'] as $key => $value)
        {
            foreach($value as $key2 => $value2)
            { 
                if($key2 === 'Name')
                {
					foreach($value as $key3 => $value3)
					{
						if($key3 === 'Addresses')
						{
							foreach	($value3 as $ws)
							{
								if($_GET["email"] === $ws){

									array_push($GetAliases,$value2);
								}
							}
						}
					}
                }
            }
        }
    }else{
		if(is_array($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo'])){
			foreach($wsGetAliasesResult['GetAliasesResult']['AliasInfos']['AliasInfo'] as $key => $value){
				if($key === 'Name'){
				   foreach($value as $key3 => $value3){
						if($key3 === 'Addresses'){
							foreach	($value3 as $ws) {
								if($_GET["email"] === $ws){
									array_push($GetAliases,$value);
								}
							}
						}
					}
				}
			}
		}
	}

    $sm_GetUser_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
    $wsResult = sm_webServiceCall($sm_GetUser_ws_url,$paramatersForGetUser,"GetUser");
	$GetUserAutoResponseInfo = sm_webServiceCall($sm_GetUser_ws_url,$paramatersForGetUser,"GetUserAutoResponseInfo");
	$GetUserForwardingInfo2 = sm_webServiceCall($sm_GetUser_ws_url,$paramatersForGetUser,"GetUserForwardingInfo2");
    //Get MAX mailbox size

    $paramatersForReqSettings = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'EmailAddress' =>  ($_GET["email"]),
        'requestedSettings' => array('maxsize', 'enabledstatus', 'isdomainadmin'),
    );

    $sm_GetRequestedUserSettings_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
    $wsResult2 = sm_webServiceCall($sm_GetRequestedUserSettings_ws_url,$paramatersForReqSettings,"GetRequestedUserSettings");

    $paramatersForGetPrimaryAdmin = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName'=>  ($_GET["d"]),
    );

    $sm_GetPrimaryDomainAdmin_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
    $wsResult3 = sm_webServiceCall($sm_GetPrimaryDomainAdmin_ws_url,$paramatersForGetPrimaryAdmin,"GetPrimaryDomainAdmin");

    $mailboxStatus = "Enabled";
	 $editUser = 'on';
	}

$smUsersArray = array();
	$totalUsers=0;
	
	if(is_array($wsGetDomainUsersResult['GetUsersResult']['Users']['UserInfo'])){
		foreach($wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo'] as $keyvar =>$userStringValue){
			if(is_array($userStringValue)){
				if(substr($userStringValue["UserName"],0,strrpos($userStringValue["UserName"], '@')) == 'postmaster') continue;
				$vall = array();
				$keyy = array_search(substr($userStringValue["UserName"],0,strrpos($userStringValue["UserName"], '@')), array_column($wsGetDomainUsersQuotas['GetUserQuotasResult']['Users']['UserQuota'], 'UserName'));
				$vall['Name'] = $userStringValue["FirstName"];
				$vall['LastName'] = $userStringValue["LastName"];
				$vall['Pass'] = $userStringValue["Password"];

				$vall['Email'] = $userStringValue["UserName"];
				$vall['User'] = substr($userStringValue["UserName"],0,strrpos($userStringValue["UserName"], '@'));
				$vall['MaxSpace'] = ((($wsGetDomainUsersQuotas['GetUserQuotasResult']['Users']['UserQuota'][$keyy]['MaxSpace'])/1024)/1024)/1024;
				$vall['CurrentSpace'] = $wsGetDomainUsersQuotas['GetUserQuotasResult']['Users']['UserQuota'][$keyy]['CurrentSpace'];
				$vall['PercentUsed'] = round($vall['CurrentSpace']*100/$wsGetDomainUsersQuotas['GetUserQuotasResult']['Users']['UserQuota'][$keyy]['MaxSpace']);

				$paramatersGetRequestedUserSettings = array(
					'AuthUserName' => $params["serverusername"],
					'AuthPassword' => $params["serverpassword"],
					'EmailAddress' => $userStringValue["UserName"],
					'requestedSettings' => array('maxsize', 'enabledstatus', 'isdomainadmin','isenabled', 'forwardaddress'),					
				);
				$paramatersGetAutoResponseInfo  = array(
					'AuthUserName' => $params["serverusername"],
					'AuthPassword' => $params["serverpassword"],
					'EmailAddress' => $userStringValue["UserName"],
				);
				
				$sm_ws_url_GetRequestedUserSettings = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
		
				$paramatersUpdateGetUser2 = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'EmailAddress' =>  $userStringValue["UserName"],
				);

				$wsResultGetUsera = sm_webServiceCall($sm_ws_url_GetRequestedUserSettings, $paramatersUpdateGetUser2, "GetUserAutoResponseInfo");
				$wsResult_sm_ws_url_GetRequestedUserSettings = sm_webServiceCall($sm_ws_url_GetRequestedUserSettings,$paramatersGetRequestedUserSettings,"GetRequestedUserSettings");
				
				$vall['Subject'] = $wsResultGetUsera["GetUserAutoResponseInfoResult"]["Subject"];
				$vall['Body'] = $wsResultGetUsera["GetUserAutoResponseInfoResult"]["Body"];
				$vall['forwardaddress'] = str_replace("forwardaddress=","", $wsResult_sm_ws_url_GetRequestedUserSettings["GetRequestedUserSettingsResult"]["settingValues"][4]);
				$vall['isenabledforwardaddress'] = ((str_replace("isenabled=","", $wsResult_sm_ws_url_GetRequestedUserSettings["GetRequestedUserSettingsResult"]["settingValues"][4])) == "") ? true : false; 
					$vall['isenabledAutoResponseInfo'] = $wsResultGetUsera["GetUserAutoResponseInfoResult"]["Enabled"]; 
				array_push($smUsersArray, $vall);
				$totalUsers++;
			}else{
				if ($keyvar == "UserName"){
					if(substr($userStringValue,0,strrpos($userStringValue, '@')) == 'postmaster') continue;
					$vall = array();
					$vall['Name'] = $wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo']["FirstName"];
					$vall['LastName'] = $wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo']["LastName"];
					$vall['Pass'] = $wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo']["Password"];

					$vall['Email'] = $userStringValue;
					$vall['User'] = substr($userStringValue,0,strrpos($userStringValue, '@'));
					$vall['MaxSpace'] = ((($wsGetDomainUsersQuotas['GetUserQuotasResult']['Users']['UserQuota']['MaxSpace'])/1024)/1024)/1024;
					$vall['CurrentSpace'] = $wsGetDomainUsersQuotas['GetUserQuotasResult']['Users']['UserQuota']['CurrentSpace'];
					$vall['PercentUsed'] = round($vall['CurrentSpace']*100/$wsGetDomainUsersQuotas['GetUserQuotasResult']['Users']['UserQuota'][$keyy]['MaxSpace']);

					$paramatersGetRequestedUserSettings = array(
					'AuthUserName' => $params["serverusername"],
					'AuthPassword' => $params["serverpassword"],
					'EmailAddress' =>  $userStringValue,
					'requestedSettings' => array('maxsize', 'enabledstatus', 'isdomainadmin','isenabled', 'forwardaddress'),					
					);

					$paramatersGetAutoResponseInfo  = array(
					'AuthUserName' => $params["serverusername"],
					'AuthPassword' => $params["serverpassword"],
					'EmailAddress' => $userStringValue,
					);
				

					$sm_ws_url_GetRequestedUserSettings = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
			
					$paramatersUpdateGetUser2 = array(
					'AuthUserName' => $params["serverusername"],
					'AuthPassword' => $params["serverpassword"],
					'EmailAddress' =>  $userStringValue["UserName"],
					);

					$wsResultGetUsera = sm_webServiceCall($sm_ws_url_GetRequestedUserSettings, $paramatersUpdateGetUser2, "GetUserAutoResponseInfo");
					$wsResult_sm_ws_url_GetRequestedUserSettings = sm_webServiceCall($sm_ws_url_GetRequestedUserSettings,$paramatersGetRequestedUserSettings,"GetRequestedUserSettings");
					$vall['Subject'] = $wsResultGetUsera["GetUserAutoResponseInfoResult"]["Subject"];
					$vall['Body'] = $wsResultGetUsera["GetUserAutoResponseInfoResult"]["Body"];
					$vall['forwardaddress'] = str_replace("forwardaddress=","", $wsResult_sm_ws_url_GetRequestedUserSettings["GetRequestedUserSettingsResult"]["settingValues"][4]);
					$vall['isenabledforwardaddress'] = ((str_replace("isenabled=","", $wsResult_sm_ws_url_GetRequestedUserSettings["GetRequestedUserSettingsResult"]["settingValues"][4])) == "") ? true : false; 
					$vall['isenabledAutoResponseInfo'] = $wsResultGetUsera["GetUserAutoResponseInfoResult"]["Enabled"]; 
						//echo "<pre>";
						//print_r($wsResult_sm_ws_url_GetRequestedUserSettings["GetRequestedUserSettingsResult"]["settingValues"]); 
						//echo "</pre>";
					array_push($smUsersArray, $vall);
					$totalUsers++;
				}
			}
		}
	}

	if($_GET['managesign'] =='GetEditSign'){
		if(isset($_POST['managea']) && $_POST['managea']=='addListSign' ){
			$paramatersGetRequestedListSettings = array(
			    'AuthUserName' => $params["serverusername"],
			    'AuthPassword' => $params["serverpassword"],
			    'DomainName' => $params['domain'],
			    'ListName' =>   $_GET['listname'],
			    'Subscribers' =>  explode("\r\n", $_POST['newSub']),
			);
		
			if($_POST['newSub'] != '')
			{
				$sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
				$wsResult = sm_webServiceCall($sm_GetListCommandAddress_ws_url, $paramatersGetRequestedListSettings,"AddSubscriber2");
				if($wsResult['AddSubscriber2Result']['Result']){
					 $message = "Assinante adicionado com sucesso";
				} else {
					$errormessage = $wsResult['AddSubscriber2Result']['Message'];
				}
			} 
		}

		$paramatersSign = array(
		'AuthUserName' => $params["serverusername"],
		'AuthPassword' => $params["serverpassword"],
		'DomainName'=> $_GET['d'],
		'ListName' =>  $_GET['listname']
		);


		//Call to get LstServ address
		$GetListSignurl = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
		$GetListSign = sm_webServiceCall($GetListSignurl,$paramatersSign,"GetSubscriberList");
		//print_r($GetListSign);
	    $editSign = 'on';
	} 

	$pagearray = array(
		'templatefile' => 'managesmartermaildomain',
		'breadcrumb' => ' > <a href="#">Manage SmarterMail</a>',
		'vars' => array(
			'GetMailingList' => $wsGetDomainUsersLists["GetMailingListsByDomainResult"]["listNames"]["string"],
			'GetListSign' => $GetListSign['GetSubscriberListResult']['Subscribers']['string'] ,
			'message' => $message,
			'ListName' => $_GET['listname'],
			'errormessage' => $errormessage,
			'totalUsers' => $totalUsers,
			'maxUsers' => $params['configoption3'],
			'canCreateUser' => ($totalUsers>=$params['configoption3'])?false:true,
			'wsGetDomainUsersResultArray' => $smUsersArray,
			'wsGetDomainUsersListResultArray' => $smUsersListsArray,
			 'wsGetUserAliasResultArray' =>$smAliasesArray,
			'DomainEliases' => $GetAliasesDomain,
  			'Moderator' =>  str_replace("'", "",(str_replace('moderator=', '',$wsResult['GetRequestedListSettingsResult']['settingValues']['string'][0]))),
			'whocanpost' =>  $whocanpost,
			'whocanpostname' =>  $whocanpostname,
        	'maxmessagesize' => str_replace("'", "",(str_replace('maxmessagesize=', '',$wsResult['GetRequestedListSettingsResult']['settingValues']['string'][6]))),
			'maxmessagesizeMB' =>  (((int)(str_replace("'", "",(str_replace('maxmessagesize=', '',$wsResult['GetRequestedListSettingsResult']['settingValues']['string'][6])))))/ 1024)."MB",
			'assinante' => "class",
			'lista' => "class='active'",
			'domainid' => $params['domain'],
			'listsgnarray' => $smAliasesArray,
			'IsDedicated' => $params["customfields"]["IsDedicated"],
			'GetAliases' => $GetAliases,
			'GetAliasesDomain' => $GetAliasesDomain,
			'FirstName' => $wsResult['GetUserResult']['UserInfo']['FirstName'],
			'isDomainAdmin' => $wsResult['GetUserResult']['UserInfo']['IsDomainAdmin'],
			'LastName' => $wsResult['GetUserResult']['UserInfo']['LastName'],
			'Password' => $wsResult['GetUserResult']['UserInfo']['Password'],
			'UserName' => $wsResult['GetUserResult']['UserInfo']['UserName'],
			'maxMailboxSize' => ltrim(strrchr(($wsResult2['GetRequestedUserSettingsResult']['settingValues']['string'][0]), '='),'='),
			'DomainName'=> $params['domain'],
			'editUser' => $editUser, 
			'editSign' => $editSign, 
			'GetUserAutoResponseInfobody' => $GetUserAutoResponseInfo["GetUserAutoResponseInfoResult"]["Body"],
			'GetUserAutoResponseInfosubject' => $GetUserAutoResponseInfo["GetUserAutoResponseInfoResult"]["Subject"],
			'GetUserAutoResponseInfoenabled' => $GetUserAutoResponseInfo["GetUserAutoResponseInfoResult"]["Enabled"],
			'GetUserForwardingInfo2'=> $GetUserForwardingInfo2["GetUserForwardingInfo2Result"]["ForwardingAddresses"]["string"]
		),
	);
	return $pagearray;
	//End Gerenciar Users e Lista
}

function smartermail_API($ws, $params, $action){
	$paramaters = array(
		'AuthUserName' => $params["serverusername"],
		'AuthPassword' => $params["serverpassword"],
		'DomainName'=> $params['domain']
	);
	$smUrl = addHttpToHostname($params["serverhostname"]);
	$sm_ws_url = $smUrl."/Services/".$ws;
	return sm_webServiceCall($sm_ws_url,$paramaters,$action);
}

//Used when an account is suspended within WHMCS.  This calls 'DisableDomain' web service on SmarterMail installation.
function smartermail_SuspendAccount($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'domainName' => $params["domain"]
    );

    $sm_SuspendAccount_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
    $wsResult = sm_webServiceCall($sm_SuspendAccount_ws_url,$paramaters,"DisableDomain");


    if($wsResult['DisableDomainResult']['Result']){
        $result = "success";
    } else {
        $result = $wsResult['DisableDomainResult']['Message'];
    }
    return $result;
}

//Used when an product is unsuspended within WHMCS.  This calls 'EnableDomain' web service on SmarterMail installation.
function smartermail_UnsuspendAccount($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'domainName' => $params["domain"]
    );

    $sm_UnsuspendAccount_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
    $wsResult = sm_webServiceCall($sm_UnsuspendAccount_ws_url,$paramaters,"EnableDomain");

    if($wsResult['EnableDomainResult']['Result']){
        $result = "success";
    } else {
        $result = $wsResult['EnableDomainResult']['Message'];
    }
    return $result;
}

//Used when the Password is changed in WHMCS.  The new Password must first be saved in WHMCS before the module is called.  This calls 'SetRequestedUserSettings' web service on SmarterMail installation.
function smartermail_ChangePassword($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'EmailAddress' => ($params["username"]."@".$params["domain"]),
        'newSettings' => array("Password = ".$params["Password"])
    );

    $sm_changePassword_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";
    $wsResult = sm_webServiceCall($sm_changePassword_ws_url,$paramaters,"SetRequestedUserSettings");

    if ($wsResult['SetRequestedUserSettingsResult']['Result']) {
        $result = "success";
    } else {
        $result = $wsResult['SetRequestedUserSettingsResult']['Message'];
    }
    return $result;

}

//Used when a product is changed within WHMCS.  This calls 'updatedomain' web service on SmarterMail installation.
function smartermail_ChangePackage($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName'=> $params["domain"],
        'ServerIP'=> $params["configoption2"],
        'ImapPort' => 143,
        'PopPort'=> 110,
        'SmtpPort' => 25,
        'MaxAliases' => $params["configoption6"],
        'MaxDomainSizeInMB' => $params["configoption4"],
        'MaxDomainUsers' => $params["configoption3"]+1,
        'MaxMailboxSizeInMB' => 0,
        'MaxMessageSize' => $params["configoption7"],
        'MaxRecipients' => $params["configoption8"],
        'MaxDomainAliases' => $params["configoption5"],
        'MaxLists'  => $params["configoption9"],
        'ShowDomainAliasMenu'  => (($params["configoption12"] == "on") ? true : false),
        'ShowContentFilteringMenu'  => (($params["configoption13"] == "on") ? true : false),
        'ShowSpamMenu'  => (($params["configoption14"] == "on") ? true : false),
        'ShowStatsMenu'  => true,
        'RequireSmtpAuthentication'  => (($params["configoption11"] == "on") ? true : false),
        'ShowListMenu'  => (($params["configoption15"] == "on") ? true : false),
        'ListCommandAddress'  =>  $params["configoption10"]
    );
		// IF HAS ADITIONAL PACKGE
		foreach ($params['configoptions'] as $key => $value) {
			if($key == 'totalDomainUsers'){
				$paramaters['MaxDomainUsers'] = $paramaters['MaxDomainUsers'] + $value;
			}
		}
		//echo'<pre>';print_r($params);print_r($paramaters);exit();


    $sm_changePackage_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
    $wsResult = sm_webServiceCall($sm_changePackage_ws_url,$paramaters,"UpdateDomain");

    if($wsResult['UpdateDomainResult']['Result']){
        $result = "success";
    } else {
        $result = $wsResult['UpdateDomainResult']['Message'];
    }
    return $result;

}

//Provides URL link in management interface for access to the SmarterMail web interface.
function smartermail_LoginLink($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);
    echo "<a href=$smUrl target=\"_blank\" style=\"color:#cc0000\">Access SmarterMail Web Interface</a>";

}

//Custom button array that allows users to interact with specified buttons below in client area.
function smartermail_ClientAreaCustomButtonArray($params) {
	$smUrl = addHttpToHostname($params["serverhostname"]);
	$paramaters3 = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
    );

    $paramaters2 = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName'=> $params["domain"]
    );
    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'domainName'=> $params["domain"]
    );

	$sm_GetUsers_ws_url = $smUrl."/Services/svcUserAdmin.asmx?WSDL";

	$sm_GetAliases_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
	$wsGetDomainSettingsResult = sm_webServiceCall($sm_GetAliases_ws_url,$paramaters,"GetDomainSettings");

	if($wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainUsers'] == 0)
		$wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainUsers'] = "∞";
	if($wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainAliases'] == 0)
		$wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainAliases'] = "∞";
	if($wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainSizeInMB'] == 0)
		$wsGetDomainSettingsResult['GetDomainSettingsResult']['MaxDomainSizeInMB'] = "∞";

	$sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
	$wsGetListCommandAddressResultListsNames = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters,"GetAllDomains");

	$smDomainArray = array();
    $IsDedicated = $params["customfields"]["IsDedicated"];

	if ($IsDedicated == 'on'){
		$total = 0;
		if(is_array($wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string'])){
			foreach	($wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string'] as $dom){
				$param = array();
				$param = array(
					'AuthUserName' => $params["serverusername"],
					'AuthPassword' => $params["serverpassword"],
					'DomainName'=> $dom,
				);

				$wsGetUsersforDomainResult = sm_webServiceCall($sm_GetUsers_ws_url, $param,"GetUsers");
				if (count($wsGetUsersforDomainResult['GetUsersResult']['Users']['UserInfo']) != 6){
					$total = $total + count($wsGetUsersforDomainResult['GetUsersResult']['Users']["UserInfo"]);
				}else {
					if (strpos($wsGetUsersforDomainResult['GetUsersResult']['Users']["UserInfo"]['UserName'], '@') !== false) {
						$total = $total + 1;
					} else {
						$total = $total + count($wsGetUsersforDomainResult['GetUsersResult']['Users']["UserInfo"]);
					}
				}
			}
		}else{
			$param = array();
			$param = array(
				'AuthUserName' => $params["serverusername"],
				'AuthPassword' => $params["serverpassword"],
				'DomainName'=> $wsGetListCommandAddressResultListsNames['GetAllDomainsResult']['DomainNames']['string'],
			);

			$wsGetUsersforDomainResult = sm_webServiceCall($sm_GetUsers_ws_url, $param,"GetUsers");
			if (count($wsGetUsersforDomainResult['GetUsersResult']['Users']['UserInfo']) != 6){
				$total = $total + count($wsGetUsersforDomainResult['GetUsersResult']['Users']["UserInfo"]);
			}else {
				if (strpos($wsGetUsersforDomainResult['GetUsersResult']['Users']["UserInfo"]['UserName'], '@') !== false) {
					$total = $total + 1;
				} else {
					$total = $total + count($wsGetUsersforDomainResult['GetUsersResult']['Users']["UserInfo"]);
				}
			}

		}
	}else{
		$total = 0;
		$wsGetUsersResult = sm_webServiceCall($sm_GetUsers_ws_url, $paramaters2,"GetUsers");
		if (count($wsGetUsersResult['GetUsersResult']['Users']['UserInfo']) != 6){
			$total = $total + count($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]);
		}else {
			if (strpos($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]['UserName'], '@') !== false) {
				$total = $total + 1;
			} else {
				$total = $total + count($wsGetUsersResult['GetUsersResult']['Users']["UserInfo"]);
			}
		}
	}
	//echo count($wsGetUsersResult['GetUsersResult']['Users']);
	if ($params["customfields"]["IsDedicated"] == 'on'){
		$buttonarray = array(
			"Gerenciar Domínios" => "managesmartermaildomain",
			'vars' => array(
			'IsDedicated' => $params["customfields"]["IsDedicated"],
			'maxUsers' =>  $params["customfields"]["AccountLimit"],
			'countUsers' => $total
			),
		);
	}else{
			$buttonarray = array(
			//"Gerenciar Domínios" => "managesmartermaildomain",
			"Gerenciar Usuários" => "managesmartermailuserdomaintpl",
			'vars' => array(
				'IsDedicated' => $params["customfields"]["IsDedicated"],
				 'maxUsers' => $wsGetDomainSettingsResult['GetDomainSettingsResult']["MaxDomainUsers"],
				 'countUsers' => $total
			),
		);
	}

    return $buttonarray;
}

function smartermail_ClientAreaAllowedFunctions(){
	    $functionarray = array(
		 "Users" => "managesmartermailuserdomaintpl",
		 "Add User" => "addsmartermailusernew",
        "Create Alias (API)" => "createsmartermailalias",
        "Edit Alias (API)" => "editsmartermailaliaspage",
        "Delete Alias (API)" => "deletesmartermailalias",
		"Create User (API)" => "createsmartermailuser",
        "Edit User (API)" => "editsmartermailuserpage",
		"Delete User (API)" => "deletesmartermailuser",
        "Save User (API)" => "savechangessmartermailuser",
        "Save Alias (API)" => "savechangessmartermailalias",
		"Manage Alias (API)" => "managesmartermailalias",
		"Manage Lists (API)" => "managesmartermailmailinglists",
		"Manage Sign (API)" => "managesmartermailmailinglistsSign",
        "Save Alias (API)" => "savechangessmartermailalias",
		"ADD User" => "addsmartermailuser",
        "ADD Alias (API)" => "addsmartermailalias",
		"ADD Lists (API)" => "addsmartermailmailinglists",
		"Create Lists (API)" => "createsmartermailmailinglists",
		"Edit Lists (API)" => "editsmartermailmailinglistspage",
		"Save Lists  (API)" => "savechangessmartermailmailinglists",
		"ADD ListsSign (API)" => "addsmartermailmailinglistsSign",
		"Create ListsSign (API)" => "createsmartermailmailinglistsSign",
		"Edit ListsSign (API)" => "editsmartermailmailinglistspageSign",
		 "Delete Sign (API)" => "deletesmartermailmailinglistsSign",
		"Save ListsSign  (API)" => "savechangessmartermailmailinglistsSign",
		 "ADD Domain (API)" => "addsmartermaildomain",
		"Create Domain (API)" => "createsmartermaildomain",
        "Edit Domain (API)" => "editsmartermaildomainpage",
        "Delete Domain (API)" => "deletesmartermaildomain",
		"Manage Domain (API)" => "managesmartermaildomain",
		"Manage Domains (API)" => "managesmartermaildomains",
		"Edit Domain(API)" => "editsmartermaildomainpage",
		"Delete Mailing Lists (API)" => "deletesmartermailmailinglists"
    );
    return $functionarray;
}


//Manage SmarterMail button on client area details page.  Calls various web services to get users and aliases to be displayed in managesmartermail.tpl
function smartermail_managesmartermail($params) {

    $smUrl = addHttpToHostname($params["serverhostname"]);

    $paramaters = array(
        'AuthUserName' => $params["serverusername"],
        'AuthPassword' => $params["serverpassword"],
        'DomainName'=>  $_POST['domain']
    );

    //Call to get Domain Users
    $sm_getDomainUsers_ws_url = $smUrl."/Services/svcDomainAdmin.asmx?WSDL";
    $wsGetDomainUsersResult = sm_webServiceCall($sm_getDomainUsers_ws_url,$paramaters,"GetUsers");

    //Call to get LstServ address
    $sm_GetListCommandAddress_ws_url = $smUrl."/Services/svcMailListAdmin.asmx?WSDL";
	$wsGetListCommandAddressResultListsNames = sm_webServiceCall($sm_GetListCommandAddress_ws_url,$paramaters,"GetMailingListsByDomain");

    $smAliasesArrayForLoop = array();
    $smAliasesArray = array();
	if(is_array($wsGetDomainUsersResult['GetUsersResult']['Users']['UserInfo'])){
	foreach($wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo'] as $keyvar =>$userStringValue){
		if ($keyvar == "UserName"){
		array_push($smUsersArray, $userStringValue);
		}
	}
	}else{
		foreach($wsGetDomainUsersResult["GetUsersResult"]["Users"]['UserInfo'] as $userStringValueA){
			array_push($smUsersArray, $userStringValueA["UserName"]);
		}
	}

//setup user array to be displayed in ManageSmarterMail Template.  Checking to see if listServ name is showing or if they are an alias which shows in a separate section.
    $smUsersArray = array();
        foreach($wsGetDomainUsersResult['GetUsersResult']['Users']['UserInfo'] as $userStringValue){
		   if((!in_array( $userStringValue , $smAliasesArray))&& (!in_array( $userStringValue , $smAliasesArray2))){
				array_push($smUsersArray, $userStringValue);
            }
        }

    sort($smUsersArray);


    $pagearray = array(
        'templatefile' => 'managesmartermail',
        'breadcrumb' => ' > <a href="#">Manage SmarterMail</a>',
        'vars' => array(
            'wsGetDomainUsersResultArray' => $smUsersArray,
			'wsGetListCommandAddressResultListsNames' => $smAliasesArray,
			'wsGetAliasesResult' => $wsGetAliasesResult,
			'DomainName'=> $_POST['domain'],
			'IsDedicated' => $params["customfields"]["IsDedicated"]
        ),
    );
    return $pagearray;
}


///////////////////////////////////// - SM Functions created for common use - /////////////////////////////////////
//  Function to call web service
function sm_webServiceCall($url, $paramaters, $ws_name){
  $soap_client = new SoapClient($url);
  $result = $soap_client->__SoapCall($ws_name, array('paramaters' => $paramaters));
//echo "res: ".print_r(sm_toArray($result));
  return sm_toArray($result);
}

function sm_webServiceCall2($url, $paramaters, $ws_name){
  $soap_client = new SoapClient($url);
  $result = $soap_client->__SoapCall($ws_name, array('paramaters' => $paramaters));
//echo "res: ".print_r($result);
  return sm_toArray($result);
}

//Converts JSON Data Structure from .NET Web Service to Array
function sm_toArray($obj){
  $return=array();
  if (is_array($obj)) {
    $keys=array_keys($obj);
  } elseif (is_object($obj)) {
    $keys=array_keys(get_object_vars($obj));
  } else {return $obj;}
  foreach ($keys as $key) {
    $value=$obj->$key;
    if (is_array($obj)) {
      $return[$key]=sm_toArray($obj[$key]);
    } else {
      $return[$key]=sm_toArray($obj->$key);
    } 
  }
  return $return;
}

function addHttpToHostname($url) {
	if(substr($url, 0, 7) != 'http://') {
	  $url = 'http://' . $url . "/";
	}
	return $url;
}
 
?>
