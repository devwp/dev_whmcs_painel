<?php


/**
 * @author Maciej Husak <maciej@modulesgarden.com>
 */

$vm                 = new NewOnApp_VM($_GET['vserver']);
$vm                 ->setconnection($params);
$api      = $vm ->getApi();
$version  = $api->sendGET('/version');
if($vm->available($user_id)){
    $vm_details     = $vm->details;
    $vm_id          = $vm_details['virtual_machine']['id'];
    if(isset($_POST['do'])){
        switch($_POST['do']){
            case 'editVPS':
                    $result = $vm->modify(array('virtual_machine'=> array(
                            'label'     => $_POST['cl_label'],
                            'memory'    => $_POST['cl_memory'],
                            'cpus'      => $_POST['cl_cpu'],      
                            'cpu_shares'=> $_POST['cl_cpu_shares']
                    )));   
            
                    if($vm->isSuccess()){
                        $_SESSION['msg_status'] = $vars['lang']['vm_updated'];
                        $vm->assignIP($_POST['cl_ips'], $vm_details['virtual_machine']['ip_addresses'][0]['ip_address']['network_id'],$vm_details['virtual_machine']['hypervisor_id']);
                        if(!$vm->isSuccess())
                        {
                           $vars['msg_error'] = $vm->error();
                           break;  
                        }
                        if(isset($_POST['cl_password']) && !empty($_POST['cl_password']))
                        {
                            $vm->changePassword(array('virtual_machine'=>array('initial_root_password'=>$_POST['cl_password'])));
                            if(!$vm->isSuccess()){
                                $vars['msg_error'] = $vm->error();
                                break;    
                            }
                        } 
                        $interface = new NewOnApp_NetworkInterface($vm_id);
                        $interface -> setconnection($params);
                        $rate_limit = $interface->getList();
                        foreach ($rate_limit as $val){
                            
                            $data = array(
                                'network_interface' => array(
                                    'rate_limit'        => $_POST['cl_rate_limit'],
                                    'label' => $val['network_interface']['label'],
                                    'primary' => $val['network_interface']['primary'],
                                )
                            );
                            $interface->save($val['network_interface']['id'],$data);
                            if(!$interface->isSuccess()){
                                $vars['msg_error'] = $interface->error();
                                break;    
                            }
                        }
            
                        header("Location: clientarea.php?action=productdetails&id=".$params['serviceid']);
                        die();
                    }    
                    else
                        $vars['msg_error']   = $vm->error();   
                
                break;       
            }
        }
        
    $results = $user->getVMList();
    if($user->isSuccess()){
        $resource                           = array();
        $resource['cpus']['used']           = 0;
        $resource['memory']['used']         = 0;
        $resource['ips']['used']            = 0;
        $resource['rate_limit']['used']     = 0;
        $resource['cpu_shares']['used']     = 0;
        $interface  = new NewOnApp_NetworkInterface(null);
        $interface  ->setconnection($params);

        foreach ($results as $key => $value) {
            $interface->setID($value['virtual_machine']['id']);
            $rate_limit                    = $interface->getList();
            foreach ($rate_limit as $val)
            $resource['rate_limit']['used'] += $val['network_interface']['rate_limit'];
            $resource['cpus']['used']       += $value['virtual_machine']['cpus'];
            $resource['memory']['used']     += $value['virtual_machine']['memory']; 
            $resource['ips']['used']        += count($value['virtual_machine']['ip_addresses']);
            $resource['cpu_shares']['used'] += $value['virtual_machine']['cpu_shares'];
        }
         $billing = new NewOnApp_Billing($search[0]['user']['billing_plan_id']);
         $billing ->setconnection($params);
         if($version['version'] &&  version_compare( $version['version'], "4.2.0", '>='))
            $limits  = $billing->getBaseResources();
         else
            $limits  = $billing->getDetails();
         $resource = OnAppCloud_Resources($limits,$resource);
        
        //OnApp Billing Integration Code
        if(file_exists(ROOTDIR.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'addons'.DIRECTORY_SEPARATOR.'OnAppBilling'.DIRECTORY_SEPARATOR.'core.php'))
        {
            require_once ROOTDIR.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'addons'.DIRECTORY_SEPARATOR.'OnAppBilling'.DIRECTORY_SEPARATOR.'core.php';
            $row = mysql_get_row("SELECT * FROM OnAppBilling_settings WHERE product_id = ? AND enable=1", array($params['packageid']));
            if($row)
            {
                //Enable Pricing For This Product
                $vars['enable_pricing'] = 1;
                //Get Resource Settinngs
                $resource_settings = unserialize($row['resource_settings']);

                //CPUS
                $resource['cpus']['price']      = number_format($resource['cpus']['type']      != 'disabled' ? $resource_settings['cpus']['price']         : 0,    5);
                //Memory
                $resource['memory']['price']    = number_format($resource['memory']['type']    != 'disabled' ? $resource_settings['memory']['price']       : 0,    5);
                //Disk Size
                $resource['disk_size']['price'] = number_format($resource['disk_size']['type'] != 'disabled' ? $resource_settings['disk_size']['price']    : 0,    5);
                //IP Addresses
                $resource['ips']['price']       = number_format($resource['ips']['type']       != 'disabled' ? $resource_settings['ip_addresses']['price'] : 0,    5);
                //Port Speed
                $resource['rate_limit']['price']= number_format($resource['network_rate']['type'] != 'disabled' ? $resource_settings['network_rate']['price'] : 0,    5);
                //CPU Shares
                $resource['cpu_shares']['price'] = number_format($resource['cpu_shares']['type'] != 'disabled' ? $resource_settings['cpu_shares']['price'] : 0,    5);

            }
        }
        //End Of OnApp Billing Intergration Code
        
         $interface->setID($vm_id);
         $vm_rate_limit                    = $interface->getList();
         $vars['rate'] = 0;
         foreach ($vm_rate_limit as $val){
            $vars['rate']+= $val['network_interface']['rate_limit'];
         }
         $resource['cpus']['max'] = !$resource['cpus']['max'] ? 100 :$resource['cpus']['max'] ;
         $resource['cpu_shares']['max']= !$resource['cpu_shares']['max'] ?100:$resource['cpu_shares']['max'];
         $resource['memory']['max'] = !$resource['memory']['max'] ? 51200: $resource['memory']['max'];
         $resource['disk_size']['max'] = !$resource['disk_size']['max'] ? 1000000 : $resource['disk_size']['max'];
         $resource['ips']['max'] =  !$resource['ips']['max'] ? 300 : $resource['ips']['max'];
         $resource['rate_limit']['max'] = !$resource['rate_limit']['max']? 100 :$resource['rate_limit']['max'];
         
         $resource['cpus']['free']          = $resource['cpus']['max']      + $vm_details['virtual_machine']['cpus']                    - $resource['cpus']['used'];
         $resource['cpu_shares']['free']    = $resource['cpu_shares']['max']+ $vm_details['virtual_machine']['cpu_shares']              - $resource['cpu_shares']['used'];
         $resource['memory']['free']        = $resource['memory']['max']    + $vm_details['virtual_machine']['memory']                  - $resource['memory']['used'];
         $resource['ips']['free']           = $resource['ips']['max']       + count($vm_details['virtual_machine']['ip_addresses'])     - $resource['ips']['used'];
         $resource['rate_limit']['free']    = $resource['rate_limit']['max']+ $vars['rate']                                             - $resource['rate_limit']['used'];
         
         $vars['maxes']       = $resource;
    } else $vars['msg_error'] = $user->error();
    $vars['vpsdata']          = $vm_details['virtual_machine'];
} else $vars['msg_error']     = $lang['mainsite']['perm_error'];
    

