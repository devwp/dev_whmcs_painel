<?php


/**
 * @author Maciej Husak <maciej@modulesgarden.com>
 */
if($vars['disallow_action']['firewall']==1){
    ob_clean();
    header ("Location: clientarea.php?action=productdetails&id=".$params['serviceid']."&modop=custom&a=management&page=vmdetails&vserver=".$_GET['vserver']);
    die();
}

$vm                 = new NewOnApp_VM($_GET['vserver']);
$vm                 ->setconnection($params);
if($vm->available($user_id)){
    $vm_details     = $vm->details;
    $vm_id          = $vm_details['virtual_machine']['id'];
    $firewall       = new NewOnApp_FirewallRule($vm_id);
    $firewall       ->setconnection($params);
    $netInterface   = new NewOnApp_NetworkInterface($vm_id);
    $netInterface   ->setconnection($params);

    if(isset($_POST['do'])){
        switch($_POST['do']){
            case 'addRule':
                $data   = array('firewall_rule' => array(
                    'address'               => $_POST['rule']['address'],
                    'command'               => $_POST['rule']['command'],
                    'port'                  => $_POST['rule']['port'],
                    'protocol'              => $_POST['rule']['protocol'],
                    'network_interface_id'  => $_POST['rule']['interface']

                ));

                $firewall->create($data);
                if($firewall->isSuccess())
                    $vars['msg_success'] = $vars['lang']['rule_added'];
                else
                    $vars['msg_error']   = $firewall->error();
            break;
            case 'removeRule':
                if($_POST['rule']>0){
                    $firewall->delete($_POST['rule']);       
                    if($firewall->isSuccess())
                        echo 'success';
                    else
                        echo $firewall->error();
                    die();
                }
            break;    
            case 'removeSelected':
                $vars['msg_success'] = null;
                $vars['msg_error']   = null;
                if(isset($_POST['rule_id']) && count($_POST['rule_id'])>0){
                    foreach($_POST['rule_id'] as $key=>$value){
                        $firewall->delete($value);
                        if($firewall->isSuccess())
                            $vars['msg_success'] .= $vars['lang']['rule_removed']."<br />";
                        else
                            $vars['msg_error']   .= $firewall->error()."<br />";
                    }
                }
            break; 
            case 'defaultRule':
                 if(isset($_POST['defaultRule']) && count($_POST['defaultRule'])>0){
                      $vars['msg_success'] = null;
                      $vars['msg_error']   = null;
                      foreach($_POST['defaultRule'] as $key=>$value){
                           $data   = array('network_interface' => 
                                            array('default_firewall_rule' =>$value['command'])
                                    ); 
                           $netInterface->save($key,$data);
                           if($netInterface->isSuccess())
                                $vars['msg_success'] .= $vars['lang']['default_rule_updated']."<br />";
                           else
                                $vars['msg_error']   .= $netInterface->error()."<br />";
                      }
                 }
            break;
            case 'apply':
                $firewall->apply();
                if($firewall->isSuccess())
                    $vars['msg_success'] .= $vars['lang']['rule_updated']."<br />";
                else
                    $vars['msg_error']   .= $firewall->error()."<br />";
            break;
            case 'saveRule':
                if(isset($_POST['rule']) && $_POST['rule']>0){
                    $data   = array('firewall_rule'=>array(
                        'address'               => $_POST['address'],
                        'command'               => $_POST['command'],
                        'port'                  => $_POST['port'],
                        'protocol'              => $_POST['protocol'],
                        'network_interface_id'  => $_POST['interface']
                    ));

                    $firewall->save($_POST['rule'],$data);
                    if($firewall->isSuccess()){
                        $_SESSION['ajax_msg_status'] = $vars['lang']['success'];
                        die('success');
                    }    
                    else
                        die($firewall->getError());
                }    
                die();
            break;

            default: die();
        }
    }


    //get rules
    $rules                      = $firewall->getList();
    $vars['interfaces']         = $netInterface->getList();

    if(count($vars['interfaces'])==0 && $netInterface->error()){
        $vars['msg_error']      = $netInterface->error();
        $vars['block_form']     = 1;
    } else {

        $vars['list_interfaces']    = array();
        $vars['rules']              = array();

        foreach($vars['interfaces'] as $key=>$val)
            $vars['list_interfaces'][$val['network_interface']['id']] = $val['network_interface']['label'];

        foreach($rules as $key=>$val){
            $val['firewall_rule']['interface_label'] = $vars['list_interfaces'][$val['firewall_rule']['network_interface_id']];
            $vars['rules'][$key]    = $val;
        }
    }

    if(isset($_SESSION['ajax_msg_status'])){
        $vars['msg_success'] = $_SESSION['ajax_msg_status'];
        unset($_SESSION['ajax_msg_status']);
    }
    $vars['vpsdata'] = $vm_details['virtual_machine'];
} else $vars['msg_error'] = $lang['mainsite']['perm_error'];