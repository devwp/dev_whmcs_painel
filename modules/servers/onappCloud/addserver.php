<?php


/**
 * @author Maciej Husak <maciej@modulesgarden.com>
 */
if(isset($_POST['do'])){
    switch($_POST['do']){
        case 'createVPS':
            $vm     = new NewOnApp_VM(null);
            $vm     ->setconnection($params,true);
            
            //check template
            $swap     = 1;
            if(strpos(strtolower($_POST['cl_template']),'windows')!== false)
                    $swap = 0;
            
            $template = explode('|',$_POST['cl_template']);
            $hypervisor_id = $product->getConfig('hypervisor_id');
            if(!$hypervisor_id)
                  $hypervisor_id = $user->getHVfree($product->getConfig('vmware') == 'Yes' ? true : false);
            if(!$hypervisor_id){
                  $vars['msg_error'] = $lang['hyp_error'];
                  break;
            }
                 
            $data = array('virtual_machine'=> array(
                        'label'                                 => $_POST['cl_label'],
                        'memory'                                => $_POST['cl_memory'],
                        'cpu_shares'                            => isset($_POST['cl_cpu_shares'])                       ? $_POST['cl_cpu_shares']           : $product->getConfig('cpu_shares'),
                        'hostname'                              => $_POST['cl_hostname'],
                        'cpus'                                  => $_POST['cl_cpu'],
                        'primary_disk_size'                     => $_POST['cl_primary_disk'],
                        'swap_disk_size'                        => ($swap==1 ? $_POST['cl_swap_disk'] : null),
                        'data_store_group_primary_id'           => isset($_POST['cl_primary_data_store'])               ? $_POST['cl_primary_data_store']   : $product->getConfig('data_store_group_primary_id'),
                        'data_store_group_swap_id'              => ($swap==1 ? isset($_POST['cl_swap_data_store'])      ? $_POST['cl_swap_data_store']      : $product->getConfig('data_store_group_swap_id') : null),
                        'template_id'                           => isset($_POST['cl_template']) ? $template[0] : null,
                        'initial_root_password'                 => $_POST['cl_password'],
                        'hypervisor_id'                         => $hypervisor_id,
                        'hypervisor_group_id'                   => $product->getConfig('hypervisor_zone'),
                        'type_of_format'                        => $product->getConfig('type_of_format'),
                        'rate_limit'                            => isset($_POST['cl_rate_limit'])                       ? $_POST['cl_rate_limit']           : $product->getConfig('rate_limit'),
                        'primary_network_group_id'              => isset($_POST['cl_network_zone'])                     ? $_POST['cl_network_zone']         : $product->getConfig('primary_network_id'),
                        'licensing_type'                        => $product->getConfig('licensing_type'),
                        'licensing_key'                         => isset($_POST['cl_licensing_key'])                    ? $_POST['cl_licensing_key']        : $product->getConfig('licensing_key'),
                        'licensing_server_id'                   => $product->getConfig('licensing_server_id'),
                        'required_virtual_machine_build'        => 1,
                        'required_ip_address_assignment'        => 1,
                        'required_virtual_machine_startup'      => 1,
                        'initial_root_password_confirmation'    => $_POST['cl_password'],
                        'required_automatic_backup'             => $product->getConfig('required_automatic_backup'),           
		));
            
            $result = $vm->create($data);   
            if($vm->error())
                    $vars['msg_error']      = $vm->error();
            else { 
                if($vm->isSuccess()){
                    $vm->setID($result['virtual_machine']['id']);
                    $vm->assignIP($_POST['cl_ips'], $result['virtual_machine']['ip_addresses'][0]['ip_address']['network_id']);
                    $_SESSION['msg_status'] = $vars['lang']['vps_created'];
                    header("Location: clientarea.php?action=productdetails&id=".$params['serviceid']);
                    die();
                }     
                else {
                    $vm->setID($result['virtual_machine']['id']);
                    $vars['msg_error']   = $vm->error();
                    $vm->delete();
                }       
            }
            break;
    }
}


$results = $user->getVMList();
$api =  $user->getApi();
$version  = $api->sendGET('/version');
if($user->isSuccess()){
    $resource                           = array();
    $resource['cpus']['used']           = 0;
    $resource['cpu_shares']['used']     = 0;
    $resource['memory']['used']         = 0;
    $resource['disk_size']['used']      = 0;
    $resource['ips']['used']            = 0;
    $resource['vm']['used']             = 0;
    $resource['backups']['used']        = 0;
    $resource['rate_limit']['used']     = 0;
    $interface  = new NewOnApp_NetworkInterface(null);
    $interface  ->setconnection($params);

    foreach ($results as $key => $value) {
        $interface->setID($value['virtual_machine']['id']);
        $rate_limit                    = $interface->getList();
        foreach ($rate_limit as $val)
            $resource['rate_limit']['used'] += $val['network_interface']['rate_limit'];
        $resource['cpus']['used']       += $value['virtual_machine']['cpus'];
        $resource['memory']['used']     += $value['virtual_machine']['memory'];
        $resource['disk_size']['used']  += $value['virtual_machine']['total_disk_size'];
        $resource['ips']['used']        += count($value['virtual_machine']['ip_addresses']);
        $resource['cpu_shares']['used'] += $value['virtual_machine']['cpu_shares'];
    }
     $billing = new NewOnApp_Billing($search[0]['user']['billing_plan_id']);
     $billing ->setconnection($params);
     if($version['version'] &&  version_compare( $version['version'], "4.2.0", '>='))
         $limits  = $billing->getBaseResources();
     else
         $limits  = $billing->getDetails();
     
     $resource = OnAppCloud_Resources($limits,$resource);

    //OnApp Billing Integration Code
     if(file_exists(ROOTDIR.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'addons'.DIRECTORY_SEPARATOR.'OnAppBilling'.DIRECTORY_SEPARATOR.'core.php'))
     {
         require_once ROOTDIR.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'addons'.DIRECTORY_SEPARATOR.'OnAppBilling'.DIRECTORY_SEPARATOR.'core.php';
         $row = mysql_get_row("SELECT * FROM OnAppBilling_settings WHERE product_id = ? AND enable=1", array($params['packageid']));
         if($row)
         {
             //Enable Pricing For This Product
             $vars['enable_pricing'] = 1;
             //Get Resource Settinngs
             $resource_settings = unserialize($row['resource_settings']);
             
             //CPUS
             $resource['cpus']['price']      = number_format($resource['cpus']['type']      != 'disabled' ? $resource_settings['cpus']['price']         : 0,    5);
             //Memory
             $resource['memory']['price']    = number_format($resource['memory']['type']    != 'disabled' ? $resource_settings['memory']['price']       : 0,    5);
             //Disk Size
             $resource['disk_size']['price'] = number_format($resource['disk_size']['type'] != 'disabled' ? $resource_settings['disk_size']['price']    : 0,    5);
             //IP Addresses
             $resource['ips']['price']       = number_format($resource['ips']['type']       != 'disabled' ? $resource_settings['ip_addresses']['price'] : 0,    5);
             //Port Speed
             $resource['rate_limit']['price']= number_format($resource['network_rate']['type'] != 'disabled' ? $resource_settings['network_rate']['price'] : 0,    5);
             //CPU Shares
             $resource['cpu_shares']['price'] = number_format($resource['cpu_shares']['type'] != 'disabled' ? $resource_settings['cpu_shares']['price'] : 0,    5);
             
         }
     }

  
     //End Of OnApp Billing Intergration Code
     if(isset($limits['billing_plan']['default_base_resources'])){
         foreach($limits['billing_plan']['default_base_resources'] as $key=>$value)
         {          
             if($value['base_resource']['resource_name']=='hypervisor_group'){
                 $resource['cpus']['max']         = empty($value['base_resource']['limits']['limit_cpu']) ? 100 : $value['base_resource']['limits']['limit_cpu'];
                 $resource['memory']['max']       = empty($value['base_resource']['limits']['limit_memory']) ? 51200 : $value['base_resource']['limits']['limit_memory'];
             }
             if($value['base_resource']['resource_name']=='data_store_group'){
                 $resource['disk_size']['max']    = empty($value['base_resource']['limits']['limit']) ? 1000000 : $value['base_resource']['limits']['limit'];
             }
             if($value['base_resource']['resource_name']=='network_group'){
                 $resource['ips']['max']       = empty($value['base_resource']['limits']['limit_ip']) ? 300 : $value['base_resource']['limits']['limit_ip'];
             }

         }
     }else{
         
         $resource['cpus']['max'] = !$resource['cpus']['max'] ? 100 :$resource['cpus']['max'] ;
         $resource['cpu_shares']['max']= !$resource['cpu_shares']['max'] ?100:$resource['cpu_shares']['max'];
         $resource['memory']['max'] = !$resource['memory']['max'] ? 51200: $resource['memory']['max'];
         $resource['disk_size']['max'] = !$resource['disk_size']['max'] ? 1000000 : $resource['disk_size']['max'];
         $resource['ips']['max'] =  !$resource['ips']['max'] ? 300 : $resource['ips']['max'];
         $resource['rate_limit']['max'] = !$resource['rate_limit']['max']? 100 :$resource['rate_limit']['max'];
         
     }

     $resource['cpus']['free']          = $resource['cpus']['max']      - $resource['cpus']['used'];
     $resource['cpu_shares']['free']    = $resource['cpu_shares']['max']- $resource['cpu_shares']['used'];
     $resource['disk_size']['free']     = $resource['disk_size']['max'] - $resource['disk_size']['used'];
     $resource['memory']['free']        = $resource['memory']['max']    - $resource['memory']['used'];
     $resource['ips']['free']           = $resource['ips']['max']       - $resource['ips']['used'];
     $resource['rate_limit']['free']    = $resource['rate_limit']['max']- $resource['rate_limit']['used'];
     if($product->getConfig('rate_limit_vm') > 0)
     {
         $resource['rate_limit']['free'] = $product->getConfig('rate_limit_vm');
     }
     $vars['maxes'] = $resource;
     $vars['swap_disk_size'] = $params['configoptions']['swap_disk_size'] ? $params['configoptions']['swap_disk_size'] : $product->getConfig('swap_disk_size');
     
     //templates
     $available = $product->getConfig('template_ids');
     $template  = new NewOnApp_Template(null);
     $template -> setconnection($params);
     $templates_list = $template->getSystemTemplates();
     if(!$template->error()){
        foreach ($templates_list as $val) {
            if(in_array($val['image_template']['id'], $available)){
                   $vars['templates'][$val['image_template']['id']] = $val['image_template']['label'];
            }
        }  
     }
     asort($vars['templates']);
     
     //data stores
     $dataStoreZone = new NewOnApp_DataStoreZone(null);
     $dataStoreZone -> setconnection($params);
     $list          = $dataStoreZone->getList();
     if(!$dataStoreZone->error())
        foreach ($list as $zone) {
            $zone_name[$zone['data_store_group']['id']] = $zone['data_store_group']['label'];
        }

     $vars['datastores'] = $zone_name;
     /*$dataStoreJoin         =  new NewOnApp_Hypervisor($product->getConfig('hypervisor_id'));
     $dataStoreJoin         -> setconnection($params);
     $data_zones            =  $dataStoreJoin->joinesDataStore();   
     $vars['datastores']    = array();
     if($dataStoreJoin->isSuccess())
     {
        foreach ($data_zones as $zone) 
        {
            $vars['datastores'][$zone['data_store_join']['data_store_id']] = $zone_name[$zone['data_store_join']['data_store_id']];
        }
     }     */
     
     //network zones
     $networkZone          = new NewOnApp_NetworkZone(null);   
     $networkZone         -> setconnection($params);   
     $networks             = $networkZone->getList();
     $vars['networkzones'] = array();
     $zone_name            = array();
     if(!$networkZone->error())
        foreach ($networks as $network) {
         if(is_int($product->getConfig('primary_network_id')) && $network['network_group']['id'] != $product->getConfig('primary_network_id')){
             continue;
         }
         $vars['networkzones'][$network['network_group']['id']] = $network['network_group']['label'];
     }
     

     /*$networks = $dataStoreJoin->networkJoins();
     if($dataStoreJoin->isSuccess())
     {
        foreach ($networks as $network) 
        {
            $vars['networkzones'][$network['network_join']['network_id']] = $zone_name[$network['network_join']['network_id']];
        }
     }*/ 
        
     
} else $vars['msg_error'] = $user->error();
    
    
?>