<?php


/**
 * @author Maciej Husak <maciej@modulesgarden.com>
 */
if($vars['disallow_action']['disk']==1){
    ob_clean();
    header ("Location: clientarea.php?action=productdetails&id=".$params['serviceid']."&modop=custom&a=management&page=vmdetails&vserver=".$_GET['vserver']);
    die();
}

$vm                 = new NewOnApp_VM($_GET['vserver']);
$vm                 ->setconnection($params);
$api =  $vm->getApi();
$version  = $api->sendGET('/version');
if($vm->available($user_id)){
    $disk           = new NewOnApp_Disk();
    $disk           -> setconnection($params,true);
    $backup         = new NewOnApp_VMBackup();
    $backup         ->setconnection($params,true);
    $data_store     = new NewOnApp_DataStore(null);
    $data_store     ->setconnection($params,true);
    $data_stores    = $data_store->getList();
    $stores         = array();
    $display_zones  = $product->getConfig('display_zones');
    
    $disk_list      = $disk->getList($_GET['vserver']);
    
    if($disk->isSuccess())
    {
        foreach($disk_list as $d)
        {
            if($d['disk']['data_store_id'] > 0)
            {
                $dsID = $d['disk']['data_store_id'];
                break;
            }
        }
    }
    
    if(!$data_store->isSuccess())
    {
        $vars['msg_error'] = $data_store->error();
        return;
    }
    
    foreach($data_stores  as $key =>$value){
        if($value['data_store']['data_store_type']!='vmware')
            $stores[$value['data_store']['id']] = $value['data_store']['label'];
    }
    //resources
    $resource                           = array();
    $resource['disk_size']['used']      = 0;
    $results = $user->getVMList();
    foreach ($results as $key => $value) {
        $resource['disk_size']['used']  += $value['virtual_machine']['total_disk_size'];
    }
    $userDet = $user->getDetails();
    $billing = new NewOnApp_Billing($userDet['user']['billing_plan_id']);
    $billing ->setconnection($params);
    if($version['version'] &&  version_compare( $version['version'], "4.2.0", '>='))
        $limits  = $billing->getBaseResources();
    else
        $limits  = $billing->getDetails();
    $resource = OnAppCloud_Resources($limits,$resource);
     
    if(isset($_REQUEST['do'])){
        switch($_REQUEST['do']){
            case 'createBackup':
                if($_POST['disk_id']>0){
                    $backup->create($_POST['disk_id']);       
                    if($backup->isSuccess())
                        $vars['msg_success'] = $vars['lang']['backup_created'];
                    else
                        $vars['msg_error']   =  $backup->error();
                }
                break;    
            case 'addDisk':
                $vars['step']   = 'addDisk';
                $vars['stores'] = $stores;
                break;
            case 'deleteDisk':
                if($_POST['disk_id']>0){
                    $disk->deleteDisk($_GET['vserver'],$_POST['disk_id']);
                    if($disk->isSuccess()){
                        $vars['msg_success'] = $vars['lang']['disk_removed'];
                    } else $vars['msg_error'] = $disk->error();
                }
                break;
            case 'saveDisk':
                if(isset($_POST['add'])){
                    $free= $resource['disk_size']['max'] - $resource['disk_size']['used'] > 0 ? $resource['disk_size']['max'] - $resource['disk_size']['used'] : 0;
                    if($resource['disk_size']['max'] && $resource['disk_size']['used'] + $_POST['add']['size'] >  $free ){
                        $vars['msg_error'] = sprintf($vars['lang']['disk_size_error'],  $_POST['add']['size'],  $free);
                        break;
                    }
                    
                    $dsID   = ($display_zones == 1) ? $_POST['add']['data_store'] : $dsID;
                    
                    $disk->addDisk($_GET['vserver'],array(
                        'disk' => array (
                            'data_store_id'         => $dsID,
                            'disk_size'             => $_POST['add']['size'],
                            'label'                 => $_POST['add']['label'],
                            'is_swap'               => $_POST['add']['is_swap'] == 1 ? 1 : 0,
                            'mount_point'           => $_POST['add']['mount_point'],
                            'require_format_disk'   => $_POST['add']['require_format_disk'] == 1 ? 1 : 0,
                            'add_to_linux_fstab'    => $_POST['add']['add_to_linux_fstab'] == 1 ? 1 : 0,
                            'min_iops'              => 100
                        )
                    ));
                    if($disk->isSuccess()){
                        $vars['msg_success']  = $vars['lang']['disk_added']; 
                    } else $vars['msg_error'] = $disk->error();
                }
                break;
        }
    }


    $disks   = $disk ->getList($_GET['vserver']);
    $value   = array();
    foreach($disks as $key=>$value){
        $disk_backups = $backup->getDiskBackup($_GET['vserver'], $value['disk']['id']);
        $value['disk']['data_store_label'] = $stores[$value['disk']['data_store_id']];
        $value['disk']['count_backups']     = count($disk_backups);
        $vars['disks'][] = $value;
    }
    $vars['vpsdata']['id'] = (int)$_GET['vserver']; 
    $vars['display_zones'] = $display_zones;
} else $vars['msg_error'] = $lang['mainsite']['perm_error'];



