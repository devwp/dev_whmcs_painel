<?php


/**
 * @author Maciej Husak <maciej@modulesgarden.com>
 */
$vm = new NewOnApp_VM($_GET['vserver']);
$vm->setconnection($params);
if ($vm->available($user_id)) {
    $vm_details   = $vm->details;     
    if(isset($_POST['do']) && $_POST['do']=='rebuildVPS'){
        if(isset($_POST['template']) && $_POST['template']>0){
            $postData = array();
            $postData['virtual_machine'] = array('template_id' => $_POST['template']);
            if($product->getConfig('licensing_type')){
                $postData['virtual_machine']['licensing_type'] = $product->getConfig('licensing_type');
            }
            $vm       -> rebuild($postData);
            if($vm->isSuccess())
            {
               $_SESSION['msg_status'] = $lang['rebuild']['vps_rebuilded'];
               header("Location: clientarea.php?action=productdetails&id=".$params['serviceid']."&modop=custom&a=management&page=vmdetails&vserver=".(int)$_GET['vserver']);
               die();
            } else $vars['msg_error'] = $vm->error();
        } else $vars['msg_error'] = $lang['rebuild']['select_template'];
    }
    
    $available = $product->getConfig('template_ids');
    $template  = new NewOnApp_Template(null);
    $template -> setconnection($params);
    $templates_list = $template->getSystemTemplates();
    if(!$template->error()){
        foreach ($templates_list as $val) {
            if(in_array($val['image_template']['id'], $available)){
                   $vars['templates'][$val['image_template']['id']] = $val['image_template']['label'];
            }
        }  
    }
    asort($vars['templates']);
    $vars['vpsdata'] = $vm_details['virtual_machine'];
}
else
    $vars['msg_error'] = $lang['mainsite']['perm_error'];


