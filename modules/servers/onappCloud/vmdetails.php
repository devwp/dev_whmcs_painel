<?php

  
/**
 * @author Maciej Husak <maciej@modulesgarden.com>
 */
$vm = new NewOnApp_VM($_GET['vserver']);
$vm->setconnection($params);
if ($vm->available($user_id)) {
    $vm_details = $vm->details;
    $vm_id = $vm_details['virtual_machine']['id'];
    /* AJAX START */
    if (isset($_POST['ajax']) && $_POST['ajax'] == 1 && isset($_POST['doAction'])) {
   
        $allowed = array('reboot', 'rebuild', 'recovery', 'stop', 'start', 'shutdown', 'recovery', 'details','logs','showPass', 'changePass', 'changePassForm');
        if (!in_array($_POST['doAction'], $allowed))
            die('Action not supported!');
        $getParams = array();
        $method = 'POST';
        $postData = array();
        switch($_POST['doAction']){
            case 'recovery':
                if($product->getConfig('vmware')=='Yes')
                   die('Method not allowed for VMware.');
                $res = $vm->recovery();
                break;  
            case 'reboot':
                $res = $vm->reboot();
                break;
            case 'rebuild':
                $postData = array('virtual_machine'=>array('template_id'=>$product->getConfig('template_id')));
                $res = $vm->rebuild($postData);
                break;
            case 'stop':
                $res = $vm->stop();
                break;
            case 'start':
                $res = $vm->start();
                break;
            case 'shutdown':
                $res = $vm->shutdown();
                break;
            case 'unlock':
                $res = $vm->unlock();
                break;
            case 'logs':
                    $transactions = $vm->getTransactions(empty($_GET['lp']) ? 0 : (int)$_GET['lp']);
                     if($vm->isSuccess()){
                         echo json_encode($transactions);
                         die();
                     }
                break;
                
            case 'showPass':
            case 'changePassForm':
            case 'details':
                $res = $vm->getDetails();
                break;
            case 'changePass':
               
                          $data    = array('virtual_machine'=> array(
                                 'initial_root_password'  => $_REQUEST['onapp_root_password'],
                              ));
                        
                        $vm -> changePassword($data);
                        if($vm->isSuccess()){
                                 sleep(2);
                                 echo json_encode(array("result" => 1, "msg" => $lang['vmdetails']['change_pass_success'] ));
                                 die();
                        }
                break;
      }   
     if($vm->isSuccess()){
        $res['virtual_machine']['monthly_bandwidth_used'] = round($res['virtual_machine']['monthly_bandwidth_used']/1024,2);
        die(json_encode ($res['virtual_machine'])); 
     }  
     else
        die(json_encode (array('error'=>$vm->error())));
    }
    /* AJAX END */
    $transactions = $vm->getTransactions((isset($_GET['lp']) && $_GET['lp']>0 ? (int)$_GET['lp'] : 0));
    if($vm->isSuccess()){
        $vars['curr_log_page'] = (isset($_GET['lp']) && $_GET['lp']>0 ? (int)$_GET['lp'] : 0);
        $vars['logs'] = $transactions;
    }
    /** depracted
    $html5   = $product->getConfig('console');
    if($html5!=1){
        $console = $vm ->getConsoleKey();
        $vars['console'] = array(
           'url' => 'http'.($params['serversecure']=='on' ? 's' :'').'://'.(empty($params['serverhostname']) ? $params['serverip'] : $params['serverhostname']),
           'key' => $console['remote_access_session']['remote_key']
        );        
    } else {
        $vars['console'] = array('html5' => 1);
    }   
     */
    $vars['console'] = array('html5' => 1);
    if(isset($_SESSION['msg_status'])){
       $vars['result']    = 'success';
       $vars['resultmsg'] = $_SESSION['msg_status'];
       unset($_SESSION['msg_status']);
    }
    $vars['vpsdata']    = $vm_details['virtual_machine'];
    $vars['vpsdata']['monthly_bandwidth_used'] = round($vars['vpsdata']['monthly_bandwidth_used']/1024,2);
    
    $disk = new NewOnApp_Disk();
    $disk -> setconnection($params);
    $disks = $disk ->getList($vm_id);
    $vars['vpsdata']['primary'] = 0;
    $vars['vpsdata']['swap']    = 0;
    foreach($disks as $value){
        if(!$value['disk']['is_swap']){
            $vars['vpsdata']['primary'] +=$value['disk']['disk_size'];
        }
        else {
            $vars['vpsdata']['swap'] +=$value['disk']['disk_size'];
        }
    }  
    
    $vars['hide_cpu'] = $product->getConfig('show_cpu_share');
    
}
else{
        $vars['resultmsg'] = $lang['mainsite']['perm_error'];
        $vars['result']     = 1;
}
        