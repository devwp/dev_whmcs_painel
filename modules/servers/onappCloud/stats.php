<?php


/**
 * @author Maciej Husak <maciej@modulesgarden.com>
 */
if($vars['disallow_action']['stats']==1){
    ob_clean();
    header ("Location: clientarea.php?action=productdetails&id=".$params['serviceid']."&modop=custom&a=management&page=vmdetails&vserver=".$_GET['vserver']);
    die();
}

$vm  = new NewOnApp_VM($_GET['vserver']);
$vm  ->setconnection($params);
if($vm->available($user_id)){      
    $vm_details = $vm->details;
    $charts     = $vm->getUsageCPUChart();
    if($vm->isSuccess()){
        $vars['hostname']   = $vm->getHostname($params);
        $vars['chart']      = $charts;
    }else {
        $vars['chart']      = null;
        $vars['msg_error']  = $vm->getError();   
    }
    $vars['vpsdata']        = $vm_details['virtual_machine'];
} else $vars['msg_error'] = $lang['mainsite']['perm_error'];


