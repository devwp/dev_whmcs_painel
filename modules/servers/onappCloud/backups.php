<?php


/**
 * @author Maciej Husak <maciej@modulesgarden.com>
 */
if($vars['disallow_action']['backups']==1){
    ob_clean();
    header ("Location: clientarea.php?action=productdetails&id=".$params['serviceid']."&modop=custom&a=management&page=vmdetails&vserver=".$_GET['vserver']);
    die();
}

$vm                 = new NewOnApp_VM($_GET['vserver']);
$vm                 ->setconnection($params);
if($vm->available($user_id)){
    $vm_details     = $vm->details;
    $vm_id          = $vm_details['virtual_machine']['id'];
    $backup           = new NewOnApp_VMBackup();
    $backup           -> setconnection($params);

    if(isset($_POST['do'])){
        switch($_POST['do']){
            case 'removeBackup':
                if($_POST['backup_id']>0){
                    $backup->delete($_POST['backup_id']);       
                    if($backup->isSuccess())
                        $vars['msg_success'] = $vars['lang']['backup_deleted'];
                    else
                        $vars['msg_error']   =  $backup->error();
                }
            break;    
            case 'restoreBackup':
                if($_POST['backup_id']>0){
                    $backup->restore($_POST['backup_id']);
                    if($backup->isSuccess())
                        $vars['msg_success'] = $vars['lang']['backup_restored'];
                    else
                        $vars['msg_error']   = $backup->error();
                }
            break;
            case 'create_templateBackup':
               /* if($_POST['backup_id']>0){]
                    $data = array('backup'=>array('label'=>$product->getConfig('backup_label'),'min_disk_size'=>$product->getConfig('backup_min_disk_size'))))
                    $backup->convert($_POST['backup_id'],$data);
                    if($backup->isSuccess())
                        $vars['msg_success'] = $vars['lang']['rebuilded'];
                    else
                        $vars['msg_error']   = $backup->error();
                }*/
            break;
        }
    }

    $backup_servers   = $backup -> getServerList();
    $backups          = $backup -> getList($vm_id);
    $servers          = array();
    $vars['backups']  = array();

    foreach($backup_servers  as $key =>$value)
        $servers[$value['backup_server']['id']] = $value['backup_server']['label'];

    foreach($backups as $key=>$value){
        $value['backup']['server_label'] = $servers[$value['backup']['backup_server_id']];
        $vars['backups'][] = $value;
    }
    $vars['vpsdata']      = $vm_details['virtual_machine'];
} else $vars['msg_error'] = $lang['mainsite']['perm_error'];    