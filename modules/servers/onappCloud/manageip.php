<?php


/**
 * @author Maciej Husak <maciej@modulesgarden.com>
 */
if($vars['disallow_action']['ip']==1){
    ob_clean();
    header ("Location: clientarea.php?action=productdetails&id=".$params['serviceid']."&modop=custom&a=management&page=vmdetails&vserver=".$_GET['vserver']);
    die();
}

$vm                 = new NewOnApp_VM($_GET['vserver']);
$vm                 ->setconnection($params);
if($vm->available($user_id)){
    $vm_details     = $vm->details;
    $vm_id          = $vm_details['virtual_machine']['id'];
    $network        = new NewOnApp_Network($vm_details['virtual_machine']['ip_addresses'][0]['ip_address']['network_id']);
    $network        -> setconnection($params);
    $network        -> setVM($vm_id);

    $ipAddress      = new NewOnApp_IPAddressJoin($vm_id);
    $ipAddress      -> setconnection($params);

    if(isset($_POST['do'])){
        switch($_POST['do']){
            case 'rebuildNetwork':
                $data   = array('shutdown_type'=>'hard','required_startup'=>1);
                $network->rebuild($data);
                if($network->isSuccess())
                    $vars['msg_success'] = $vars['lang']['rebuilded'];
                else
                    $vars['msg_error']   = $network->error();
            break;
        }
    }

    
    
    switch($_REQUEST['doAction']){
        
         case 'add_ip_address':
            
             if(isset($_POST['ip'])){
                 $ip_join    = new NewOnApp_IPAddressJoin($vm_id);
                 $ip_join->setapi($vm->getApi());
                 $hypervisor_id = $vm_details['virtual_machine']['hypervisor_id'];
                 if($hypervisor_id){
                    $hypervisor = new NewOnApp_Hypervisor($hypervisor_id);
                    $hypervisor ->setconnection($params);
                    $res = $hypervisor->details();
                    $zoneId = $res['hypervisor']['hypervisor_group_id'];
                    $hypZone = new NewOnApp_HypervisorZone( $zoneId );
                    $hypZone ->setconnection($params);
                    $res = $hypZone->networkJoins();
                    $intDet = $vm->getNetworkInterface($_POST['ip']['network_interface_id']);
                    foreach($res as $key=>$value){
                        if($value['network_join']['id'] == $intDet['network_interface']['network_join_id']){
                            $network_id = $value['network_join']['network_id'];
                            break;
                        }
                    }
                }
                if(!$network_id){
                    $vars['msg_error'] ="Primary network has not been found";
                }else{
                     $ip_address = new NewOnApp_IPAddress($network_id);
                     $ip_address->setconnection($params);
                     $ip_pools = $ip_address->getList();

                     $cn_ip = (int) $_POST['ip']['used_ip'];
                     for ($j = 1; $j <= $cn_ip; $j++) {
                        foreach ($ip_pools as $key => $value) {
                            if ($i == $cn_ip)
                                break;

                            if ($i <= $cn_ip) {
                                if (isset($value['ip_address']['free']) && $value['ip_address']['free'] && empty($value['ip_address']['user_id'])) {
                                    $data = array('ip_address_join' => array('ip_address_id' => $value['ip_address']['id'], 'network_interface_id' => $_POST['ip']['network_interface_id']));
                                    $ip_join->assign($data);
                                    if ($ip_join->isSuccess()) {
                                        $i++;
                                    } else {
                                        $vars['msg_error'] = $ip_join->error();
                                        break;
                                    }
                                } else
                                    continue;
                            }
                        }
                     }
                     if($ip_join->isSuccess())
                         $vars['msg_success'] = $vars['lang']['ipadded'];
                }
              }
              $vars['formAddIp'] = true;
              $vars['interfaces'] =  $vm->getNetworkInterfaces();
              
              break;
          
             
         case 'removeIP':
             $ip_join    = new NewOnApp_IPAddressJoin($vm_id);
             $ip_join->setapi($vm->getApi());
             ob_get_clean();
             $ip_join->delete($_POST['ip_id']);
             if($ip_join->isSuccess())
                 echo "success";
             else
                 echo $ip_join->error ();
             die();
             break;

    }

    $networks               = $network->getList();
    $ip_addresses           = $ipAddress->getList();
    $interfaces = array();
    foreach($vm->getNetworkInterfaces() as $int){
        $interfaces[$int['network_interface']['id']] = $int['network_interface']['label'];
    }
    
    $networks_labels        = array();
    $vars['ip_addresses']   = array();

    
    if(is_array($networks) && count($networks)>0)
    foreach($networks as $key=>$value)
        $networks_labels[$value['network']['id']] = $value['network']['label'];

    if(is_array($ip_addresses) && count($ip_addresses)>0)
    foreach($ip_addresses as $key=>$value){
        $value['ip_address_join']['ip_address']['network_label'] = $networks_labels[$value['ip_address_join']['ip_address']['network_id']];
        $value['interface'] = $interfaces[$value['ip_address_join']['network_interface_id']];
        $vars['ip_addresses'][] = $value;
    }

    $vars['vpsdata'] = $vm_details['virtual_machine'];
} else $vars['msg_error'] = $lang['mainsite']['perm_error'];