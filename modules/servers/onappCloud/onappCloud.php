<?php


/**
 * @author Maciej Husak <maciej@modulesgarden.com>
 */
if (!defined('DS'))
    define('DS', DIRECTORY_SEPARATOR);

include_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'utility.php';
onapp_loadCLass();
include_once dirname(__FILE__)         . DS . 'class'        . DS . 'class.Product.php';
include_once dirname(__FILE__)         . DS . 'class'        . DS . 'class.NewOnAppCloud_User.php';

if (!function_exists('onappCloud_ConfigOptions')) {
    function onappCloud_ConfigOptions($prameters = array()) {
        $moduleVersion  = '1.6.9';
        $ex = explode(DS, $_SERVER['SCRIPT_FILENAME']);
        if ( (empty($prameters) ||  $_GET['action']=='edit') && end($ex) == 'configproducts.php' && isset($_GET['id']) && $_GET['id']) {
            // setup params
            $product = new onappCloud_Product($_GET['id']);
            $params  = $product->getParams();
           
            if (empty($params)) {
                echo '<div class="errorbox"><strong><span class="title">Please setup onappCloud server first</span></strong></div>';
                return array();
            }
            
            $billing       = new NewOnApp_Billing();
            $hpv           = new NewOnApp_Hypervisor(null);
            $hpvZone       = new NewOnApp_HypervisorZone(null);
            $template      = new NewOnApp_Template(null);
            $networkZone   = new NewOnApp_NetworkZone(null);
            $dataStoreZone = new NewOnApp_DataStoreZone(null);
            $userGroup     = new NewOnApp_UserGroup(null); 
            $userRole      = new NewOnApp_UserRole(null);
            $hypervisor  = new NewOnApp_HypervisorZone($product->getConfig('hypervisor_zone'));
            
            $hpv           -> setconnection($params);
            $hpvZone       -> setconnection($params);
            $template      -> setconnection($params);
            $networkZone   -> setconnection($params);
            $dataStoreZone -> setconnection($params);
            $userGroup     -> setconnection($params);
            $userRole      -> setconnection($params);
            $billing       -> setconnection($params);
            $hypervisor   -> setconnection($params);

            $api           = $billing->getApi();
            if(!$api->testConnection())
            {
                echo '<div class="errorbox"><strong><span class="title">'.$api->getError().'</span></strong></div>';
                return array();
            }
            
            $template_groups = $template->getTemplateStores();
            if($template->isSuccess())
            {
                $groups   = array();
                foreach($template_groups as $val)
                {
                    $product->defaultConfig['template_group']['options'][$val['id']]   = $val['label'];
                }   
            }
            
            $hypervisors    = $hypervisor->lisHPV();
             if($hypervisor->isSuccess()){
                    foreach($hypervisors as $key => $val){
                        if($val['hypervisor']['online'] == 1)
                            $product->defaultConfig['hypervisor_id']['options'][$val['hypervisor']['id']] = $val['hypervisor']['label'];
                    }
            }

            $hypervisor_zones = $hpvZone->getZones();
            if(!$hpvZone->error())
                foreach ($hypervisor_zones as $key => $value) {
                    $product->defaultConfig['hypervisor_zone']['options'][$value['hypervisor_group']['id']] = $value['hypervisor_group']['label'];
                }
      
            $templates = $template->getSystemTemplates();
            if(!$template->error())
                foreach ($templates as $template) {
                    $product->defaultConfig['template_ids']['options'][$template['image_template']['id']]                    = $template['image_template']['label'];
                    
                }

            asort($product->defaultConfig['template_ids']['options']);    
                
            $networks  = $networkZone->getList();
            if(!$networkZone->error())
                foreach ($networks as $network) {
                    $product->defaultConfig['primary_network_id']['options'][$network['network_group']['id']] = $network['network_group']['label'];
                }
            
            $data_zones = $dataStoreZone->getList();
            if(!$dataStoreZone->error())
            foreach ($data_zones as $zone) {
                $product->defaultConfig['data_store_group_primary_id']['options'][$zone['data_store_group']['id']] = $zone['data_store_group']['label'];
                $product->defaultConfig['data_store_group_swap_id']['options'][$zone['data_store_group']['id']]    = $zone['data_store_group']['label'];
            }
            
            $user_groups = $userGroup->getList();
            if(!$userGroup->error())
            foreach ($user_groups as $group) {
                $product->defaultConfig['user_group']['options'][$group['user_group']['id']]   = $group['user_group']['label'];
            }
            
            $user_roles = $userRole->getList();
            if(!$userRole->error())
            foreach ($user_roles as $role) {
                $product->defaultConfig['user_role']['options'][$role['role']['id']]   = $role['role']['label'];
            } 
            
            $billing_plans = $billing->getPlans();
            if(!$billing->error()){
                if(count($billing_plans) > 500 || count($billing_plans) == 0)
                {
                    $product->defaultConfig['user_billing_plan']['type'] = 'text';
                } else {
                    foreach ($billing_plans as $plan) {
                        $product->defaultConfig['user_billing_plan']['options'][$plan['billing_plan']['id']]   = $plan['billing_plan']['label'];
                    }
                    natcasesort($product->defaultConfig['user_billing_plan']['options']);
                }
            } else 
            {
                $product->defaultConfig['user_billing_plan']['type'] = 'text';
            }
      
            
            $memory_unit  = $product->getConfig('memory_unit') ? $product->getConfig('memory_unit') : "MB";
            $primary_unit = $product->getConfig('primary_unit') ? $product->getConfig('primary_unit') : "GB";
            $swap_unit    = $product->getConfig('swap_unit') ? $product->getConfig('swap_unit') : "GB";
            
            $scripts = '<script type="text/javascript">
				jQuery(document).ready(function(){
                                        jQuery("select[name=\'customconfigoption[memory_unit]\']").val("'.$memory_unit.'");
                                        jQuery("select[name=\'customconfigoption[primary_unit]\']").val("'.$primary_unit.'");
                                        jQuery("select[name=\'customconfigoption[swap_unit]\']").val("'.$swap_unit.'");
                                        
                                        var group       = jQuery("select[name=\'customconfigoption[template_group][]\']").val();
                                        selectTemplates(group);
                                       
					jQuery("select[name=\'customconfigoption[template_group][]\']").change(function(){
						var group = jQuery(this).val();
                                                selectTemplates(group);
					});
                                        
                                        jQuery("select[name=\'customconfigoption[hypervisor_zone]\']").change(function(){
                                                getHypervisor(jQuery(this).val());

                                        });

				});
                                
                                function getHypervisor(val)
                                {
                                    if(val != "-- not specified --")
                                    {
                                        jQuery("select[name=\'customconfigoption[hypervisor_id]\']").attr("disabled",true);
                                        jQuery.post(window.location.href, {onAppCloud_action:1, action: \'gethypervisors\',zone: val,"packageconfigoption":null, "productid":'.(int)$_GET['id'].'},function(data){
                                                jQuery("select[name=\'customconfigoption[hypervisor_id]\']").attr("disabled",false);
                                                jQuery("select[name=\'customconfigoption[hypervisor_id]\']").html(data);
                                        });
                                    }
                                }

                                function selectTemplates(group)
                                {
                                    jQuery("select[name=\'customconfigoption[template_ids][]\']").attr("disabled",true);
                                    jQuery.post(window.location.href, {"onAppCloud_action":1,action: \'getTemplates\',group:group,"productid":'.(int)$_GET['id'].'},function(data){
                                            if(data!="")
                                                    jQuery("select[name=\'customconfigoption[template_ids][]\']").html(data);
                                            jQuery("select[name=\'customconfigoption[template_ids][]\']").attr("disabled",false);     
                                            
                                    });

                                }

			</script>';
            
             $scripts .='<script type="text/javascript">
                        function setupCustomFields(){
                            jQuery.post(window.location.href, {"onAppCloud_action":1,"action":"onappCloud_setup_custom_fields", "productid":'.(int)$_GET['id'].',"packageconfigoption":null}, function(res){
                                                alert(res.result);
                                    window.location.href = "configproducts.php?action=edit&id='.(int)$_GET['id'].'&tab=3";
                            }, "json");
                            return false;
                        }
                        
                        function setupConfigOptions(){
                            jQuery.post(window.location.href, {"onAppCloud_action":1,"action":"onappCloud_setup_configurable_options", "productid":'.(int)$_GET['id'].'}, function(res){
                                    alert(res.result);
                                    window.location.href = "configproducts.php?action=edit&id='.(int)$_GET['id'].'&tab=4";
                            }, "json");
                            return false;
                        }
                        
                        </script>';
            echo	'<tr>
                            <td class="fieldlabel mg">Configurable Options</td>
                            <td class="fieldarea mg"><a href="#" onclick="setupConfigOptions();return false;">Generate default</a> <a href="" class="so_popup" rel="help-gen-configurable-options.php"><img src="../modules/servers/onappCloud/img/info.png" /></a></td>
                            <td class="fieldlabel mg">Custom Fields</td>
                            <td class="fieldarea mg"><a href="#" onclick="setupCustomFields();return false;">Generate default</a> <a href="" class="so_popup" rel="help-gen-custom-fields.php"><img src="../modules/servers/onappCloud/img/info.png" /></a></td> 
                        </tr>';
            
            echo 	'<style type="text/css">
                        .mgContact {
                                float: right;
                                margin: 0;
                                background-color: #1c4b8c;
                                -moz-border-radius: 5px;
                                -webkit-border-radius: 5px;
                                -o-border-radius: 5px;
                                border-radius: 5px;
                                position: relative;
                                top: -5px;
                                width: 400px;
                                height: 30px;
                                padding: 3px;
                                text-align: center;
                                position: relative;
                                -webkit-border-radius: 5px;
                                -moz-border-radius: 5px;
                                border-radius: 5px;
                                background-color: #1c4b8c;
                        }
                </style>
                <tr><td colspan="4" id="created-by-todelete"></td></tr>
                <script type="text/javascript">
            $(document).ready(function() 
            {
                                        // Created By
                $("#created-by-todelete").closest(".form").before(\'<p id="created-by-mg" style="text-align: left; margin-bottom: 5px; margin-top: 0;"><small>Version '.$moduleVersion.'</small></p>\');

                                        // Now remove this unused row
                $("#created-by-todelete").remove();
           });
                </script>';

            echo $product->renderConfigOptions($scripts);
            return array();
        }
    }
}

/**
* FUNCTION onappCloud_CreateAccount
* Create user & VM
* @params array
* @return string
*/ 
if (!function_exists('onappCloud_CreateAccount')){
	function onappCloud_CreateAccount($params){
                $product = new onappCloud_Product($params['pid'],$params['serviceid']);
                if(!$product->getConfig('user_billing_plan'))
                    return 'Billing plan not specifed.';

                if(!onapp_customFieldExists($params['pid'], 'userid')){
                     $product->setupDefaultCustomFields(); 
                }

                $params['username'] = uniqid ();
                $params['password'] = onapp_pass_generator();
                
                $user    = new NewOnApp_User(null);
                $user    -> setconnection($params);
                $client  = new NewOnApp_Client($params['clientsdetails']['id']);
                $username_prefix = $product->getConfig('user_prefix');
                $username = $username_prefix.str_replace($username_prefix, '', $params['username']);
                $billing  = new NewOnApp_Billing($product->getConfig('user_billing_plan'));
                $billing  ->setconnection($params);
                
                $api =  $user->getApi();
                $version  = $api->sendGET('/version');

                $result = $billing  ->create_copy();
                if($billing->isSuccess()){
                    $billing->setBillingPlanID($result['billing_plan']['id']);
                    //edit billing plan
                    $data = array(
                        'billing_plan'=>array(
                            'label' => 'billing_'.$username,

                        ));
                    $billing->edit($data);
                    if(is_array($result['billing_plan']['default_base_resources']))
                        $res = $result['billing_plan']['default_base_resources'];
                    else $res = array();
                
                    if(is_array($result['billing_plan']['base_resources']));
                        $res += $result['billing_plan']['base_resources'];
               

                if($billing->isSuccess()){
                    
                    $disk_size  = $product->getValueWithUnit(isset($params['configoptions']['primary_disk_size'])  ? $params['configoptions']['primary_disk_size'] : $product->getConfig('primary_disk_size'),  $product->getConfig('primary_unit'), 'GB');
                    $disk_size += $product->getValueWithUnit(isset($params['configoptions']['swap_disk_size'])     ? $params['configoptions']['swap_disk_size']    : $product->getConfig('swap_disk_size'),     $product->getConfig('swap_unit'), 'GB');
                    $cpu = (isset($params['configoptions']['cpus'])     ? $params['configoptions']['cpus']     : $product->getConfig('cpus'));
                    $cpu_shares =(isset($params['configoptions']['cpu_shares']) ? $params['configoptions']['cpu_shares'] : $product->getConfig('cpu_shares'));
                    $memory = $product->getValueWithUnit(isset($params['configoptions']['memory']) ? $params['configoptions']['memory'] : $product->getConfig('memory'), $product->getConfig('memory_unit'));
                    $vm_limit =isset($params['configoptions']['vm_limit']) ? $params['configoptions']['vm_limit'] : $product->getConfig('vm_limit');
                    $backup = isset($params['configoptions']['backup_limits']) ? $params['configoptions']['backup_limits'] : $product->getConfig('backup_limits');
                    $ips   = isset($params['configoptions']['ip_addresses']) ? $params['configoptions']['ip_addresses'] : $product->getConfig('ip_addresses');
                    if($version['version'] &&  version_compare( $version['version'], "4.2.0", '>=')){
                       $data = array(
                                'base_resource' => array(
                                    'resource_class'     => 'Resource::Backup',
                                    'billing_plan_id'    => $result['billing_plan']['id'],
                                    'limits' => array( 'limit'              => $backup ),
                                    )
                                );
                       $billing->add_base_resources($data); 
                       $data = array(
                                'base_resource' => array(
                                    'resource_class'     => 'Resource::VmLimit',
                                    'billing_plan_id'    => $result['billing_plan']['id'],
                                    'limit'              => $vm_limit,
                                    )
                                );
                        $billing->add_base_resources($data);
                            
                       $res  = $billing->getBaseResources();
                       
                       foreach($res as $key=> $val){
                            //Bucket Limits
                            if($val['base_resource']['is_bucket']=="1"){
                                $data = array(
                                    'base_resource' => array(
                                        'limit_cpu'              => $cpu,
                                        'limit_cpu_share'        => $cpu_shares,
                                        'limit_memory'           => $memory,

                                        )
                                    );
                                $billing->edit_base_resources($val['base_resource']['id'],$data);
                            }
                            if($val['base_resource']['resource_name']=='vm_limit')
                            {
                                // set vm limit limit
                                $data = array(
                                    'base_resource' => array(
                                        'limit'              => $vm_limit,
                                        )
                                    );
                                $billing->edit_base_resources($val['base_resource']['id'],$data);
                            }
                            if($val['base_resource']['resource_name']=='backup'){
                                // edit backups limit
                                 $data = array(
                                    'base_resource' => array(
                                        'limit'              => $backup,
                                        )
                                    );
                                $billing->edit_base_resources($val['base_resource']['id'],$data);
                            }
                            if($val['base_resource']['resource_name']=='data_store_group')
                            {
                                // set disk size limit
                                $data = array(
                                    'base_resource' => array(
                                        'limit'              => $disk_size,
                                        )
                                    );
                                $billing->edit_base_resources($val['base_resource']['id'],$data);
                            }
                            if($val['base_resource']['resource_name']=='network_group'){
                                // add ip addresses limit, network limit
                                $data = array(
                                    'base_resource' => array(
                                        'limit_ip'              => $ips,
                                        'limit_rate'              => (isset($params['configoptions']['rate_limit']) ? $params['configoptions']['rate_limit'] : $product->getConfig('rate_limit')),
                                        )
                                    );
                                $billing->edit_base_resources($val['base_resource']['id'],$data);

                            }
                        }
                    } else if($version['version'] &&  version_compare( $version['version'], "4.1.0", '>=')){
                    
                           // add backup limit
                            $data = array(
                                'base_resource' => array(
                                    'resource_class'     => 'Resource::Backup',
                                    'billing_plan_id'    => $result['billing_plan']['id'],
                                    'limits' => array( 'limit'              => (isset($params['configoptions']['backup_limits']) ? $params['configoptions']['backup_limits'] : $product->getConfig('backup_limits'))),
                                    )
                                );
                            $billing->add_base_resources($data);  
                            
                            
                            // add backup limit
                            $data = array(
                                'base_resource' => array(
                                    'resource_class'     => 'Resource::VmLimit',
                                    'billing_plan_id'    => $result['billing_plan']['id'],
                                    'limits' => array( 'limit'              => (isset($params['configoptions']['vm_limit']) ? $params['configoptions']['vm_limit'] : $product->getConfig('vm_limit')),),
                                    )
                                );
                            $billing->add_base_resources($data);
                            
                            // add template stores
                            $stores     = $product->getConfig('template_group');
                            if($stores[0] != 'all')
                            {
                                foreach($stores as $store)
                                {
                                    $data = array(
                                        'base_resource' => array(
                                            'resource_class'     => 'Resource::TemplateGroup',
                                            'billing_plan_id'    => $result['billing_plan']['id'],
                                            'target_type'        => 'ImageTemplateGroup',
                                            'target_id'          => $store
                                        )
                                    );
                                    $billing->add_base_resources($data);
                                }
                            }

                       
                            foreach($res as $key=> $val){
                                if($val['billing_resource_vm_limit']['resource_name']=='vm_limit')
                                {
                                    // set vm limit limit
                                    $data = array(
                                        'base_resource' => array(
                                            'limit'              => (isset($params['configoptions']['vm_limit']) ? $params['configoptions']['vm_limit'] : $product->getConfig('vm_limit')),
                                            )
                                        );
                                    $billing->edit_base_resources($val['billing_resource_vm_limit']['id'],$data);
                                }
                                if($val['billing_resource_backup']['resource_name']=='backup'){
                                    // edit backups limit
                                     $data = array(
                                        'base_resource' => array(
                                            'limit'              => (isset($params['configoptions']['backup_limits']) ? $params['configoptions']['backup_limits'] : $product->getConfig('backup_limits')),
                                            )
                                        );
                                    $billing->edit_base_resources($val['billing_resource_backup']['id'],$data);
                                }
                                if($val['billing_resource_data_store_group']['resource_name']=='data_store_group')
                                {
                                    // set disk size limit
                                    $data = array(
                                        'base_resource' => array(
                                            'limit'              => $disk_size,
                                            )
                                        );
                                    $billing->edit_base_resources($val['billing_resource_data_store_group']['id'],$data);
                                }
                                if($val['billing_resource_hypervisor_group']['resource_name']=='hypervisor_group'){
                                    //set following limits:
                                    // cpu limit, cpu_share, memory
                                    $data = array(
                                        'base_resource' => array(
                                            'limit_cpu'              => (isset($params['configoptions']['cpus'])     ? $params['configoptions']['cpus']     : $product->getConfig('cpus')),
                                            'limit_cpu_share'        => (isset($params['configoptions']['cpu_shares']) ? $params['configoptions']['cpu_shares'] : $product->getConfig('cpu_shares')),
                                            'limit_memory'           => $product->getValueWithUnit(isset($params['configoptions']['memory']) ? $params['configoptions']['memory'] : $product->getConfig('memory'), $product->getConfig('memory_unit')),
                                            )
                                        );
                                    $billing->edit_base_resources($val['billing_resource_hypervisor_group']['id'],$data);
                                }
                                if($val['billing_resource_network_group']['resource_name']=='network_group'){
                                    // add ip addresses limit, network limit
                                    $data = array(
                                        'base_resource' => array(
                                            'limit_ip'              => (isset($params['configoptions']['ip_addresses']) ? $params['configoptions']['ip_addresses'] : $product->getConfig('ip_addresses')),
                                            'limit_rate'              => (isset($params['configoptions']['rate_limit']) ? $params['configoptions']['rate_limit'] : $product->getConfig('rate_limit')),
                                            )
                                        );
                                    $billing->edit_base_resources($val['billing_resource_network_group']['id'],$data);

                                }
                                
                            }
                    }else{ // Ona APP < 4.1
                        

                             // add backup limit
                            $data = array(
                                'base_resource' => array(
                                    'resource_class'     => 'Resource::Backup',
                                    'billing_plan_id'    => $result['billing_plan']['id'],
                                    'limit'              => (isset($params['configoptions']['backup_limits']) ? $params['configoptions']['backup_limits'] : $product->getConfig('backup_limits')),
                                    )
                                );
                            $billing->add_base_resources($data);    

                            // add vm limit
                            $data = array(
                                'base_resource' => array(
                                    'resource_class'     => 'Resource::VmLimit',
                                    'billing_plan_id'    => $result['billing_plan']['id'],
                                    'limit'              => (isset($params['configoptions']['vm_limit']) ? $params['configoptions']['vm_limit'] : $product->getConfig('vm_limit')),
                                    )
                                );
                            $billing->add_base_resources($data);

                            // add template stores
                            $stores     = $product->getConfig('template_group');

                            if($stores[0] != 'all')
                            {
                                foreach($stores as $store)
                                {
                                    $data = array(
                                        'base_resource' => array(
                                            'resource_class'     => 'Resource::TemplateGroup',
                                            'billing_plan_id'    => $result['billing_plan']['id'],
                                            'target_type'        => 'ImageTemplateGroup',
                                            'target_id'          => $store
                                        )
                                    );
                                    $billing->add_base_resources($data);
                                }
                            }


                            foreach($res as $key=> $val){
                                if($val['base_resource']['resource_name']=='data_store_group')
                                {
                                    // set disk size limit
                                    $data = array(
                                        'base_resource' => array(
                                            'limit'     => $disk_size,
                                            )
                                        );
                                    $billing->edit_base_resources($val['base_resource']['id'],$data);
                                }

                                if($val['base_resource']['resource_name']=='hypervisor_group'){
                                    //set cpu limit
                                    $data = array(
                                        'base_resource' => array(
                                            'limit_cpu'              => (isset($params['configoptions']['cpus'])     ? $params['configoptions']['cpus']     : $product->getConfig('cpus')),
                                            )
                                        );
                                    $billing->edit_base_resources($val['base_resource']['id'],$data);

                                     // set cpu_share limit
                                    $data = array(
                                        'base_resource' => array(
                                            'limit_cpu_share'        => (isset($params['configoptions']['cpu_shares']) ? $params['configoptions']['cpu_shares'] : $product->getConfig('cpu_shares')),
                                            )
                                        );
                                    $billing->edit_base_resources($val['base_resource']['id'],$data);

                                    // set memory limit
                                    $data = array(
                                        'base_resource' => array(
                                            'limit_memory'           => $product->getValueWithUnit(isset($params['configoptions']['memory']) ? $params['configoptions']['memory'] : $product->getConfig('memory'), $product->getConfig('memory_unit')),
                                            )
                                        );
                                    $billing->edit_base_resources($val['base_resource']['id'],$data);

                                }

                                if($val['base_resource']['resource_name']=='network_group'){
                                    // add ip addresses limit
                                    $data = array(
                                        'base_resource' => array(
                                            'limit_ip'              => (isset($params['configoptions']['ip_addresses']) ? $params['configoptions']['ip_addresses'] : $product->getConfig('ip_addresses')),
                                            )
                                        );
                                    $billing->edit_base_resources($val['base_resource']['id'],$data);

                                    // add network limit
                                    $data = array(
                                        'base_resource' => array(
                                            'limit_rate'              => (isset($params['configoptions']['rate_limit']) ? $params['configoptions']['rate_limit'] : $product->getConfig('rate_limit')),
                                            )
                                        );
                                    $billing->edit_base_resources($val['base_resource']['id'],$data);

                                }

                                if($val['base_resource']['resource_name']=='vm_limit'){
                                    // edit vm limit
                                    $data = array(
                                        'base_resource' => array(
                                            'limit'              => (isset($params['configoptions']['vm_limit']) ? $params['configoptions']['vm_limit'] : $product->getConfig('vm_limit')),
                                            )
                                        );
                                    $billing->edit_base_resources($val['base_resource']['id'],$data);
                                } 

                                if($val['base_resource']['resource_name']=='backup'){
                                    // edit vm limit
                                    $data = array(
                                        'base_resource' => array(
                                            'limit'              => (isset($params['configoptions']['backup_limits']) ? $params['configoptions']['backup_limits'] : $product->getConfig('backup_limits'))
                                            )
                                        );
                                    $billing->edit_base_resources($val['base_resource']['id'],$data);
                                }
                        }
                    }
               }
                else 
                    return $billing->error ();

                //create new user
                $newmail = $_SERVER['HTTP_HOST']=="192.168.56.101" ? $username.'@dev.modulesgarden-demo.com' :  $username.'@'.$_SERVER['HTTP_HOST'];
                $data = array(
                        'user' => array(
                            'login'             => $username,
                            'first_name'        => $params['clientsdetails']['firstname'],
                            'last_name'         => $params['clientsdetails']['lastname'],
                            'email'             => $newmail,
                            'password'          => $params['password'],
                            'role_ids'          => array($product->getConfig('user_role')),
                            'status'            => 'active',
                            'billing_plan_id'   => $result['billing_plan']['id'],
                            'user_group_id'     => $product->getConfig('user_group')
                        )
                 );    
                 $result = $user->create($data);
                 if($user->error()){
                        return $user->error();
                 } else 
                {
                    $search   = $user->search($username);
                    if($user->isSuccess()){
                        $user_id  = $search[0]['user']['id'];
                        onapp_addCustomFieldValue('userid', $params['pid'],$params['serviceid'],$user_id);
                    }  

                    $client -> updateHostingUsername($username,$params['serviceid'],$params['pid']);
                    $client -> updateHostingPassword($params['password'],$params['serviceid'],$params['pid']);
                    return 'success';
                 }    
    } else return $billing->error();
}
}

/**
* FUNCTION onappCloud_TerminateAccount
* Remove user
* @params array
* @return string
*/ 
function onappCloud_TerminateAccount($params){	

    
    if(!empty($params['customfields']['userid'])){
        $user    = new NewOnApp_User($params['customfields']['userid']);
        $user    ->setconnection($params);
    }else{
        $user    = new NewOnApp_User(null);
        $user    ->setconnection($params);
        if(empty($params['username']))
            return 'Username is empty';
        $search  = $user->search($params['username']);
        if(!$user->isSuccess())
            return $user->error();
         $user->setUserID($search[0]['user']['id']);
    }
    
    $user->delete(array('force'=>1));
    if($user->isSuccess()){
        onapp_addCustomFieldValue('userid', $params['pid'],$params['serviceid'],'');
        /*$billing  = new NewOnApp_Billing($search[0]['user']['billing_plan_id']);
        $billing  ->setconnection($params);
        $billing  ->delete();
        */
        $onapCloudUser = new NewOnAppCloud_User($params['clientsdetails']['userid']);
        $onapCloudUser->delete();
        return 'success';
    }    
    else
        return $user->error();

}

/**
* FUNCTION onappCloud_SuspendAccount
* Disable VM
* @params array
* @return string
*/ 
if (!function_exists('onappCloud_SuspendAccount')){
	function onappCloud_SuspendAccount($params){	
            $user    = new NewOnApp_User(null);
            $user    ->setconnection($params);
            $search  = $user->search($params['username']);
            if(!$user->isSuccess())
                return $user->error();
            else {
                $user->setUserID($params['customfields']['userid']);
                $user->suspend();
                if($user->isSuccess())
                    return 'success';
                else
                    return $user->error();
            }
        }
}


/**
* FUNCTION onappCloud_UnsuspendAccount
* Enable VM
* @params array
* @return string
*/ 
if (!function_exists('onappCloud_UnspendAccount')){
	function onappCloud_UnsuspendAccount($params){	
            $user    = new NewOnApp_User(null);
            $user    ->setconnection($params);
            $search  = $user->search($params['username']);
            if(!$user->isSuccess())
                return $user->error();
            else {
                $user->setUserID($params['customfields']['userid']);
                $user->active();
                if($user->isSuccess())
                    return 'success';
                else
                    return $user->error();
            }
        }
}


/**
* FUNCTION onappCloud_ChangePackage
* Modify VM
* @params array
* @return string
*/ 
if (!function_exists('onappCloud_ChangePackage')){
	function onappCloud_ChangePackage($params){	
            $product = new onappCloud_Product($params['pid']);
            $user    = new NewOnApp_User(null);
            $user    ->setconnection($params);
            $api =  $user->getApi();
            $version  = $api->sendGET('/version');
            $search  = $user->search($params['username']);
            if(!$user->isSuccess())
                return $user->error();
            else {
                //create billing plan
                $data = array(
                    'billing_plan'=>array(
                        'label' => 'billing_'.$params['username'],
                        
                    ));
                $billing  = new NewOnApp_Billing($search[0]['user']['billing_plan_id']);
                $billing  ->setconnection($params);    
                
                $result   = $billing->getDetails();

                if($billing->isSuccess()){
                    $user           -> setUserID($params['customfields']['userid']);
                    $user_details   = $user->getDetails();
                    $vm_list        = $user->getVMList();
                    
                    if($user->isSuccess())
                    {
                        $used   = array(
                            'cpus'      => $user_details['user']['used_cpus'],
                            'memory'    => $user_details['user']['used_memory'],
                            'shares'    => $user_details['user']['used_cpu_shares'],
                            'disk'      => $user_details['user']['used_disk_size'],
                            'ips'       => count($user_details['user']['used_ip_addresses']),
                            'vms'       => count($vm_list)
                        );
                    }

                    // edit billing plan
                    if(is_array($result['billing_plan']['default_base_resources']))
                        $res = $result['billing_plan']['default_base_resources'];
                    else $res = array();
                    if(is_array($result['billing_plan']['base_resources']));
                        $res += $result['billing_plan']['base_resources'];
                            
                    $disk_size  = $product->getValueWithUnit(isset($params['configoptions']['primary_disk_size'])  ? $params['configoptions']['primary_disk_size'] : $product->getConfig('primary_disk_size'),  $product->getConfig('primary_unit'), 'GB');
                    $disk_size += $product->getValueWithUnit(isset($params['configoptions']['swap_disk_size'])     ? $params['configoptions']['swap_disk_size']    : $product->getConfig('swap_disk_size'),     $product->getConfig('swap_unit'), 'GB');
                        
                    $cpu        = (isset($params['configoptions']['cpus'])          ? $params['configoptions']['cpus']          : $product->getConfig('cpus'));
                    $cpu_shares = (isset($params['configoptions']['cpu_shares'])    ? $params['configoptions']['cpu_shares']    : $product->getConfig('cpu_shares'));
                    $ips        = (isset($params['configoptions']['ip_addresses'])  ? $params['configoptions']['ip_addresses']  : $product->getConfig('ip_addresses'));
                    $memory     = $product->getValueWithUnit(isset($params['configoptions']['memory']) ? $params['configoptions']['memory'] : $product->getConfig('memory'), $product->getConfig('memory_unit'), 'MB');
                    $vm_limit   = (isset($params['configoptions']['vm_limit']) ? $params['configoptions']['vm_limit'] : $product->getConfig('vm_limit'));
                    
                    
                    if($disk_size < $used['disk'])
                    {
                        return 'Unable to change package, disk size is too small';
                    }
                    
                    if($cpu < $used['cpus'])
                    {
                        return 'Unable to change package, cpu is too small';
                    }
                    
                    if($cpu_shares < $used['shares'])
                    {
                        return 'Unable to change package, cpu shares is too small';
                    }
                    
                    if($ips < $used['ips'])
                    {
                        return 'Unable to change package, number of ips is too small';
                    }
                    
                    if($memory < $used['memory'])
                    {
                        return 'Unable to change package, memory is too small';
                    }
                    
                    if($vm_limit < count($vm_list))
                    {
                        return 'Unable to change package, number of virtual machines is too small';
                    }
                    if($version['version'] &&  version_compare( $version['version'], "4.2.0", '>=')){
                        
                       $data = array(
                                'base_resource' => array(
                                    'resource_class'     => 'Resource::Backup',
                                    'billing_plan_id'    => $result['billing_plan']['id'],
                                    'limits' => array( 'limit'              => $backup ),
                                    )
                                );
                       $billing->add_base_resources($data); 
                       $data = array(
                                'base_resource' => array(
                                    'resource_class'     => 'Resource::VmLimit',
                                    'billing_plan_id'    => $result['billing_plan']['id'],
                                    'limit'              => $vm_limit,
                                    )
                                );
                        $billing->add_base_resources($data);
                        $res  = $billing->getBaseResources();
                        foreach($res as $key=> $val){
                            //Bucket Limits
                            if($val['base_resource']['is_bucket']=="1"){
                                $data = array(
                                    'base_resource' => array(
                                        'limit_cpu'              => $cpu,
                                        'limit_cpu_share'        => $cpu_shares,
                                        'limit_memory'           => $memory,

                                        )
                                    );
                                $billing->edit_base_resources($val['base_resource']['id'],$data);
                            }
                            if($val['base_resource']['resource_name']=='vm_limit')
                            {
                                // set vm limit limit
                                $data = array(
                                    'base_resource' => array(
                                        'limit'              => $vm_limit,
                                        )
                                    );
                                $billing->edit_base_resources($val['base_resource']['id'],$data);
                            }
                            if($val['base_resource']['resource_name']=='backup'){
                                // edit backups limit
                                 $data = array(
                                    'base_resource' => array(
                                        'limit'              => (isset($params['configoptions']['backup_limits']) ? $params['configoptions']['backup_limits'] : $product->getConfig('backup_limits')),
                                        )
                                    );
                                $billing->edit_base_resources($val['base_resource']['id'],$data);
                            }
                            if($val['base_resource']['resource_name']=='data_store_group')
                            {
                                // set disk size limit
                                $data = array(
                                    'base_resource' => array(
                                        'limit'              => $disk_size,
                                        )
                                    );
                                $billing->edit_base_resources($val['base_resource']['id'],$data);
                            }
                            if($val['base_resource']['resource_name']=='network_group'){
                                // add ip addresses limit, network limit
                                $data = array(
                                    'base_resource' => array(
                                        'limit_ip'              => $ips,
                                        'limit_rate'              => (isset($params['configoptions']['rate_limit']) ? $params['configoptions']['rate_limit'] : $product->getConfig('rate_limit')),
                                        )
                                    );
                                $billing->edit_base_resources($val['base_resource']['id'],$data);

                            }
                        }
                    }
                    else if($version['version'] &&  version_compare( $version['version'], "4.1.0", '>=')){ //4.1
                         // add backup limit
                        $data = array(
                            'base_resource' => array(
                                'resource_class'     => 'Resource::Backup',
                                'billing_plan_id'    => $result['billing_plan']['id'],
                                'limits' => array( 'limit'              => (isset($params['configoptions']['backup_limits']) ? $params['configoptions']['backup_limits'] : $product->getConfig('backup_limits'))),
                                )
                            );
                        
                        $billing->add_base_resources($data);  
                        
                        // add backup limit
                        $data = array(
                            'base_resource' => array(
                                'resource_class'     => 'Resource::VmLimit',
                                'billing_plan_id'    => $result['billing_plan']['id'],
                                'limits' => array( 'limit'              => (isset($params['configoptions']['vm_limit']) ? $params['configoptions']['vm_limit'] : $product->getConfig('vm_limit')),),
                                )
                            );
                        $billing->add_base_resources($data);
                        
                         
                        //4.1
                        foreach($res as $key=> $val){
                            
                            if($val['billing_resource_vm_limit']['resource_name']=='vm_limit')
                            {
                                // set vm limit limit
                                $data = array(
                                    'base_resource' => array(
                                        'limit'              => $vm_limit,
                                        )
                                    );
                                $billing->edit_base_resources($val['billing_resource_vm_limit']['id'],$data);
                            }
                            if($val['billing_resource_backup']['resource_name']=='backup'){
                                // edit backups limit
                                 $data = array(
                                    'base_resource' => array(
                                        'limit'              => (isset($params['configoptions']['backup_limits']) ? $params['configoptions']['backup_limits'] : $product->getConfig('backup_limits')),
                                        )
                                    );
                                $billing->edit_base_resources($val['billing_resource_backup']['id'],$data);
                            }
                            if($val['billing_resource_data_store_group']['resource_name']=='data_store_group')
                            {
                                // set disk size limit
                                $data = array(
                                    'base_resource' => array(
                                        'limit'              => $disk_size,
                                        )
                                    );
                                $billing->edit_base_resources($val['billing_resource_data_store_group']['id'],$data);
                            }
                            if($val['billing_resource_hypervisor_group']['resource_name']=='hypervisor_group'){
                                //set following limits:
                                // cpu limit, cpu_share, memory
                                $data = array(
                                    'base_resource' => array(
                                        'limit_cpu'              => $cpu,
                                        'limit_cpu_share'        => $cpu_shares,
                                        'limit_memory'           => $memory,
                                        
                                        )
                                    );
                                $billing->edit_base_resources($val['billing_resource_hypervisor_group']['id'],$data);
                            }
                            if($val['billing_resource_network_group']['resource_name']=='network_group'){
                                // add ip addresses limit, network limit
                                $data = array(
                                    'base_resource' => array(
                                        'limit_ip'              => $ips,
                                        'limit_rate'              => (isset($params['configoptions']['rate_limit']) ? $params['configoptions']['rate_limit'] : $product->getConfig('rate_limit')),
                                        )
                                    );
                                $billing->edit_base_resources($val['billing_resource_network_group']['id'],$data);

                            }
                        }
                        $stores     = $product->getConfig('template_group');
                        if( is_array($stores))
                            {
                                foreach($stores as $store)
                                {
                                    if($store== 'all') continue;
                                    $data = array(
                                        'base_resource' => array(
                                            'resource_class'     => 'Resource::TemplateGroup',
                                            'billing_plan_id'    => $result['billing_plan']['id'],
                                            'target_type'        => 'ImageTemplateGroup',
                                            'target_id'          => $store
                                        )
                                    );
                                    $billing->add_base_resources($data);
                                }
                            }
                        
                    }else{ //OnApp  version  < 4.1
                        
                        foreach($res as $key=> $val){


                            if($val['base_resource']['resource_name'] == 'template_group')
                            {
                                $billing->delete_base_resource($val['base_resource']['id']);

                            }

                            if($val['base_resource']['resource_name']=='data_store_group')
                            {
                                // set disk size limit
                                $data = array(
                                    'base_resource' => array(
                                        'limit'              => $disk_size,
                                        )
                                    );
                                $billing->edit_base_resources($val['base_resource']['id'],$data);
                            }

                            if($val['base_resource']['resource_name']=='hypervisor_group'){
                                //set cpu limit
                                $data = array(
                                    'base_resource' => array(
                                        'limit_cpu'              => $cpu,
                                        )
                                    );
                                $billing->edit_base_resources($val['base_resource']['id'],$data);

                                 // set cpu_share limit
                                $data = array(
                                    'base_resource' => array(
                                        'limit_cpu_share'        => $cpu_shares,
                                        )
                                    );
                                $billing->edit_base_resources($val['base_resource']['id'],$data);

                                // set memory limit
                                $data = array(
                                    'base_resource' => array(
                                        'limit_memory'           => $memory,
                                        )
                                    );
                                $billing->edit_base_resources($val['base_resource']['id'],$data);

                            }

                            if($val['base_resource']['resource_name']=='network_group'){
                                // add ip addresses limit
                                $data = array(
                                    'base_resource' => array(
                                        'limit_ip'              => $ips,
                                        )
                                    );
                                $billing->edit_base_resources($val['base_resource']['id'],$data);

                                // add network limit
                                $data = array(
                                    'base_resource' => array(
                                        'limit_rate'              => (isset($params['configoptions']['rate_limit']) ? $params['configoptions']['rate_limit'] : $product->getConfig('rate_limit')),
                                        )
                                    );
                                $billing->edit_base_resources($val['base_resource']['id'],$data);

                            }

                            if($val['base_resource']['resource_name']=='vm_limit'){
                                // edit vm limit
                                $data = array(
                                    'base_resource' => array(
                                        'limit'              => $vm_limit,
                                        )
                                    );
                                $billing->edit_base_resources($val['base_resource']['id'],$data);
                            } 

                            if($val['base_resource']['resource_name']=='backup'){
                                // edit backups limit
                                 $data = array(
                                    'base_resource' => array(
                                        'limit'              => (isset($params['configoptions']['backup_limits']) ? $params['configoptions']['backup_limits'] : $product->getConfig('backup_limits')),
                                        )
                                    );
                                $billing->edit_base_resources($val['base_resource']['id'],$data);
                            }

                            
                        }
                    }
                    
                    
                    if($version['version'] &&  version_compare( $version['version'], "4.1.0", '<')){ //OnApp < 4.1
                        //add cpu limit
                        $data = array(
                            'base_resource' => array(
                                'resource_class'     => 'Resource::Cpu',
                                'billing_plan_id'    => $result['billing_plan']['id'],
                                'limit'              => (isset($params['configoptions']['cpus'])     ? $params['configoptions']['cpus']     : $product->getConfig('cpus')),
                                )
                            );
                        $billing->add_base_resources($data);

                        // add cpu_share limit
                        $data = array(
                            'base_resource' => array(
                                'resource_class'     => 'Resource::CpuShare',
                                'billing_plan_id'    => $result['billing_plan']['id'],
                                'limit'              => (isset($params['configoptions']['cpu_shares']) ? $params['configoptions']['cpu_shares'] : $product->getConfig('cpu_shares')),
                                )
                            );
                        $billing->add_base_resources($data);

                        // add memory limit
                        $data = array(
                            'base_resource' => array(
                                'resource_class'     => 'Resource::Memory',
                                'billing_plan_id'    => $result['billing_plan']['id'],
                                'limit'              => $product->getValueWithUnit(isset($params['configoptions']['memory']) ? $params['configoptions']['memory'] : $product->getConfig('memory'), $product->getConfig('memory_unit')),
                                )
                            );
                        $billing->add_base_resources($data);

                        // add disk size limit
                        $data = array(
                            'base_resource' => array(
                                'resource_class'     => 'Resource::DiskSize',
                                'billing_plan_id'    => $result['billing_plan']['id'],
                                'limit'              => $disk_size,
                                )
                            );
                        $billing->add_base_resources($data);

                        // add ip addresses limit
                        $data = array(
                            'base_resource' => array(
                                'resource_class'     => 'Resource::IpAddress',
                                'billing_plan_id'    => $result['billing_plan']['id'],
                                'limit'              => (isset($params['configoptions']['ip_addresses']) ? $params['configoptions']['ip_addresses'] : $product->getConfig('ip_addresses')),
                                )
                            );
                        $billing->add_base_resources($data);


                            // add vm limit
                            $data = array(
                                'base_resource' => array(
                                    'resource_class'     => 'Resource::VmLimit',
                                    'billing_plan_id'    => $result['billing_plan']['id'],
                                    'limit'              => (isset($params['configoptions']['vm_limit']) ? $params['configoptions']['vm_limit'] : $product->getConfig('vm_limit')),
                                    )
                                );
                            $billing->add_base_resources($data);

                            // add backup limit
                            $data = array(
                                'base_resource' => array(
                                    'resource_class'     => 'Resource::Backup',
                                    'billing_plan_id'    => $result['billing_plan']['id'],
                                    'limit'              => (isset($params['configoptions']['backup_limits']) ? $params['configoptions']['backup_limits'] : $product->getConfig('backup_limits')),
                                    )
                                );
                            $billing->add_base_resources($data);   
                  
                            // add template stores
                            $stores     = $product->getConfig('template_group');
                            if($stores[0] != 'all')
                            {
                                foreach($stores as $store)
                                {
                                    $data = array(
                                        'base_resource' => array(
                                            'resource_class'     => 'Resource::TemplateGroup',
                                            'billing_plan_id'    => $result['billing_plan']['id'],
                                            'target_type'        => 'ImageTemplateGroup',
                                            'target_id'          => $store
                                        )
                                    );
                                    $billing->add_base_resources($data);
                                }
                            }
                    }  
                    
                } 
                else 
                    return $billing->error ();
               
                if(isset($result['billing_plan']['id'])){
                    $data = array('user'=>array('billing_plan_id'=>$result['billing_plan']['id']));
                    $user->setUserID($search[0]['user']['id']);
                    $user->edit($data);
                    if($user->isSuccess()){
                        #$billing-> setBillingPlanID($search[0]['user']['billing_plan_id']);
                        #$billing->delete();
                        return 'success';
                    }else
                        return $user->error ();
                    
                }
               
                return 'success';
            }
           
        }
}        
         
/**
* FUNCTION onappCloud_ChangePassword
* Change root password for VM
* @params array
* @return string
*/ 
if(!defined('CLIENTAREA'))
{
    if (!function_exists('onappCloud_ChangePassword')){
            function onappCloud_ChangePassword($params){	

                $user    = new NewOnApp_User(null);
                $user    ->setconnection($params);
                $search  = $user->search($params['username']);
                if(!$user->isSuccess())
                    return $user->error();
                else {
                    $data = array('user'=>array('password'=>$params['password']));
                    $user->setUserID($search[0]['user']['id']);
                    $user->edit($data);
                    if($user->isSuccess())
                        return 'success';
                    else
                        return $user->error();
                }
            }
       }
}


/**
* FUNCTION onapVPS_ClientAreaCustomButtonArray
* Display buttons in clientArea
* @return array
*/ 
if (!function_exists('onappCloud_ClientAreaCustomButtonArray')){
    function onappCloud_ClientAreaCustomButtonArray() {
        $buttonarray = array(
            "Management" => "management",
        );
    return $buttonarray;
    }
}


/**
* FUNCTION onapVPS_AdminLink
* Login to admin panel
* @params int
* @return array
*/ 
if(!function_exists('onappCloud_AdminLink')){
    function onappCloud_AdminLink($params){
       return '<form target="_blank" action="http'.($params['serversecure']=='on' ? 's' :'').'://'.(empty($params['serverhostname']) ? $params['serverip'] : $params['serverhostname']).'/users/sign_in" method="post">
                  <input type="hidden" name="user[login]" value="'.$params['serverusername'].'" />
                  <input type="hidden" name="user[password]" value="'.$params['serverpassword'].'" />
                  <input type="hidden" name="commit" value="Sign In" />
                  <input type="submit" value="Login to Control Panel" />
               </form>';
    }
}

/**
* FUNCTION onappCloud_ClientArea
* Display clientarea template
* @params array
*/ 
if (!function_exists('onappCloud_ClientArea')){
     function onappCloud_ClientArea($params){	
         global $smarty,$CONFIG;	
         
         $moduledir = substr(dirname(__FILE__), strlen(ROOTDIR)+1);
         $lang      = onappCloud_getLang($params);         
         $product   = new onappCloud_Product($params['pid']);
       
         $smarty->assign('lang', $lang['mainsite']);
         $smarty->assign('dir', $moduledir);
         
         $user    = new NewOnApp_User($params['customfields']['userid']);
         $user    ->setconnection($params);
         $search  = $user->getDetails();
         $api =  $user->getApi();
         $version  = $api->sendGET('/version');
         if(!$user->isSuccess()){
             $smarty->assign('result',1);
             $smarty->assign('resultmsg',$user->error());
         }else {
         $user_id = $search['user']['id'];
         if(isset($_POST['ajax']) && $_POST['ajax']==1 && isset($_POST['doAction'])){
             ob_clean();
             switch ($_POST['doAction']){
                 case 'vm_terminate':
                     $vm         = new NewOnApp_VM($_POST['vserverid']);
                     $vm         ->setconnection($params);
                     $vm_details = $vm->getDetails();
                     if($vm_details['virtual_machine']['user_id']==$user_id){
                         $vm->delete();
                         if($vm->isSuccess())
                             die(json_encode(array('success'=>1,'msg'=>'OK')));
                         else 
                             die(json_encode(array('success'=>0,'msg'=>$vm->error())));
                     } else 
                             die(json_encode(array('success'=>0,'msg'=>$lang['perm_error'])));
                 
                     break;
             }
             die();
         }
         $results = $user->getVMList(); 
         if($user->isSuccess()){        
             
             $backup     = new NewOnApp_VMBackup();
             $backup     ->setconnection($params);
             $interface  = new NewOnApp_NetworkInterface(null);
             $interface  ->setconnection($params);
             
             $resource                      = array();
             $resource['cpus']['used']      = 0;
             $resource['cpu_shares']['used']= 0;
             $resource['memory']['used']    = 0;
             $resource['disk_size']['used'] = 0;
             $resource['ips']['used']       = 0;
             $resource['vm']['used']        = 0;
             $resource['backups']['used']   = 0;
             $resource['rate_limit']['used']= 0;
             
             foreach($results as $key=>$value){
                 $interface->setID($value['virtual_machine']['id']);
                 $rate_limit                    = $interface->getList();
                 foreach ($rate_limit as $val)
                     $resource['rate_limit']['used'] += $val['network_interface']['rate_limit'];
                 $resource['cpus']['used']      += $value['virtual_machine']['cpus'];      
                 $resource['memory']['used']    += $value['virtual_machine']['memory'];
                 $resource['disk_size']['used'] += $value['virtual_machine']['total_disk_size'];
                 $resource['ips']['used']       += count($value['virtual_machine']['ip_addresses']);
                 $resource['vm']['used']        += 1;
                 $resource['cpu_shares']['used']+= $value['virtual_machine']['cpu_shares'];
                 $resource['backups']['used']   += count($backup->getList($value['virtual_machine']['id']));
             }
             
             if(isset($results) && isset($results[0]))
                $smarty->assign('vpslist',$results);
             
             $billing = new NewOnApp_Billing($search['user']['billing_plan_id']);
             $billing ->setconnection($params);
             if($version['version'] &&  version_compare( $version['version'], "4.2.0", '>='))
                 $limits  = $billing->getBaseResources();
             else
                 $limits  = $billing->getDetails();
             
             $resource = OnAppCloud_Resources($limits,$resource);
             
             $resource['disk_size']['used_gb']  = onapp_formatBytes($resource['disk_size']['used']*1073741824);
             $resource['disk_size']['max_gb']   = onapp_formatBytes($resource['disk_size']['max']*1073741824);
             $resource['disk_size']['free']     = onapp_formatBytes(($resource['disk_size']['max']-$resource['disk_size']['used'])*1073741824);
             
             $resource['memory']['used_mb']     = onapp_formatBytes($resource['memory']['used']*1048576);
             $resource['memory']['max_mb']      = onapp_formatBytes($resource['memory']['max']*1048576);
             $resource['memory']['free']        = onapp_formatBytes(($resource['memory']['max']-$resource['memory']['used'])*1048576);
             
             $smarty->assign('resources',$resource);
             
             if(isset($_SESSION['msg_status'])){
                 $smarty->assign('result','success');
                 $smarty->assign('resultmsg',$_SESSION['msg_status']);
                 unset($_SESSION['msg_status']);
             }
             
             $smarty->assign('disallow_action',array(
                'firewall'     => $product->getConfig('manage_firewall'),
                'ip'           => $product->getConfig('manage_ip'),
                'network'      => $product->getConfig('manage_network'),
                'stats'        => $product->getConfig('manage_stats'),
                'disk'         => $product->getConfig('manage_disk'),
                'backups'      => $product->getConfig('manage_backups'),
                'autoscalling' => $product->getConfig('manage_autoscalling'),
                 'api_info' => $product->getConfig('show_api_info'),
             ));
                          
         }    
         else {
             $smarty->assign('result',1);
             $smarty->assign('resultmsg',$user->error());
         }
     }
     
    //OnApp Billing Integration Code
    if(file_exists(ROOTDIR.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'addons'.DIRECTORY_SEPARATOR.'OnAppBilling'.DIRECTORY_SEPARATOR.'core.php'))
    {
        require_once ROOTDIR.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'addons'.DIRECTORY_SEPARATOR.'OnAppBilling'.DIRECTORY_SEPARATOR.'core.php';
        $row = mysql_get_row("SELECT product_id FROM OnAppBilling_settings WHERE product_id = ? AND enable=1", array($params['packageid']));
        if($row)
        {
            $row = mysql_get_row("SELECT currency FROM tblclients WHERE id = ?", array($_SESSION['uid']));
            $user_currency = $row['currency'];

            getCurrency($_SESSION['uid']);

            $account       =   new OnAppBillingAccount($params['serviceid']);
            //Get Summay usage
            $summary       =   $account->getSummaryLines($params['packageid']);

            $out[] = array();
            foreach($summary['lines'] as $sum_key => $sum)
            {
                $out[$sum_key]['total']            =   formatCurrency(convertCurrency($sum['amount'], 1, $user_currency));
                $out[$sum_key]['usage']            =   number_format($sum['usage'], 2);
                $out[$sum_key]['FriendlyName']     =   $sum['name'];
                $out[$sum_key]['name']             =   isset($sum['partName']) ? $sum['partName'] : '';
                $out[$sum_key]['unit']             =   $sum['unit'];
            }

            if($out)
            {
               $smarty->assign('mg_lang', MG_Language::getLang()); 
               $smarty->assign('billing_resources', $out);
               $smarty->assign('records_range', array(
                   'start_date'    =>  $summary['startDate'],
                   'end_date'      =>  $summary['endDate'] 
               ));
            }
        }
    }
}
}



/**
* FUNCTION onappCloud_management
* Display extended pages in clientarea
* @params array
* @return array
*/ 
if (!function_exists('onappCloud_management')){
     function onappCloud_management($params){	
         global $CONFIG;	
         $lang              = onappCloud_getLang($params);         
         $moduledir         = substr(dirname(__FILE__), strlen(ROOTDIR)+1);
         $product           = new onappCloud_Product($params['pid']);
 
         $page              = (isset($_GET['page']) ? preg_replace('/[^A-Za-z0-9]/','',$_GET['page']) : 'mainsite');
         $vars['main_lang'] = $lang['mainsite'];
         $vars['lang']      = $lang[(!isset($lang[$page]) ? 'mainsite' : $page)];
         $vars['dir']       = $moduledir;
         $vars['main_dir']  = dirname(__FILE__);
         $vars['params']   = $params;

         if(empty($page) || !file_exists(dirname(__FILE__).DS.$page.'.php') || !file_exists(dirname(__FILE__).DS.'templates'.DS.$page.'.tpl')){
             $vars['resultmsg'] = $lang['module']['error5'];
             return array('vars'         => $vars);
         }        

         $user    = new NewOnApp_User(null);
         $user    ->setconnection($params);
         $search  = $user->search($params['username']);
         if(!$user->isSuccess()){
             global $smarty;
             $smarty->assign('result',1);
             $smarty->assign('resultmsg',$user->error());
         }else {
             $user->setUserID($search[0]['user']['id']);    
             $user_id = $search[0]['user']['id'];
             $vars['disallow_action'] = array(
                'firewall'     => $product->getConfig('manage_firewall'),
                'ip'           => $product->getConfig('manage_ip'),
                'network'      => $product->getConfig('manage_network'),
                'stats'        => $product->getConfig('manage_stats'),
                'disk'         => $product->getConfig('manage_disk'),
                'backups'      => $product->getConfig('manage_backups'),
                'autoscalling' => $product->getConfig('manage_autoscalling'),
                'api_info' => $product->getConfig('show_api_info'),
             );
             require_once(dirname(__FILE__).DS.$page.'.php');  
         }   
        
 
         if($page=='console' ){
              try{ //whmcs 6.0
                  global  $templates_compiledir;
                  $sm = new Smarty();
                  $sm->compile_dir = $templates_compiledir;
                  $sm->template_dir =  dirname(__FILE__) . DS .'templates'. DS ;
                  $sm->force_compile = 1;
                  foreach( $vars as $k => $v)
                     $sm->assign($k,   $v);

                  echo  $sm->fetch( 'console.tpl');
                  die();
              } catch (Exception $ex) {
                  echo $ex->getMessage();
                  die();
              }
         }
         return array(
                   'templatefile' => 'templates/'.$page,
                   'breadcrumb'   => ' > <a href="#">Server Details</a>',
                   'vars'         => $vars
         );
     }
}


/**
* FUNCTION onappClioud_AdminServiceTabFields
* Display details
* @params array
* @return string
*/ 
if (!function_exists('onappCloud_AdminServicesTabFields')){
	function onappCloud_AdminServicesTabFields($params){	
            if(empty($params['customfields']['userid']))
                return array();
         $user    = new NewOnApp_User($params['customfields']['userid']);
         $user    -> setconnection($params);
         $results = $user->getVMList(); 
         $api =  $user->getApi();
         $version  = $api->sendGET('/version');
         if($user->isSuccess()){        
                          
             $backup     = new NewOnApp_VMBackup();
             $backup     ->setconnection($params);
             $interface  = new NewOnApp_NetworkInterface();
             $interface  ->setconnection($params);
             
             $resource                      = array();
             $resource['cpus']['used']      = 0;
             $resource['cpu_shares']['used']= 0;
             $resource['memory']['used']    = 0;
             $resource['disk_size']['used'] = 0;
             $resource['ips']['used']       = 0;
             $resource['vm']['used']        = 0;
             $resource['backups']['used']   = 0;
             $resource['rate_limit']['used']= 0;
             
             
             foreach($results as $key=>$value){
                 $interface->setID($value['virtual_machine']['id']);
                 $rate_limit                    = $interface->getList();
                 foreach ($rate_limit as $val)
                    $resource['rate_limit']['used'] += $val['network_interface']['rate_limit'];
                 $resource['cpus']['used']      += $value['virtual_machine']['cpus'];    
                 $resource['cpu_shares']['used']+= $value['virtual_machine']['cpu_shares'];    
                 $resource['memory']['used']    += $value['virtual_machine']['memory'];
                 $resource['disk_size']['used'] += $value['virtual_machine']['total_disk_size'];
                 $resource['ips']['used']       += count($value['virtual_machine']['ip_addresses']);
                 $resource['vm']['used']        += 1;
                 $resource['backups']['used']   += count($backup->getList($value['virtual_machine']['id']));
             }
             
             if(isset($results) && isset($results[0]))
                $vps_list  = $results;
             $user    = new NewOnApp_User(null);
             $user    ->setconnection($params);
             $search  = $user->search($params['username']);
             $billing = new NewOnApp_Billing($search[0]['user']['billing_plan_id']);
             $billing ->setconnection($params);
             
             if($version['version'] &&  version_compare( $version['version'], "4.2.0", '>='))
                 $limits  = $billing->getBaseResources();
             else
                 $limits  = $billing->getDetails();

             $resource = OnAppCloud_Resources($limits,$resource);
                          
             $resource['disk_size']['used_gb']  = onapp_formatBytes($resource['disk_size']['used']*1073741824);
             $resource['disk_size']['max_gb']   = onapp_formatBytes($resource['disk_size']['max']*1073741824);
             $resource['disk_size']['free']     = onapp_formatBytes(($resource['disk_size']['max']-$resource['disk_size']['used'])*1073741824);
             
             $resource['memory']['used_mb']     = onapp_formatBytes($resource['memory']['used']*1048576);
             $resource['memory']['max_mb']      = onapp_formatBytes($resource['memory']['max']*1048576);
             $resource['memory']['free']        = onapp_formatBytes(($resource['memory']['max']-$resource['memory']['used'])*1048576);
             
             
                
             
             $data = array();
             foreach($vps_list as $key => $vm)
                 $data['VPS List'] .= $vm['virtual_machine']['hostname'].'<br />';
             
             $data['Resources'] =  '<table   class="table" style="width:400px;"><tr><td><b>Field</b></td><td><b>Used / Limit</b></td>';
             foreach ($resource as $key => $info){
                $data['Resources'] .= '<tr><td>'.str_replace('_',' ',$key).'</td><td>'.
                        ($info['used_gb'] ? $info['used_gb'].' / '.
                            (empty($info['max']) ? 'Unlimited' : $info['max_gb']) 
                        : (!$info['used_mb'] ? $info['used'] . ' / '.
                            (empty($info['max']) ? 'Unlimited' : $info['max']) 
                        : $info['used_mb'] . ' / '.(empty($info['max']) ? 'Unlimited' : $info['max_mb']))).'</td></tr>';
             }
             $data['Resources'] .= '</table>
             <style type="text/css">  .table td {border-bottom:1px solid #fff;width:50%;text-transform: capitalize;} </style>';
             
            return $data;
        }
   }
}


/**
* FUNCTION onappCloud_getLang
* Get user language
* @params array
* @return string
*/ 
if(!function_exists('onappCloud_getLang')){
    function onappCloud_getLang($params){
            global $CONFIG;
            if(!empty($_SESSION['Language']))
                 $language = strtolower($_SESSION['Language']);
             else if(strtolower($params['clientsdetails']['language'])!='')
                 $language = strtolower($params['clientsdetails']['language']);
             else
                 $language = $CONFIG['Language'];

             $langfilename = dirname(__FILE__).DS.'lang'.DS.$language.'.php';
             if(file_exists($langfilename)) 
                require_once($langfilename);
             else
                require_once(dirname(__FILE__).DS.'lang'.DS.'english.php');

             if(isset($lang))
                 return $lang;
    }
}

function OnAppCloud_Resources($limits,$resource = array()){

    if(isset($limits) && isset($limits['0']['base_resource'])){ //OnApp API >= 4.2
        foreach($limits as $base){
            if($base['base_resource']['is_bucket']=="1"){
                $resource['cpus']['max']      = $base['base_resource']['limits']['limit_cpu'];
                $resource['cpu_shares']['max']= $base['base_resource']['limits']['limit_cpu_share'];
                $resource['memory']['max']    = $base['base_resource']['limits']['limit_memory']; 
            }
            else if($base['base_resource']['resource_name']=="backup"){
                $resource['backups']['max']   = $base['base_resource']['limits']['limit'];
            }
            else if($base['base_resource']['resource_name']=="vm_limit"){
                $resource['vm']['max']        = $base['base_resource']['limits']['limit'];
            }
            else if($base['base_resource']['resource_name']=="data_store_group"){
                $resource['disk_size']['max']        = $base['base_resource']['limits']['limit'];
            }
            else if($base['base_resource']['resource_name']=="network_group"){
                $resource['ips']['max']        = $base['base_resource']['limits']['limit_ip'];
                $resource['rate_limit']['max']        = $base['base_resource']['limits']['limit_rate'];
            }
        }
    }
    else if(isset($limits) && isset($limits['billing_plan']['base_resources']['0'])){ //OnApp API 4.1
        foreach($limits['billing_plan']['base_resources'] as $base_resources){

            foreach($base_resources as $value){

               if($value['resource_name']=='cpu')
                   $resource['cpus']['max']      = $value['limits']['limit'];

               if($value['resource_name']=='memory')
                   $resource['memory']['max']    = $value['limits']['limit'];

               if($value['resource_name']=='disk_size')
                   $resource['disk_size']['max'] = $value['limits']['limit'];

               if($value['resource_name']=='ip_address')
                   $resource['ips']['max']       = $value['limits']['limit'];

               if($value['resource_name']=='vm_limit')
                   $resource['vm']['max']        = $value['limits']['limit'];

               if($value['resource_name']=='backup')
                   $resource['backups']['max']   = $value['limits']['limit'];

               if($value['resource_name']=='data_store_group')
                   $resource['disk_size']['max'] = $value['limits']['limit'];

               if($value['resource_name']=='hypervisor_group')
                   $resource['memory']['max']    = $value['limits']['limit_memory'];

               if($value['resource_name']=='hypervisor_group')
                   $resource['cpus']['max']      = $value['limits']['limit_cpu'];

               if($value['resource_name']=='network_group')
                   $resource['ips']['max']      = $value['limits']['limit_ip'];

               if($value['resource_name']=='network_group'){
                  $resource['rate_limit']['max'] = $value['limits']['limit_rate'];
               }
               if($value['resource_name']=='cpu_share'){
                   $resource['cpu_shares']['max'] = $value['limits']['limit'];
               }
               if($value['resource_name']=='hypervisor_group'){
                  $resource['cpu_shares']['max'] = $value['limits']['limit_cpu_share'];
               }
            }

       }

      foreach($limits['billing_plan']['default_base_resources'] as $key=>$value){          
          if($value['base_resource']['resource_name']=='hypervisor_group'){
              $resource['cpus']['max']         += $value['base_resource']['limits']['limit_cpu'];
              $resource['memory']['max']       += $value['base_resource']['limits']['limit_memory'];
          }
          if($value['base_resource']['resource_name']=='data_store_group'){
              $resource['disk_size']['max']    += $value['base_resource']['limits']['limit'];
          }
          if($value['base_resource']['resource_name']=='network_group'){
              $resource['ips']['max']          += $value['base_resource']['limits']['limit_ip'];
          }
          if($value['base_resource']['resource_name']=='network_group'){
              $resource['rate_limit']['max']   += $value['base_resource']['limits']['limit_rate'];
          }

       }
    }else if(isset($limits) && isset($limits['billing_plan'])){   //OnApp API < 4.1
        foreach($limits['billing_plan']['base_resources'] as $key=>$value){
           if($value['base_resource']['resource_name']=='cpu')
               $resource['cpus']['max']      = $value['base_resource']['limits']['limit'];

           if($value['base_resource']['resource_name']=='memory')
               $resource['memory']['max']    = $value['base_resource']['limits']['limit'];

           if($value['base_resource']['resource_name']=='disk_size')
               $resource['disk_size']['max'] = $value['base_resource']['limits']['limit'];

           if($value['base_resource']['resource_name']=='ip_address')
               $resource['ips']['max']       = $value['base_resource']['limits']['limit'];

           if($value['base_resource']['resource_name']=='vm_limit')
               $resource['vm']['max']        = $value['base_resource']['limits']['limit'];

           if($value['base_resource']['resource_name']=='backup')
               $resource['backups']['max']   = $value['base_resource']['limits']['limit'];

           if($value['base_resource']['resource_name']=='data_store_group')
               $resource['disk_size']['max'] = $value['base_resource']['limits']['limit'];

           if($value['base_resource']['resource_name']=='hypervisor_group')
               $resource['memory']['max']    = $value['base_resource']['limits']['limit_memory'];

           if($value['base_resource']['resource_name']=='hypervisor_group')
               $resource['cpus']['max']      = $value['base_resource']['limits']['limit_cpu'];

           if($value['base_resource']['resource_name']=='network_group')
               $resource['ips']['max']      = $value['base_resource']['limits']['limit_ip'];

           if($value['base_resource']['resource_name']=='network_group'){
              $resource['rate_limit']['max'] = $value['base_resource']['limits']['limit_rate'];
           }
           if($value['base_resource']['resource_name']=='cpu_share'){
               $resource['cpu_shares']['max'] = $value['base_resource']['limits']['limit'];
           }
           if($value['base_resource']['resource_name']=='hypervisor_group'){
              $resource['cpu_shares']['max'] = $value['base_resource']['limits']['limit_cpu_share'];
           }

       }

      foreach($limits['billing_plan']['default_base_resources'] as $key=>$value){          
          if($value['base_resource']['resource_name']=='hypervisor_group'){
              $resource['cpus']['max']         += $value['base_resource']['limits']['limit_cpu'];
              $resource['memory']['max']       += $value['base_resource']['limits']['limit_memory'];
          }
          if($value['base_resource']['resource_name']=='data_store_group'){
              $resource['disk_size']['max']    += $value['base_resource']['limits']['limit'];
          }
          if($value['base_resource']['resource_name']=='network_group'){
              $resource['ips']['max']          += $value['base_resource']['limits']['limit_ip'];
          }
          if($value['base_resource']['resource_name']=='network_group'){
              $resource['rate_limit']['max']   += $value['base_resource']['limits']['limit_rate'];
          }

       }
      }

      return (array)$resource;
}
/**
 * Test Connection
 * @param array $params
 * @return array
 * @throws Exception
 */
function onappCloud_TestConnection($params) {
      try {
            
            $user    = new NewOnApp_User(null);
            $user    ->setconnection($params);
            $user->getVersion();
            if (!$user->isSuccess())
                 throw new Exception((string) $user->error ());

            return array(
                'success' => true
            );
      } catch (Exception $e) {
            return array(
                'error' => $e->getMessage()
            );
      }
}