<link rel="stylesheet" type="text/css" href="{$dir}/css/style.css" />
<div>
    <a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=management&page=vmdetails&vserver={$vpsdata.id}" class="btn btn-small"><i class="icon-arrow-left"></i> {$lang.back}</a>
    <h2 class="header_label">{$lang.main_header}</h2>
    {if $msg_error or $msg_success}
        <div class="alert {if $msg_error}alert-danger{else}alert-success{/if}">
            <p></p><li>{if $msg_error}{$msg_error}{else}{$msg_success}{/if}</li><p></p>
        </div>
    {/if}
    <div class="attention-alert alert alert-danger">
        <span class="icon-warning-sign"></span>
        <span>{$lang.attention}</span>
    </div>
    <form method="post" id="onapp_add_form">
        <input type="hidden" name="do" value="rebuildVPS" />
        <table class="table table-bordered table-striped table-template">
            <thead>
                <tr><th>{$lang.template}</th><th>{$lang.select}</th></tr>
            </thead>
            <tbody>
                {foreach from=$templates item="entry" key="k"}
                    <tr><td>{$entry}</td><td><input type="radio" name="template" value="{$k}" />
                {foreachelse}
                    <tr><td colspan="2">{$lang.template_unavailable}</td></tr>
                {/foreach}
            </tbody>
        </table> 
        {if $templates|@count >0}
            <div class='rebuild-btn'><input type='submit' onclick="return confirm('{$lang.confirm_rebuild}');" class='btn btn-success' value='{$lang.rebuild}' /></div>
        {/if}
    </form>    
</div>
