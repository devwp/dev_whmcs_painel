
{**
 * @author Maciej Husak <maciej@modulesgarden.com>
 *}
 {literal}
 <style>
  /*<![CDATA[*/
    #no_vnc_status_bar {
      position: absolute;
      padding: 10px;
    }
    
    .submit button {
      float: right;
    }
  /*]]>*/
</style>
{/literal}
<div id='no_vnc_status_bar'>
<span id='no_vnc_status'>{$lang.loading}</span>
<canvas id='no_vnc_canvas'>{$lang.not_supported}</canvas>
<div class='submit'>
<button data-confirm='{$lang.confirm}' id='send_ctrl_alt_del_button'>Ctrl+Alt+Del</button>
</div>
</div>
<script type="text/javascript" src="{$dir}/js/jquery.js"></script>
<script src="{$dir}/console/util.js" type="text/javascript"></script>
<script src="{$dir}/console/webutil.js" type="text/javascript"></script>
<script src="{$dir}/console/base64.js" type="text/javascript"></script>
<script src="{$dir}/console/websock.js" type="text/javascript"></script>
<script src="{$dir}/console/des.js" type="text/javascript"></script>
<script src="{$dir}/console/input.js" type="text/javascript"></script>
<script src="{$dir}/console/display.js" type="text/javascript"></script>
<script src="{$dir}/console/jsunzip.js" type="text/javascript"></script>
<script src="{$dir}/console/rfb.js" type="text/javascript"></script>
<script>
  {literal}
  //<![CDATA[
    (function() {
        {/literal} 
        var rfb,
        $status = $('#no_vnc_status'),
        $cad = $('#send_ctrl_alt_del_button'),
        host = '{if $params.serverhostname}{$params.serverhostname}{else}{$params.serverip}{/if}',
        port = 443,
        password = '{$vm.remote_access_password}',
        path = 'vncproxy?key={$vm.identifier}',
        $mainDiv = $('#no_vnc_status_bar'),
        {literal}
        windowInnerDimensions = (function(){
           if(window.innerWidth!= undefined){
               return {width: window.innerWidth, height: window.innerHeight};
           } else {
               var B = document.body,
                D = document.documentElement;
               return {width: Math.max(D.clientWidth, B.clientWidth), height: Math.max(D.clientHeight, B.clientHeight)};
           }
        }()),
        windowOuterDimensions = (function() {
           var wW, wH, cW, cH, barsW, barsH;
    
           if (window.outerWidth) {
               wW = window.outerWidth;
               wH = window.outerHeight;
           } else {
               cW = document.body.offsetWidth;
               cH = document.body.offsetHeight;
               window.resizeTo(500, 500);
               barsW = 500 - document.body.offsetWidth;
               barsH = 500 - document.body.offsetHeight;
               wW = barsW + cW;
               wH = barsH + cH;
               window.resizeTo(wW,wH);
           }
           return { width: wW, height: wH };
        }()),
        h = parseInt(windowOuterDimensions.height, 10) - parseInt(windowInnerDimensions.height, 10),
        lr = parseInt(windowOuterDimensions.width, 10) - parseInt(windowInnerDimensions.width, 10),
        oldDimensions = {
          height: 0,
          width: 0
        };
    
      function updateState(rfb, state, oldstate, msg) {
          var level;
    
          switch (state) {
              case 'failed':
              case 'fatal':
                  level = "error";  break;
              case 'normal':
              case 'disconnected':
              case 'loaded':
                  level = "notice"; break;
              default:
                  level = "notice-w";   break;
          }
    
          $cad.attr('disabled', !(state === 'normal'));
    
          if (typeof(msg) !== 'undefined') {
              $status.attr("class", "no_vnc_status flash " + level).html(msg);
          }
    
          if(level === 'error') {
            $cad.closest('.submit').hide();
            resizeWindow();
          }
      }
    
      function resizeWindow() {
          var height = $mainDiv.outerHeight(true) + h,
            width = $mainDiv.outerWidth(true) + lr;
    
          if(oldDimensions.width != width || oldDimensions.height != height) {
            //window.resizeTo(width, height);
          }
    
          oldDimensions = {height: height, width: width};
      }
    
      function FBUComplete() {
          resizeWindow();
      }
    
      function sendCtrlAltDel() {
          rfb.sendCtrlAltDel();
          return false;
      }
    
      $cad.on('click', function() {
          var $this = $(this);
          if($this.data('confirm') && !confirm($this.data('confirm'))) {
            return;
          }
          sendCtrlAltDel();
          return false;
      });
    
      WebUtil.init_logging('none');
    
      (function() {
          document.documentElement.style.overflow = 'hidden';  // firefox, chrome
          document.body.scroll = "no"; // ie only
      }())
    
      rfb = new RFB({'target':             $D('no_vnc_canvas'),
                     'encrypt':            true,
                     'true_color':         true,
                     'local_cursor':       true,
                     'shared':             true,
                     'view_only':          false,
                     'onUpdateState':      updateState,
                     'onFBUComplete':      FBUComplete,
                     'check_rate':         16,
                     'fbu_req_rate':       32
      });
    
      rfb.connect(host, port, password, path);
    }());
  //]]>


    {/literal}
</script>