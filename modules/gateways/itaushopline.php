<?php
/**
*
* @ webplus itaubanline V1 By webplus  
*
* @ Version  : 1
* @ Author   : webplus <suporte@webplus.com.br>
* @ Release on : 14-jun-2016
* @ Email  : suporte@webplus.com.br
*
**/
/*
ERROR: 5 'Itau Shopline Must be installed and configured with permissions: Full Administrator, Sales Operator, Support Operator'
*/
if (!defined("WHMCS")) {
    die("This file cannot be accessed directly");
}
function itaushopline_MetaData()
{
    return array(
        'DisplayName' => 'Itau ShopLine',
        'APIVersion' => '1.0', // Use API Version 1.1
        'DisableLocalCredtCardInput' => true,
        'TokenisedStorage' => false,
    );
}

function itaushopline_config() {
	$q1=full_query("SELECT * FROM tbladdonmodules WHERE module='itaushopline_addon' AND setting='access' AND value='1,2,3'");
	if(($q1 && $r1=mysql_fetch_array($q1)) && array_key_exists('value',$r1)) {
		$configarray = array( 
			'FriendlyName'		=> array( 'Type' => 'System', 'Value' => 'Ita&uacute; ShopLine',),
			'codigoEmpresa' 	=> array( 'FriendlyName' => 'C&oacute;digo Empresa',				'Type' => 'text',		'Size' => '26' ), 
			'chave' 			=> array( 'FriendlyName' => 'Chave',								'Type' => 'text',		'Size' => '16' ), 
			'codigoInscricao' 	=> array( 'FriendlyName' => 'C&oacute;digo Inscri&ccedil;&atilde;o','Type' => 'dropdown',	'Options' => array('01'=>'01 - CPF','02'=>'02 - CNPJ') ), 
			'numeroInscricao' 	=> array( 'FriendlyName' => 'CPF ou CNPJ (Somente N&uacute;mero)', 	'Type' => 'text',		'Size' => '14',		'Description' => 'Somente n&uacute;meros. Ex. "01234567890" para CPF ou "012345678000190" para CNPJ.' ),
			//'multa'	 			=> array( 'FriendlyName' => '% de multa por atraso (ex: 2.00)',		'Type' => 'text',		'Size' => '10' ),
			//'juros'	 			=> array( 'FriendlyName' => '% de juros ao mes (ex: 1.00)',			'Type' => 'text',		'Size' => '10' ),
			'valorMinimo' 	  	=> array( 'FriendlyName' => 'Valor M&iacute;nimo',					'Type' => 'text',		'Size' => '14', 'Value' => '10.00',		'Description' => 'Ex. "30.00". Se o valor da fatura for inferior a este valor, ir&aacute; solicitar para que o usu&aacute;rio escolha outra forma de pagamento.' ), 
			'observacao' 	  	=> array( 'FriendlyName' => 'Observa&ccedil;&atilde;o',				'Type' => 'text',		'Size' => '40' ), 
			'urlRetorna'	 	=> array( 'FriendlyName' => 'Url de Retorno',						'Type' => 'text',		'Size' => '60' ),
			'observacao_add1' 	=> array( 'FriendlyName' => 'Observa&ccedil;&atilde;o Adicional 1 (Opcional)',	'Type' => 'text',		'Size' => '60' ), 
			'observacao_add2' 	=> array( 'FriendlyName' => 'Observa&ccedil;&atilde;o Adicional 2 (Opcional)',	'Type' => 'text',		'Size' => '60' ), 
			'observacao_add3' 	=> array( 'FriendlyName' => 'Observa&ccedil;&atilde;o Adicional 3 (Opcional)',	'Type' => 'text',		'Size' => '60' ), 
			'debugar' 			=> array( 'FriendlyName' => 'Debugar ItauCripto?',					'Type' => 'yesno',		'Size' => '25',		'Description' => 'Quando ativado, exp&otilde;e vari&aacute;veis do sistema em um coment&aacute;rio no bot&atilde;o da fatura.'  ), 

		);
	} else {
		$configarray = array( 
			'ERROR' 	=> array( 'FriendlyName' => '::::ERROR::::',						'Type' => 'yesno',		'Size' => '25', 'Description' => 'ERROR: 5 Itau Shopline Must be installed and configured with permissions: Full Administrator, Sales Operator, Support Operator'), 
		);

	}
	return $configarray;
}

function itaushopline_link($params) {
	return itaushopline_buttton($params);
}

function itaushopline_buttton($params) {

	if ( !isset($_SESSION["uid"]) && !isset($_SESSION['adminid']) ) {
		redirSystemURL("", "clientarea.php");
	}
	$GATEWAY = getGatewayVariables("itaushopline");
	if (!$GATEWAY["type"]) die("Module Not Activated");
	
	$data2		= array();
	$result	= select_query("tblinvoices","",array("id"=>$params['invoiceid']));
	$data2		= mysql_fetch_array($result);
	$id			= $data2["id"];
	$userid		= $data2["userid"];
	$duedate	= $data2["duedate"];
	$subtotal	= $data2["subtotal"];
	$credit		= $data2["credit"];
	$tax		= $data2["tax"];
	$taxrate	= $data2["taxrate"];
	//$total		= $data2["total"];
	$total		= $params['amount'];

	if ( $id && $userid && ( isset($_SESSION['adminid']) || $_SESSION["uid"]==$userid ) ) {} 
	else die("Invalid Access Attempt");

	$q1=full_query("SELECT * FROM tbladdonmodules WHERE module='itaushopline_addon' AND setting='access' AND value='1,2,3'");
	if(($q1 && $r1=mysql_fetch_array($q1)) && array_key_exists('value',$r1)) {
		if($params['amount']>=$params['valorMinimo']){
			if($p = GetValidBoleto($params['invoiceid'],$params['amount'])) {
				//$p = $p;
			} else {
				$clientsdetails 	 = $params['clientdetails'];
				$d['invoiceid']      = $params['invoiceid'];
				$d['codEmp']         = $params['codigoEmpresa'];
				$d['valor']          = $params['amount'];
				$d['observacao']     = $params['observacao'];
				$d['chave']          = $params['chave'];
				//$d['nomeSacado']     = $clientsdetails['fullname'];// NOME USUARIO
				$d['nomeSacado']     = $clientsdetails['companyname'];// NOME EMPRESA
				$d['codigoInscricao']= $params['codigoInscricao'];
				$d['numeroInscricao']= $params['numeroInscricao'];
				$d['enderecoSacado'] = $clientsdetails['address1'];
				$d['bairroSacado']   = $clientsdetails['address2'];
				$d['cepSacado']      = str_replace(array('-',' '),'',$clientsdetails['postcode']);
				$d['cidadeSacado']   = $clientsdetails['city'];
				$d['estadoSacado']   = $clientsdetails['state'];
				if(strtotime($duedate) >= strtotime(date('Y-m-d'))){
					$d['dataVencimento'] = $duedate;
				} else {
					$d['dataVencimento'] = date('Y-m-d', strtotime(date('Y-m-d'). ' + 1 days'));
				}
				$d['urlRetorna']     = $params['urlRetorna'];
				$d['obsAdicional1']  = $params['observacao_add1'];
				$d['obsAdicional2']  = $params['observacao_add2'];
				$d['obsAdicional3']	 = $params['observacao_add3'];
				$p = createBoletoOnDb($d);
			}
			
			$p['Vencimento'] = date("dmY",strtotime($p['dataVencimento']));
			$p['total']          = str_replace(".",",",$p['valor']);
			
			require_once("itaushopline/include/Itaucripto.php");
			$cripto = new Itaucripto();
			$dadoscripto = $cripto->geraDados($p['codEmp'],$p['pedido'],$p['total'],$p['observacao'],$p['chave'],$p['nomeSacado'],$p['codigoInscricao'],$p['numeroInscricao'],$p['enderecoSacado'],$p['bairroSacado'],$p['cepSacado'],$p['cidadeSacado'],$p['estadoSacado'],$p['Vencimento'],$p['urlRetorna'],$p['obsAdicional1'],$p['obsAdicional2'],$p['obsAdicional3']);
			$novoVenc = '';
			
			if($p['dataVencimento'] != $duedate){
				$novoVenc ='<div class="small-text">2&ordm; via com vencimento para '.date('d/m/Y',strtotime($p['dataVencimento'])).'</div>';
			}
			
			$botao  ='<form action="https://shopline.itau.com.br/shopline/shopline.asp" method="post" name="form" target="SHOPLINE">';
			$botao .=$novoVenc ;	
			$botao .='<input type="hidden" name="DC" value="'.$dadoscripto.'">';
			$botao .='<input type="submit" name="Shopline" value="'.$params['langpaynow'].'">';
			$botao .='</form>';
			
		} else {
			
			$botao = '<div class="small-text" style="color:#E00; background-color:#FEE; padding:5px 5px 5px 5px;">Pagamento via '.$params['name'].' est&aacute; dispon&iacute;vel apenas para valores a partir de <strong>R$ '.str_replace(".",",",$params['valorMinimo']).'</strong>.<br />
			Escolha outra forma de pagamento para quitar o montante de: <strong>R$ '.str_replace(".",",",$params['amount']).'</strong>.</div>';
			
		}
		
		$botao .=($params['debugar']=="on")?'<!-- '.var_export($params,1).' -->':'';
		return $botao;
		
	} else {
		return '<font color="RED"><b>ERROR: 5</b></font>';
	}
}

function createBoletoOnDb($p){
	$query = ("INSERT INTO mod_itaushoplineboletos (invoiceid,codEmp,valor,observacao,chave,nomeSacado,codigoInscricao,numeroInscricao,enderecoSacado,bairroSacado,cepSacado,cidadeSacado,estadoSacado,dataVencimento,obsAdicional1,obsAdicional2,obsAdicional3,urlRetorna) 
			   VALUES ( ".$p['invoiceid'].",'".$p['codEmp']."','".$p['valor']."','".$p['observacao']."','".$p['chave']."','".$p['nomeSacado']."','".$p['codigoInscricao']."','".$p['numeroInscricao']."','".$p['enderecoSacado']."','".$p['bairroSacado']."','".$p['cepSacado']."','".$p['cidadeSacado']."','".$p['estadoSacado']."','".$p['dataVencimento']."','".$p['urlRetorna']."','".$p['obsAdicional1']."','".$p['obsAdicional2']."','".$p['obsAdicional3']."')");
  	$result = mysql_query($query);
	$p['pedido'] = mysql_insert_id();
	return $p;
}
function GetValidBoleto($invoiceid,$valor){
	$result	= full_query('SELECT * FROM mod_itaushoplineboletos WHERE invoiceid='.$invoiceid.' AND valor='.$valor.' AND DATE(dataVencimento) >= DATE(NOW()) ORDER BY pedido DESC LIMIT 1');
	$data		= mysql_fetch_array($result);
	return (!$data)?false:$data;
}
?>