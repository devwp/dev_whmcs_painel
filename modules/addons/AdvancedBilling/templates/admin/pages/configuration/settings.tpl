<div id="Modal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 70%">
        <div class="modal-content">
                <input name="pid" value="{$product->id}" hidden />
                <div class="modal-header">
                    <h3 class="modal-title">{$MGLANG->T('Set Settings For')} <b>{$product->name}</b></h3>
                    <button type="button" class="close" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i></button>
                </div>
                <div class="modal-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        {foreach from=$extensions key=index item=extension}
                                <li role="presentation" {if $index eq 0}class="active"{/if}>
                                    <a href="#{$extension->name}Tab" aria-controls="{$extension->name}Tab" role="tab" data-toggle="tab">{$extension->friendlyName}</a>
                                </li>
                        {/foreach}
                        {if $configuration}
                            <li role="presentation">
                                <a href="#moduleSettingsTab" aria-controls="moduleSettingsTab" role="tab" data-toggle="tab">{$MGLANG->T("Module Settings")}</a>
                            </li>
                        {/if}
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                    {if $extensions or $configuration}
                        <div role="tabpanel" class="tab-pane" id="moduleSettingsTab">
                            <form id="moduleSettings">
                                {foreach from=$configuration key=name item=field}
                                    <div class='row' style='margin-bottom: 10px'>
                                        <label class="col-sm-3 control-label text-right" style="line-height: 35px;">{$field.friendlyName}</label>
                                        <div class="col-sm-9">
                                            {if $field.type eq 'select'}
                                                <select name="{$name}" class="form-control">
                                                    {foreach from=$field.options item=option key=opValue}
                                                        <option value="{$opValue}" {if $opValue==$field.value}selected{/if}>
                                                            {$option}
                                                        </option>
                                                    {/foreach}
                                                </select>
                                            {/if}
                                            {if $field.type eq 'text'}
                                                <input name="{$name}" type="text" value="{$field.value}"  class="form-control" style="width:40%">
                                            {/if}
                                        </div>
                                    </div>
                                {/foreach}
                            </form>
                        </div>
                            
                        <div class="alert alert-danger" id="extensionAlert" style="display: none"></div>
                        {foreach from=$extensions key=index item=extension}
                            <div role="tabpanel" class="tab-pane {if $index eq 0}active{/if}" id="{$extension->name}Tab">
                                {if $extension->hasProductConfiguration()}
                                    {$extension->displayProductConfiguration()}
                                {else}
                                    <div class="text-center">
                                        <span>{$MGLANG->T('Settings for this extension are not available')}</span>
                                    </div>
                                {/if}
                            </div>
                        {/foreach}
                    {else}
                        {$MGLANG->T('Settings for this module are not available')}
                    {/if}
                    </div>

                  </div>
                
                <div class="modal-footer">
                  <button id="saveSettingsBtn" class="btn btn-success btn-inverse" data-dismiss="modal">{$MGLANG->T('Save')}</button>
                  <button class="btn btn-danger" data-dismiss="modal">{$MGLANG->T('Cancel')}</button>
                </div>
        </div>

    </div>
</div>

{literal}                
<script type="text/javascript">    
    function initModal()
    {
        $.when( $(".bootstrap-switcher").bootstrapSwitch() ).done(function()
        {
            $('#Modal').modal('show');
        });
        
        $("#RecurringBilling [name='enable']").on('switchChange.bootstrapSwitch', function(){
            if( $("#RecurringBilling [name='enable']").bootstrapSwitch('state') === true
                && $("#CreditBilling [name='enable']").bootstrapSwitch('state') === true )
            {
                $("#RecurringBilling [name='enable']").bootstrapSwitch('state', false);
                $("#extensionAlert").html("{/literal}{$MGLANG->T("Recurring Billing and Credit Billing cannot be enabled simultaneously")}{literal}");
                $("#extensionAlert").show().delay(3000).fadeOut();;
            }
        });
        
        $("#CreditBilling [name='enable']").on('switchChange.bootstrapSwitch', function(){
            if( $("#RecurringBilling [name='enable']").bootstrapSwitch('state') === true
                && $("#CreditBilling [name='enable']").bootstrapSwitch('state') === true )
            {
                $("#CreditBilling [name='enable']").bootstrapSwitch('state', false);
                $("#extensionAlert").html("{/literal}{$MGLANG->T("Recurring Billing and Credit Billing cannot be enabled simultaneously")}{literal}");
                $("#extensionAlert").show().delay(3000).fadeOut();
            }
        });
        
        $("#saveSettingsBtn").click(function(e)
        {
            var productId = $("[name='pid']").val();
            $("#Modal form").each(function()
            {                
                var formId = $(this).attr("id");

                var vars = $("#"+formId).serializeArray();
                vars.push({name: 'extension', value: formId});
                vars.push({name: 'pid', value: productId});
                
                postAJAX("configuration|saveSettings", vars, "json", "resultMessage");
            });
            
            var moduleSettings = $("#moduleSettings").serializeArray();
            moduleSettings.push({name: 'productId', value : productId});

            $.when(postAJAX("configuration|saveModuleSettings", moduleSettings, "json", "resultMessage"))
            .done(function()
            {
                refreshEnabledExtensions(productId);
            });
        });
    }
</script>
{/literal}   