<div class="panel panel-primary">
    <div class="panel-body">{$MGLANG->T('On this page you can find information regarding archived usage records')}.</div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">{$MGLANG->T('Archive Information')}</h3>
    </div>
    <div class="panel-body">
        <div class="panel panel-info">
            <div class="panel-body">
                <label>{$MGLANG->T('Archived Items')}:</label> <span id='archivedAmount'>{$stats.rows}</span><br />
                <label>{$MGLANG->T('Space On Disk')}: </label> <span id='archivedDatasize'>{$stats.size} MB<br />
            </div>
        </div>
        <br />
        <button id='flushArchive' class='btn btn-inverse btn-info'>{$MGLANG->T('Flush')}</button>
    </div>
</div>
 
{literal}
<script type="text/javascript">
    $("#flushArchive").on("click", function(){
        postAJAX("Settings|flushArchive", null, 'json', "resultMessage", function(result){
            if(result === 'success')
            {
                postAJAX("Settings|getArchiveStats", null, 'json', null, function(result){
                    $("#archivedAmount").html(result.rows);
                    $("#archivedDatasize").html(result.size);
                });
            }
        });
    });
</script>
{/literal}