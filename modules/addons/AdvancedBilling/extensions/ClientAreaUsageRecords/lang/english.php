<?php
$_LANG['ClientAreaUsageRecords']['Pricing for usage records'] = 'Pricing For Usage Records';

$_LANG['ClientAreaUsageRecords']['Usage Records'] = 'Usage Records';
$_LANG['ClientAreaUsageRecords']['Usage Records History'] = 'Usage Records History';
$_LANG['ClientAreaUsageRecords']['Nothing to Display'] = 'Usage Records History';
$_LANG['ClientAreaUsageRecords']['Hide']  = 'Hide';
$_LANG['ClientAreaUsageRecords']['Show']  = 'Show';
$_LANG['ClientAreaUsageRecords']['Total'] = 'Total';
$_LANG['ClientAreaUsageRecords']['Date']  = 'Date';
$_LANG['ClientAreaUsageRecords']['Page']  = 'Page';
$_LANG['ClientAreaUsageRecords']['of']    = 'of';
$_LANG['ClientAreaUsageRecords']['Next']  = 'Next';
$_LANG['ClientAreaUsageRecords']['Prev']  = 'Prev';