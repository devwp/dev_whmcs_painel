<?php

/* * ********************************************
 *                  CLIENTARA
 * ******************************************* */
$_LANG['CreditBilling']['Enable Auto Refill'] = 'Enable Auto Refill';
$_LANG['CreditBilling']['Enable to automatically charge from a credit card'] = 'Enable to automatically charge from a credit card';
$_LANG['CreditBilling']['Auto Refill Value'] = 'Auto Refill Value';
$_LANG['CreditBilling']['How much should be refiled in a single refill operation'] = 'How much should be refiled in a single refill operation';
$_LANG['CreditBilling']['Auto Refill Under'] = 'Auto Refill Under';
$_LANG['CreditBilling']['Refil if your credit balance falls below this value'] = 'Refil if your credit balance falls below this value';
$_LANG['CreditBilling']['Save'] = 'Save';
$_LANG['CreditBilling']['Wrong value. Value cannot be lower than 0'] = 'Wrong value. Value cannot be lower than 0';
$_LANG['CreditBilling']['Wrong value. Value cannot be lower than x'] = 'Wrong value. Value cannot be lower than x';
$_LANG['CreditBilling']['Wrong value. Value cannot be greater x'] = 'Wrong value. Value cannot be greater x';
$_LANG['CreditBilling']['Configuration has been saved'] = 'Configuration has been saved';
$_LANG['CreditBilling']['Auto Refill'] = 'Auto Refill';
$_LANG['CreditBilling']['Value cannot be higher then '] = 'Value cannot be higher then ';
$_LANG['CreditBilling']['Value cannot be lower then '] = 'Value cannot be lower then ';

/* * ****************************************************
 *                      CONFIGURATION
 * *************************************************** */
$_LANG['addonAA']['pagesLabels']['credits']['label'] = 'Credits';
$_LANG['addonAA']['CreditBilling']['User Credits'] = 'User Credits';

$_LANG['addonAA']['CreditBilling']['Client Name'] = 'Client Name';
$_LANG['addonAA']['CreditBilling']['Hosting'] = 'Hosting';
$_LANG['addonAA']['CreditBilling']['Internal Credit'] = 'Internal Credit';
$_LANG['addonAA']['CreditBilling']['Already Paid For Hosting'] = 'Already Paid For Hosting';
$_LANG['addonAA']['CreditBilling']['Action'] = 'Action';
$_LANG['addonAA']['CreditBilling']['Refund'] = 'Refund';
$_LANG['addonAA']['CreditBilling']['Client credits has been refunded'] = 'Client credits has been refunded';

//$_LANG['configuration']['enable_credit_billing']           = 'Enable';
//$_LANG['configuration']['enable_credit_billing_help']      = 'Check this option if you want to enable credit billing extension for this product';
//$_LANG['configuration']['create_invoice_each']             = 'Create Invoices Each';
//$_LANG['configuration']['create_invoice_each_help']        = 'Days (30 by default)';
//$_LANG['configuration']['minimal_credit']                  = 'Minimum Credit';
//$_LANG['configuration']['minimal_credit_help']             = 'Minimum amount that will be charged from client account';
//$_LANG['configuration']['low_credit_notification']         = 'Low Credit Notification';
//$_LANG['configuration']['low_credit_notification_help']    = 'Send email to your client about low credit amount on account';
//$_LANG['configuration']['email_interval']                  = 'Email Interval';
//$_LANG['configuration']['email_interval_help']             = 'Set email interval for email notifications';
//$_LANG['configuration']['autosuspend']                     = 'Autosuspend';
//$_LANG['configuration']['autosuspend_help']                = 'Autosusped account when user does not have sufficient funds';
//$_LANG['configuration']['due_date']                        = 'Due Date';
//$_LANG['configuration']['due_date_help']                   = 'Days (7 by default)';
//$_LANG['configuration']['auto_refill_enable']              = 'Autorefill';
//$_LANG['configuration']['auto_refill_enable_help']         = 'Enable to charge from client\'s credit cart when client\'s credit balance reach 0';
//$_LANG['configuration']['auto_refill_gateway']             = 'Gateway';
//$_LANG['configuration']['auto_refill_gateway_help']        = '';
//$_LANG['configuration']['auto_refill_minimal_amount']      = 'Minimum Amount';
//$_LANG['configuration']['auto_refill_minimal_amount_help'] = 'Minimum amount to charge from client\'s credit card.';
//$_LANG['configuration']['auto_refill_maximal_amount']      = 'Maximum Amount';
//$_LANG['configuration']['auto_refill_maximal_amount_help'] = 'Maximum amount to charge from client\'s credit card.';