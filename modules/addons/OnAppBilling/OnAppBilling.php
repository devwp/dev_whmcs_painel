<?php

//SOME DEFINES
defined('DS') ? null : define('DS',DIRECTORY_SEPARATOR);
defined('WHMCS_MAIN_DIR') ? null : define('WHMCS_MAIN_DIR', substr(dirname(__FILE__), 0, strpos(dirname(__FILE__), 'modules'.DS.'addons')));

/**
 * Just change function name. Do not edit anything more.
 */ 

 
function OnAppBilling_config()
{
    //SOME USEFUL STUFF
    require_once dirname(__FILE__).DS.'core'.DS.'functions.php';
    
    require_once dirname(__FILE__).DS.'config.php';
    
    $module = OnAppBilling_getModuleClass(__FILE__);
    $MGC = new $module();
    return array
    (
        'name'          =>  $MGC->system_name,
        'description'   =>  $MGC->description,
        'version'       =>  $MGC->version,
        'author'        =>  $MGC->author
    );
}

function OnAppBilling_activate()
{
    //SOME USEFUL STUFF
    require_once dirname(__FILE__).DS.'core'.DS.'functions.php';
    
    require_once dirname(__FILE__).DS.'config.php';

    $module = OnAppBilling_getModuleClass(__FILE__);
    $MGC = new $module();
    $MGC->activate();
}

function OnAppBilling_deactivate()
{
    //SOME USEFUL STUFF
    require_once dirname(__FILE__).DS.'core'.DS.'functions.php';
    
    require_once dirname(__FILE__).DS.'config.php';

    $module = OnAppBilling_getModuleClass(__FILE__);
    $MGC = new $module();
    $MGC->deactivate();
}


function OnAppBilling_upgrade($vars)
{
    //SOME USEFUL STUFF
    require_once dirname(__FILE__).DS.'core'.DS.'functions.php';
    
    require_once dirname(__FILE__).DS.'config.php';

    $module = OnAppBilling_getModuleClass(__FILE__);
    $MGC = new $module($vars['version']);
    $MGC->upgrade($vars['version']);
}

/**
 * Admin area output
 * @param type $vars 
 */
function OnAppBilling_output($vars) 
{
    require_once dirname(__FILE__).DS.'config.php';
    require_once dirname(__FILE__).DS.'core'.DS.'output.php';
}

function OnAppBilling_getModuleClass($file)
{
    $dirname = dirname($file);
    $basename = basename($dirname);
    return $basename;
}