<?php



/**
 * @author Mariusz Miodowski <mariusz@modulesgarden.com>
 */

if($clients)
{
    foreach($clients as $client)
    {
        echo '<tr>
                <td><a href="clientssummary.php?userid='.$client['user_id'].'">'.$client['firstname'].' '.$client['lastname'].'</a></td>
                <td><a href="clientshosting.php?userid='.$client['user_id'].'&id='.$client['hosting_id'].'">'.$client['hosting_id'].' - '.($client['domain'] ? $client['domain'] : 'no domain').'</a></td>
                <td>'.$client['credit'].'</td>
                <td>'.$client['paid'].'</td> 
                <td>
                    <a href="addonmodules.php?module=OnAppBilling&modpage=credits&modaction=refund&hosting_id='.$client['hosting_id'].'&client_id='.$client['user_id'].'" class="btn btn-danger">Refund</a>
                </td>
              </tr>';
    }
}
else
{
    echo '<tr><td colspan="5" style="text-align: center"><b>Nothing to display</b></tr>';
}