<?php

/**
 * Base class for all resource class. You MUST implements this class!
 * @author Mariusz Miodowski
 */
            
                        
if(!class_exists('OnAppBillingResource'))
{
    class OnAppBillingResource
    {
        private $reservedFieldNames = array
        (
            'id',
            'hosting_id',
            'client_id',
            'product_id',
            'rel_id',
            'extended_id',
            'date',
            'total',
            'record_id'
        );
        /**
         * You can set up supported server modules. For this products this module will be automatically loaded
         * @var type 
         */
        protected $supported_modules = array();
        /**
         *  Add custom HTML to configuration area
         * @var type 
         */
        protected $configuration_area = '';
        /**
         * Billing options 
         * @var type 
         */
        protected $billing_options = array();

        protected $compute_type = array();
        /**
         * Module resources
         * Ex: array('my_resource' => 'My OnAppBillingResource')
         * @var type 
         */
        protected $resources = array();

        /**
         * Current product id
         * @var type 
         */
        protected $product_id = null;

        /**
         * Automatically loaded configuratiom
         * @var type 
         */
        protected $servers_configuration = null;

        /**
         * Product type
         * @var type 
         */
        protected $type = null;

        /**
         * Display custom configuration for your module
         * @var type 
         */
        protected $module_settings = array();


        /**
         * Module description
         * @var type 
         */
        const description = 'Sample description';

        /**
         * Module settins array
         * @var type 
         */
        protected $configuration = array();

        /**
         * Submodule version 
         */
        const version = 1.0;

        /**
         * Module name
         */
        const name = 'MyCustomName';
        
        /**
         * Usage Records Pricing
         * @var type 
         */
        protected $pricing = array();

        /**
         * Interval for updating usage records. Use it only if you know what you are doing ;]
         * @var type 
         */
        protected $interval = 0;

        /**
         * Set this value as true when your module is supporting extendedPricing
         * @var type 
         */
        protected $extendedPricing = false;
        /**
         * Initialize class
         */
        public function __construct($product_id = 0)
        {
            //Set type
            $this->type = strtolower(substr(get_class($this), 0, strpos(get_class($this), '_resource')));
            //Set Product Type
            $this->product_id = $product_id;
        }

        /**
         * Create submodules tables 
         */
        final function install()
        {
            $keys = array_keys($this->resources); 
            $sql = '';
            foreach($keys as $key)
            {
                $sql .= '`'.$key.'` DECIMAL(16, 6), ';
            }

            mysql_safequery("CREATE TABLE IF NOT EXISTS `OnAppBilling_".$this->type."_records`
                (
                    `id` INT NOT NULL AUTO_INCREMENT,
                    `hosting_id`    INT NOT NULL,
                    `client_id`     INT NOT NULL,
                    `product_id`    INT NOT NULL,
                    `rel_id`        VARCHAR(128) DEFAULT '0',
                    ".$sql."
                    `date` DATETIME,
                    PRIMARY KEY(`id`),
                    KEY(`hosting_id`),
                    KEY(`client_id`),
                    KEY(`date`),
                    KEY(`rel_id`)
                )ENGINE=MyISAM;") or die(mysql_error());

            mysql_safequery("CREATE TABLE IF NOT EXISTS `OnAppBilling_".$this->type."_prices`
                (
                    `record_id`     INT NOT NULL,
                    `hosting_id`    INT NOT NULL,
                    `client_id`     INT NOT NULL,
                    `product_id`    INT NOT NULL,
                    `rel_id`        VARCHAR(128) DEFAULT '0',
                    ".$sql."
                    `date` DATETIME,
                    `total` DECIMAL(16,6),
                    KEY(`record_id`),
                    KEY(`hosting_id`),
                    KEY(`client_id`),
                    KEY(`date`),
                    KEY(`rel_id`)
                )ENGINE=MyISAM;") or die(mysql_error());
                    
            mysql_safequery("CREATE TABLE IF NOT EXISTS `OnAppBilling_".$this->type."_extendedPricing`
                (
                    `hosting_id`    INT NOT NULL,
                    `rel_id`        VARCHAR(128),
                    `resource`      VARCHAR(128),
                    `extended_id`   VARCHAR(128),
                    `value`         DECIMAL(16,6),
                    KEY(`hosting_id`),
                    KEY(`resource`),
                    KEY(`extended_id`)
                )ENGINE=MyISAM;") or die(mysql_error()); 
        }
        
        
        /**
         * This function should ran after module upgrade.
         * It should add or delete columns from database
         */
        final function upgrade()
        {
            $records = mysql_get_array("SHOW COLUMNS IN `OnAppBilling_".$this->type."_records`");
            if($records)
            {
                //Get Old Fields
                $old_fields = array();
                foreach($records as &$record)
                {
                    if(in_array($record['Field'], $this->reservedFieldNames))
                    { 
                        continue;
                    }
                    $old_fields[] = $record['Field'];
                }
                //Get New Fields
                $new_fields = array_keys($this->resources);
                
                //Fields to create
                $create = array_diff($new_fields, $old_fields);
                //Field to delete
                $delete = array_diff($old_fields, $new_fields);
                
                foreach($create as &$field)
                {
                    mysql_safequery("ALTER TABLE `OnAppBilling_".$this->type."_records` ADD COLUMN ".$field.' DECIMAL(16, 6)') or die(mysql_error());
                }
                
                foreach($delete as &$field)
                {
                    mysql_safequery("ALTER TABLE `OnAppBilling_".$this->type."_records` DROP COLUMN ".$field) or die(mysql_error());
                }
            }
            
            $records = mysql_get_array("SHOW COLUMNS IN `OnAppBilling_".$this->type."_prices`");
            if($records)
            {
                //Get Old Fields
                $old_fields = array();
                foreach($records as &$record)
                {
                    if(in_array($record['Field'], $this->reservedFieldNames))
                    { 
                        continue;
                    }
                    $old_fields[] = $record['Field'];
                }
                //Get New Fields
                $new_fields = array_keys($this->resources);
                
                //Fields to create
                $create = array_diff($new_fields, $old_fields);
                //Field to delete
                $delete = array_diff($old_fields, $new_fields);
                
                foreach($create as &$field)
                {
                    mysql_safequery("ALTER TABLE `OnAppBilling_".$this->type."_prices` ADD COLUMN ".$field.' DECIMAL(16, 6)') or die(mysql_error());
                }
                
                foreach($delete as &$field)
                {
                    mysql_safequery("ALTER TABLE `OnAppBilling_".$this->type."_prices` DROP COLUMN ".$field) or die(mysql_error());
                }
            }
        }

        /**
         * Uninstall submodule table
         */
        final function uninstall()
        {
            mysql_safequery("DROP TABLE IF EXISTS `OnAppBilling_".$this->type."_prices`");
            mysql_safequery("DROP TABLE IF EXISTS `OnAppBilling_".$this->type."_records`");
            mysql_safequery("DROP TABLE IF EXISTS `OnAppBilling_".$this->type."_extendedPricing`");
        }
        /**
         * Set product ID. Function is called automatically by cron
         * @param type $product_id 
         */
        final public function setProductId($product_id)
        {
            $this->product_id = $product_id;
        }

        /**
         * Function is called by main configuration and cron. Should return available resources
         * @return type 
         * @Mariusz Miodowski
         */
        final public function getResources()
        {
            return $this->resources;
        }

        final public function getConfiguration()
        {
            return $this->configuration;
        }

        final public function setConfiguration($conf)
        {
            foreach($conf as $key => $value)
            {
                if(isset($this->configuration[$key]))
                {
                    $this->configuration[$key]['Value'] = $value;
                }
            }
        }

        /**
         * Add resources to database
         * @param type $client_id
         * @param type $hosting_id
         * @param type $domain
         * @param type $resources
         * @return type 
         * @author Mariusz Miodowski
         */
        protected function insertResource($client_id, $hosting_id, $resources = array(), $rel_id = 0)
        {
            //Is type setup?
            if(!$this->type)
            {
                $this->logError('Cannot add usage records because module type is not setup');
                return;
            }

            //Is pricing setup?
            if(!$this->pricing)
            {
                $this->logError('Cannot add resource. Pricing doest not exists for this module ('.$this->type.')');
                return;
            }

            //Re-Format Resources
            foreach($resources as $res_key => &$res_val)
            {
                if(!is_array($res_val))
                {
                    $val = $res_val;
                    $res_val = array
                    (
                        'type'      =>  'SinglePricing',
                        'resources' =>  array
                        (
                            $val
                        )
                    );
                }
                elseif(is_array($res_val) && !isset($res_val['type']))
                {
                    $key = key($res_val);
                    $val = current($res_val);
                    
                    $res_val = array
                    (
                        'type'      =>  'ExtendedPricing',
                        'resources' =>  array
                        (
                            $key    =>  $val
                        )
                    );
                }
            }
            
            //Just To Be Sure
            foreach($resources as $res_key => &$res_val)
            {
                if(!is_array($res_val) || !in_array($res_val['type'], array('ExtendedMultiPricing', 'ExtendedPricing', 'SinglePricing')))
                {
                    die('Invalid Pricing Type. '.$res_val['type'].' is not supported');
                }
            }
                
            //Get Current Usage
            $current_usage = array();
            $rows = mysql_get_array("SELECT resource, extended_id, value, rel_id FROM `OnAppBilling_".$this->type."_extendedPricing` WHERE hosting_id = ?", array($hosting_id));
            foreach($rows as $row)
            {
                $current_usage[$row['rel_id']][$row['resource']][$row['extended_id']] = $row['value'];
            }
            
            //Update Current Usage
            foreach($resources as $res_key => &$res_val)
            {
                foreach($res_val['resources'] as $ext_key => $ext_val)
                {
                    if(!isset($current_usage[$rel_id][$res_key][$ext_key]))
                    {
                        mysql_safequery("INSERT INTO `OnAppBilling_".$this->type."_extendedPricing` (`hosting_id`, `rel_id`, `resource`, `extended_id`, `value`) VALUES(?, ?, ?, ?, ?)", array(
                            $hosting_id,
                            $rel_id,
                            $res_key,
                            $ext_key,
                            $ext_val,
                        )) or die(mysql_error());
                    }
                    else
                    {
                        mysql_safequery("UPDATE `OnAppBilling_".$this->type."_extendedPricing` SET `value` = `value` + ? WHERE `hosting_id` = ? AND `resource` = ? AND `extended_id` = ? AND `rel_id` = ?", array(
                            $ext_val,
                            $hosting_id,
                            $res_key,
                            $ext_key,
                            $rel_id
                        )) or die(mysql_error());
                    }
                }
            }
 
            //Current Time
            $curr_time = time();
            //MySQL Date
            $date = date('Y-m-d H:i:s', $curr_time);
            
            //Count Summary
            foreach($resources as $res_key => &$res_val)
            {
                $records[$res_key] = array_sum($res_val['resources']);
            }
            
            $records['date']        =   $date;
            $records['hosting_id']  =   $hosting_id;
            $records['product_id']  =   $this->product_id;
            $records['client_id']   =   $client_id;
            $records['rel_id']      =   $rel_id;
            //Get Resource Keys;
            
            $keys = array_keys($records);
            
            //Add Records To Database
            mysql_safequery("INSERT INTO `OnAppBilling_".$this->type."_records` (".implode(',', $keys).") VALUES(".implode(',', array_fill(0, count($keys), '?')).")", $records) or die(mysql_error());
            $record_id = mysql_insert_id();

            //Set the time diff
            $time_diff = 0.0001;
            if($this->interval)
            {
                $time_diff = $this->interval;
            }
            else
            {
                $last_update = strtotime($this->getLastUpdate($hosting_id, $rel_id));
                if(!$last_update)
                {
                    $last_update = doubleval($curr_time) - doubleval(0.0001);
                }
                
                $time_diff = doubleval($curr_time) - doubleval($last_update);
            }
            
            
            //Count prices
            $records = array_fill_keys(array_keys($resources), 0);
            
            foreach($resources as $res_key => &$res_val)
            {
                //Get Value, Pricing, and Free Limits
                foreach($res_val['resources'] as $sub_key => &$sub_val)
                {
                    $value = 0;
                    $price = 0;
                    $free_limit = 0;
                    
                    switch($res_val['type'])
                    {
                        case 'SinglePricing':
                            $value      = doubleval($sub_val);
                            $price      = doubleval($this->pricing[$res_key]['price']);
                            $free_limit = doubleval($this->pricing[$res_key]['free_limit']);
                            break;
                        
                        case 'ExtendedPricing':
                        case 'ExtendedMultiPricing':
                            if(isset($this->pricing[$res_key]['ExtendedPricing'][$sub_key]))
                            {
                                $value      = doubleval($sub_val);
                                $price      = doubleval($this->pricing[$res_key]['ExtendedPricing'][$sub_key]['Price']);
                                $free_limit = doubleval($this->pricing[$res_key]['ExtendedPricing'][$sub_key]['FreeLimit']); 
                            }
                            else
                            {
                                $value      = doubleval($sub_val);
                                $price      = doubleval($this->pricing[$res_key]['price']);
                                $free_limit = doubleval($this->pricing[$res_key]['free_limit']);
                            }                 
                            break;
                    }

                    //Count Summary Prices
                    switch($this->pricing[$res_key]['type'])
                    { 
                        case 'average': 
                                $diff = doubleval($value) - doubleval($free_limit);
                                if($diff > 0)
                                {
                                    $records[$res_key] += (doubleval($time_diff) / (double)3600) * doubleval($price) * doubleval($diff);
                                }
                                else
                                {
                                    $records[$res_key] += 0;
                                }
                            break;
                        
                        case 'summary':
                            if($free_limit > 0)
                            {
                                if($res_val['type'] == 'SingleResource')
                                {
                                    $row = mysql_get_row("SELECT SUM(".$res_key.") as `sum` FROM `OnAppBilling_".$this->type."_records` WHERE product_id = ? AND hosting_id = ? AND rel_id = ? AND id <> ?", array($this->product_id, $hosting_id, $rel_id, $record_id));
                                    $sum = $row['sum'];
                                } 
                                else
                                {
                                    $sum = $current_usage[$rel_id][$res_key][$sub_key];
                                }
                                
                                $diff = doubleval($sum) - doubleval($free_limit);

                                if($diff > 0)
                                {
                                    $records[$res_key] += doubleval($value) * doubleval($price);
                                }
                                else 
                                { 
                                    if($diff + $value > 0)
                                    {
                                        $records[$res_key] += doubleval($diff + $value)  * doubleval($price);
                                    }
                                    else
                                    {
                                        $records[$res_key] += 0;
                                    }
                                }
                            }
                            else
                            {
                                $records[$res_key] += doubleval($value) * doubleval($price);
                            }
                        break;
                        
                        default:
                            $records[$res_key] += 0;
                    }
                } 
            }
            
            $prices                 =   $records;
            $prices['date']         =   $date;
            $prices['hosting_id']   =   $hosting_id;
            $prices['product_id']   =   $this->product_id;
            $prices['client_id']    =   $client_id;
            $prices['record_id']    =   $record_id;
            $prices['rel_id']       =   $rel_id;
            $prices['total']        =   array_sum($records);
            //Get Resource Keys
            $keys = array_keys($prices);
            
            foreach($keys as &$key)
            {
                $key = '`'.$key.'`';
            }
            
            //Insert Prices
            mysql_safequery("INSERT INTO `OnAppBilling_".$this->type."_prices` (".implode(',', $keys).") VALUES(".implode(',', array_fill(0, count($keys), '?')).")", $prices) or die(mysql_error());
                    
            //Insert Last Update
            mysql_safequery("DELETE FROM OnAppBilling_updates WHERE hosting_id = ? AND rel_id = ?", array($hosting_id, $rel_id));
            mysql_safequery("INSERT INTO OnAppBilling_updates(`hosting_id`, `rel_id`, `timestamp`) VALUES(?, ?, ?)", array($hosting_id, $rel_id, $date));
            
            //Insert Billed Account
            mysql_safequery("REPLACE INTO OnAppBilling_billed_hostings SET `date` = ?, hosting_id = ?", array($date, $hosting_id));
            
            MG_EventManager::call('OnAppBillingResourceAdded', $client_id, $this->product_id, $hosting_id, $record_id);
        } 
        
        protected function insertExtendedPricing($client_id, $hosting_id, $multi_resources = array())
        {
            //Is type setup?
            if(!$this->type)
            {
                $this->logError('Cannot add usage records because module type is not setup');
                return;
            }

            //Is pricing setup?
            if(!$this->pricing)
            {
                $this->logError('Cannot add resource. Pricing doest not exists for this module ('.$this->type.')');
                return;
            }
            
            //Re-Format Resources
            foreach($multi_resources as &$resources)
            {
                foreach($resources['Resources'] as $res_key => &$res_val)
                {
                    if(!is_array($res_val))
                    {
                        $val = $res_val;
                        $res_val = array
                        (
                            'type'      =>  'SinglePricing',
                            'resources' =>  array
                            (
                                $val
                            )
                        );
                    }
                    elseif(is_array($res_val) && !isset($res_val['type']))
                    {
                        $key = key($res_val);
                        $val = current($res_val);

                        $res_val = array
                        (
                            'type'      =>  'ExtendedPricing',
                            'resources' =>  array
                            (
                                $key    =>  $val
                            )
                        );
                    }
                }
            }
            
            //Just To Be Sure
            foreach($multi_resources as &$resources)
            {
                foreach($resources['Resources'] as $res_key => &$res_val)
                {
                    if(!is_array($res_val) || !in_array($res_val['type'], array('ExtendedMultiPricing', 'ExtendedPricing', 'SinglePricing')))
                    {
                        die('Invalid Pricing Type. '.$res_val['type'].' is not supported');
                    }
                }
            }
            
            //Get Current Usage
            $current_usage = array();
            $rows = mysql_get_array("SELECT resource, extended_id, value, rel_id FROM `OnAppBilling_".$this->type."_extendedPricing` WHERE hosting_id = ?", array($hosting_id));
            foreach($rows as $row)
            {
                $current_usage[$row['rel_id']][$row['resource']][$row['extended_id']] = $row['value'];
            }
            
            //Update Current Usage
            foreach($multi_resources as &$resources)
            {
                //Rel ID
                $rel_id = $resources['Relid'];
                
                foreach($resources['Resources'] as $res_key => &$res_val)
                {
                    foreach($res_val['resources'] as $ext_key => $ext_val)
                    {
                        if(!isset($current_usage[$rel_id][$res_key][$ext_key]))
                        {
                            mysql_safequery("INSERT INTO `OnAppBilling_".$this->type."_extendedPricing` (`hosting_id`, `rel_id`, `resource`, `extended_id`, `value`) VALUES(?, ?, ?, ?, ?)", array(
                                $hosting_id,
                                $rel_id,
                                $res_key,
                                $ext_key,
                                $ext_val,
                            )) or die(mysql_error());
                        }
                        else
                        {
                            mysql_safequery("UPDATE `OnAppBilling_".$this->type."_extendedPricing` SET `value` = `value` + ? WHERE `hosting_id` = ? AND `resource` = ? AND `extended_id` = ? AND `rel_id` = ?", array(
                                $ext_val,
                                $hosting_id,
                                $res_key,
                                $ext_key,
                                $rel_id
                            )) or die(mysql_error());
                        }
                    }
                }
            }
            
            //Calculate summary from multi array
            $summarized_resource = array();
            
            //Summarized records
            $summarized = array();
            
            //Summarize Multi Resources
            $multi_resources_summarized = array();

            foreach($multi_resources as $resource_key => &$resources)
            {
                foreach($resources['Resources'] as $key => &$val)
                {
                    foreach($val['resources'] as $ext_id => &$ext_val)
                    {
                        if(!isset($summarized_resource[$key]['resources'][$ext_id]))
                        {
                            $summarized_resource[$key] = array
                            (
                                'type'          =>  $val['type'],
                                'resources'     =>  array
                                (
                                    $ext_id =>  0
                                )
                            );
                        }
                        $summarized_resource[$key]['resources'][$ext_id] += doubleval($ext_val);

                        if(!isset($summarized[$key]))
                        {
                            $summarized[$key] = 0;
                        }
                        $summarized[$key] += doubleval($ext_val);

                        if(!isset($multi_resources_summarized[$resource_key][$key]))
                        {
                            $multi_resources_summarized[$resource_key][$key] = 0;
                        }
                        $multi_resources_summarized[$resource_key][$key] += doubleval($ext_val);
                    }
                }
            }

            //Current Time
            $curr_time = time();
            
            //MySQL Date
            $date = date('Y-m-d H:i:s', $curr_time);
            
            //Records IDs
            $records_ids = array();
            
            //Set the time diff
            $time_diff = 0.0001;
            if($this->interval)
            {
                $time_diff = $this->interval;
            }
            else
            {
                $last_update = strtotime($this->getLastUpdate($hosting_id));
                if(!$last_update)
                {
                    $last_update = doubleval($curr_time) - doubleval(0.0001);
                }
                
                $time_diff = doubleval($curr_time) - doubleval($last_update);
            }
             
            //Insert Usage Records
            foreach($multi_resources as $resource_key => &$resource)
            {
                $records                =   $multi_resources_summarized[$resource_key];
                $records['date']        =   $date;
                $records['hosting_id']  =   $hosting_id;
                $records['product_id']  =   $this->product_id;
                $records['client_id']   =   $client_id;
                $records['rel_id']      =   $resource['Relid'];
                //Get Resource Keys
                $keys = array_keys($records);
                 
                //Add 
                foreach($keys as &$key)
                {
                    $key = '`'.$key.'`';
                } 

                //Add Records To Database
                mysql_safequery("INSERT INTO `OnAppBilling_".$this->type."_records` (".implode(',', $keys).") VALUES(".implode(',', array_fill(0, count($keys), '?')).")", $records) or die(mysql_error());
                
                //Save Record ID. We need it!
                $records_ids[$resource_key] = mysql_insert_id();
            }

            
            //Records
            $records = array(); 
            
            foreach($summarized_resource as $res_key => &$res_val)
            {
                foreach($res_val['resources'] as $sub_key => &$sub_val)
                {
                    $value = 0;
                    $price = 0;
                    $free_limit = 0;

                    switch($res_val['type'])
                    {
                        case 'SinglePricing':
                            $value      = doubleval($sub_val);
                            $price      = doubleval($this->pricing[$res_key]['price']);
                            $free_limit = doubleval($this->pricing[$res_key]['free_limit']);
                            break;

                        case 'ExtendedPricing':
                        case 'ExtendedMultiPricing':
                            if(isset($this->pricing[$res_key]['ExtendedPricing'][$sub_key]))
                            {
                                $value      = doubleval($sub_val);
                                $price      = doubleval($this->pricing[$res_key]['ExtendedPricing'][$sub_key]['Price']);
                                $free_limit = doubleval($this->pricing[$res_key]['ExtendedPricing'][$sub_key]['FreeLimit']); 
                            }
                            else
                            {
                                $value      = doubleval($sub_val);
                                $price      = doubleval($this->pricing[$res_key]['price']);
                                $free_limit = doubleval($this->pricing[$res_key]['free_limit']);
                            }                 
                            break;
                    }

                    //Count Summary Prices
                    switch($this->pricing[$res_key]['type'])
                    { 
                        case 'average': 
                                $diff = doubleval($value) - doubleval($free_limit);
                                if($diff > 0)
                                {
                                    $records[$res_key][$sub_key] += (doubleval($time_diff) / (double)3600) * doubleval($price) * doubleval($diff);
                                }
                                else
                                {
                                    $records[$res_key][$sub_key] += 0;
                                }
                            break;

                        case 'summary':
                            if($free_limit > 0)
                            {
                                if($res_val['type'] == 'SingleResource')
                                {
                                    $row = mysql_get_row("SELECT SUM(".$res_key.") as `sum` FROM `OnAppBilling_".$this->type."_records` WHERE product_id = ? AND hosting_id = ?", array($this->product_id, $hosting_id));
                                    $sum = $row['sum'];
                                }
                                else
                                {
                                    $sum = $current_usage[$res_key][$sub_key];
                                }

                                $diff = doubleval($sum) - doubleval($free_limit);

                                if($diff > 0)
                                {
                                    $records[$res_key][$sub_key] += doubleval($value) * doubleval($price);
                                }
                                else 
                                { 
                                    if($diff + $value > 0)
                                    {
                                        $records[$res_key][$sub_key] += doubleval($diff + $value) * doubleval($price);
                                    }
                                    else
                                    {
                                        $records[$res_key][$sub_key] += 0;
                                    }
                                }
                            }
                            else
                            {
                                $records[$res_key][$sub_key] += doubleval($value) * doubleval($price);
                            }
                        break;
                        
                        default:
                            $records[$res_key][$sub_key] = 0;
                    }
                }
            }
            
            //Summarized Price
            $summarized_price = array();
            foreach($records as $res_key => &$res_val)
            {
                $summarized_price[$res_key] = array_sum($res_val);
            }
            
            foreach($multi_resources as $resource_key =>&$resource)
            {
                $prices = array_fill_keys(array_keys($summarized), 0);
            
                foreach($resource['Resources'] as $res_key => &$res_val)
                {
                    foreach($res_val['resources'] as $ext_key => &$ext_val)
                    {
                        $prices[$res_key] += ($ext_val / $summarized_resource[$res_key]['resources'][$ext_key]) * $summarized_price[$res_key];
                    }
                    
                }
                
                $prices['total']        =   array_sum($prices);
                $prices['date']         =   $date;
                $prices['hosting_id']   =   $hosting_id;
                $prices['product_id']   =   $this->product_id;
                $prices['client_id']    =   $client_id;
                $prices['record_id']    =   $records_ids[$resource_key];
                $prices['rel_id']       =   $resource['Relid'];
                //Get Resource Keys
                $keys = array_keys($prices);
                
                foreach($keys as &$key)
                {
                    $key = '`'.$key.'`';
                }

                //Insert Prices
                mysql_safequery("INSERT INTO `OnAppBilling_".$this->type."_prices` (".implode(',', $keys).") VALUES(".implode(',', array_fill(0, count($keys), '?')).")", $prices) or die(mysql_error());
            }
                        
            //Insert Last Update
            mysql_safequery("DELETE FROM OnAppBilling_updates WHERE hosting_id = ? AND rel_id = 0", array($hosting_id, 0));
            mysql_safequery("INSERT INTO OnAppBilling_updates(`hosting_id`, `rel_id`, `timestamp`) VALUES(?, ?, ?)", array($hosting_id, 0, $date));

            //Insert Billed Account
            mysql_safequery("REPLACE INTO OnAppBilling_billed_hostings SET `date` = ?, hosting_id = ?", array($date, $hosting_id));
            
            foreach($records_ids as $id)
            {
                MG_EventManager::call('OnAppBillingResourceAdded', $client_id, $this->product_id, $hosting_id, $id);
            }         
        }

        /**
         * Simple helper to get product accounts. For standard products it should works
         * @return type 
         * @author Mariusz Miodowski
         */
        protected function getProductAccounts()
        {
            $accounts = array();
            $q = mysql_safequery("SELECT DISTINCT h.id as hosting_id, c.email, h.domain, h.username as customerid, h.password as customerpass, s.ipaddress, s.hostname, s.username, s.password, s.accesshash, s.secure
                                  FROM `tblhosting` h 
                                  JOIN `tblproducts` p ON(h.packageid = p.id) 
                                  LEFT JOIN `tblservers` s ON(h.server = s.id)
                                  JOIN `tblclients` c ON(c.id = h.userid)
                                  WHERE p.servertype = ? AND p.id = ?", array($this->type, $this->product_id));

            while($row = mysql_fetch_assoc($q))
            {
                $accounts[] = array
                (
                    'username'          =>  $row['customerid'],
                    'client_id'         =>  $row['customerid'],
                    'customerpass'      =>  $row['customerpass'],
                    'hosting_id'        =>  $row['hosting_id'],
                    'serverip'          =>  $row['ipaddress'],
                    'serverhostname'    =>  $row['hostname'],
                    'serverusername'    =>  $row['username'],
                    'serverpassword'    =>  decrypt($row['password']),
                    'serveraccesshash'  =>  $row['accesshash'],
                    'serversecure'      =>  $row['secure'],
                    'domain'            =>  $row['domain'],
                    'email'             =>  $row['email']
                );
            }

            return $accounts;
        }

        public function setBillingOptions($billing_options)
        {
            $this->billing_options = $billing_options;
        }

        public function getDescription()
        {
            return $this->description;
        }
   
        public function getSupportedModules()
        {
            return $this->getSupportedModules();
        }
        /**
         * Zwraca czas ostatniego uzycia getSample. Czasem może sie przydac ;] 
         * @param type $hosting_id
         * @return type 
         * @author Mariusz Miodowski
         */
        protected function getLastUpdate($hosting_id, $rel_id = 0)
        {
            $q = mysql_safequery('SELECT `timestamp` FROM OnAppBilling_updates WHERE hosting_id = ? AND rel_id = ?', array($hosting_id, $rel_id));
            $row = mysql_fetch_assoc($q);
            
            if($row)
            {
                return $row['timestamp'];
            }
            
            return false;
        }

        /**
         * Simple function to storing data per account. 
         * @param type $hosting_id
         * @param type $data 
         * @author Mariusz Miodowski
         */
        protected function saveSettings($hosting_id, $data = array())
        {
            $q = mysql_safequery("REPLACE INTO OnAppBilling_submodules_data SET hosting_id = ?, data = ?", array($hosting_id, serialize($data)));
        }

        /**
         * Read module data for account
         * @param type $hosting_id
         * @return type 
         * @author Mariusz Miodowski
         */
        protected function getSettings($hosting_id)
        {
            $q = mysql_safequery("SELECT data FROM OnAppBilling_submodules_data WHERE hosting_id = ? LIMIT 1", array($hosting_id));
            if(mysql_num_rows($q))
            {
                $row = mysql_fetch_assoc($q);
                return unserialize($row['data']);
            }

            return array();
        }

        protected function hasResourceRecords($hosting_id)
        {
            $type = strtolower($this->type);
            $row = mysql_get_row("SELECT * FROM OnAppBilling_".$type."_prices WHERE hosting_id = ? AND product_id = ? LIMIT 1", array($hosting_id, $this->product_id));

            return $row ? true : false;
        }

        public function getConfigurationArea()
        {
            return $this->configuration_area;
        }

        public function setPricing($pricing = array())
        {
            $this->pricing = $pricing;
        }

        protected function setInterval($interval)
        {
            $this->interval = $interval;
        }

        protected function logError($error)
        {
            if(!file_exists(($filename = ONAPP_BILLING_DIR.DS.'cron'.DS.'logs'.DS.'errorlog.log')))
            {
                fopen($filename, 'w');
            }
            if(file_exists($filename))
            {
                file_put_contents($filename, date("Y-m-d G:i:s").': '.$error."\n", FILE_APPEND);
            }
        }

        protected function logInfo($info)
        {
            if(!file_exists(($filename = ONAPP_BILLING_DIR.DS.'cron'.DS.'logs'.DS.'info.log')))
            {
                    fopen($filename, 'w');
            }
            if(file_exists($filename))
            {
                file_put_contents($filename, date("Y-m-d G:i:s").': '.$info."\n", FILE_APPEND);
            }
        }


        /****************** 1.4 *******************/
        public function getRelIDName($rel_id){}
    }
}