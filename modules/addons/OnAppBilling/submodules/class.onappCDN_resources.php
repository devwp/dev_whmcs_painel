<?php 

if(!class_exists('onappCDN_resources'))
{
    class onappCDN_resources extends OAResource
    {
        //Set interval
        protected $interval = 3600;
        //Enable extended pricing
        protected $extendedPricing = true;
        //Enable mulit parsts
        protected $enableMultiItems = true;
        
        //modules descprion
        const description = '';

        //module name
        const name = 'OnApp CDN';

        protected $resources = array(
            'traffic'                   =>  array
            (
                'FriendlyName'          =>  'Traffic',
                'Description'           =>  'Bill your client by traffic',
                'InvoiceDescription'    =>  '',
                'Unit'                  =>  'MB',
                'AvailableTypes'        =>  array('summary'),
                'AvailableUnits'        =>  array
                (
                    'MB'                =>  1,
                    'GB'                =>  0.0009765625
                )
            ),
        );
        
        
        protected $configuration = array
        (
            'default_unit'   =>  array
            (
                'FriendlyName'      =>  'Default Unit',
                'Description'       =>  'Specify Default Unit',
                'Type'              =>  'Select',
                'Select'            =>  array
                (
                    '1048576'       =>  'MB',
                    '1073741824'    =>  'GB',
                )
            )
        );

        public function __construct($product_id = 0)
        {
            parent::__construct($product_id);
            
            //Use GB!
            if($this->configuration['default_unit']['Value'] == '1073741824')
            {
                unset($this->resources['traffic']['AvailableUnits']);
                $this->resources['traffic']['Unit'] = 'GB';
            }
        }

        public function loadExtendedPricingConfiguration()
        {
            require_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'class.OnApp_Connection.php';
            require_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'class.OnApp_WrapperAPI.php';
            require_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'class.OnApp_CDNEdgeGroup.php';

            $params = mysql_get_row("
                    SELECT
                            s.ipaddress AS serverip, s.hostname AS serverhostname, s.username AS serverusername, s.password AS serverpassword, s.secure AS serversecure,
                            configoption1,configoption2,configoption3,configoption4,configoption5,configoption6,configoption7,configoption8,configoption9
                    FROM tblservers AS s
                    JOIN tblservergroupsrel AS sgr ON sgr.serverid = s.id
                    JOIN tblservergroups AS sg ON sgr.groupid = sg.id
                    JOIN tblproducts AS p ON p.servergroup = sg.id
                    WHERE p.id = ?
                    ORDER BY s.active DESC
                    LIMIT 1",
                    array($this->product_id)
            );

            $params['serverpassword'] = decrypt($params['serverpassword']);

            $edgeGroup = new NewOnApp_CDNEdgeGroup();
            $edgeGroup ->setconnection($params, false);
            $edge_list = $edgeGroup->getList();

            if($edge_list)
            {
                foreach($edge_list as $lst)
                {
                    $this->resources['traffic']['ExtendedPricing'][] = array
                    (
                        'FriendlyName'  =>  $lst['edge_group']['label'],
                        'Relid'         =>  $lst['edge_group']['id']
                    );
                }
            }
        }

        public function getProductAccounts() 
        {
            $accounts = mysql_get_array("SELECT DISTINCT h.id as hosting_id, h.userid as user_id, h.username, h.password, s.ipaddress as serverip, s.hostname as serverhostname, s.username as serverusername, s.password as serverpassword, s.secure as serversecure
                                  FROM `tblhosting` h 
                                  JOIN `tblproducts` p ON(h.packageid = p.id) 
                                  LEFT JOIN `tblservers` s ON(h.server = s.id)
                                  WHERE p.servertype = ? AND p.id = ? AND h.domainstatus = 'Active'", array($this->type, $this->product_id));

            foreach($accounts as &$a)
            {
                $a['password']          =   decrypt($a['password']);
                $a['serverpassword']    =   decrypt($a['serverpassword']);

                $customfields = mysql_get_array("SELECT c.fieldname, v.value
                    FROM tblcustomfields c
                    LEFT JOIN tblcustomfieldsvalues v ON (c.id = v.fieldid AND v.relid = ?)
                    WHERE c.type='product' AND c.relid=?", array($a['hosting_id'], $this->product_id));

                foreach($customfields as $c)
                {
                    if(strpos($c['fieldname'], '|'))
                    {
                        $exp            =   explode('|', $c['fieldname']);
                        $c['fieldname'] =   $exp[0];
                    }
                    $a[$c['fieldname']] = $c['value'];
                }
            }

            return $accounts;
        }

        public function getSample()
        {
            $unitFactor = 1024 * 1024;
            if($this->configuration['default_unit']['Value'])
            {
                $unitFactor = $this->configuration['default_unit']['Value'];
            }
            
            $accounts = $this->getProductAccounts();

            if(!$accounts)
            {
                return false;
            }

            //load files
            require_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'class.OnApp_Connection.php';
            require_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'class.OnApp_WrapperAPI.php';
            require_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'class.OnApp_CDNResource.php';

            
            foreach($accounts as $account)
            {
                //Get Time
                $time = (int)gmdate('U');
                //get settings
                $settings = $this->getSettings($account['hosting_id']);
                     
                if(!$settings)
                {
                    $settings       =   array
                    (
                        'start_date' =>  $time,
                    );

                    $this->saveSettings($account['hosting_id'], $settings);
                }
                elseif($settings['start_date'] + 3600 > $time)
                {
                    $this->saveSettings($account['hosting_id'], $settings);
                    continue;
                }

                $resource  = new NewOnApp_CDNResource();
                $resource  ->setconnection($account, true);
                $resources = $resource->getList();

                //Get only user CDNs
                $new = array();
                foreach($resources as $key =>$value)
                {
                    if($value['cdn_resource']['user_id']== $account['cdnuserid'])
                    {
                        $new[] = $value;
                    
                        //Update CDN records
                        mysql_query(
                            "INSERT INTO `OnAppBilling_CDN_hostnames` (cdn_id, hosting_id, hostname) 
                             VALUES('{$value["cdn_resource"]["id"]}', {$account['hosting_id']}, '{$value["cdn_resource"]["cdn_hostname"]}')
                             ON DUPLICATE KEY UPDATE hosting_id={$account['hosting_id']}, hostname='{$value["cdn_resource"]["cdn_hostname"]}'"
                        );
                    }
                }
                $resources  =   $new;

                if($resource->error())
                {
                    $this->logError($resource->error());
                    continue;
                }

                if(!$resources)
                {
                    continue;
                }
                
                foreach($resources as &$res)
                {
                    $res_id     =   $res['cdn_resource']['id'];

                    $billing    =   $resource->billing($res_id, array(
                    'period'    =>  array
                    (
                        'startdate'         =>  gmdate('Y-m-d+H:i:s', $settings['start_date']),
                    )));

                    if($billing)
                    {
                        $usage = array();
                        
                        foreach($billing as $bill)
                        {
                            if(!isset($usage[$bill['user_hourly_stat']['edge_group_id']]))
                            {
                                $usage[$bill['user_hourly_stat']['edge_group_id']] = 0;
                            }
                            
                            $usage[$bill['user_hourly_stat']['edge_group_id']] += (doubleval($bill['user_hourly_stat']['value']) / ($unitFactor));
                        }
                        
                        foreach($usage as $ext_key => $ext_val)
                        {
                            //reset usage records
                            $account_resources_usages = array();

                            //Trafiic
                            $account_resources_usages['traffic'][$ext_key] = $ext_val;
                            
                            //Insert Usage Record
                            $this->insertResource($account['user_id'], $account['hosting_id'], $account_resources_usages, $res_id);
                        }
                    }
                }

                //Update Settings
                $this->saveSettings($account['hosting_id'], array(
                    'start_date'    =>  $time
                ));
            }
        }
        
        public function getMultiItemsList($accountId, $relIdsArray = array())
        {
            require_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'class.OnApp_Connection.php';
            require_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'class.OnApp_WrapperAPI.php';
            require_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'class.OnApp_CDNResource.php';
            
            
            $account = mysql_get_row("SELECT DISTINCT h.id as hosting_id, h.userid as user_id, h.username, h.password, s.ipaddress as serverip, s.hostname as serverhostname, s.username as serverusername, s.password as serverpassword, s.secure as serversecure
                    FROM `tblhosting` h 
                    JOIN `tblproducts` p ON(h.packageid = p.id) 
                    LEFT JOIN `tblservers` s ON(h.server = s.id)
                    WHERE p.servertype = ? AND p.id = ? AND h.domainstatus = 'Active' AND h.id = ?", array($this->type, $this->product_id, $accountId));

            if(!$account)
            {
                return false;
            }

            $account['password']          =   decrypt($account['password']);
            $account['serverpassword']    =   decrypt($account['serverpassword']);

            $customfields = mysql_get_array("SELECT c.fieldname, v.value
                FROM tblcustomfields c
                LEFT JOIN tblcustomfieldsvalues v ON (c.id = v.fieldid AND v.relid = ?)
                WHERE c.type='product' AND c.relid=?", array($account['hosting_id'], $this->product_id));

            foreach($customfields as $c)
            {
                if(strpos($c['fieldname'], '|'))
                {
                    $exp            =   explode('|', $c['fieldname']);
                    $c['fieldname'] =   $exp[0];
                }
                $account[$c['fieldname']] = $c['value'];
            }
            unset($customfields);
//            $resource  = new NewOnApp_CDNResource();
//            $resource  ->setconnection($account, true);
//            $resources = $resource->getList();
//            
//            $out = array();
//            foreach($resources as $cdn)
//            {
//                if(in_array($cdn['cdn_resource']['id'], $relIdsArray))
//                {
//                    $out[$cdn['cdn_resource']['id']] = $cdn['cdn_resource']['cdn_hostname'];
//                }
//            }
            $cdnResources = mysql_get_array("SELECT *
                FROM OnAppBilling_CDN_hostnames
                WHERE hosting_id=?", array($account['hosting_id'])
            );
            
            $out = array();
            foreach($cdnResources as $cdn)
            {
                if(in_array($cdn['cdn_id'], $relIdsArray))
                {
                    $out[$cdn['cdn_id']] = $cdn['hostname'];
                }
            }
            unset($cdnResources, $account);
            
            //just to be sure
            if(count($out) != count($relIdsArray))
            {
                foreach($relIdsArray as $relId)
                {
                    if(!isset($out[$relId]))
                    {
                        $out[$relId] = $relId;
                    }
                }
            }
            
            return $out;
        }
    }
}