<?php 

if(!class_exists('onappVPS_resources'))
{
    class onappVPS_resources extends OAResource
    {
        protected $interval = 3600;

        const name = 'OnAppVPS';

        //modules descprion
        const description = '';

        protected $resources = array
        (
            'disk_size'                 =>  array
            (
                'FriendlyName'          =>  'Disk Size',
                'Description'           =>  'Disk Size',
                'InvoiceDescription'    =>  '',
                'Unit'                  =>  'GB/hr',
                'AvailableTypes'        =>  array('average')
            ),
            'disk_data_read'            =>  array
            (
                'FriendlyName'          =>  'Disk Data Read',
                'Description'           =>  'Disk Data Read',
                'InvoiceDescription'    =>  '',
                'Unit'                  =>  'Kb/hr',
                'AvailableTypes'        =>  array('average')
            ),
            'disk_data_written'         =>  array
            (
                'FriendlyName'          =>  'Disk Data Written',
                'Description'           =>  'Disk Data Written',
                'InvoiceDescription'    =>  '',
                'Unit'                  =>  'Kb/hr',
                'AvailableTypes'        =>  array('average')
            ),
            'disk_reads_completed'      =>  array
            (
                'FriendlyName'          =>  'Disk Reads Completed',
                'Description'           =>  'Disk Reads Completed',
                'InvoiceDescription'    =>  '',
                'Unit'                  =>  '/hr',
                'AvailableTypes'        =>  array('average')
            ),
            'disk_writes_completed'      =>  array
            (
                'FriendlyName'          =>  'Disk Writes Completed',
                'Description'           =>  'Disk Writes Completed',
                'InvoiceDescription'    =>  '',
                'Unit'                  =>  '/hr',
                'AvailableTypes'        =>  array('average')
            ),
            //Network
            'ip_addresses'              =>  array
            (
                'FriendlyName'          =>  'IP Addresses',
                'Description'           =>  'IP Addresses',
                'InvoiceDescription'    =>  '',
                'Unit'                  =>  '/hr',
                'AvailableTypes'        =>  array('average')
            ),
            'bandwidth_in'              =>  array
            (
                'FriendlyName'          =>  'Bandwidth IN',
                'Description'           =>  'Bandwidth IN',
                'InvoiceDescription'    =>  '',
                'Unit'                  =>  'MB',
                'AvailableTypes'        =>  array('summary'),
                'AvailableUnits'        =>  array
                (
                    'MB'                =>  1,
                    'GB'                =>  0.0009765625
                )
            ),
            'bandwidth_out'              =>  array
            (
                'FriendlyName'          =>  'Bandwidth OUT',
                'Description'           =>  'Bandwidth OUT',
                'InvoiceDescription'    =>  '',
                'Unit'                  =>  'MB',
                'AvailableTypes'        =>  array('summary'),
                'AvailableUnits'        =>  array
                (
                    'MB'                =>  1,
                    'GB'                =>  0.0009765625
                )
            ),
            'bandwidth_total'           =>  array
            (
                'FriendlyName'          =>  'Bandwidth Total',
                'Description'           =>  'Bandwidth Total',
                'InvoiceDescription'    =>  '',
                'Unit'                  =>  'MB',
                'AvailableTypes'        =>  array('summary'),
                'AvailableUnits'        =>  array
                (
                    'MB'                =>  1,
                    'GB'                =>  0.0009765625
                )
            ),
            'network_rate'              =>  array
            (
                'FriendlyName'          =>  'Network Rate',
                'Description'           =>  'Network Rate',
                'InvoiceDescription'    =>  '',
                'Unit'                  =>  'Mbps/hr',
                'AvailableTypes'        =>  array('average')
            ),
            'cpu_usage'                 =>  array
            (
                'FriendlyName'          =>  'CPU Usage',
                'Description'           =>  'CPU Usage',
                'InvoiceDescription'    =>  '',
                'Unit'                  =>  '/hr',
                'AvailableTypes'        =>  array('average')
            ),
            'cpu_shares'                =>  array
            (
                'FriendlyName'          =>  'CPU Shares',
                'Description'           =>  'CPU Shares',
                'InvoiceDescription'    =>  '',
                'Unit'                  =>  '/hr',
                'AvailableTypes'        =>  array('average')
            ),
            'cpus'                      =>  array
            (
                'FriendlyName'          =>  'CPUs',
                'Description'           =>  'CPUs',
                'InvoiceDescription'    =>  '',
                'Unit'                  =>  '/hr',
                'AvailableTypes'        =>  array('average')
            ),
            'memory'                    =>  array
            (
                'FriendlyName'          =>  'Memory',
                'Description'           =>  'Memory',
                'InvoiceDescription'    =>  '',
                'Unit'                  =>  'MB/hr',
                'AvailableTypes'        =>  array('average')
            ),
            'backups'                   =>  array
            (
                'FriendlyName'          =>  'Backups',
                'Description'           =>  'Backups',
                'InvoiceDescription'    =>  '',
                'Unit'                  =>  'GB/hr',
                'AvailableTypes'        =>  array('average')
            )
        );

        public function loadExtendedPricingConfiguration()
        {
            require_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'class.OnApp_Connection.php';
            require_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'class.OnApp_WrapperAPI.php';
            require_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'class.OnApp_NetworkZone.php';
            require_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'class.OnApp_DataStoreZone.php';
            
            $params = mysql_get_row("
                    SELECT
                            s.ipaddress AS serverip, s.hostname AS serverhostname, s.username AS serverusername, s.password AS serverpassword, s.secure AS serversecure,
                            configoption1,configoption2,configoption3,configoption4,configoption5,configoption6,configoption7,configoption8,configoption9
                    FROM tblservers AS s
                    JOIN tblservergroupsrel AS sgr ON sgr.serverid = s.id
                    JOIN tblservergroups AS sg ON sgr.groupid = sg.id
                    JOIN tblproducts AS p ON p.servergroup = sg.id
                    WHERE p.id = ?
                    ORDER BY s.active DESC
                    LIMIT 1",
                    array($this->product_id)
            );

            $params['serverpassword'] = decrypt($params['serverpassword']);

            //Network Zone
            $networkZone   = new NewOnApp_NetworkZone(null);
            $networkZone   -> setconnection($params);
            $networks  = $networkZone->getList();
            
            if(!$networkZone->error())
            {
                foreach ($networks as $network) 
                {
                    $this->resources['bandwidth_in']['ExtendedPricing'][] = array
                    (
                        'FriendlyName'  =>  $network['network_group']['label'],
                        'Relid'         =>  $network['network_group']['id']
                    );
                                        
                    $this->resources['bandwidth_out']['ExtendedPricing'][] = array
                    (
                        'FriendlyName'  =>  $network['network_group']['label'],
                        'Relid'         =>  $network['network_group']['id']
                    );
                    
                    $this->resources['bandwidth_total']['ExtendedPricing'][] = array
                    (
                        'FriendlyName'  =>  $network['network_group']['label'],
                        'Relid'         =>  $network['network_group']['id']
                    );                  
                    
                    $this->resources['network_rate']['ExtendedPricing'][] = array
                    (
                        'FriendlyName'  =>  $network['network_group']['label'],
                        'Relid'         =>  $network['network_group']['id']
                    );
                    
                    $this->resources['ip_addresses']['ExtendedPricing'][] = array
                    (
                        'FriendlyName'  =>  $network['network_group']['label'],
                        'Relid'         =>  $network['network_group']['id']
                    );
                }
            }   
            
            //Get Data Stores
            $dataStoreZone  =   new NewOnApp_DataStoreZone(null);         
            $dataStoreZone  ->  setconnection($params);
            $zones          =   $dataStoreZone->getList();   
            
            if(!$dataStoreZone->error())
            {
                foreach($zones as $zone)
                {
                    $this->resources['disk_size']['ExtendedPricing'][] = array
                    (
                        'FriendlyName'  =>  $zone['data_store_group']['label'],
                        'Relid'         =>  $zone['data_store_group']['id']
                    );
                                        
                    $this->resources['disk_data_read']['ExtendedPricing'][] = array
                    (
                        'FriendlyName'  =>  $zone['data_store_group']['label'],
                        'Relid'         =>  $zone['data_store_group']['id']
                    );
                    
                    $this->resources['disk_data_written']['ExtendedPricing'][] = array
                    (
                        'FriendlyName'  =>  $zone['data_store_group']['label'],
                        'Relid'         =>  $zone['data_store_group']['id']
                    );
                    
                    $this->resources['disk_reads_completed']['ExtendedPricing'][] = array
                    (
                        'FriendlyName'  =>  $zone['data_store_group']['label'],
                        'Relid'         =>  $zone['data_store_group']['id']
                    );
                    
                    $this->resources['disk_writes_completed']['ExtendedPricing'][] = array
                    (
                        'FriendlyName'  =>  $zone['data_store_group']['label'],
                        'Relid'         =>  $zone['data_store_group']['id']
                    );
                    
                }
            } 
        }
        
        public function getProductAccounts() 
        {
            $accounts = mysql_get_array("SELECT DISTINCT h.id as hosting_id, h.userid as user_id, h.username, h.password, s.ipaddress as serverip, s.hostname as serverhostname, s.username as serverusername, s.password as serverpassword, s.secure as serversecure
                                  FROM `tblhosting` h 
                                  JOIN `tblproducts` p ON(h.packageid = p.id) 
                                  LEFT JOIN `tblservers` s ON(h.server = s.id)
                                  WHERE p.servertype = ? AND p.id = ?", array($this->type, $this->product_id));

            foreach($accounts as &$a)
            {
                $a['password']          =   decrypt($a['password']);
                $a['serverpassword']    =   decrypt($a['serverpassword']);

                $customfields = mysql_get_array("SELECT c.fieldname, v.value
                    FROM tblcustomfields c
                    LEFT JOIN tblcustomfieldsvalues v ON (c.id = v.fieldid AND v.relid = ?)
                    WHERE c.type='product' AND c.relid=?", array($a['hosting_id'], $this->product_id));

                foreach($customfields as $c)
                {
                    if(strpos($c['fieldname'], '|'))
                    {
                        $exp            =   explode('|', $c['fieldname']);
                        $c['fieldname'] =   $exp[0];
                    }
                    $a[$c['fieldname']] = $c['value'];
                }
            }

            return $accounts;
        }

        public function getSample()
        {
            $accounts = $this->getProductAccounts();
            
            if(!$accounts)
            {
                return false;
            }

            //load files
            require_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'class.OnApp_Connection.php';
            require_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'class.OnApp_WrapperAPI.php';
            require_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'class.OnApp_Users.php';
            require_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'class.OnApp_VM.php';
            require_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'class.OnApp_Network.php';
            require_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'class.OnApp_NetworkZone.php';
            require_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'class.OnApp_NetworkInterface.php';
            require_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'class.OnApp_IPAddressJoin.php'; 
            require_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'class.OnApp_VMBackup.php';
            
            foreach($accounts as $account)
            {
                if(!$account['vmid'])
                {
                    continue;
                }
                
                $time = (int)gmdate('U');
                
                //get settings
                $settings = $this->getSettings($account['hosting_id']);

                if(!$settings)
                {
                    $settings       =   array
                    (
                        'start_date'    =>  $time,
                    );

                    $this->saveSettings($account['hosting_id'], $settings);
                }
                elseif($settings['start_date'] + 3600 > $time)
                {
                    $this->saveSettings($account['hosting_id'], $settings);
                    continue;
                }
                 
                //Create Class And Gets Machine Stats
                $vm     =       new NewOnApp_VM($account['vmid']);
                $vm->setConnection($account);
                $stats =    $vm->getStats(array(
                    'period'    =>  array
                    (
                        'startdate'         =>  gmdate('Y-m-d+H:i:s', $settings['start_date']),
                    )
                )); 

                //Get VM Info
                $info  =    $vm->getDetails();
                
                if(!$stats || $vm->error() || !$info)
                {
                    if($vm->error())
                    {
                        $this->logError('Hosting #'.$account['hosting_id'].': '.$vm->error());
                    }
                    continue;
                }
                    
                //Get Network Joins
                $ip_join    = new NewOnApp_IPAddressJoin($account['vmid']);
                $ip_join    ->setconnection($account);
                $response   = $ip_join->getList();
                $interfaces = array();
                foreach($response as $res)
                {
                    $interfaces[$res['ip_address_join']['network_interface_id']] = $res['ip_address_join']['ip_address_id'];
                }
                
                //VM IP Addresses
                $ip_addresses = array();
                foreach($info['virtual_machine']['ip_addresses'] as $ip)
                {
                    $ip_addresses[$ip['ip_address']['id']] = $ip['ip_address']['network_id'];
                }
                
                //Get All Available Networks
                $network    =   new NewOnApp_Network(null); 
                $network->setconnection($account);
                $response   =   $network->getList();
                $networks = array();
                foreach($response as $res)
                {
                    $networks[$res['network']['id']] = $res['network']['network_group_id'];
                }
                unset($response);
                
                //Connect Interfaces With Zones!
                $zones = array();
                foreach($interfaces as $network_join_id => $ip_address_id)
                {
                    $zones[$network_join_id] = $networks[$ip_addresses[$ip_address_id]];
                }

                //Get VM Disk. We need data store ID!
                $response = $vm->getDisks();
                $disks = array();

                foreach($response as $res)
                {
                    $disks[$res['disk']['id']] = $res['disk']['data_store_id'];
                }
                unset($response);
                
                
                //Get VM Backups
                $backups = new NewOnApp_VMBackup();
                $backups->setconnection($account);
                $response = $backups->getList($account['vmid']);
                $backupUsage = 0;
                
                if(!empty($response))
                {
                    foreach($response as $backup)
                    {
                        $backupUsage += ($backup['backup']['backup_size'] / (1024*1024));
                    }
                }
         
                foreach($stats as $stat)
                {
                    //reset usage records
                    $account_resources_usages = array();
                    foreach(array_keys($this->resources) as $key)
                    {
                        $account_resources_usages[$key] = array();
                    }

                    $values = array();
                    foreach($stat['vm_hourly_stat']['billing_stats'] as $record_type => $records_values)
                    {
                        foreach($records_values as $records_value)
                        {          
                            //Parent ID. We need it for extended pricing
                            $parent_id = isset($records_value['id']) ? $records_value['id'] : 0;

                            foreach($records_value['costs'] as $cost)
                            {
                                if(isset($values[$record_type][$cost['resource_name']][$parent_id]))
                                {
                                    $values[$record_type][$cost['resource_name']][$parent_id] += doubleval($cost['value']);
                                }
                                else
                                {
                                    $values[$record_type][$cost['resource_name']][$parent_id] = doubleval($cost['value']);
                                }
                            }
                        }
                    }
                    
                    //Disk Size
                    if(isset($values['disks']['disk_size']))
                    { 
                        foreach($values['disks']['disk_size'] as $parent_id => $value)
                        {
                            if(!isset($account_resources_usages['disk_size'][$disks[$parent_id]]))
                            {
                                $account_resources_usages['disk_size'][$disks[$parent_id]] = 0;
                            }
                            
                            $account_resources_usages['disk_size'][$disks[$parent_id]] += $value;
                        }
                    }
                    else
                    {
                        $account_resources_usages['disk_size'][0] =   doubleval($values['disks']['disk_size']) > 0 ? doubleval($values['disks']['disk_size']) : doubleval($info['virtual_machine']['total_disk_size']);
                    }
                    
                    
                    //Disk Data Read                     
                    if(isset($values['disks']['data_read']))
                    {
                        foreach($values['disks']['data_read'] as $parent_id => $value)
                        {
                            if(!isset($account_resources_usages['disk_data_read'][$disks[$parent_id]]))
                            {
                                $account_resources_usages['disk_data_read'][$disks[$parent_id]] = 0;
                            }
                            
                            $account_resources_usages['disk_data_read'][$disks[$parent_id]] += $value;
                        }
                    }
                    else
                    {
                        $account_resources_usages['disk_data_read'] = 0;
                    }
                    
                    //Disk Data Written
                    if(isset($values['disks']['data_written']))
                    {
                        foreach($values['disks']['data_written'] as $parent_id => $value)
                        {
                            if(!isset($account_resources_usages['disk_data_written'][$disks[$parent_id]]))
                            {
                                $account_resources_usages['disk_data_written'][$disks[$parent_id]] = 0;
                            }
                            
                            $account_resources_usages['disk_data_written'][$disks[$parent_id]] += $value;
                        }
                    }
                    else
                    {
                        $account_resources_usages['disk_data_written'] = 0;
                    }


                    //Disk Read Completed
                    if(isset($values['disks']['reads_completed']))
                    {
                        foreach($values['disks']['reads_completed'] as $parent_id => $value)
                        {
                            if(!isset($account_resources_usages['disk_reads_completed'][$disks[$parent_id]]))
                            {
                                $account_resources_usages['disk_reads_completed'][$disks[$parent_id]] = 0;
                            }
                            
                            $account_resources_usages['disk_reads_completed'][$disks[$parent_id]] += $value;
                        }
                    }
                    else
                    {
                        $account_resources_usages['disk_reads_completed'] = 0;
                    }
                    
                    
                    //Disk Writes Completed                   
                    if(isset($values['disks']['writes_completed']))
                    {  
                        foreach($values['disks']['writes_completed'] as $parent_id => $value)
                        {
                            if(!isset($account_resources_usages['disk_writes_completed'][$disks[$parent_id]]))
                            {
                                $account_resources_usages['disk_writes_completed'][$disks[$parent_id]] = 0;
                            }
                            
                            $account_resources_usages['disk_writes_completed'][$disks[$parent_id]] += $value;
                        }
                    }
                    else
                    {
                        $account_resources_usages['disk_writes_completed'] = 0;
                    }
                    
                    //IP Addresses                      
                    if(isset($values['network_interfaces']['ip_addresses']))
                    {
                        foreach($values['network_interfaces']['ip_addresses'] as $parent_id => $value)
                        {
                            if(!isset($account_resources_usages['ip_addresses'][$zones[$parent_id]]))
                            {
                                $account_resources_usages['ip_addresses'][$zones[$parent_id]] = 0;
                            }
                            
                            $account_resources_usages['ip_addresses'][$zones[$parent_id]] += $value;
                        }
                    }
                    else
                    {
                        $account_resources_usages['ip_addresses'] = 0;
                    }
                    
                    //Network Interfaces               
                    if(isset($values['network_interfaces']['rate']))
                    {
                        foreach($values['network_interfaces']['rate'] as $parent_id => $value)
                        {
                            if(!isset($account_resources_usages['network_rate'][$zones[$parent_id]]))
                            {
                                $account_resources_usages['network_rate'][$zones[$parent_id]] = 0;
                            }
                            
                            $account_resources_usages['network_rate'][$zones[$parent_id]] += $value;
                        }
                    }
                    else
                    {
                        $account_resources_usages['network_rate'] = 0;
                    }
                    
                    //Data Receive                   
                    if(isset($values['network_interfaces']['data_received']))
                    { 
                        foreach($values['network_interfaces']['data_received'] as $parent_id => $value)
                        {
                            if(!isset($account_resources_usages['bandwidth_in'][$zones[$parent_id]]))
                            {
                                $account_resources_usages['bandwidth_in'][$zones[$parent_id]] = 0;
                            }
                            
                            $account_resources_usages['bandwidth_in'][$zones[$parent_id]] += $value / 1024;
                        } 
                    }
                    else
                    {
                        $account_resources_usages['bandwidth_in'] = 0;
                    }

                    //Data Sent                     
                    if(isset($values['network_interfaces']['data_sent']))
                    {  
                        foreach($values['network_interfaces']['data_sent'] as $parent_id => $value)
                        {
                            if(!isset($account_resources_usages['bandwidth_out'][$zones[$parent_id]]))
                            {
                                $account_resources_usages['bandwidth_out'][$zones[$parent_id]] = 0;
                            }
                            
                            $account_resources_usages['bandwidth_out'][$zones[$parent_id]] += $value / 1024;
                        }
                    }
                    else
                    {
                        $account_resources_usages['bandwidth_out'] = 0;
                    }
                    
                    //Data Total
                    if(isset($values['network_interfaces']['data_sent']))
                    {  
                        foreach($values['network_interfaces']['data_sent'] as $parent_id => $value)
                        {
                            if(!isset($account_resources_usages['bandwidth_total'][$zones[$parent_id]]))
                            {
                                $account_resources_usages['bandwidth_total'][$zones[$parent_id]] = 0;
                            }
                            
                            $account_resources_usages['bandwidth_total'][$zones[$parent_id]] += $value / 1024;
                        }
                    }
                    else
                    {
                        $account_resources_usages['bandwidth_total'] = 0;
                    }
                    
                    if(isset($values['network_interfaces']['data_received']))
                    { 
                        foreach($values['network_interfaces']['data_received'] as $parent_id => $value)
                        {
                            if(!isset($account_resources_usages['bandwidth_total'][$zones[$parent_id]]))
                            {
                                $account_resources_usages['bandwidth_total'][$zones[$parent_id]] = 0;
                            }
                            
                            $account_resources_usages['bandwidth_total'][$zones[$parent_id]] += $value / 1024;
                        } 
                    }
                    else
                    {
                        $account_resources_usages['bandwidth_total'] = 0;
                    }
                     
                    //CPU Usage
                    if(isset($values['virtual_machines']['cpu_usage']))
                    {
                        $account_resources_usages['cpu_usage'] = doubleval(current($values['virtual_machines']['cpu_usage']));
                    }
                    else
                    {
                        $account_resources_usages['cpu_usage'] =   0;
                    }

                    //CPU Shares
                    if(isset($values['virtual_machines']['cpu_shares']))
                    {
                        $account_resources_usages['cpu_shares'] = doubleval(current($values['virtual_machines']['cpu_shares']));
                    }
                    else
                    {
                        $account_resources_usages['cpu_shares'] = doubleval($info['virtual_machine']['cpu_shares']);
                    }
                    
                    //CPUS
                    if(isset($values['virtual_machines']['cpus']))
                    {
                        $account_resources_usages['cpus'] =  doubleval(current($values['virtual_machines']['cpus']));
                    }
                    else
                    {
                        $account_resources_usages['cpus'] = doubleval($info['virtual_machine']['cpus']);
                    }

                    //Memory
                    if(isset($values['virtual_machines']['memory']))
                    {
                        $account_resources_usages['memory'] = doubleval(current($values['virtual_machines']['memory']));
                    }
                    else
                    { 
                        $account_resources_usages['memory'] = doubleval($info['virtual_machine']['memory']);
                    }
                    
                    //Backup Usage
                    $account_resources_usages['backups'] = $backupUsage;
                    
                    //Insert Usage Records
                    $this->insertResource($account['user_id'], $account['hosting_id'], $account_resources_usages);

                    //Update Settings
                    $this->saveSettings($account['hosting_id'], array(
                        'start_date'    =>  $time
                    ));
                }
            }
        }
    }
}