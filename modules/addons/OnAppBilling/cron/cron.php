<?php
  
/* 
if(isset($_SERVER['HTTP_USER_AGENT']))
    die('Cannot run directly');*/

ob_start();
sleep(3); // SECURITY AGAINST MULTIPLE CALLS


$start = time();
/*************************************
 * 
 *  LOAD ENVIRONMENT
 * 
 *************************************/
define('DS', DIRECTORY_SEPARATOR); 
define('WHMCS_MAIN_DIR', substr(dirname(__FILE__),0, strpos(dirname(__FILE__),'modules'.DS.'addons')));  
define('OnAppBillingDIR', substr(dirname(__FILE__), 0, strpos(dirname(__FILE__), DS.'cron')));

/**********************
 * BEGIN
 **********************/

//WHMCS
if(file_exists(WHMCS_MAIN_DIR.DS.'init.php')) // 
{
    require_once WHMCS_MAIN_DIR.DS.'init.php';
}
else // Older than 5.2.2
{
    require_once WHMCS_MAIN_DIR.DS."configuration.php";
    require_once WHMCS_MAIN_DIR.DS."dbconnect.php";
    require_once WHMCS_MAIN_DIR.DS."includes".DS."functions.php";
}

//ADDON FUNCTIONS
require_once OnAppBillingDIR.DS.'core'.DS.'functions.php';
require_once OnAppBillingDIR.DS.'core'.DS."class.ModulesGarden.php";
if(!class_exists('MG_Language'))
{
    require_once OnAppBillingDIR.DS.'core'.DS.'class.MG_Language.php';
}
//OnApp Billing FUNCTIONS
require_once OnAppBillingDIR.DS.'class.OAProduct.php';
require_once OnAppBillingDIR.DS.'core.php';



/********************************
 * 
 * ERROR REPORTING
 * 
 ******************************/
set_exception_handler('exceptionHandler');
set_error_handler('errorHandler');
register_shutdown_function('importShutDown');


function exceptionHandler($error)
{
    OnAppBillingLogger::error($error);
}

function errorHandler($errno, $errstr, $errfile, $errline)
{
    if($errno == 1 || $errno == 64)
    {
        OnAppBillingLogger::error("ERROR: [$errno] $errstr <br />on line $errline in file $errfile");
    }
}

function importShutDown() 
{
    $error = error_get_last();
    if ($error['type'] == 1 || $error['type'] == 64) 
    {
        OnAppBillingLogger::error("ERROR: {$error['message']} <br />on line {$error['line']} in file {$error['file']}");
    }
}


//Do not log warnings
error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);  


/************************************* MONTHLY BILLING **********************************************/
OnAppBillingEventManager::attach('OnAppBillingCronLoop', 'OnAppBillingMonthlyBilling');

function OnAppBillingMonthlyBilling($product_id, $billing_settings)
{
    if($billing_settings['credit_billing']['enable'])
    {
        return false;
    }
    //IF bill on invoice generate is enabled we cannot run this function
    if($billing_settings['bill_on_invoice_generate'])
    {
        return false;
    }
    //Monthly billing is enabled?
    if($billing_settings['bill_per_month'] != 1)
    {
        return false;
    }
    //Get current day
    $c_date = getdate(); 
    
    if($c_date['mday'] != '1')
    {
        return false;
    }
    
    //Create Product Class
    $p = new OAProduct($product_id);
    //Get Server Type
    $type = $p->getServerType();

    $start_date     =   date('Y-m-01 00:00:00', strtotime('-1 month'));
    $end_date       =   date('Y-m-t 23:59:59', strtotime('-1 month'));
    
    //Get accounts
    $accounts = mysql_get_array("SELECT h.id, h.userid
        FROM tblhosting h
        WHERE h.packageid = ?", array($product_id));
    
    foreach($accounts as $account)
    {
        $has_records = mysql_get_row("SELECT record_id FROM OnAppBilling_".$type."_prices 
                WHERE DATE(`date`) BETWEEN ? AND ? AND product_id = ? AND hosting_id = ? LIMIT 1", array($start_date, $end_date, $product_id, $account['id']));
        
        //Nope?
        if(!$has_records)
        {
            continue;
        }
            
        if($billing_settings['autogenerate_invoice'] == 1)
        {
            $invoice_id = OnAppBillingGenerateInvoice($account['id'], $product_id, $start_date, $end_date, array(
                'autoapplycredit'   =>  $billing_settings['autoapplycredit'],
                'duedate'           =>  $billing_settings['billing_duedate']
            ));
            
            if($invoice_id !== false)
            {
                OnAppBillingDeleteUsageRecord($account['id'], $product_id, $start_date, $end_date);
            }
        }
        else
        {
            $invoice_id = OnAppBillingGenerateAwaitingInvoice($account['id'], $product_id, $start_date, $end_date, array(
                'autoapplycredit'   =>  $billing_settings['autoapplycredit'],
                'duedate'           =>  $billing_settings['billing_duedate']
            ));
            
            if($invoice_id !== false)
            {
                OnAppBillingDeleteUsageRecord($account['id'], $product_id, $start_date, $end_date);
            }
        }  
    }
}
/************************************** END OF MONTHLY BILLING ***************************************/
 

/************************************** BILL EACH DAY ******************************************/
OnAppBillingEventManager::attach('OnAppBillingCronLoop', 'OnAppBillingEachDayBilling');

function OnAppBillingEachDayBilling($product_id, $billing_settings)
{
    if($billing_settings['credit_billing']['enable'])
    {
        return false;
    }
    
    //IF bill on invoice generate is enabled we cannot run this function
    if($billing_settings['bill_on_invoice_generate'])
    {
        return false;
    }
    //Monthly billing is enabled?
    if($billing_settings['bill_per_month'] == 1)
    {
        return false;
    }
     
    if(intval($billing_settings['billing_period']) == 0)
    {
        return false;
    }
    
    //Create Product Class
    $p = new OAProduct($product_id);
    //Get Server Type
    $type = $p->getServerType();
    //interval
    $interval = intval($billing_settings['billing_period']);
    
    $start_date = date('Y-m-d', strtotime('-'.$interval.' days'));
    $end_date = date('Y-m-d');
    
        //Get accounts
    $accounts = mysql_get_array("SELECT h.id, h.userid
        FROM tblhosting h
        WHERE h.packageid = ?", array($product_id));
    
    foreach($accounts as &$account)
    {
        $has_records = mysql_get_row("SELECT record_id, record_id, DATE(`date`) as `date` FROM OnAppBilling_".$type."_prices 
                WHERE DATE(`date`) <= ? AND product_id = ? AND hosting_id = ? LIMIT 1", array($start_date, $product_id, $account['id']));
        //Nope?
        if(!$has_records)
        {
            continue;
        } 
        
        //Change start date to first record
        $start_date = $has_records['date'];
        
        if($billing_settings['autogenerate_invoice'] == 1)
        {
            $invoice_id = OnAppBillingGenerateInvoice($account['id'], $product_id, $start_date, $end_date, array(
                'autoapplycredit'   =>  $billing_settings['autoapplycredit'],
                'duedate'           =>  $billing_settings['billing_duedate']
            ));
            
            if($invoice_id !== false)
            {
                OnAppBillingDeleteUsageRecord($account['id'], $product_id, $start_date, $end_date);
            }
        }
        else
        {
            $invoice_id = OnAppBillingGenerateAwaitingInvoice($account['id'], $product_id, $start_date, $end_date, array(
                'autoapplycredit'   =>  $billing_settings['autoapplycredit'],
                'duedate'           =>  $billing_settings['billing_duedate']
            ));
            
            if($invoice_id !== false)
            {
                OnAppBillingDeleteUsageRecord($account['id'], $product_id, $start_date, $end_date);
            }
        }
    }
}
/*************************************** END OF BILL EACH DAY **********************************/

$modules_products = mysql_get_array("SELECT product_id FROM OnAppBilling_settings
    WHERE enable = 1");

if($modules_products)
{
    OnAppBillingLogger::info('Starting...');
    
    foreach($modules_products as $product)
    {
        //products settings
        $p = new OAProduct($product['product_id']);
        $settings = $p->getSettings();
        //Billing Settings
        $billing_settings = $settings['billing_settings'];
        
        //get resource settings
        $resources_settings = $p->getResources(); 
        if($resources_settings === false)
        {
            OnAppBillingLogger::error('Cannot get resource settings for: '.$p->getServerType());
            continue;
        }

        //Module Exists?
        if(!$p->module())
        {
            OnAppBillingLogger::error('Cannot get module!');
            continue;
        }
        
        $m_start = time();
        $p->module()->getSample();
        OnAppBillingLogger::info('Getting data from server... Module: '.$p->getServerType().' Product ID '.$product['product_id'].' Time: '.(time() - $m_start));
        
        //Run 
        OnAppBillingEventManager::call('OnAppBillingCronLoop', $product['product_id'], $billing_settings);
    }

    //DONE!
    OnAppBillingLogger::info('Done. Running time: '.(time() - $start));
}
        
echo 'done';