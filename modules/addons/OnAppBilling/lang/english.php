<?php

$LANG['free']                   =   'Free';
$LANG['usage_records']          =   'Usage Records';
$LANG['usage_records_pricing']  =   'Pricing For Usage Records';
$LANG['credit_billing']         =   'Credit Billing';
$LANG['current_credit']         =   'Credits Used For This Service';
$LANG['current_paid']           =   'Alredy Paid For This Service';
$LANG['record']                 =   'Record';
$LANG['usage']                  =   'Usage';
$LANG['total']                  =   'Total';
$LANG['resource_deleted']       =   'Resource Deleted';
$LANG['total_for']              =   'Total For';
$LANG['period']                 =   'Period';