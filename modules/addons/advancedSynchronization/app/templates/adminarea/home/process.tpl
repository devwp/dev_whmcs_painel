<h5>{$settings.serverName}</h5>
<form method="post" action="{'home'|addon_url:'save'}" id="save_settings_form">
    <input type='hidden' value='{$settings.serverID}' id='serverID'/>
    <table>
        <tr>
            <td><label class="options-label" for="ResetPasswords">{$lang->_('Reset Password')}</label></td>
            <td><input type="checkbox" id="ResetPasswords" {if $settings.resetPasswords eq 'true'} checked {/if}></td>
            <td><label class="options-label-description">{$lang->_('Reset user passwords on server when importing accounts')}</label></td>
        </tr>
        <tr>
            <td><label class="options-label" for="WelcomeEmail">{$lang->_('WHMCS Welcome Email')}</label></td>
            <td><input type="checkbox" id="WelcomeEmail" {if $settings.welcomeEmail eq 'true'} checked {/if}></td>
            <td><label class="options-label-description">{$lang->_('Send email to new users with the Client Area login and password details')}</label></td>
        </tr>
        <tr>
            <td><label class="options-label" for="WelcomeHostingEmail">{$lang->_('Hosting Welcome Email')}</label></td>
            <td><input type="checkbox" id="WelcomeHostingEmail" {if $settings.welcomeHostingEmail eq 'true'} checked {/if}></td>
            <td><label class="options-label-description">{$lang->_('Send email to users with new hosting login and password')}</label></td>
        </tr>
        
    </table>
{if $global}{$global}{/if}
<!--    <table>
        <tr>
            <td><label class="options-label">{$lang->_('Product type')}</label></td>
            <td><select name="product_type">
                    <option value='default'>{$lang->_('Default ')}{$settings.serverType}{$lang->_(' Imported Product')}</option>
                {foreach from=$settings.products item=product key=number}
                    <option {if $settings.product_type eq $product->id} selected {/if} value='{$product->id}'>{$product->name}</option>
                {/foreach}</select></td>
            <td><label class="options-label-description">{$lang->_('Select type of imported products')}</label></td>
        </tr>
    </table>
   --> 
    <div class="form-actions">
        <button id='save_settings' class="button primary"><i class="icon-ok"> </i>{$lang->_('Update Settings')}</button>
    </div>
</form>
<form method="post">
    <table class="table table-bordered gridTable">
        <thead>
            <tr>
                <th width="20" class="nosort"><input type="checkbox" class="toggleAll"></th>
                <th width="250"><a href="#">{$lang->_('Domain')}</a></th>
                <th width="100"><a href="#">{$lang->_('Username')}</a></th>
                <th><a href="#">{$lang->_('Email')}</a></th>
                <th width="150"><a href="#">{$lang->_('Plan')}</a></th>
                <th><a href="#">{$lang->_('IP Address')}</a></th>
                <th width="100"><a href="#">{$lang->_('Created At')}</a></th>
                <th width="80"><a href="#">{$lang->_('Sync Status')}</a></th>
                <th width="260" class="nosort"><a href="#">{$lang->_('Actions')}</a></th>
                <th width="auto" class="nosort" style='color: #fff;'>{$lang->_('WHMCS Product')}</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
        
    <div class="clear"></div>
    <div class="control-group">
        <label>{$lang->_('Action With Selected:')}</label>
        <select name="action">
            <option value="sync">{$lang->_('Synchronize (will perform default action for specified kind of account)')}</option>
            <option value="terminate">{$lang->_('Terminate On Server (only if applicable)')}</option>
            <option value="create">{$lang->_('Create On Server (only if applicable)')}</option>
            <option value="createInWhmcs">{$lang->_('Create In WHMCS (only if applicable)')}</option>
            <option value="terminateInWhmcs">{$lang->_('Terminate In WHMCS (only if applicable)')}</option>
        </select>
    </div>
    <div class="form-actions">
        <button type="submit" class="button primary big" id="batchActionButton"><span class="icon-plus-sign"></span> {$lang->_('Perform Action')}</button>
    </div>
</form>
    
{literal}
<script type="text/javascript">
    jQuery(document).ready(function(){
           $("#DataTables_Table_0_length").after('<div class="dataTables_length" style="margin-top:4px; margin-left:10px;" >  <label><a href="#" id="dataTableRefresh">Refresh<i class="icon-refresh"></i></a> </label></div>');
           $("#dataTableRefresh").click(function(e){
                e.preventDefault();
                ProductsSync.table.fnReloadAjax();
           });
    });
</script>
{/literal} 