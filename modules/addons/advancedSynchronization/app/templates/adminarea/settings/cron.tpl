<h5>{$lang->_('Cron Settings')}</h5>
<form method="post" action="{'settings'|addon_url:'saveCron'}" id="save_settings_form">
    <input type="hidden" id="cron" value="true"/>
    <input type="hidden" id="serverID" name="serverID" value="{$serverID}"/>
      <table>
        <tr>
            <td><label class="options-label" for="action">{$lang->_('Action')}</label></td>
            <td><select id="action" name="action"  style='margin-bottom: 0px;'>
                  {if $suportCron}
                    <option value="sync" {if $config.action eq 'sync'} selected {/if}>{$lang->_('Synchronize')}</option>
                    <option value="export" {if $config.action eq 'export'} selected {/if}>{$lang->_('Export')}</option>
                    <option value="import" {if $config.action eq 'import'} selected {/if}>{$lang->_('Import')}</option>
                  {/if}
                  <option value="nameserversScan" {if $config.action eq 'nameserversScan'} selected {/if}>{$lang->_('Name Servers Scan')}</option>
                  
                </select></td>
            <td><label class="options-label-description">{$lang->_('Choose action for cron task')}</label></td>
        </tr>
    </table>
{if $suportCron}
      <h5>{$lang->_('Synchronization')}</h5>
      
    <table>
        
        <tr>
            <td><label class="options-label" for="ResetPasswords">{$lang->_('Reset Password')}</label></td>
            <td><input type="checkbox" id="ResetPasswords" name="resetPasswords" {if $config.resetPasswords eq 'true'} checked {/if}></td>
            <td><label class="options-label-description">{$lang->_('Reset user passwords on server when importing accounts')}</label></td>
        </tr>
        <tr>
            <td><label class="options-label" for="WelcomeEmail">{$lang->_('WHMCS Welcome Email')}</label></td>
            <td><input type="checkbox" id="WelcomeEmail" name="welcomeEmail" {if $config.welcomeEmail eq 'true'} checked {/if}></td>
            <td><label class="options-label-description">{$lang->_('Send email to new users with the Client Area login and password details')}</label></td>
        </tr>
        <tr>
            <td><label class="options-label" for="WelcomeHostingEmail">{$lang->_('Hosting Welcome Email')}</label></td>
            <td><input type="checkbox" id="WelcomeHostingEmail" name="welcomeHostingEmail" {if $config.welcomeHostingEmail eq 'true'} checked {/if}></td>
            <td><label class="options-label-description">{$lang->_('Send email to users with new hosting login and password')}</label></td>
        </tr>
    </table>
  {/if}
  
     <h5>{$lang->_('Name Servers Scan')}</h5>
     <table>
           {section  name=foo start=1 loop=21 step=1}
             {assign var="i" value=$smarty.section.foo.index-1}
                {if not ($smarty.section.foo.index >4 && !$settings.nameServers.$i)}
                   <tr>
                        {assign var="lastNs" value=$smarty.section.foo.index}

                       <td><label class="options-label" for="asNs{$smarty.section.foo.index}">{$lang->_("Nameserver")} {$smarty.section.foo.index}</label></td>
                       <td><input type="text" id="asNs1" name="nameServers[]" value="{$config.nameServers.$i}">
                       {if $smarty.section.foo.index >4} 
                             <button class="button primary as_remove_field" style="margin-bottom: 8px;"><i class="icon-remove" > </i> </button>
                       {/if}
                       </td>
                       <td><label class="options-label-description"></label></td>
                   </tr>
                 {/if}
           {/section}
             <tr>
                  <td colspan="3">
                            <button id="as_add_field_button" type="button" class="button">
                              <i class="icon-plus"> </i> {$lang->_("Add More Fields")}
                              </button>
                 </td>
             </tr>
             <tr>
                  <td><label class="options-label" for="as">{$lang->_("Synchronized DNS Per Account")}</label></td>
                  <td colspan="e"><input type="text" id="asNs1" name="synchronizedDNS" value="{$config.synchronizedDNS}" style="width: 30px;">
                        <label class="options-label-description"><a href="#"  data-toggle="tooltip"  data-placement="right" 
                                                                    title="{$lang->_("Minimum number of nameservers mathing nameservers provided above to mark hosting as correct.")}"><i class="icon-question-sign"> </i></a></label>
                  </td>
              </tr>
            <tr>
                  <td><label class="options-label" for="triminateOnServer">{$lang->_('Terminate On Server')}</label></td>
                  <td><input type="checkbox" id="triminateOnServer" name="triminateOnServer" value="true" {if $config.triminateOnServer eq 'true'} checked {/if}> 
                        <label class="options-label-description">{$lang->_('')}</label></td>
                  <td></td>
             </tr>
              <tr>
                  <td><label class="options-label" for="terminateInWHMCS">{$lang->_('Terminate In WHMCS ')}</label></td>
                  <td><input type="checkbox" id="terminateInWHMCS" name="terminateInWHMCS"  value="true" {if $config.terminateInWHMCS eq 'true'} checked {/if}>
                  <label class="options-label-description">{$lang->_('')}</label></td>
                  <td></td>
              </tr>
     </table>
        {if $global}{$global}{/if}
    <div class="form-actions">
        <button id='save_settings2' class="button primary"><i class="icon-ok"> </i>{$lang->_('Update Settings')}</button>
    </div>
</form>
{literal}
<script type="text/javascript">
    jQuery(document).ready(function(){
          
          var max_fields      = 20; //maximum input boxes allowed
          var lastNs ={/literal}{$lastNs}{literal};
          var nameServerName = "{/literal}{$lang->_("Nameserver")}{literal}";
          
          var x = 1; //initlal text box count
          $("#as_add_field_button").click(function(e){ //on add input button click
              e.preventDefault();
              if($(".as_remove_field").size() < max_fields -4 ){
                    lastNs++;
                    $(this).parent('td').parent('tr').before('<tr><td><label class="options-label" for="asNs'+lastNs+'">'+nameServerName+' '+lastNs+'</label></td>\n\
                    <td colspan="2"><input type="text" id="asNs'+lastNs+'" name="nameServers[]" value=""/>\n\
                    <button type="button" class="button primary as_remove_field" style="margin-bottom: 8px;"><i class="icon-remove" ></i></button> </td></tr>');
              }
          });
          //remove additional field
          $('body').on( "click", ".as_remove_field", function(e){ 
               e.preventDefault(); 
              $(this).parent('td').parent('tr').remove();  
              lastNs--;
          });
          
          $('#save_settings_form').submit(function(e){
              e.preventDefault();
                jQuery.ajax({
                        url: $(this).attr('action'),
                        type: "POST",
                        data: $(this).serialize(),
                        success: function(data){
                              console.log(data);
                              if(data.result==0)
                                    alert(data.msg);
                              else
                                 window.location.href = "addonmodules.php?module=advancedSynchronization&modpage=cron";   
                        }
                    });
          });
    });
</script>
{/literal}