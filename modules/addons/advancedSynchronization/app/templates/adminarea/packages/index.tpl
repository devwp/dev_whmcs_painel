<h5>{$lang->_('Package Dependencies Configuration')}</h5>
<p>{$lang->_('You can manually set dependence between a remote server and a WHMCS product. It means that all the services imported from the chosen remote server with such a plan will be automatically assigned to the selected WHMCS product.')}</p>
<form method="GET" id="dependency-form">
    <input type="hidden" name="module" value="advancedSynchronization">
    <input type="hidden" name="modpage" value="packages">
    <input type="hidden" name="modsubpage" value="save">
    <table id="config-table" >
        <tr>
            <td>
                    <label>{$lang->_('Please Select A Server')}</label>
                    <input type="hidden" value="{'packages'|addon_url:'getPackagesList'}" id="package-action"/>
                    <select name="serverID" class="span4" id="package-dependencies" style="padding: 2px; font-size: 12px;">
                        <option></option>
                        {foreach from=$servers item=server}
                            <option value="{$server->id}">{$server->name} ({$server->countAccounts}/{$server->maxaccounts} {$lang->_('accounts')})</option>
                        {/foreach}

                    </select>
                        <div class="dataTables_processing" style="display: none;">{$lang->_('Processing...')}</div>
                
            </td>
            <td id="packages-data"> 
            </td>
            <td id="packages-WHMCS">

                
            </td>
            <td id="config-action" style="display: none">
                <label>{$lang->_('Save Configuration')}</label>
                <input type="submit" class="button primary" value="{$lang->_('Save Dependency')}" />
            </td>
        </tr>
    </table>
</form>           
<h5>{$lang->_('Package Dependencies List')}</h5>
<table id="dependencies-table" class="table">
    <thead>
        <tr>
            <th>{$lang->_('Server')}</th>
            <th>{$lang->_('Package On Remote Server')}</th>
            <th>{$lang->_('WHMCS Product')}</th>
            <th style="width: 90px;">{$lang->_('Actions')}</th>
        </tr>
    </thead>
    <tbody>
    {if $dependencies}
    {foreach from=$dependencies item=dependency key=number}
        <tr id="{$dependency.id}">
            <td>{$dependency.server}</td>
            <td>{$dependency.plan}</td>
            <td>{$dependency.product}</td>
            <td  style="width: 90px;"><a class="button danger" href="{'packages'|addon_url:'delete'}&id={$dependency.id}">{$lang->_('Delete')}</a></td>
        </tr>
    {/foreach}
    {else}
        <tr id="no-data"><td colspan="4" class="dataTables_empty">No Data</td></tr>
    {/if}

    
    </tbody>
</table>