<h5>{$lang->_('Start Synchronization')}</h5>

<p>{$lang->_('Here you can manually synchronize the accounts with any supported server.')}</p>
<p>{$lang->_('Start with selecting a server, then wait for the accounts to be loaded and choose an action you wish to perform.')}</p>

<form method="get">
    <input type="hidden" name="module" value="advancedSynchronization">
    <input type="hidden" name="modpage" value="scanAccounts">
    <input type="hidden" name="modsubpage" value="process">
    <div class="control-group" id="sync-server-select">
        <label>{$lang->_('Please Select A Valid Server')}</label>
        <select name="serverID" class="span4" style="padding: 2px; font-size: 12px;">
        {foreach from=$servers item=server}
            <option value="{$server->id}">{$server->name} ({$server->countAccounts}/{$server->maxaccounts} {$lang->_('accounts')})</option>
        {/foreach}
        </select>
    </div>
    <div class="form-actions">
        <button class="button primary"><i class="icon-list"> </i>{$lang->_('List Accounts')}</button>
    </div>
</form>