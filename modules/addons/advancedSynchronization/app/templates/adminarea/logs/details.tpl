<h5>{$log->title}</h5>
<table id="log-details">
    <tr>
        <td>{$lang->_('Title')}:</td>
        <td>{$log->title}</td>
    </tr>
    <tr>
        <td>{$lang->_('Description')}:</td>
        <td>{$log->description}</td>
    </tr>
    <tr>
        <td>{$lang->_('Server')}:</td>
        <td>{$server}</td>
    </tr>
    <tr>
        <td>{$lang->_('Date')}:</td>
        <td>{$log->date}</td>
    </tr>
    <tr>
        <td>{$lang->_('Status')}:</td>
        <td>{$log->status}</td>
    </tr>
</table>
<div class="right-buttons">

    <a href="{'logs'|addon_url:'index'}" class="button">{$lang->_('Back To List')}</a>
    <a href="{'logs'|addon_url:'deleteEntry'}&id={$log->id}" class="button danger delete-log-entry"><span class="icon-remove "></span> {$lang->_('Delete Entry')}</a>
</div>