<h5>{$lang->_('Start Export')}</h5>

<p>{$lang->_('Here you can manually export the accounts to any supported server.')}</p>
<p>{$lang->_('Start with selecting a server, then wait for the accounts to be loaded and choose which accounts you want to export.')}</p>

<form method="get" >
    <input type="hidden" name="module" value="advancedSynchronization">
    <input type="hidden" name="modpage" value="export">
    <input type="hidden" name="modsubpage" value="process">
    <div class="control-group" id="sync-server-select">
        <label>{$lang->_('Please Select A Valid Server')}</label>
        <select name="serverID" class="span4" style="padding: 2px; font-size: 12px;">
        {foreach from=$servers item=server}
            <option value="{$server->id}">{$server->name} ({$server->countAccounts}/{$server->maxaccounts} {$lang->_('accounts')})</option>
        {/foreach}
        </select>
    </div>
    <div class="form-actions">
        <button class="button primary"><i class="icon-list"> </i>{$lang->_('List Accounts')}</button>
    </div>
</form>