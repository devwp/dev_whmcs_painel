<h5>{$settings.serverName}</h5>
<form method="post">
    <table class="table table-bordered gridTable">
        <thead>
            <tr>
                <th width="20" class="nosort"><input type="checkbox" class="toggleAll"></th>
                <th width="250"><a href="#">{$lang->_('Domain')}</a></th>
                <th width="100"><a href="#">{$lang->_('Username')}</a></th>
                <th><a href="#">{$lang->_('Email')}</a></th>
                <th width="150"><a href="#">{$lang->_('Plan')}</a></th>
                <th><a href="#">{$lang->_('IP Address')}</a></th>
                <th width="100"><a href="#">{$lang->_('Created At')}</a></th>
                <th width="80"><a href="#">{$lang->_('Sync Status')}</a></th>
                <th width="260" class="nosort"><a href="#">{$lang->_('Actions')}</a></th>
                <th width="auto" class="nosort" style='color: #fff;'>{$lang->_('WHMCS Product')}</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
        
    <div class="clear"></div>
    <div class="control-group" style="display: none;">
        <label>{$lang->_('Action With Selected:')}</label>
        <select name="action">
            <option value="create" selected="selected">{$lang->_('Create On Server (only if applicable)')}</option>
        </select>
    </div>
    <div class="form-actions">
        <button type="submit" class="button primary big" id="batchActionButton"><span class="icon-plus-sign"></span> {$lang->_('Export Selected')}</button>
    </div>
</form>
{literal}
<script type="text/javascript">
    jQuery(document).ready(function(){
           $("#DataTables_Table_0_length").after('<div class="dataTables_length" style="margin-top:4px; margin-left:10px;" >  <label><a href="#" id="dataTableRefresh">Refresh<i class="icon-refresh"></i></a> </label></div>');
           $("#dataTableRefresh").click(function(e){
                e.preventDefault();
                ProductsSync.table.fnReloadAjax();
           });
    });
</script>
{/literal} 