<p>{$lang->_('Cron Synchronization allows you to enable automatic synchronization between specified servers and your WHMCS. The only thing you have to do is set up the servers which you would like to synchronize and add new cronjob.')}</p>

<h5>{$lang->_('Cron Job Setup')}</h5>

<p>{$lang->_('In order to make cron job work properly add a new line to your cron tab file. You can do this directly from your server or use server management software (like cPanel)')}</p>
<p>{$lang->_('A correct cron job line should look like the examples below:')}</p>
<table cellpadding='4'>
    <tr>
        <td><span>{$lang->_('Every 5 minutes: ')}</span></td>
        <td><code>{$cronLine}</code></td>
    </tr>
    <tr>
        <td><span>{$lang->_('Daily: ')}</span></td>
        <td><code>{$dayCronLine}</code></td>  
    </tr>
</table>

<h5>{$lang->_('Create A New Synchronization Cron Task')}</h5>

<form method="post">
    <div class="control-group">
        <label>{$lang->_('Please Select A Valid Server')}</label> 
        <select name="serverID" class="span4" style="padding: 2px; font-size: 12px;">
        {foreach from=$servers item=server}
            <option value="{$server->id}">{$server->name} ({$server->countAccounts}/{$server->maxaccounts} {$lang->_('accounts')})</option>
        {/foreach}
        </select>
    </div>
    <div class="form-actions">
        <button type="submit" class="button"><i class="icon-ok-sign"> </i>{$lang->_('Create A New Job')}</button>
    </div>
</form>

<h5>{$lang->_('Current Tasks List')}</h5>
{if count($jobs)}
<table class="table table-bordered">
    <thead>
        <th>{$lang->_('Server')}</th>
        <th>{$lang->_('Last Execution Time')}</th>
        <th>{$lang->_('Last Log Entry ')}</th>
        <th width="230">{$lang->_('Actions')}</th>
    </thead>
    <tbody>
        {foreach from=$jobs item=job}
        <tr>
            <td><strong>{$job->server->name}</strong></td>
            <td>{$job->lastExecution}</td>
            <td>{$job->lastLog}</td>
            <td nowrap>
                <a href="{'settings'|addon_url:'cron'}&serverID={$job->server_id}" class="button primary"><span class="icon-wrench"></span> {$lang->_('Settings')}</a>
                <a href="{$job->deleteURL}" class="button danger delete-job"><span class="icon-remove-sign"></span> {$lang->_('Delete From Queue')}</a>
            </td>
        </tr>
        {/foreach}
    </tbody>
</table>
    {else}
        <div style='text-align: center'>{$lang->_('No Cron Job Set')}</div>
    {/if}