function GetURLParameter(sParam, source)
{
    source = typeof source !== 'undefined' ? source : window.location.search.substring(1);
    var sPageURL = source;
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
};
var ProductsSync = {
  table : '',
  ajaxQueue : [],
  _init : function()
  {
      this.loadDataTables();
      this.eventActions();
      this.progressBar(0);
  },
  eventActions : function()
  {
      $(document).on('click', '.toggleAll', function()
      {
          $('.checkThis').prop('checked', $(this).is(':checked'));
      });
      
      $('table thead th a').on('click', function(e)
      {
          e.preventDefault(); 
      });

      $(document).on('click', '#modalCloseButton', function()
      {
          ProductsSync.ajaxQueue = [];
          $.colorbox.close();
          
          return false; 
      });
      $(document).on('click', '.add-email', function(e)
      {
          e.preventDefault();
         var addEmailBtn = $(this);
         $("body").append("<div class='modal-part' id='modal-wrapper' style='left: 0; top: 0;position: fixed; z-index: 99; background: #000; opacity: 0.6; height: 100%; width: 100%;'></div><div class='modal-part' style='text-align: center; z-index: 100;width: 300px; height: 140px; background: #fff; position: absolute; left: 44%; top: 40%; border-radius: 5px;'><input style='text-align: center;top: 40px;position: relative;width: 80%;height: 30px;' type='text' id='option-value' placeholder='Set New Email Option'/><div style='position: absolute;bottom: 10px;right: 10px;'><input type='button' class='button danger' id='modal-close' value='Cancel'/><input type='button' class='button primary' id='addOption' value='Add'/></div></div>");
         $('#modal-close').click(function(){
             $('.modal-part').remove();
         });
         $("#modal-wrapper").click(function(){
             $('.modal-part').remove();
         });
         $('#option-value').focus();
         $('#addOption').click(function(){
             var value = $('#option-value').val();
             var valid = validateEmail(value);
             if (valid) {
                $('select.clients').prepend("<option value='"+value+"'>"+value+"</option>");
                addEmailBtn.siblings("select.clients").children("option:selected").removeAttr("selected");
                addEmailBtn.siblings("select.clients").children("option[value='"+value+"']").attr("selected", "selected");
                addEmailBtn.siblings("select.clients").change();
                $('.modal-part').remove();
             } else {
                 alert("Email is invalid");
             }
             
         });
         function validateEmail(email) { 
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        } 
      });
      $(document).on('click', '#batchActionButton', function()
      {
          if ($('[name="action[]"]:checked').length > 0) {
          var confirmMessage = [];
          ProductsSync.ajaxQueue = [];
          var action = $('select[name=action] option:selected').val();
          if (action === "sync") {
            $('[name="action[]"]:checked').each(function() {
                var href = $(this).val();
                if (href != "synchronized") {
                    ProductsSync.ajaxQueue.push(href);
                    var username = GetURLParameter("username", href);
                    var domain = GetURLParameter("domain", href);
                    var subpage = GetURLParameter("modsubpage", href);
                    switch (subpage) {
                        case "import":
                            confirmMessage.push("The account " + username + " with domain " + domain + " will be created in WHMCS.");
                            break
                        case "create":
                            confirmMessage.push("The account " + username + " with domain " + domain + " will be created on remote server.");
                            break
                        default:
                            confirmMessage.push("The account " + username + " with domain " + domain + " will be synchronized.");
                    }
                }
            });
          } else if (action === "terminate") {
            $('[terminate][name="action[]"]:checked').each(function() {
                var href = $(this).attr("terminate");
                ProductsSync.ajaxQueue.push(href);
                var username = GetURLParameter("username", href);
                var domain = GetURLParameter("domain", href);
                confirmMessage.push("The account "+username+" with domain "+domain+" will be terminated."); 
            });
          } else if (action === "create") {
            $('[create][name="action[]"]:checked').each(function() {
                var href = $(this).attr("create");
                ProductsSync.ajaxQueue.push(href);
                var username = GetURLParameter("username", href);
                var domain = GetURLParameter("domain", href);
                confirmMessage.push("The account " + username + " with domain " + domain + " will be created on remote server.");                 
            });
          } else if (action === "createInWhmcs") {
            $('[import][name="action[]"]:checked').each(function() {
                var href = $(this).attr("import");
                ProductsSync.ajaxQueue.push(href);
                var username = GetURLParameter("username", href);
                var domain = GetURLParameter("domain", href);
                confirmMessage.push("The account " + username + " with domain " + domain + " will be created in WHMCS.");
            });  
          } else if (action === "terminateInWhmcs") {
            $('[terminateinwhmcs][name="action[]"]:checked').each(function() {
                var href = $(this).attr("terminateinwhmcs");
                ProductsSync.ajaxQueue.push(href);
                var username = GetURLParameter("username", href);
                var domain = GetURLParameter("domain", href);
                confirmMessage.push("The account " + username + " with domain " + domain + " will be terminated in WHMCS.");
            });
          }
          
          if (ProductsSync.ajaxQueue.length > 0) {
                var message = confirmMessage.join('<br>');
                ProductsSync.openConfirmModal(message);

          } else {
              alert("Action Is Not Applicable");
          }
            } else {
                alert("You Have Not Selected Any Account");
            }
            return false;
      });
      $(document).on('click', '#confirmModalButton', function()
      {
          if ($('[name="action[]"]:checked').length > 0) {
          var confirmMessage = [];
          ProductsSync.ajaxQueue = [];
          var action = $('select[name=action] option:selected').val();
          if (action === "sync") {
            $('[name="action[]"]:checked').each(function() {
                var href = $(this).val();
                if (href != "synchronized") {
                    ProductsSync.ajaxQueue.push(href);
                    var username = GetURLParameter("username", href);
                    var domain = GetURLParameter("domain", href);
                    var subpage = GetURLParameter("modsubpage", href);
                    switch (subpage) {
                        case "import":
                            confirmMessage.push("The account " + username + " with domain " + domain + " will be created in WHMCS.");
                            break
                        case "create":
                            confirmMessage.push("The account " + username + " with domain " + domain + " will be created on remote server.");
                            break
                        default:
                            confirmMessage.push("The account " + username + " with domain " + domain + " will be synchronized.");
                    }
                }
            });
          } else if (action === "terminate") {
            $('[terminate][name="action[]"]:checked').each(function() {
                var href = $(this).attr("terminate");
                ProductsSync.ajaxQueue.push(href);
                var username = GetURLParameter("username", href);
                var domain = GetURLParameter("domain", href);
                confirmMessage.push("The account "+username+" with domain "+domain+" will be terminated."); 
            });
          } else if (action === "create") {
            $('[create][name="action[]"]:checked').each(function() {
                var href = $(this).attr("create");
                ProductsSync.ajaxQueue.push(href);
                var username = GetURLParameter("username", href);
                var domain = GetURLParameter("domain", href);
                confirmMessage.push("The account " + username + " with domain " + domain + " will be created on remote server.");                 
            });
          } else if (action === "createInWhmcs") {
            $('[import][name="action[]"]:checked').each(function() {
                var href = $(this).attr("import");
                ProductsSync.ajaxQueue.push(href);
                var username = GetURLParameter("username", href);
                var domain = GetURLParameter("domain", href);
                confirmMessage.push("The account " + username + " with domain " + domain + " will be created in WHMCS.");
            });  
          } else if (action === "terminateInWhmcs") {
            $('[terminateinwhmcs][name="action[]"]:checked').each(function() {
                var href = $(this).attr("terminateinwhmcs");
                ProductsSync.ajaxQueue.push(href);
                var username = GetURLParameter("username", href);
                var domain = GetURLParameter("domain", href);
                confirmMessage.push("The account " + username + " with domain " + domain + " will be terminated in WHMCS.");
            });
          }
          
          if (ProductsSync.ajaxQueue.length > 0) {
                ProductsSync.openDefaultModal();
          } else {
              alert("Action Is Not Applicable");
          }
          var title = "";
          switch (action) {
              case "sync":
                  title = "Synchronization";
                  break
              case "terminate":
                  title = "Terminate On Remote Server";
                  break
              case "create":
                  title = "Create On Remote Sever";
                  break
              case "createInWhmcs":
                  title = "Create In WHMCS";
                  break
              case "terminateInWhmcs":
                  title = "Terminate In WHMCS";
                  break
          }
          var serverid = GetURLParameter("serverID");
                    $.ajax({
                        url: "addonmodules.php?module=advancedSynchronization&modpage=logs&modsubpage=addAction",
                        data: {title: title, serverid: serverid},
                        async: false,
                    });
                    for (var i = 0; i < confirmMessage.length; i++) {
                        $('#modalContent #synchronizeLog').append('<code index="' + i + '" style="color:#DA8404;overflow: auto;line-height: 27px;">' + confirmMessage[i] + '<input type="button" value="Cancel" class="button danger small" style="float:right;" index="' + i + '"/></code>');
                    }
                    jQuery("input[index]").click(function() {
                        var index = jQuery(this).attr("index");
                        jQuery("code[index='" + index + "'] input").remove();
                        jQuery("code[index='" + index + "']").append(" Canceled.");
                        ProductsSync.ajaxQueue[index] = "";
                    });
                    if (ProductsSync.ajaxQueue.length > 1) {
                        ProductsSync.performAction(ProductsSync.ajaxQueue[0], false, false, 0);
                    } else {
                        ProductsSync.performAction(ProductsSync.ajaxQueue[0], false, true, 0);
                    }

            } else {
                alert("You Have Not Selected Any Account");
            }
            return false;
      });
      
      $(document).on('click', '.ajaxAction', function()
      {
          ProductsSync.ajaxQueue = [];
          var $self = $(this);
          var title;
          var href = $self.attr('href');
            if (href.indexOf("terminate") > -1) {
                action = "terminate this account?";
                title = "Terminate On Remote Server";
            } else if (href.indexOf("create") > -1) {
                action = "create this account?";
                title = "Create On Remote Server";
            } else if (href.indexOf("import") > -1) {
                action = "import this account?";
                title = "Create In WHMCS";
            } else {
                action = "do this?";
            }
          if(! confirm('Are you sure you want to '+action))
          {
              return false;
          }
          var serverid = GetURLParameter("serverID");
          $.ajax({
            url: "addonmodules.php?module=advancedSynchronization&modpage=logs&modsubpage=addAction",
            data: { title: title,  serverid: serverid },
            async: false,
          });
          ProductsSync.openDefaultModal();
          ProductsSync.performAction($self.attr('href'), false, true, 0);
          
          return false;
      });
      
      function insertParam(key, value, source){
                    key = escape(key); value = escape(value);
                    var kvp = source.split('&');
                    var i=kvp.length; var x; while(i--) 
                    {
                        x = kvp[i].split('=');

                        if (x[0]==key)
                        {
                            x[1] = value;
                            kvp[i] = x.join('=');
                            break;
                        }
                    }
                    if(i<0) {kvp[kvp.length] = [key,value].join('=');}

                    //this will reload the page, it's likely better to store this until finished
                    return kvp.join('&'); 
            }
            function removeParam(key, sourceURL) {
                    var rtn = sourceURL.split("?")[0],
                        param,
                        params_arr = [],
                        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
                    if (queryString !== "") {
                        params_arr = queryString.split("&");
                        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                            param = params_arr[i].split("=")[0];
                            if (param === key) {
                                params_arr.splice(i, 1);
                            }
                        }
                        rtn = rtn + "?" + params_arr.join("&");
                    }
                    return rtn;
            }
            /*
        $(document).on('click', 'input[type=radio]', function() {
            var name = $(this).attr('name');
            if ($(this).val() === "existing") {
                $('select[name="'+name+'"]').attr("disabled", false);
                $('select[name="'+name+'"]').attr("disabled", false);
                var productId = $('select[name="'+name+'"] option:selected').val();
                var actionLink = $("a[import][name='"+name+"']");
                var newLink = removeParam("productId", actionLink.attr("href"));
                var checkbox = $('.checkThis[import="'+newLink+'"]');
                newLink = insertParam("productId", productId, newLink);
                actionLink.attr("href",newLink);
                checkbox.val(newLink);
                
            } else {
                $('select[name="'+name+'"]').attr("disabled", "disabled");
                var actionLink = $("a[import][name='"+name+"']");
                var newLink = removeParam("productId", actionLink.attr("href"));
                var checkbox = $('.checkThis[import="'+newLink+'"]');
                actionLink.attr("href",newLink);
                checkbox.val(newLink);
            }
        });*/
        $(document).on('change', 'select[product]', function() {
            var name = $(this).attr('name');
            var productId = $('select[name="'+name+'"] option:selected').val();
            var actionLink = $("a[import][name='"+name+"']");
            var newLink = removeParam("productId", actionLink.attr("href"));
            var checkbox = $('.checkThis[domain="'+name+'"]');
            newLink = insertParam("productId", productId, newLink);
            actionLink.attr("href",newLink);
            checkbox.val(newLink);
            checkbox.attr("import", newLink);
        });
        $(document).on('change', 'select[vmid]', function() {
            var vmid = $(this).attr('vmid');
            var email = $('select[vmid="'+vmid+'"] option:selected').val();
            var actionLink = $("a[import][vmid='"+vmid+"']");
            var newLink = removeParam("email", actionLink.attr("href"));
            var checkbox = $('.checkThis[domain="'+vmid+'"]');
            newLink = insertParam("email", email, newLink);
            actionLink.attr("href",newLink);
            checkbox.val(newLink);
            checkbox.attr("import", newLink);
        });
        
        

  },
  progressBar : function()
  {
    (function( $ ){
        $.fn.animateProgress = function(progress, callback) {
          return this.each(function() {
            $(this).animate({
              width: progress+'%'
            }, {
              duration: 1000,

              easing: 'swing',

              step: function( progress ){
                var labelEl = $('.ui-label', this),
                    valueEl = $('.value', labelEl);

                if (Math.ceil(progress) < 20 && $('.ui-label', this).is(":visible")) {
                  labelEl.hide();
                }else{
                  if (labelEl.is(":hidden")) {
                    labelEl.fadeIn();
                  };
                }

                if (Math.ceil(progress) == 100) {
                  labelEl.text('Done');
                  setTimeout(function() {
                    labelEl.fadeOut();
                  }, 1000);
                  $(this).width(0);
                  
                }else{
                  valueEl.text(Math.ceil(progress) + '%');
                }
              },
              complete: function(scope, i, elem) {
                if (callback) {
                  callback.call(this, i, elem );
                };
              }
            });
          });
        };
      })( jQuery );
  },
  openModal : function(title, content)
  {
      $.colorbox({html : '<div id="mg-wrapper" style="margin: 0;">' +
                          '<h2 class="section-heading">'+title+'</h2>' + 
                          '<div id="modalContent"  style="height: 243px;">' + content + '</div>' + 
                          '<div class="form-actions" style="text-align: right;margin-top: 0px; margin-bottom: 0px;"><button type="submit" class="button big primary" id="modalCloseButton"><span class="icon-close"></span> Close</button></div>' + 
                          '</div>', width : '700px', height : '410px'});
  },
  
  openDefaultModal : function()
  {
       var content = 
                      '<div id="progress_bar" class="ui-progress-bar ui-container">' +
                      '<div class="ui-progress" style="width: 0%;">' +
                      '<span class="ui-label" style="display:none;">Processing <b class="value">0%</b></span>' +
                      '</div><!-- .ui-progress -->' +
                      '</div><!-- #progress_bar -->' +
                      '<div id="synchronizeLog" style="height: 196px; overflow-y: auto; overflow-x: hidden;"></div>';
                  
       ProductsSync.openModal('Performing Action', content);
  },
  openConfirmModal : function(message)
  {
        $.colorbox({html : '<div id="mg-wrapper" style="margin: 0;">' +
                          '<h2 class="section-heading">Are you sure you want to do this?</h2>' + 
                          '<div id="modalContent" style="height: 248px; overflow-y: scroll; ">' + message + '</div>' + 
                          '<div class="form-actions" style="text-align: right;margin-top: 0px; margin-bottom: 0px;"><button class="button big primary" id="confirmModalButton"><span class="icon-close"></span> Confirm</button><button class="button big primary" id="refuteModalButton"><span class="icon-close"></span> Close</button></div>' + 
                          '</div>', width : '700px', height : '410px'});      
        $("#refuteModalButton").click(function(){
            $("#confirmModal").remove();
            ProductsSync.ajaxQueue = [];
            $.colorbox.close();
        });           
       
  },
  performAction : function(href, autoClose, autoRefresh, i)
  {
      autoClose = typeof autoClose == 'undefined' ? false : autoClose;
      autoRefresh = typeof autoRefresh == 'undefined' ? false : autoRefresh;
      $("code[index='"+i+"']").remove();
      $.ajaxQueue({
          url : href,
          dataType : 'json'
      }).done(function(result)
      {
          $('#modalContent #synchronizeLog').append('<code>'+result.html+'</code>');
          $('#colorboxSpinner').hide();
          var total = (ProductsSync.ajaxQueue.length > 0) ? ProductsSync.ajaxQueue.length : 1;
          var progressPercentage = Math.round(((i + 1) * 100) / total);
          $('#progress_bar .ui-progress').animateProgress(progressPercentage);
          if (ProductsSync.ajaxQueue.length > i) {
                var j = i + 1;
                while (ProductsSync.ajaxQueue[j] == "") {
                    j++;
                }
                if(j == (ProductsSync.ajaxQueue.length - 1))
                {
                    ProductsSync.performAction(ProductsSync.ajaxQueue[j], false, true, j);
                } else {
                    ProductsSync.performAction(ProductsSync.ajaxQueue[j], false, false, j);
                }
                if (ProductsSync.ajaxQueue.length <= j) {
                    $('#progress_bar .ui-progress').animateProgress(100);
                }
          }
                   
          if(autoRefresh)
          {
              ProductsSync.table.fnReloadAjax();
          }
          
          if(autoClose)
          {
              $.colorbox.close();
          }
      });
  },
  loadDataTables : function()
  {

        
        $.fn.dataTableExt.oApi.fnReloadAjax = function ( oSettings, sNewSource, fnCallback, bStandingRedraw )
        {
            if ( typeof sNewSource != 'undefined' && sNewSource != null )
            {
                oSettings.sAjaxSource = sNewSource;
            }
            this.oApi._fnProcessingDisplay( oSettings, true );
            var that = this;
            var iStart = oSettings._iDisplayStart;
            var aData = [];

            this.oApi._fnServerParams( oSettings, aData );

            oSettings.fnServerData( oSettings.sAjaxSource, aData, function(json) {
                /* Clear the old information from the table */
                that.oApi._fnClearTable( oSettings );

                /* Got the data - add it to the table */
                var aData =  (oSettings.sAjaxDataProp !== "") ?
                    that.oApi._fnGetObjectDataFn( oSettings.sAjaxDataProp )( json ) : json;

                for ( var i=0 ; i<aData.length ; i++ )
                {
                    that.oApi._fnAddData( oSettings, aData[i] );
                }

                oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
                that.fnDraw();

                if ( true )
                {
                    oSettings._iDisplayStart = iStart;
                    that.fnDraw( false );
                }

                that.oApi._fnProcessingDisplay( oSettings, false );

                /* Callback user function - for event handlers etc */
                if ( typeof fnCallback == 'function' && fnCallback != null )
                {
                    fnCallback( oSettings );
                }
            }, oSettings );
        }
      var controllerName = GetURLParameter("modpage");
      this.table = $('.gridTable').dataTable( {

        'bStateSave': true,
        'bProcessing': true,
        //'bServerSide': true,
        'sAjaxSource': 'addonmodules.php?module=advancedSynchronization&modpage='+controllerName+'&modsubpage=loadData',
        //'pageLength': 10,
        //'aLengthMenu' : [10],
        'aoColumns': [
            { 'mData' : 'checkbox'},
            { 'mData' : 'domain' },
            { 'mData' : 'username'},
            { 'mData' : 'email'},
            { 'mData' : 'package' },
            { 'mData' : 'ip'},
            { 'mData' : 'createdAt'},
            { 'mData' : 'syncLabel'},
            { 'mData' : 'actionButtons'},
            { 'mData' : 'productTypes'}
        ],
        'aoColumnDefs' : [ {
            'bSortable' : false,
            'aTargets' : [ 'nosort' ]
        }]
     }); 
     
  }
};  

$(function()
{
    jQuery(".delete-job").click(function(event)
    {
        return confirm("Are you sure you want to delete the cron job from the queue?");
    });
    
    ProductsSync._init();
    jQuery('.delete-log-entry').click(function(){
        return confirm("Are you sure you want to delete this entry from log list?");
    });
    jQuery('.delete-log-entries').click(function(){
        return confirm("Are you sure you want to delete all entries from log list?");
    });
    jQuery('#save_settings').click(function(e){
        e.preventDefault();
        if (jQuery('#ResetPasswords:checked').length > 0)
            var resetPasswords = "true";
        else
            var resetPasswords = "false";
        
        if (jQuery('#WelcomeEmail:checked').length > 0)
            var welcomeEmail = "true";
        else
            var welcomeEmail = "false";
        
        if (jQuery('#SyncStatusEmail:checked').length > 0)
            var syncStatusEmail = "true";
        else
            var syncStatusEmail = "false";
        
        if (jQuery('#MaxSynchronizations').val() != "")
            var maxSynchronizations = jQuery('#MaxSynchronizations').val();
        else
            var maxSynchronizations = "0";
        
        if (jQuery('#WelcomeHostingEmail:checked').length > 0)
            var welcomeHostingEmail = "true";
        else
            var welcomeHostingEmail = "false";
        if (jQuery('#cron').length > 0)
            var cron = jQuery('#cron').val();
        else
            var cron = "false";
        var product_type =  jQuery('select[name=product_type] option:selected').val();
        var action = jQuery('#save_settings_form').attr('action');
        var serverID = jQuery('#serverID').val();
        var actionType = jQuery('select#action option:selected').val();
        console.log(jQuery('#save_settings').serialize());
        jQuery.ajax({
            url: action,
            type: "POST",
            data: { welcomeEmail: welcomeEmail, resetPasswords: resetPasswords, welcomeHostingEmail: welcomeHostingEmail, serverID: serverID, product_type: product_type, cron: cron, maxSynchronizations:  maxSynchronizations, syncStatusEmail: syncStatusEmail, action: actionType                 
                },
            success: function(){
                  
                if (actionType) {
                    window.location.href = "addonmodules.php?module=advancedSynchronization&modpage=cron";
                } else {
                    alert('Options were successfully updated!');
                }
            }
        });
    });
    
    jQuery("#package-dependencies").change(function(){
        if (jQuery("#package-dependencies option:selected").val() == "") {
            jQuery("#packages-data").html("");
            jQuery("#packages-WHMCS").html("");
            jQuery("#config-action").hide();
            jQuery(".dataTables_processing").hide();
            return false;
        }
        jQuery.ajax({
            beforeSend: function() {
                jQuery(".dataTables_processing").show();
            },
            url: jQuery("#package-action").val() + "&serverID=" + jQuery("#package-dependencies option:selected").val(),
            success: function(data) {
                var html = "<label>Please Select A Package From Remote Server</label><select id='package-name' name='packageName'><option></option>";
                var packages = data;
                if (packages.html.package.length == 0) {
                    html = "<label>There Are No Packages On Remote Server Or Connection Settings Are Incorrect</label>";
                    jQuery("#packages-data").html(html);
                    jQuery("#packages-WHMCS").html("");
                    jQuery("#config-action").hide();
                    jQuery(".dataTables_processing").hide();
                    return false;
                }
                if (packages.status != "success" || packages.html == null || packages.html.length == 0) {
                    jQuery("#packages-data").html("");
                    jQuery("#packages-WHMCS").html("");
                    jQuery("#config-action").hide();
                    jQuery(".dataTables_processing").hide();
                    return false;
                }
                for (i = 0; i < packages.html.package.length; i++) {
                    html = html + "<option value='" + packages.html.package[i].name + "'>" + packages.html.package[i].name + "</option>";
                }
                html = html + "</select>";
                jQuery("#packages-data").html(html);
                jQuery("#package-name").change(function() {
                    jQuery("#packages-WHMCS").html(packages.options);
                    jQuery("#config-action").show();
                });
                
            }
        }).done(function() {
                jQuery(".dataTables_processing").hide();
            });


    });
    var form = $('#dependency-form');
    jQuery(form).submit(function(event) {
        event.preventDefault();
        $.ajax({
            beforeSend: function() {
                jQuery(".dataTables_processing").show();
            },
            url: 'addonmodules.php',
            data: form.serialize(),
            success: function(response) {
                if (response.status == "add") {
                    var html = "<tr style='display: none; color: #1EA83A;' id='"+response.dependency.id+"'>"+
                            "<td>"+response.dependency.server+"</td>"+
                            "<td>"+response.dependency.plan+"</td>"+
                            "<td>"+response.dependency.product+"</td>"+
                            "<td style='width: 90px;'><a href='addonmodules.php?module=advancedSynchronization&modpage=packages&modsubpage=delete&id="+response.dependency.id+"' class='button danger'>Delete</a></td>"+
                            "</tr>";
                    jQuery("#dependencies-table tbody").append(html);
                    jQuery("#no-data").remove();
                    jQuery("#dependencies-table tbody tr[id='"+response.dependency.id+"']").fadeIn("slow");
                } else if (response.status == "update") {
                    var path = "#dependencies-table tbody tr[id='"+response.dependency.id+"']";
                    jQuery(path + " td:nth-child(3)").css('color', '#1EA83A');
                    jQuery(path + " td:nth-child(3)").html(response.dependency.product);
                }
                jQuery("#packages-data").html("");
                jQuery("#packages-WHMCS").html("");
                jQuery("#config-action").hide();
                jQuery(".dataTables_processing").hide();
            }
        }).done(function(){
            jQuery("#packages-data").html("");
            jQuery("#packages-WHMCS").html("");
            jQuery("#config-action").hide();
            jQuery(".dataTables_processing").hide();
            jQuery("#package-dependencies option[value=]").attr("selected", "selected");
        });
    });


});