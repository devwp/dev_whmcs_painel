﻿<?php

	header("Content-Type: text/plain");
	require_once($_SERVER['DOCUMENT_ROOT']."/init.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/includes/clientfunctions.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/nfse_ginfes/nfse/classes/NFSe.class.php");
	
	function tirarAcentos($string){
		return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
	}
	function Xml_to_Json($array){
		$xml = simplexml_load_string($array, "SimpleXMLElement", LIBXML_NOCDATA);
		$json = json_encode($xml);
		return $json;
	}
	
	
	$nfse = new NFSe();
	$arquivoProtocol = 'rps/prot_1.xml';
	$arquivoRPS = 'rps/rps_1.xml';
	$arquivoRPSAssinado = 'rps_assinado/rps_1_assinado.xml';
	$companyname =  isset($_POST['companyname']) ? $_POST['companyname'] : null;
	$cpfcnpj= preg_replace( '#[^0-9]#', '', $_POST['cpfcnpj'] );
	$numeroRps =55568;//$_POST['invoiceid'];   
	$valor = str_replace(',', '.', str_replace('R$', '', $_POST['valor']));
	$dadosTomador = array('nome'=> $companyname,'cpfcnpj' => $cpfcnpj, 'tp' => 'pf', 'valor' => $valor);
	$n = full_query("SELECT * FROM tblinvoiceitems t where invoiceid = ".$_POST['invoiceid']." and relid != 0;");
	//$date = new DateTime('NOW');//date(DATE_ATOM);
	$dtRef = date("Y-m-d\TH:i:s", time());
	$dtCadastro = date("Y-m-d\TH:i:s", time());
	while($ultimo = mysql_fetch_array($n)){
		if ($ultimo["type"] == 'Upgrade'){
			$upgrade = full_query("SELECT * FROM tblupgrades where relid =".$ultimo["relid"]." ORDER BY relid DESC LIMIT 1"); 
			while($up = mysql_fetch_array($upgrade)){
				$descriptionfiscal = full_query("SELECT * FROM tblcustomfieldsvalues where relid =".$up["relid"]." ORDER BY relid DESC LIMIT 1"); 
				while($df = mysql_fetch_array($descriptionfiscal)){
					$descriptionI.=$ultimo['id'].' '.trim($df['value']).' '; 	
				}	
			}	
		}else{
			$descriptionfiscal = full_query("SELECT * FROM tblcustomfieldsvalues where relid =".$ultimo["relid"]." ORDER BY relid DESC LIMIT 1"); 
			while($df = mysql_fetch_array($descriptionfiscal)){
				$descriptionI.=$ultimo['id'].' '.trim($df['value']).' '; 	 
			}	
			
		}
	} 
	
    $descriptionI = str_replace('ç', 'c', tirarAcentos(trim($descriptionI)));//; x

	//Gera o XML da RPS
	if($nfse->gerarRPS($arquivoRPS, $dadosTomador, $descriptionI, $dtRef, $numeroRps )){  


		//Valida se o XML da RPS é valido perante ao schema XSD
	$retornoValidacao = $nfse->validarXML($arquivoRPS, 'Schemas/servico_enviar_lote_rps_envio_v03.xsd');

	if($retornoValidacao['status'] == true){ 
			
			//Assina o xml da RPS
		if($nfse->assinarXML($arquivoRPS, $arquivoRPSAssinado, 'EnviarLoteRpsEnvio', array('EnviarLoteRpsEnvio'))){
					
					//Valida o XML da RPS Assinado
					$retornoValidacaoAss = $nfse->validarXML($arquivoRPSAssinado, 'Schemas/servico_enviar_lote_rps_envio_v03.xsd');
				
					if($retornoValidacaoAss['status'] == true){
						
						$rs = $nfse->enviarXML($arquivoRPSAssinado, 'RecepcionarLoteRpsV3'); 
						$result = str_replace(array('ns3:','ns2:'), '', $rs);//; x
						$json = json_encode(simplexml_load_string($result));
						$encoded = json_decode($json);
						full_query("INSERT INTO nfseGinfes (invoiceid,razaoSocial,cpfcnpj, valor,xml_Envio,codVerificacao,numeroNFSE, protocolo, DataReferencia,DataCadastro, numeroRps, dadosTomador) VALUES ('". $_POST['invoiceid']."','".$companyname."', '".$cpfcnpj."', '".$valor."', '".$rs."', 'aguardando gerar nota', 'aguardando gerar nota','".$encoded->Protocolo."','".$dtRef."','".$dtCadastro."','".$numeroRps."', '".$dadosTomador."')"); 	
						echo "Aguarde sua nota já esta sendo gerada!";
					}else{
						echo '<p>'.$retornoValidacaoAss['mensagem'].'</p>'; 
					}
		}else{
			echo '<p>'.$retornoValidacaoAss['mensagem'].'</p>'; 
		}
	}else{
		echo $retornoValidacao['mensagem'];//print_r($dadosTomador);  
	}		
	}else{
		echo "Erro ao gerar"	;
	}			
	
	

	
/*	$xml_consulta = 'consulta_lote/'.time().'_consulta.xml';
	$xml_consulta_assinado = 'consulta_lote/'.time().'_consultaASS.xml'; 
	
	$x = $nfse->ConsultarNfsePorRps($xml_consulta, $numeroRps);
	 echo $x;
	 
	$retornoValidacao = $nfse->validarXML($xml_consulta, 'Schemas/servico_consultar_nfse_rps_envio_v03.xsd');
	
	echo "\n\n\n".$retornoValidacao['mensagem']."\n\n\n";

    $nfse->assinarXML($xml_consulta, $xml_consulta_assinado, 'ConsultarNfseRpsEnvio', array('ConsultarNfseRpsEnvio'));
	
	$retornoValidacao2 = $nfse->validarXML($xml_consulta_assinado, 'Schemas/servico_consultar_nfse_rps_envio_v03.xsd');
	
	echo "\n\n\n".$retornoValidacao2['mensagem']."\n\n\n";
	
	$nfse->enviarXML($xml_consulta_assinado, 'ConsultarNfseRpsEnvio');

	//$nfse->cabecalho();
	//$nfse->validarCabecalhoXML();
//*/
	
	
	