﻿<?php

	if(!isset($_GET['protocolo']) || !is_numeric($_GET['protocolo'])){
		header('location: nfe_protocolos.php');
	}
	
	$protocolo = $_GET['protocolo'];
	
	//NFSE
	include_once("classes/NFSe.class.php");

	$nfse = new NFSe();

	$xml_consulta = 'consulta_lote/consulta3_'.$protocolo.'.xml';
	$xml_consulta_assinado = 'consulta_lote_assinado/consulta3_protocolo_'.$protocolo.'.xml'; 
		
	if($nfse->ConsultarSituacaoLoteRpsEnvio($xml_consulta, $protocolo)){
		$retornoValidacao = $nfse->validarXML($xml_consulta, 'Schemas/servico_consultar_lote_rps_envio_v03.xsd');
		if($retornoValidacao['status'] == true){
			$retornoAssinatura = $nfse->assinarXML($xml_consulta, $xml_consulta_assinado, 'ConsultarLoteRpsEnvio', array('ConsultarLoteRpsEnvio'));
			//print_r($retornoAssinatura);
			if($retornoAssinatura){						
				$retornoValidacao = $nfse->validarXML($xml_consulta_assinado, 'Schemas/servico_consultar_lote_rps_envio_v03.xsd');
				if($retornoValidacao['status'] == true){
					echo '<p>'.$retornoValidacao['mensagem']."</p>";
					$nfse->enviarXML($xml_consulta_assinado, 'ConsultarLoteRpsV3');
				}
				else{
					echo '<p>'.$retornoValidacao['mensagem']."</p>";	 
				}
			}
			else{
				echo '<p>Erro ao gerar XML assinado</p>';	 
			}
		}
		else{
			echo '<p>'.$retornoValidacao['mensagem'].'</p>'; 
		}
	}
	else{
		echo '<p>Erro ao gerar o arquivo XML de consulta.</p>';	
	}
	
?>
			 