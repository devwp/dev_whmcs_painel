﻿<?php
require_once($_SERVER['DOCUMENT_ROOT']."/init.php");
include_once($_SERVER['DOCUMENT_ROOT']."/modules/addons/nfse_ginfes_addon/nfse_ginfes/nfse/classes/NFSe.class.php"); 

function tirarAcentos($string){
	return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
}

function Xml_to_Json($array){
	$xml = simplexml_load_string($array, "SimpleXMLElement", LIBXML_NOCDATA);
	$json = json_encode($xml);
	return $json; 
}
	
function cancelarNota($invoiceid){
		$xml_consulta =$_SERVER['DOCUMENT_ROOT'].'/modules/addons/nfse_ginfes_addon/nfse_ginfes/nfse/consulta_lote/cancelar_'.$invoiceid.'.xml';
		$xml_consulta_assinado = $_SERVER['DOCUMENT_ROOT'].'/modules/addons/nfse_ginfes_addon/nfse_ginfes/nfse/consulta_lote_assinado/cancelarass_'.$invoiceid.'.xml'; 
		$nfse = new NFSe();
		$dados = full_query("SELECT * FROM nfseGinfes where invoiceID like '".$invoiceid."'");

		while($linha = mysql_fetch_array($dados)){
			$nfse->cancelarNotaV2($xml_consulta , $linha['numeroNFSE']);
				$retornoAssinatura = $nfse->assinarXML($xml_consulta, $xml_consulta_assinado, 'CancelarNfseEnvio', array('CancelarNfseEnvio'));		
				$verif = $nfse->enviarXML_V2($xml_consulta_assinado, 'CancelarNfse'); 
				$resultverif = str_replace(array('ns3:','ns2:','ns4:','ns5:'),'',$verif);
				$jsonverif  = json_encode(simplexml_load_string($resultverif));
				$encoded2 = json_decode($jsonverif);
			
				if ($encoded2->{'Sucesso'} == '﻿false'){
					
				}else{
					
				}
				
				$update  =	full_query("UPDATE nfseGinfes SET cancelado = '0' where invoiceID = '".$invoiceid."'");   
				enviaEmailCancelarNota($invoiceid, $linha["linkNota"], $linha['numeroNFSE']);
		} 
};

function enviaNota($invoiceid){ 
	$q1=full_query("SELECT * FROM tbladdonmodules WHERE module='nfse_ginfes_addon' AND setting='access' AND value='1'");
	if(($q1 && $r1=mysql_fetch_array($q1)) && array_key_exists('value',$r1)) {
	$nfse = new NFSe();
	$arquivoProtocol = $_SERVER['DOCUMENT_ROOT'].'/modules/addons/nfse_ginfes_addon/nfse_ginfes/nfse/rps/prot_1.xml';
	$arquivoRPS = $_SERVER['DOCUMENT_ROOT'].'/modules/addons/nfse_ginfes_addon/nfse_ginfes/nfse/rps/rps_1.xml';
	$arquivoRPSAssinado = $_SERVER['DOCUMENT_ROOT'].'/modules/addons/nfse_ginfes_addon/nfse_ginfes/nfse/rps_assinado/rps_1_assinado.xml';
	$numeroRps = $invoiceid;   

	$dadosInvoice = full_query("SELECT * FROM tblinvoices where id = '".$invoiceid."'");
    $possuidados = 0;
	while($linha = mysql_fetch_array($dadosInvoice)){
		$possuidados = 1;
		$userid =  $linha['userid'];
		$status =  $linha['status'];
	}  
	
	$dadosUserCustomFields = full_query("SELECT * FROM tblcustomfieldsvalues where relid = '".$userid."'");
	$possuidadosCustomFields  = 0;

	while($linha = mysql_fetch_array($dadosUserCustomFields)){ 
		//print_r($type);
		if ( $linha['fieldid'] == '5'){
			$cpfcnpj = $linha['value'];
		}
		if ( $linha['fieldid'] == '4'){
			$type =  $linha['value'];
		}
		if ( $linha['fieldid'] == '6'){
			$RGIE = $linha['value'];
		}
		
		if ( $linha['fieldid'] == '7'){
			$numero = $linha['value'];
		}
	
		if ( $linha['fieldid'] == '27'){
			$cidade = $linha['value'];
		}
	}
	
	$numero = 0;
	
	$dadosUser = full_query("SELECT * FROM tblclients where id = '".$userid."'");
    $possuidados = 0;
	
	while($linha = mysql_fetch_array($dadosUser)){
		$possuidados = 1;
		//print_r($linha['companyname']);
		if(($type == 'Pessoa Jurídica')){
			$name = $linha['companyname'];
			$tp = 'pj';
		}else{
			$name = ($linha['firstname'].' '.$linha['lastname']); 
			$tp = 'pf';
		}
		
		$endereco = $linha['address1'];
		$bairro = $linha['address2'];
		//$cidade = $linha['city'];
		$estado = $linha['state'];
		$cep = str_replace('-', '', tirarAcentos(trim($linha['postcode'])));
		$pais = $linha['country'];
		$email = $linha['email'];
		$telefone = str_replace('.', '', str_replace(')', '', str_replace('(', '', str_replace('-', '',$linha['phonenumber']))));
	}

	$n = full_query("SELECT * FROM tblinvoiceitems t where invoiceid = ".$invoiceid." and relid != 0;");
	$dtRef = date("Y-m-d\TH:i:s", time());
	$dtCadastro = date("Y-m-d\TH:i:s", time());
	$valor = 0.0;
	while($ultimo = mysql_fetch_array($n)){ 
		$valor = $valor + $ultimo["amount"];
		if ($ultimo["type"] == 'Upgrade'){
			$upgrade = full_query("SELECT * FROM tblupgrades where relid =".$ultimo["relid"]." ORDER BY relid DESC LIMIT 1"); 
			while($up = mysql_fetch_array($upgrade)){
				$descriptionfiscal = full_query("SELECT * FROM tblcustomfieldsvalues where relid =".$up["relid"]." ORDER BY relid DESC LIMIT 1"); 
				while($df = mysql_fetch_array($descriptionfiscal)){
					$descriptionI.=$ultimo['id'].' '.trim($df['value']).' '; 	
				}	
			}	
		}else{
			$descriptionfiscal = full_query("SELECT * FROM tblcustomfieldsvalues where relid =".$ultimo["relid"]." ORDER BY relid DESC LIMIT 1"); 
			while($df = mysql_fetch_array($descriptionfiscal)){
				$descriptionI.=$ultimo['id'].' '.trim($df['value']).' '; 	 
			}	
			
		}
	} 
	$cpfcnpj =str_replace(array('/','.','-'), '', $cpfcnpj);
	$valor =   str_replace('ç', 'c', number_format((float)$valor, 2, '.', ''));
	$bairro = str_replace('ç', 'c', tirarAcentos(trim($bairro)));
	$endereco = str_replace('ç', 'c', tirarAcentos(trim($endereco))); 
	$descriptionI = str_replace('ç', 'c', tirarAcentos(trim($descriptionI)));//; x
	$dadosTomador = array('nome'=> $name,'cpfcnpj' => $cpfcnpj, 'tp' => $tp, 'valor' => $valor, 'endereco' =>$endereco, 'bairro' => $bairro, 'cidade' => $cidade,'estado' => $estado, 'cep' => $cep, 'pais' => $pais, 'email' => $email, 'rgie' => $RGIE, 'numero' => $numero, 'telefone' => $telefone, 'descricao' => $descriptionI);
	if($status != 'Cancelled'){
    if ($valor != "0.00"){
	//echo $descriptionI; 
	//Gera o XML da RPS
	if($nfse->gerarRPS($arquivoRPS, $dadosTomador, $descriptionI, $dtRef, $numeroRps )){  
 

		//Valida se o XML da RPS é valido perante ao schema XSD
	$retornoValidacao = $nfse->validarXML($arquivoRPS, $_SERVER['DOCUMENT_ROOT'].'/modules/addons/nfse_ginfes_addon/nfse_ginfes/nfse/Schemas/servico_enviar_lote_rps_envio_v03.xsd');

	if($retornoValidacao['status'] == true){ 
			 
			//Assina o xml da RPS
		if($nfse->assinarXML($arquivoRPS, $arquivoRPSAssinado, 'EnviarLoteRpsEnvio', array('EnviarLoteRpsEnvio'))){
					
					//Valida o XML da RPS Assinado
					$retornoValidacaoAss = $nfse->validarXML($arquivoRPSAssinado, $_SERVER['DOCUMENT_ROOT'].'/modules/addons/nfse_ginfes_addon/nfse_ginfes/nfse/Schemas/servico_enviar_lote_rps_envio_v03.xsd');
				
					if($retornoValidacaoAss['status'] == true){
						
						$rs = $nfse->enviarXML($arquivoRPSAssinado, 'RecepcionarLoteRpsV3'); 
						$result = str_replace(array('ns3:','ns2:'), '', $rs); //x
						$json = json_encode(simplexml_load_string($result));
						$encoded = json_decode($json);
						//print_r($encoded);
						full_query("INSERT INTO nfseGinfes (invoiceid,razaoSocial,cpfcnpj, valor,xml_Envio,codVerificacao,numeroNFSE, protocolo, DataReferencia,DataCadastro, numeroRps, dadosTomador, ip) VALUES ('". $invoiceid."','".$name."', '".$cpfcnpj."', '".$valor."', '".$json."', 'aguardando gerar nota', 'aguardando gerar nota','".$encoded->Protocolo."','".$dtRef."','".$dtCadastro."','".$numeroRps."', '".serialize($dadosTomador)."', '".$_SERVER['REMOTE_ADDR']."')"); 	 
						echo "Aguarde sua nota já esta sendo gerada!"; 
						
					}else{
						echo '<p>'.$retornoValidacaoAss['mensagem'].'</p>'; 
					}
		}else{ 
			echo '<p>'.$retornoValidacaoAss['mensagem'].'</p>'; 
		}
	}else{
		echo $retornoValidacao['mensagem'];//print_r($dadosTomador);  
	}		
	}else{
		echo "Erro ao gerar";
	}			
	}else{
		echo "remover_link";
		
	}
	}
	}
}

function consultaNota($invoiceid){
	$q1=full_query("SELECT * FROM tbladdonmodules WHERE module='nfse_ginfes_addon' AND setting='access' AND value='1'");
	if(($q1 && $r1=mysql_fetch_array($q1)) && array_key_exists('value',$r1)) {
	$nfse = new NFSe();
	$dados = full_query("SELECT * FROM nfseGinfes where invoiceID like '".$invoiceid."' ORDER BY invoiceID DESC LIMIT 1" );
     
	$possuidados = 0;
	while($linha = mysql_fetch_array($dados)){
	$possuidados = 1;
	$protocolo = $linha['protocolo'];
	$xml_consulta = $_SERVER['DOCUMENT_ROOT'].'/modules/addons/nfse_ginfes_addon/nfse_ginfes/nfse/consulta_lote/consulta3_'.$protocolo .'.xml';
		$xml_consulta_assinado =  $_SERVER['DOCUMENT_ROOT'].'/modules/addons/nfse_ginfes_addon/nfse_ginfes/nfse/consulta_lote_assinado/consulta3_protocolo_'.$protocolo .'.xml'; 
		if($nfse->ConsultarSituacaoLoteRpsEnvio($xml_consulta, $protocolo)){
			$retornoValidacao = $nfse->validarXML($xml_consulta, $_SERVER['DOCUMENT_ROOT'].'/modules/addons/nfse_ginfes_addon/nfse_ginfes/nfse/Schemas/servico_consultar_lote_rps_envio_v03.xsd');
			if($retornoValidacao['status'] == true){
				$retornoAssinatura = $nfse->assinarXML($xml_consulta, $xml_consulta_assinado, 'ConsultarLoteRpsEnvio', array('ConsultarLoteRpsEnvio'));
				if($retornoAssinatura){						
					$retornoValidacao = $nfse->validarXML($xml_consulta_assinado, $_SERVER['DOCUMENT_ROOT'].'/modules/addons/nfse_ginfes_addon/nfse_ginfes/nfse/Schemas/servico_consultar_lote_rps_envio_v03.xsd');
					if($retornoValidacao['status'] == true){
					$verif = $nfse->enviarXML($xml_consulta_assinado, 'ConsultarLoteRpsV3');
				
					$resultverif = str_replace(array('ns3:','ns2:','ns4:'),'',$verif);
			
					$jsonverif  = json_encode(simplexml_load_string($resultverif)); 
					$encoded2 = json_decode($jsonverif );

					if (empty($protocolo)){
							$update  =	full_query("UPDATE nfseGinfes SET codVerificacao='".$encoded2->{'ListaNfse'}->{'CompNfse'}->{'Nfse'}->{'InfNfse'}->{'CodigoVerificacao'}."' , numeroNFSE='".$encoded2->{'ListaNfse'}->{'CompNfse'}->{'Nfse'}->{'InfNfse'}->{'Numero'}."', urlNota='"."Erro ao gerar a nota, favor entre em contato com nosso suporte!"."' where invoiceID = '".$invoiceid."'"); 
							echo "Erro ao gerar a nota, favor entre em contato com nosso suporte!";
					}else{
						if($encoded2->{'ListaNfse'}->{'CompNfse'}->{'Nfse'}->{'InfNfse'}->{'CodigoVerificacao'}!=''){
								$update  =	full_query("UPDATE nfseGinfes SET codVerificacao='".$encoded2->{'ListaNfse'}->{'CompNfse'}->{'Nfse'}->{'InfNfse'}->{'CodigoVerificacao'}."' , numeroNFSE='".$encoded2->{'ListaNfse'}->{'CompNfse'}->{'Nfse'}->{'InfNfse'}->{'Numero'}."', linkNota='"."<a href=\"http://sjrp.ginfesh.com.br/report/consultarNota?__report=nfs_ver9&cdVerificacao=".$encoded2->{'ListaNfse'}->{'CompNfse'}->{'Nfse'}->{'InfNfse'}->{'CodigoVerificacao'}."&numNota=".$encoded2->{'ListaNfse'}->{'CompNfse'}->{'Nfse'}->{'InfNfse'}->{'Numero'}."&cnpjPrestador=null\">Visualizar nota fiscal de serviços número ".$encoded2->{'ListaNfse'}->{'CompNfse'}->{'Nfse'}->{'InfNfse'}->{'Numero'}." </a>"."' where invoiceID = '".$invoiceid."'"); 
								
								if ($linha["isEnviado"] == "0"){
									enviaEmail($invoiceid, "<a href=\"http://sjrp.ginfesh.com.br/report/consultarNota?__report=nfs_ver9&cdVerificacao=".$encoded2->{'ListaNfse'}->{'CompNfse'}->{'Nfse'}->{'InfNfse'}->{'CodigoVerificacao'}."&numNota=".$encoded2->{'ListaNfse'}->{'CompNfse'}->{'Nfse'}->{'InfNfse'}->{'Numero'}."&cnpjPrestador=null\">http://sjrp.ginfesh.com.br/report/consultarNota?__report=nfs_ver9&cdVerificacao=".$encoded2->{'ListaNfse'}->{'CompNfse'}->{'Nfse'}->{'InfNfse'}->{'CodigoVerificacao'}."&numNota=".$encoded2->{'ListaNfse'}->{'CompNfse'}->{'Nfse'}->{'InfNfse'}->{'Numero'}."&cnpjPrestador=null</a>", $encoded2->{'ListaNfse'}->{'CompNfse'}->{'Nfse'}->{'InfNfse'}->{'Numero'});
									
									$update  =	full_query("UPDATE nfseGinfes SET isEnviado = '1' where invoiceID = '".$invoiceid."'"); 
								}
							echo "<a href=\"http://sjrp.ginfesh.com.br/report/consultarNota?__report=nfs_ver9&cdVerificacao=".$encoded2->{'ListaNfse'}->{'CompNfse'}->{'Nfse'}->{'InfNfse'}->{'CodigoVerificacao'}."&numNota=".$encoded2->{'ListaNfse'}->{'CompNfse'}->{'Nfse'}->{'InfNfse'}->{'Numero'}."&cnpjPrestador=null\">Visualizar nota fiscal de serviços número ".$encoded2->{'ListaNfse'}->{'CompNfse'}->{'Nfse'}->{'InfNfse'}->{'Numero'}." </a>"; 	
						}else{
							print_r("Processando.. Aguarde enquanto sua nota fiscal eletrônica de serviços esta sendo gerada!");
						}
					}
				}else{
					echo '<p>'.$retornoValidacao['mensagem']."</p>";	 
				}
				} else{
					echo '<p>Erro ao gerar XML assinado</p>';	 
				}
			}else{
				echo '<p>'.$retornoValidacao['mensagem'].'</p>'; 
			}
		} else{
			echo '<p>Erro ao gerar o arquivo XML de consulta.</p>';	
		}
	}
	if ($possuidados== 0){
		$dadosInvoice = full_query("SELECT * FROM tblinvoices where id = '".$invoiceid."'");
		while($linha = mysql_fetch_array($dadosInvoice)){
			$status =  $linha['status'];
			$total =  $linha['subtotal'];
		}  
		if ($status != 'Paid'){
			if ($total  != '0.00'){
				echo "<i class=\"fa fa-check-circle\"></i><input type=\"submit\" class=\"postnota\" id=\"gerarnota\" value =\"Gerar Nota Fiscal Antecipada\" ></input>";	
			}
		}
	}
	}else{
		echo "remover_link";
		
	}
	
}

function enviaEmail($invoiceid, $url, $numero){

$command = 'SendEmail';
$values = array( 'messagename' => 'Enviar NFSE Ginfes', 'id' => $invoiceid, 'customvars' => base64_encode (serialize( array ('customvars'=>$url, 'customvars2'=>$numero))));
$adminuser = 'webplusidc';
 
// Call API
$results = localAPI($command, $values, $adminuser);
if ($results['result']!="success") echo "An Error Occurred: ".$results['result'];
}

function enviaEmailCancelarNota($invoiceid, $url, $numero){
	$command = 'SendEmail';
	$values = array( 'messagename' => 'Cancelar NFSE Ginfes', 'id' => $invoiceid, 'customvars' => base64_encode (serialize( array ('customvars'=>$url, 'customvars2'=>$numero))));
	$adminuser = 'webplusidc';

	// Call API
	$results = localAPI($command, $values, $adminuser);
	if ($results['result']!="success") echo "An Error Occurred: ".$results['result'];
}
?>
			 