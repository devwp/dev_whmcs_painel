<?php
/*
 **********************************************
 
 *** SmarterTrack Help Desk Module ***
 
 This module ties together SmarterTrack and WHMCS in regards
 to the support system.
 
 **********************************************
 */
require_once('smartertrack_functions.php');
if (!defined("WHMCS"))
    die("This file cannot be accessed directly");


function smartertrack_config()
{
    $configarray = array(
        "name" => "SmarterTrack Help Desk",
        "description" => "This addon hooks WHMCS to the SmarterTrack help desk",
        "version" => "2.2",
        "author" => "SmarterTools Inc.",
        "language" => "english",
        "fields" => array(
            "url" => array(
                "FriendlyName" => "SmarterTrack Help Desk URL",
                "Type" => "text",
                "Size" => "25",
                "Description" => "Full URL to your SmarterTrack help desk (e.g., https://support.example.com).  HTTPS recommended.",
                "Default" => "https://"
            ),

            "admin" => array(
                "FriendlyName" => "Administrator Name",
                "Type" => "text",
                "Size" => "25",
                "Description" => "The name of a SmarterTrack administrator. (E.g., admin or administrator)"
            ),

            "password" => array(
                "FriendlyName" => "Administrator Password",
                "Type" => "password",
                "Size" => "25",
                "Description" => "The password for the Administrator account above"
            ),

            "overridekb" => array(
                "FriendlyName" => "Use SmarterTrack KB",
                "Type" => "yesno",
                "Size" => "25",
                "Description" => "Change the knowledge base link in WHMCS to point to SmarterTrack."
            ),

            "redirectcontact" => array(
                "FriendlyName" => "Override Pre-Sales 'Contact Us'",
                "Type" => "yesno",
                "Size" => "25",
                "Description" => "Replace the 'Send Inquiry' function to send tickets through SmarterTrack without requiring users to log in first."
            ),

            "contactdefaultdepartment" => array(
                "FriendlyName" => "Default Department for 'Contact Us'",
                "Type" => "text",
                "Size" => "25",
                "Description" => "Name of the department you would like to use for sales inquiries.  (e.g., 'Sales Department')"
            ),

            "deptsToHide" => array(
                "FriendlyName" => "Hidden Departments",
                "Type" => "text",
                "Size" => "25",
                "Description" => "List any departments that should not be visible to users, separated by commas"
            ),
   
            "helpphone" => array(
                "FriendlyName" => "Help Phone Number",
                "Type" => "text",
                "Size" => "25",
                "Description" => "Displays on error pages in case customers encounter issues."
            )
		)
    );
    return $configarray;
}

function smartertrack_activate()
{
    # Create DB Table to link together WHMCS users with ST users
    $query = "CREATE TABLE IF NOT EXISTS `smartertrack_whmcs_users` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `whm_userid` int(11) NOT NULL,
      `st_userid` int(11) NOT NULL,
	  `st_username` varchar(255) NOT NULL,
	  `st_password` varchar(255) NOT NULL,
	  `st_email` varchar(255) NOT NULL,
      UNIQUE KEY `id` (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
    $result = mysql_query($query);

    # Return Result
    return array(
        'status' => 'success',
        'description' => 'SmarterTrack Module requires the url for the web services, an administrator agent name, ' .
            'and the administrators password, click configure to continue setup'
    );
}

function smartertrack_deactivate()
{
    # Remove Custom DB Table
    $query = "DROP TABLE `smartertrack_whmcs_users`";
    $result = mysql_query($query);

    # Return Result
    return array(
        'status' => 'success',
        'description' => 'SmarterTrack Module has been deactivated, thank you.'
    );
}

function smartertrack_upgrade($vars)
{
    $version = $vars['version'];
    # Run SQL Updates for V1.0 to V1.1 like the following if you need it
    // if ($version < 1.1) { }
}

function smartertrack_functions_importusers()
{
    $st_settings = smartertrack_settings();
    $all_track_users = array();
    $all_whmcs_users = array();
    $bundle_set = array();
    $bundles = array();
    $bundle_set_count = 0;
    $total_whmcs_users = 0;
    $users_per_bundle = 100;
	
	global $cc_encryption_hash;

    # Load all track users
    $track_user_query = select_query('smartertrack_whmcs_users', 'whm_userid, st_userid, st_username, st_password, st_email', array());
    while ($data = mysql_fetch_array($track_user_query)) {
        if ($data['st_userid'] != null) {
            try {
				if ($st_settings['md5hashenabled'] == true)
					$data['st_password'] = ($data['st_password']);
					//$data['st_password'] = decrypt($data['st_password'],$cc_encryption_hash);
					//$data['st_password'] = $data['st_password'];
            } catch (Exception $e) {
            }
            $all_track_users[$data['whm_userid']] = $data;
        }
    }

    # Load all WHMCS users
    $whmcs_user_query = select_query('tblclients', 'id, firstname, lastname, email, password', array());
    while ($data = mysql_fetch_array($whmcs_user_query)) {
        if ($data['id'] != null) {
            try {
 				if ($st_settings['md5hashenabled'] == true)
					$data['password'] = ($data['password']);
					//$data['password'] = decrypt($data['password'],$cc_encryption_hash);
					//$data['password'] = $data['password'];
            } catch (Exception $e) {
            }
            $all_whmcs_users[$data['id']] = $data;
        }
    }

    # Loop through all WHMCS users and create lists of 100 users to process
    foreach ($all_whmcs_users as $data_id => $data) {
        if ($bundle_set_count == $users_per_bundle) {
            $bundles[] = $bundle_set;
            $bundle_set = array();
            $bundle_set_count = 0;
        }
        $bundle_set[] = $data;
        $bundle_set_count = $bundle_set_count + 1;
        $total_whmcs_users = $total_whmcs_users + 1;
    }
    if ($bundle_set_count > 0)
        $bundles[] = $bundle_set;

    # Process lists of users
    $counter = 0;
    foreach ($bundles as $bundle_set) {

        # Show status to user
        $set_start = $counter * $users_per_bundle;
        $set_end = $set_start + $users_per_bundle - 1;
        if ($set_end > $total_whmcs_users)
            $set_end = $total_whmcs_users;
        echo '<p>Processing users ' . $set_start . '-' . $set_end . ' of ' . $total_whmcs_users . '</p>';

        # Do the work
        smartertrack_functions_process_user_bundle($bundle_set, $st_settings, $all_track_users);
   }

    echo '<BR/><h4>PROCESS COMPLETED!</h4><br>';
}

function smartertrack_functions_process_user_bundle($bundle_set, $st_settings, $all_track_users)
{
    // Prepare request
    $users = array();
    foreach ($bundle_set as $user){
        $converted_user = array(
            "Username"=> $user['email'],
            "Password"=>$user['password'],
            "EmailAddress"=> $user['email'],
            "IsEmailVerified"=>true,
            "DisplayName"=>$user['firstname'] . " " . $user['lastname'],
            "RolesToAdd"=> array(1,6),
            "WhmcsUserId"=>$user['id']
        );
        $users[] = $converted_user;
    }

    // Send request
    $params = array(
        "authUserName" => $st_settings['admin'],
        "authPassword" => $st_settings['password'],
        "users" => $users
    );

    try {
        $main_return = smartertrack_webServiceCall($st_settings['url'] . "/services2/svcIntegrations.asmx?WSDL", $params, "WhmcsEnsureUsersCreated");
        $return = $main_return["WhmcsEnsureUsersCreatedResult"];

        // Process results
        if (!$return || !$return['Result']){
            echo "<br/> There was a problem calling SmarterTrack with this set of users. ". $return['Message'] ." <br/><br/>";
            return;
        }

        $result_items = $return['Items']['WhmcsEnsureUsersCreatedResultItem'];
        foreach ($result_items as $r){

            if (!$r['UserId']){
                if ($r['Message'])
                    echo "<br/>" . $r['Message'];
                else
                    echo "<br>Skipped " .  $r['Username'];
                continue;
            }

            // Find the user in the bundle
            $new_user_id = $r['UserId'];
            $whmcs_user_id = $r['WhmcsUserId'];
            $user_name = $r['Username'];
            $found_user = null;
            $found_request = null;

            // Find original request
            foreach ($users as $user){
                if ($user['WhmcsUserId'] == $whmcs_user_id)
                {
                    $found_request = $user;
                    break;
                }
            }

            // Find user in initial track list
            foreach ($all_track_users as $v => $track_user){
                if ($track_user['whm_userid'] == $whmcs_user_id)
                {
                    $found_user = $track_user;
                    break;
                }
            }

            if (!$found_request)
                continue;

            if ($found_user) {
                update_query("smartertrack_whmcs_users", array(
                    "st_userid" => $new_user_id,
                    "st_password" => $found_request['Password'],
                    "st_username" => $user_name,
                    "st_email" => $found_request['EmailAddress']
                ), array("whm_userid" => $whmcs_user_id));
            } else {
                insert_query("smartertrack_whmcs_users", array(
                    "st_userid" => $new_user_id,
                    "st_password" => $found_request['Password'],
                    "st_username" => $user_name,
                    "st_email" => $found_request['EmailAddress'],
                    "whm_userid" => $whmcs_user_id
                ));
                echo "<br>Created <strong>" . $found_request['DisplayName'] . "</strong><br>";
            }
        }
    } catch (Exception $e) {
        echo "<br/> There was a problem calling SmarterTrack with this set of users. ". $e ." <br/><br/>";
        return;
    }
}

function smartertrack_output($vars)
{
    $modulelink = $vars['modulelink'];
    $version = $vars['version'];
    $LANG = $vars['_lang'];
    $lastUser = "";

    echo '<p>' . $LANG['intro'] . '</p>' . '<p>' . $LANG['description'] . '</p>' . '<p>' . $LANG['documentation'] . '</p>';

    echo "<br><br>";
    echo "Click to generate SmarterTrack Users for users in the WHMCS database and synchronize the two systems (duplicates will not be created):";
    echo "<style>
        .smartertrack_integration_button { background:ivory !important; border:solid 1px #333 !important; border-radius: 0 !important; margin-top: 10px; }
         .smartertrack_integration_button:hover { background:white !important; border:solid 1px #000 !important; border-radius: 0 !important; margin-top: 10px; }
        </style><form name='submitusers' method='post' enctype='multipart/form-data' id='submituser'>";
    echo "<input class='btn primary smartertrack_integration_button' type='submit' name='save' value='Synchronize Users' />";
    echo "</form>";
    echo "<br><br>";

    if ($_POST) {
        smartertrack_functions_importusers();
    }
    else {
        $blah = select_query("smartertrack_whmcs_users", 'st_email', array());
        $counter = 0;
        while ($data1 = mysql_fetch_array($blah)) {
              $counter = $counter+1;
        }

        echo "<strong>There are ". $counter ." WHMCS users synced to SmarterTrack:</strong><p>To get a precise list, use this database query:</p><pre>select st_email FROM whmcs.smartertrack_whmcs_users order by st_email;</pre>";
    }

}

function smartertrack_sidebar($vars)
{
    $modulelink = $vars['modulelink'];
    $version = $vars['version'];
    $LANG = $vars['_lang'];
    $sidebar = '<span class="header"><img src="../modules/addons/smartertrack/images/smartertrack.jpg" class="absmiddle" width="16" height="16" /> SmarterTrack Help Desk Module</span>
    <ul class="menu">
        <li><a href="http://portal.smartertools.com">SmarterTools Inc (help / kbs)</a></li>
        <li><a href="http://portal.smartertools.com">Version: ' . $version . '</a></li>
    </ul>';
    return $sidebar;
}

?>