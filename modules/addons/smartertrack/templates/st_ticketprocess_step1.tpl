<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">{$LANG.navopenticket}</h3>
			</div>
			<div class="panel-body">
				<p>{$LANG.supportticketsheader}</p>
				{foreach from=$departments key=num item=department}
					{if $department.deptid != 17}
					<hr />
					<p><strong><a href="{$smarty.server.PHP_SELF}?step=2&amp;deptid={$department.deptid}"><i class="fa fa-envelope"></i>&nbsp;&nbsp;{$department.name}</a></strong></p>
					{if $department.description}
						<p>{$department.description}</p>
					{/if}
						{/if}
				{foreachelse}
					<div class="alert alert-info">
						{$LANG.nosupportdepartments}
					</div>
				{/foreach}
			</div>
		</div>
	</div>
</div>







{*

<div class="page-header">
    <div class="styled_title"><h1>Open Ticket</h1></div>
</div>
{if $errormessage}
<div class="alert-message error">
    <p>{$errormessage}</p>
</div>
{/if}
<p>{$LANG.supportticketsheader}</p>

<br />
<table class="departments table table-bordered table-hover table-list dataTable no-footer dtr-inline" border="0" cellpadding="10" cellspacing="0">
    {foreach from=$departments item=department}
        <!--tr>
                <td>
					<img src="images/emails.gif" /> &nbsp;
				</td-->
				<td>
					<strong>
						<a href="{$smarty.server.PHP_SELF}?step=2&amp;deptid={$department.deptid}">{$department.name}</a>
					</strong>
				</td>
		</tr>
    {/foreach}
</table>
<br>
<br>
<br>
*}
