<script type="text/javascript">
    var lang = [];
{foreach from=$lang key=kk item=langphrase}
    lang['{$kk}'] = "{$langphrase}";
{/foreach}
</script>
{literal}
    <style>
        .cancel {
            width: 50px;
        }
        .adddef {
            width: 110px;
        }
    </style>
{/literal}
<h2>{if !$edit}{$lang.addrule}{else}{$lang.editrule}{/if}</h2>
<div class="clear"></div>

{if $chooseservertype}
    <div style="margin-top: 30px;">
        <label class="control-label" for="servertype">{$lang.choseservertype}</label>
        <select id="servertype" name="servertype" >
            <option value="--" >{$lang.selecttype}</option>
            {foreach from=$servertypes  item=servertype}
                <option value="{$servertype.type}" >{$servertype.type}</option>
            {/foreach}
        </select>
        <button id="selcteservertype" class="btn btn-success" value="Next" >{$lang.next}</button>
    </div>
{else}
<form style="margin-top: 30px;" id="ruledefform" action="" method="post">
    <input type="hidden" name="saverule" value="1" />
    <table class="table" id="ruledeftable">
        <thead>
            <tr>
                <th colspan="2">{$lang.ruledefenition}</th>
                <th class="span2">{$lang.actionslabel}</th>
            </tr>
        </thead>
        <tbody>
            {if $edit}
                {foreach from=$rule.configurableoptions item=configoption}
                    <tr>
                        <td class="span4 select-col">
                            <select id="coselect">
                                <option value="-">{$lang.selectconfigurableoption}</option>
                                {foreach from=$configOptions item=option}
                                    <option value="{$option.id}" {if $configoption.id == $option.id}selected{/if} >{$option.name} - {$option.optionname}</option>
                                {/foreach}
                            </select>
                            <select style="display: none;" id="addselect">
                                <option value="-">{$lang.selectaddon}</option>
                                {foreach from=$addons item=addon}
                                    <option value="{$addon.id}">{$addon.name}</option>
                                {/foreach}
                            </select>
                            <select style="display: none;" id="cfselect">
                                <option value="-">{$lang.selectcustomfield}</option>
                                {foreach from=$customfilds item=customf}
                                    <option value="{$customf.id}">{$customf.productname} - {$customf.fieldname}</option>
                                {/foreach}
                            </select>
                        </td>
                        <td class="defbody">
                            {if $configoption.optiontype == 1}
                                <select name="rule[configoption][{$configoption.id}]">
                                    {foreach from=$configoption.sub item=sub}
                                        <option value="{$sub.id}" {if $sub.id == $configoption.value}selected{/if}>{$sub.optionname}</option>
                                    {/foreach}
                                </select>
                            {elseif $configoption.optiontype == 2}
                                {foreach from=$configoption.sub item=sub}
                                    <label><input type="radio" value="{$sub.id}" name="rule[configoption][{$configoption.id}]" {if $sub.id == $configoption.value}checked{/if}>{$sub.optionname}</label>
                                    {/foreach}
                                {elseif $configoption.optiontype == 3}
                                <label>
                                    <input type="hidden" name="rule[configoption][{$configoption.id}][]" value="1">
                                    <input type="checkbox" value="1" name="rule[configoption][{$configoption.id}][check]" {if $configoption.check}checked=""{/if} >
                                    {$configoption.sub[0].optionname}
                                </label>
                            {elseif $configoption.optiontype == 4}
                                <span>
                                    {$lang.min} 
                                    <input name="rule[configoption][{$configoption.id}][minval]" id="minval" style="width: 160px;" type="text" value="{$configoption.minval}" placeholder="{$configoption.qtyminimum}" />
                                    <input name="" id="qtyminimum" type="hidden" value="{$configoption.qtyminimum}" />
                                </span>
                            <span style="display: inline-block">
                                {$lang.max} 
                                <input name="rule[configoption][{$configoption.id}][maxval]" id="maxval" style="width: 160px;" type="text" value="{$configoption.maxval}" placeholder="{$configoption.qtymaximum}" /> ({$lang.leaveblank})
                                <input name="" id="qtymaximum" type="hidden" value="{$configoption.qtymaximum}" />
                            </span>
                            {/if}
                        </td>
                        <td>
                            <a href="#" class="btn  btn-warning cancel">{$lang.remove}</a>
                            <div class="btn-group pull-right">
                                <a class="btn btn-info dropdown-toggle deftoggle" data-toggle="dropdown" href="#">
                                    {$lang.changedefinition}
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a tabindex="-1" class="addco" href="#">{$lang.configurableoption}</a></li>
                                    <li><a tabindex="-1" class="addadd" href="#">{$lang.addon}</a></li>
                                    <li><a tabindex="-1" class="addcf" href="#">{$lang.customfield}</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                {/foreach}
                {foreach from=$rule.addons item=addon}
                    <tr>
                        <td class="span4 select-col">
                            <select id="addselect">
                                <option value="-">{$lang.selectaddon}</option>
                                {foreach from=$addons item=addono}
                                    <option value="{$addono.id}" {if $addon.id == $addono.id}selected{/if}>{$addono.name}</option>
                                {/foreach}
                            </select>
                            <select style="display: none;" id="coselect">
                                <option value="-">{$lang.selectconfigurableoption}</option>
                                {foreach from=$configOptions item=option}
                                    <option value="{$option.id}" >{$option.name} - {$option.optionname}</option>
                                {/foreach}
                            </select>
                            <select style="display: none;" id="cfselect">
                                <option value="-">{$lang.selectcustomfield}</option>
                                {foreach from=$customfilds item=customf}
                                    <option value="{$customf.id}">{$customf.productname} - {$customf.fieldname}</option>
                                {/foreach}
                            </select>
                        </td>
                        <td class="defbody">
                            <input type="hidden" name="rule[addon][{$addon.id}]" value="{$addon.id}" />
                            <span style="word-wrap: break-word;">{$lang.addondescription}: {$addon.description}</span>
                        </td>
                        <td>
                            <a href="#" class="btn  btn-warning cancel">{$lang.remove}</a>
                            <div class="btn-group pull-right">
                                <a class="btn btn-info dropdown-toggle deftoggle" data-toggle="dropdown" href="#">
                                    {$lang.changedefinition}
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a tabindex="-1" class="addco" href="#">{$lang.configurableoption}</a></li>
                                    <li><a tabindex="-1" class="addadd" href="#">{$lang.addon}</a></li>
                                    <li><a tabindex="-1" class="addcf" href="#">{$lang.customfield}</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                {/foreach}
                {foreach from=$rule.customfields item=customfild}
                    <tr>
                        <td class="span4 select-col">
                            <select id="cfselect">
                                <option value="-">{$lang.selectcustomfield}</option>
                                {foreach from=$customfilds item=customf}
                                    <option value="{$customf.id}" {if $customfild.id == $customf.id}selected{/if}>{$customf.productname} - {$customf.fieldname}</option>
                                {/foreach}
                            </select>
                            <select style="display: none;" id="coselect">
                                <option value="-">{$lang.selectconfigurableoption}</option>
                                {foreach from=$configOptions item=option}
                                    <option value="{$option.id}" >{$option.name} - {$option.optionname}</option>
                                {/foreach}
                            </select>
                            <select style="display: none;" id="addselect">
                                <option value="-">{$lang.selectaddon}</option>
                                {foreach from=$addons item=addon}
                                    <option value="{$addon.id}">{$addon.name}</option>
                                {/foreach}
                            </select>
                        </td>
                        <td class="defbody">
                            {if $customfild.fieldtype == "dropdown"}
                                <select name="rule[customfield][{$customfild.id}]">
                                    {foreach from=$customfild.fieldoptions item=sub}
                                        <option value="{$sub}" {if $sub == $customf.value}selected{/if}>{$sub}</option>
                                    {/foreach}
                                </select>
                            {elseif $customfild.fieldtype == "tickbox"}
                                <label>
                                    <input type="hidden" name="rule[customfield][{$customfild.id}][{$customfild.id}]" value="1">
                                    <input type="checkbox" value="1" {if $customfild.check}checked=""{/if} name="rule[customfield][{$customfild.id}][check]">
                                    {$customfild.fieldname}
                                </label>
                            {elseif $customfild.fieldtype == "text"}
                                <input name="rule[customfield][{$customfild.id}]"  style="width: 160px;" type="text" value="{$customfild.value}" /> {$customfild.description}
                            {/if}
                        </td>
                        <td>
                            <a href="#" class="btn  btn-warning cancel">{$lang.remove}</a>
                            <div class="btn-group pull-right">
                                <a class="btn btn-info dropdown-toggle deftoggle" data-toggle="dropdown" href="#">
                                    {$lang.changedefinition}
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a tabindex="-1" class="addco" href="#">{$lang.configurableoption}</a></li>
                                    <li><a tabindex="-1" class="addadd" href="#">{$lang.addon}</a></li>
                                    <li><a tabindex="-1" class="addcf" href="#">{$lang.customfield}</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                {/foreach}
            {/if}
            <tr>
                <td class="span4 select-col">
                    <select style="display: none;" id="coselect">
                        <option value="-">{$lang.selectconfigurableoption}</option>
                        {foreach from=$configOptions item=option}
                            <option value="{$option.id}" >{$option.name} - {$option.optionname}</option>
                        {/foreach}
                    </select>
                    <select style="display: none;" id="addselect">
                        <option value="-">{$lang.selectaddon}</option>
                        {foreach from=$addons item=addon}
                            <option value="{$addon.id}">{$addon.name}</option>
                        {/foreach}
                    </select>
                    <select style="display: none;" id="cfselect">
                        <option value="-">{$lang.selectcustomfield}</option>
                        {foreach from=$customfilds item=customf}
                            <option value="{$customf.id}">{$customf.productname} - {$customf.fieldname}</option>
                        {/foreach}
                    </select>
                </td>
                <td class="defbody"></td>
                <td>
                    <a href="#" class="btn  btn-warning cancel" style="display: none;">{$lang.cancel}</a>
                    <div class="btn-group pull-right">
                        <a class="btn btn-success dropdown-toggle deftoggle adddef" data-toggle="dropdown" href="#">
                            {$lang.adddefinition}
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a tabindex="-1" class="addco" href="#">{$lang.configurableoption}</a></li>
                            <li><a tabindex="-1" class="addadd" href="#">{$lang.addon}</a></li>
                            <li><a tabindex="-1" class="addcf" href="#">{$lang.customfield}</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div>
        <label class="control-label" for="servers">{$lang.serverslabel}</label>
        <select id="servers" name="servers[]" multiple="">
            {foreach from=$pservers  item=server}
                <option value="{$server.id}" {if $server.id|in_array:$rule.servers}selected{/if}>{$server.name}</option>
            {/foreach}
        </select>
    </div>
</form>    
<div>
    <a class="btn btn-success" href="#" id="saverulebtn">{$lang.savechanges}</a>
    <a class="btn btn-info" href="addonmodules.php?module=serverswitcher&modpage=ssconfig">{$lang.cancel}</a>   
</div>
{/if}
<div class="clear"></div>

<script type="text/javascript" src="../modules/addons/serverswitcher/core/assets/js/addrule.js" ></script>