{literal}
    <style>
        div#users-contain {
            width: 350px; 
            margin: 20px 0; 
        }
        div#users-contain table { 
            margin: 1em 0; 
            border-collapse: collapse; 
            width: 100%; 
        }
        div#users-contain table td, div#users-contain table th { 
            border: 1px solid #eee;
            padding: .6em 10px;
            text-align: left; 
        }
        .ui-dialog .ui-state-error { 
            padding: .3em; 
        }
        .validateTips { 
            border: 1px solid transparent; 
            padding: 0.3em; 
        }
        
        #rulestable .btn {
            margin-left: 3px !important;
        }
        
        ul.inline li {
            display: inline-block;
            margin-left: 8px !important;
        }
        
        .control-group .controls #serverhint {
            display: block !important;
            padding-top: 5px !important;
        }
    </style>
{/literal}

<h2>{$lang.switcherrules}</h2>
<p style="margin-top: 20px;">{$lang.switcherruleshint}.</p>

<div id="addgroupform" class="form-horizontal" style="display: none;">
    <div class="control-group">
        <label class="control-label" for="inputName">{$lang.namelabel}</label>
        <div class="controls">
            <input type="text" id="inputName" placeholder="Group Name">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputProductg">{$lang.productlabel}</label>
        <div class="controls">
            <select multiple="" id="inputProductg" >
                {foreach from=$prodgroups key=id item=name}
                    <option value="{$id}">{$name}</option>
                {/foreach}
            </select>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputServer">{$lang.dserverslabel}</label>
        <div class="controls">
            <select multiple="" id="inputServer" >
                
            </select>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" id="addgroupbtn" class="btn btn-success">{$lang.savelabel}</button>
            <a class="btn btn-warning" href="#" id="addgroup" >{$lang.cancel}</a>
        </div>
    </div>
</div>         


<div class="clear"></div>

<!-- Rules table -->


    <div style="margin-bottom: 20px;"><a class="btn btn-info" href="#" id="addgroup" >{$lang.addgrouplabel}</a></div>
    <div class="clear"></div>
    <table class="table table-striped table-bordered" id="rulestable">
        <thead>
            <tr>
                <th>{$lang.namelabel}</th>
                <th style="width: 270px;">{$lang.actionslabel}</th>
            </tr>
        </thead>
        <tbody id="sortable">
            {foreach from=$rulegroups item=rgroup}
                <tr class="info" id="g{$rgroup.id}">
                    <input type="hidden" name="rulegruopid" class="rulegruopid" value="{$rgroup.id}" />
                    <td colspan="1">
                        <div class="row-fluid">
                            <div class="span3" style="overflow: hidden;">{$rgroup.name}</div>
                            <div class="span3">
                                <ul class="inline" style="margin: 0 !important;">
                                    <li style="margin: 0 !important;"><strong>{$lang.productgroups}:</strong></li>
                                    {foreach from=$rgroup.pgroups item=pgroup}
                                        <li style="margin: 0 !important;"><a href="configproducts.php?action=editgroup&ids={$pgroup.id}">{$pgroup.name}</a></li>
                                    {/foreach}
                                </ul>
                            </div>
                            <div class="span3"></div>
                            {if $rgroup.defaultservers && $rgroup.defaultservers != "null"}
                                <div class="span2  pull-right" style="overflow: hidden;">
                                    <ul class="inline" style="margin: 0 !important;">
                                        <li style="margin: 0 !important;"><strong>{$lang.defaultservers}:</strong></li>
                                        {foreach from=$rgroup.defaultservers item=server}
                                            <li style="margin: 0 !important;max-width:150px;"><a href="configservers.php?action=manage&id={$server.id}">{$server.name}</a></li>
                                        {/foreach}
                                    </ul>
                                </div>
                            {/if}
                        </div>
                    </td>
                    <td style="text-align: right; width: 272px;">
                        <!--<a class="editrulegroup" href="#"><i class="icon icon-pencil"></i></a>-->
                        <a class="addrule btn btn-success secondbutton" href="#" data-original-title="">{$lang.addrule}</a>
                        <a class="delrulegroup btn btn-danger thirdbutton" href="#" data-original-title="">{$lang.removegroup}</a>
                    </td>
                </tr>
                {foreach from=$rules[$rgroup.id] item=rule}
                    <tr>
                        <input type="hidden" class="ruleid" name="ruleid" value="{$rule.id}" />
                        <td colspan="1">
                            <div class="row-fluid">
                                {if $rule.configurableoptions}
                                    <div class="span3"  style="overflow: hidden;">
                                        <ul class="unstyled">
                                           <li><strong>{$lang.configurableoptions}</strong></li>
                                           {foreach from=$rule.configurableoptions item=configurableoption}
                                                <li style="max-width:200px;">
                                                    {if $configurableoption.optiontype == 4}
                                                        {$configurableoption.optionname} <i class="icon-arrow-right"></i>
                                                        {if $configurableoption.minval == $configurableoption.maxval} 
                                                            {$configurableoption.minval}
                                                        {elseif $configurableoption.minval && $configurableoption.maxval}
                                                            {$lang.from} {$configurableoption.minval} {$lang.to} {$configurableoption.maxval}
                                                        {elseif $configurableoption.minval}
                                                            {$lang.more} {$configurableoption.minval}
                                                        {elseif $configurableoption.maxval}
                                                            {$lang.less} {$configurableoption.maxval}
                                                        {/if}
                                                    {elseif $configurableoption.optiontype == 3}
                                                        {$configurableoption.optionname} <i class="icon-arrow-right"></i> {if $configurableoption.check}{$lang.checked}{else}{$lang.unchecked}{/if}
                                                    {else}
                                                        {$configurableoption.optionname} <i class="icon-arrow-right"></i> {$configurableoption.value}
                                                    {/if}
                                                </li>
                                            {/foreach}
                                        </ul>
                                   </div>
                                {/if}
                                {if $rule.addons}
                                    <div class="span3"  style="overflow: hidden;">
                                        <ul class="unstyled">
                                            <li><strong>{$lang.addons}</strong></li>
                                            {foreach from=$rule.addons item=addon}
                                                <li style="max-width:200px;">
                                                    {$addon.name}
                                                </li>
                                            {/foreach}
                                        </ul>
                                    </div>
                                {/if}
                                {if $rule.customfields}
                                    <div class="span3"  style="overflow: hidden;">
                                        <ul class="unstyled">
                                            <li><strong>{$lang.customfields}</strong></li>
                                            {foreach from=$rule.customfields item=customfield}
                                                <li>
                                                    {$customfield.fieldname} <i class="icon-arrow-right"></i> 
                                                    {if $customfield.fieldtype == 'text'}
                                                        {if $customfield.value}{$customfield.value}{else}{$lang.empty}{/if}
                                                    {elseif $customfield.fieldtype == 'tickbox'}
                                                        {if $customfield.check}{$lang.checked}{else}{$lang.unchecked}{/if}
                                                    {else}
                                                        {$customfield.value}
                                                    {/if}
                                                </li>
                                            {/foreach}
                                        </ul>
                                   </div>
                                {/if}
                                <div class="span2 pull-right"  style="overflow: hidden;">
                                    <ul class="unstyled">
                                        <li><strong>{$lang.server}</strong></li>
                                        {foreach from=$rule.servers item=server}
                                            <li style="max-width:150px;">
                                                <a href="configservers.php?action=manage&id={$server.serverid}">{$server.name}</a>
                                            </li>
                                        {/foreach}
                                   </ul>
                                </div>
                            </div>
                        </td>
                        <td colspan="1" align="right">
                            <a class="btn btn-info" href="addonmodules.php?module=serverswitcher&modpage=links&id={$rule.id}">{$lang.links}</a>
                            <a class="editrule btn btn-warning secondbutton" href="#" >{$lang.editrule}</a>
                            <a class="delrule btn btn-danger thirdbutton" href="#" >{$lang.removerule}</a>
                        </td>
                    </tr>
                    {foreachelse}
                        <tr>
                            <td colspan="2"><p style="text-align: center;">{$lang.norules}.</p></td>
                        </tr>
                    {/foreach}
                {foreachelse}
                    <tr id="errrow">
                        <td colspan="2"><p style="text-align: center;">{$lang.nogroups}</p></td>
                    </tr>
                {/foreach}
        </tbody>            
    </table>


<script type="text/javascript" src="../modules/addons/serverswitcher/core/assets/js/config.js" ></script>