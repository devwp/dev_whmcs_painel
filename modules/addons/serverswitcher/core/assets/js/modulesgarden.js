
jQuery(function()
{
    jQuery('section [href^=#]').click(function (e) {
      e.preventDefault()
    });
    
    jQuery(".btn-delete").click(function(event)
    {
        return confirm("Are you sure you want to procced?");
    });
    
    jQuery('.tooltip-box').tooltip({
      selector: "a[rel=tooltip]",
      delay: { show: 0, hide: 0 } 
    });
     
    //We don't want jQuery buttons!
    var options = {
        buttons: {}
    };
    jQuery('.body button').button("destroy");
    
});

//Datatables
jQuery(function(){
    $("table[data-tables='1']").each(function(){
        var settings = new Object();
        
        
        settings.sDom = '<"clear">';
        //SEARCH
        if($(this).attr('data-search'))
        {
            settings.sDom += '<"table-search form-horizontal"f><"clear">';
        }
        
        settings.sDom += 't';
        
        //PAGINATION
        if($(this).attr("data-pagination"))
        {
            settings.sPaginationType = 'full_numbers';
            settings.sDom += '<"pagination pagination-right"p>';
        }
        
        //AJAX SOURCE
        if($(this).attr("data-source"))
        {
            settings.sAjaxSource = $(this).attr("data-source")+"&ajax=1";
        }
        
        //SERVER SIDE
        if($(this).attr("data-server"))
        {
            settings.bServerSide  = 1;
        }
        
        //Run it!
        $(this).dataTable(settings);
        
        //Remove last and first buttons 
        $('.pagination .dataTables_paginate').find('a.first, a.last').remove();
    });
});