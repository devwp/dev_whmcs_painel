jQuery(function() {

    // choose ruletype

    jQuery(".inner").on("click", "#selcteservertype", function(e) {
        e.preventDefault();

        var servertype = jQuery("#servertype").val();


        if (servertype !== '--') {
            window.location = window.location.href + "&servertype=" + servertype;
        } else {
            showAlert("Please select server type", "error", 3000);
        }
    });

    // rule configuration

    jQuery(".inner").on("click", ".addco", function(e) {
        e.preventDefault();

        addDefinition(this);
    });

    jQuery(".inner").on("click", ".addadd", function(e) {
        e.preventDefault();

        addDefinition(this);
    });

    jQuery(".inner").on("click", ".addcf", function(e) {
        e.preventDefault();

        addDefinition(this);
    });

    jQuery("#ruledeftable").on("click", ".cancel", function(e) {
        e.preventDefault();

        var row = jQuery(this).parents('tr');

        if (jQuery("#ruledeftable tbody tr").length !== 1) {
            row.remove();
        } else {
            row.find("#coselect").hide();
            row.find("#addselect").hide();
            row.find("#cfselect").hide();
            row.find(".defbody").html("");
            jQuery(this).hide();
        }
    });

    function addDefinition(btn) {

        var row = jQuery(btn).parents("tr");
        var id = (jQuery(btn).attr("class")).substr(3);

        jQuery(row.find("td:first select")).each(function() {
            if (jQuery(this).attr("id") !== id + "select") {
                jQuery(this).hide();
            }
        });

        row.find(".defbody").html("");

        jQuery(row.find("#coselect")).prop('selectedIndex', 0);
        jQuery(row.find("#cfselect")).prop('selectedIndex', 0);
        jQuery(row.find("#addselect")).prop('selectedIndex', 0);

        if (jQuery(row.next("tr")).length == 0) {
            var newrow = row.clone();
            jQuery(newrow).insertAfter(row);
        }

        jQuery(btn).parents("div.btn-group").find("a.dropdown-toggle").html(lang['changedefinition'] + ' <span class="caret"></span>');
        jQuery(btn).parents("div.btn-group").find("a.dropdown-toggle").switchClass("btn-success", "btn-info", 0).removeClass("adddef");
        row.find("#" + id + "select").show();
        row.find(".cancel").show();
    }

    // on Configurable Option Select
    jQuery(".inner").on("change", "#coselect", function() {

        var id = jQuery(this).val();

        var td = jQuery(this).parents('td.select-col');

        td.next("td.defbody").html("");

        if (id != '-') {

            jQuery.post("addonmodules.php?module=serverswitcher&modpage=ssconfig&modsubpage=getcoparamsAjax", {
                "ajax": 1,
                "id": id
            }, function(data) {
                if (typeof (data) === 'object') {

                    var row = '';

                    switch (data.optiontype) {
                        case "1" :
                            // Dropdown
                            row += '<select name="rule[configoption][' + data.id + ']">';
                            jQuery.each(data.sub, function(k, sub) {
                                row += '<option value="' + sub.id + '">' + sub.optionname + '</option>';
                            });
                            row += '</select>';
                            break;
                        case "2" :
                            // Radio
                            jQuery.each(data.sub, function(k, sub) {
                                row += '<label><input type="radio" value="' + sub.id + '" name="rule[configoption][' + data.id + ']">' + sub.optionname + '</label>';
                            });
                            break;
                        case "3" :
                            // Yes/No
                            row += '<label><input type="hidden" value="1" name="rule[configoption][' + data.id + '][]"><input type="checkbox" value="1" name="rule[configoption][' + data.id + '][check]">' + data.sub[0].optionname + '</label>';
                            break;
                        case "4" :
                            // Quantity
                            console.log(data);
                            row += '<span>Min value <input name="rule[configoption][' + data.id + '][minval]" id="minval" style="width: 160px;" type="text" placeholder="' + data.qtyminimum + '" />';
                            row += '<input name="" id="qtyminimum" type="hidden" value="' + data.qtyminimum + '" /></span>';
                            row += '<span style="display: inline-block;">Max value <input name="rule[configoption][' + data.id + '][maxval]" id="maxval" style="width: 160px;" type="text" placeholder="' + data.qtymaximum + '" /> (Leave blank for no range value)';
                            row += '<input name="" id="qtymaximum" type="hidden" value="' + data.qtymaximum + '" /></span>';
                            break;
                        default:
                            break;
                    }

                    td.next("td").append(row);
                }
            });
        }
    });


    // on Addon Select
    jQuery(".inner").on("change", "#addselect", function() {

        var id = jQuery(this).val();
        var td = jQuery(this).parents('td.select-col');

        td.next("td").html("");

        if (id != '-') {
            jQuery.post("addonmodules.php?module=serverswitcher&modpage=ssconfig&modsubpage=getaddparamsAjax", {
                "ajax": 1,
                "id": id
            }, function(data) {
                if (typeof (data) === 'object') {

                    var row = '';
                    row += '<input type="hidden" name="rule[addon][' + data.id + ']" value="' + data.id + '" />';
                    row += '<span style="word-wrap: break-word;">Addon Description: ' + data.description + '</span>';
                    td.next("td").append(row);
                }
            });
        }
    });


    // on Custom Field Select
    jQuery(".inner").on("change", "#cfselect", function() {

        var id = jQuery(this).val();
        var td = jQuery(this).parents('td.select-col');

        td.next("td").html("");

        if (id != '-') {
            jQuery.post("addonmodules.php?module=serverswitcher&modpage=ssconfig&modsubpage=getcfparamsAjax", {
                "ajax": 1,
                "id": id
            }, function(data) {
                if (typeof (data) === 'object') {
                    var row = '';
                    switch (data.fieldtype) {
                        case "dropdown" :
                            // Dropdown
                            row += '<select name="rule[customfield][' + data.id + ']">';
                            arr = (data.fieldoptions).split(",");
                            jQuery.each(arr, function(k, v) {
                                row += '<option value="' + v + '">' + v + '</option>';
                            });
                            row += '</select>';
                            break;
                        case "tickbox" :
                            // Tickbox
                            row += '<label><input type="hidden" value="1" name="rule[customfield][' + data.id + '][' + data.id + ']"><input type="checkbox" value="1" name="rule[customfield][' + data.id + '][check]">' + data.description + '</label>';
                            break;
                        case "text" :
                            // text | Password
                            row += '<input name="rule[customfield][' + data.id + ']"  style="width: 160px;" type="text" /> ' + data.description;
                            break;
                        default:
                            break;
                    }
                    td.next("td").append(row);
                }
            });
        }
    });


    jQuery(".inner").on("click", "#saverulebtn", function(e) {
        e.preventDefault();

        var field = false;
        var valerror = false;

        jQuery("#ruledeftable tbody tr").each(function() {
            if (jQuery(this).find(".defbody").html() !== "") {
                if (jQuery(this).find(".defbody #minval").length) {

                    var minval = jQuery(this).find(".defbody #minval").val();
                    var qtyminimum = jQuery(this).find(".defbody #qtyminimum").val();
                    var maxval = jQuery(this).find(".defbody #maxval").val();
                    var qtymaximum = jQuery(this).find(".defbody #qtymaximum").val();

                    if (!isNumValid(minval) || !isNumValid(maxval)) {
                        showAlert("Please provide valid values for Maximum and Minimum fields", "error", 5000);
                        valerror = true;
                        return;
                    }

                    if (minval === "" && maxval === "") {
                        showAlert("Please fill in at least one of the fields", "error", 5000);
                        valerror = true;
                        return;
                    }

                    if (maxval !== "" && parseInt(minval) > parseInt(maxval)) {
                        showAlert("Maximum value must be greater or equal than minimum value", "error", 5000);
                        valerror = true;
                        return;
                    }

                    if (minval !== "" && parseInt(qtyminimum) !== 0 && parseInt(minval) < parseInt(qtyminimum)) {
                        showAlert("Minimum value cannot be less than " + qtyminimum, "error", 5000);
                        valerror = true;
                        return;
                    }

                    if (maxval !== "" && parseInt(qtymaximum) !== 0 && parseInt(maxval) > parseInt(qtymaximum)) {
                        showAlert("Maximum value cannot be greater than " + qtymaximum, "error", 5000);
                        valerror = true;
                        return;
                    }
                }

                field = true;
            }
        });

        if (valerror) {
            return;
        }

        if (!field) {
            showAlert("Please add at least one definition!", "error", 3000);
            return;
        }
        
        if (!jQuery("#servers").val()) {
            showAlert("Please select server!", "error", 3000);
            return;
        }


        var formData =  jQuery("form#ruledefform").serialize();
        var href = window.location.href;
        
        href = href.substr(0, href.indexOf("modsubpage")) + href.substr(href.indexOf("modsubpage")+19) + "&modsubpage=checkExistingAjax";

        jQuery.post(href, formData, 
        function(data) {
            if(data.status === 'success') {
                jQuery("#ruledeftable tbody tr").each(function() {
                    if (jQuery(this).find(".defbody").html() === "") {
                        jQuery(this).remove();
                    }
                });
                
                jQuery("form#ruledefform").submit();
            } else {
                showAlert("This rule already exist!", "error", 3000);
                return;
            }
        }, "json");
    });


    function showAlert(msg, type, duration) {

        var divalert = jQuery("div.alert");

        if(divalert.length) {
            divalert.remove();
        }

        divalert = '<div class="alert alert-' + type + '" style="margin-top: 20px;"><button type="button" class="close" data-dismiss="alert">&times;</button>' + msg + '</div>';

        jQuery(divalert).insertAfter("div.inner h2:first").delay(duration).queue(function(next) {
            jQuery(this).remove();
            next();
        });
    }


    function isNumValid(val) {
        if (val === "") {
            return true;
        }

        if (val.match(/^[0-9]+$/))
            return true;

        return false;
    }
});
