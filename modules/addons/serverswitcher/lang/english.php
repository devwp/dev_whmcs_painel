<?php
$LANG['validation_required']	= 'Field % is required';
$LANG['validation_date']	= 'Incorrect date in %';
$LANG['validation_datetime']	= 'Incorrect date in %';
$LANG['validation_is_int']	= 'Field % must be integer value';
$LANG['test']			=   'Lang test!';

// configuration main page
$LANG['switcherrules']		= 'Switcher Rules';
$LANG['switcherruleshint']	= 'Here you are able to configure all your switching rules';
$LANG['namelabel']		= 'Name';
$LANG['productlabel']		= 'Product Group/s';
$LANG['dserverslabel']		= 'Default Servers';
$LANG['savelabel']		= 'Save';
$LANG['rulegrouplabel']		= 'Select Rule Group';
$LANG['chooserulegrup']		= 'Choose Rule Group';
$LANG['addgrouplabel']		= 'Add Group';
$LANG['actionslabel']		= 'Actions';
$LANG['productgroups']		= 'Product Groups';
$LANG['defaultservers']		= 'Default Server';
$LANG['addrule']		= 'Add Rule';
$LANG['removegroup']		= 'Remove Group';

$LANG['configurableoptions']	= 'Configurable Options';
$LANG['from']			= 'From';
$LANG['to']			= 'to';
$LANG['less']			= 'Less or equal';
$LANG['more']			= 'More or equal';
$LANG['checked']		= 'Checked';
$LANG['unchecked']		= 'Unchecked';

$LANG['addons']			= 'Addons';

$LANG['customfields']		= 'Custom Fields';
$LANG['empty']			= 'Empty';

$LANG['server']			= 'Server';

$LANG['links']			= 'Links';
$LANG['editrule']		= 'Edit Rule';
$LANG['removerule']		= 'Remove Rule';


$LANG['norules']		= 'No Rules In This Group';
$LANG['nogroups']		= 'No Rules Found';

// add/edit page
$LANG['addrule']		= 'Add Rule';
$LANG['editrule']		= 'Edit Rule';

$LANG['choseservertype']	= 'Please Choose Server Type';
$LANG['selecttype']		= 'Select Type';
$LANG['next']			= 'Next';
$LANG['ruledefenition']		= 'Rule Definition';
$LANG['selectconfigurableoption']= 'Please Select Configurable Option';
$LANG['selectaddon']		= 'Please Select Addon';
$LANG['selectcustomfield']	= 'Please Select Custom Field';
$LANG['min']			= 'Min value';
$LANG['max']			= 'Max value';
$LANG['leaveblank']		= 'Leave blank for no range value';
$LANG['remove']			= 'Remove';
$LANG['configurableoption']	= 'Configurable Option';
$LANG['addon']			= 'Addon';
$LANG['customfield']		= 'Custom Field';

$LANG['changedefinition']	= 'Edit';


$LANG['adddefinition']		= 'Add Definition';

$LANG['cancel']			= 'Cancel';

$LANG['serverslabel']		= 'Server/s';
$LANG['savechanges']		= 'Save Changes';
$LANG['addondescription']	= 'Addon Description';

// Links Page

$LANG['linkshint']		= 'Here you are able to get links to your products with configured Configurable Options/Product Addons/Custom Fields.';
$LANG['back']			= 'Back';