<?php
# AWIT IPPM - Invoice Post Processing Module
# Copyright (c) 2013, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.



# Make sure we not being accssed directly
if (!defined("WHMCS"))
	die("This file cannot be accessed directly");


# Addon configuration
function awit_ippm_config() {

	# Configuration
	$configarray = array(
			"name" => "AWIT IPPM",
			"description" => "This is an invoice post processing module including some utilities such as invoice item description and sales person / commission recording.",
			"version" => "0.4",
			"author" => "AllWorldIT",
			"language" => "english",
			"fields" => array(
				# Invoice include days
				"invoice_include_days" => array (
						"FriendlyName" => "Invoice Include Days",
						"Description" => "If invoice is made out between DueDate - InvoiceIncludeDays, the service period will be billed if the invoice is generated between that and the due date.",
						"Type" => "text", "Size" => "5",
						"Default" => "5"
				),
				# Sales Person
				"sales_person" => array (
						"FriendlyName" => "Sales Person Custom Field",
						"Description" => "Sales person custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Sales person"
				),
				# Sales Commission
				"sales_commission" => array (
						"FriendlyName" => "Sales Commission Custom Field",
						"Description" => "Sales commission custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Sales commission"
				),
				# Sales Commission Structure
				"sales_commission_s" => array (
						"FriendlyName" => "Sales Commission Structure",
						"Description" => "Sales commission structure (eg. A=50,B=30,C=20,D=10)",
						"Type" => "text", "Size" => "80",
						"Default" => "A=50,B=30,C=20,D=15,E=10,F=7.5,G=5,H=2.5"
				),
				# Billing Type
				"billing_type" => array (
						"FriendlyName" => "Billing Type Custom Field",
						"Description" => "Billing type custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Billing type"
				),
				# Billing Start
				"billing_start" => array (
						"FriendlyName" => "Billing Start Custom Field",
						"Description" => "Billing start custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Billing start"
				),
				# Billing End
				"billing_end" => array (
						"FriendlyName" => "Billing End Custom Field",
						"Description" => "Billing end custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Billing end"
				),
				# Variable Quantity
				"variable_quantity" => array (
						"FriendlyName" => "Variable Quantity Custom Field",
						"Description" => "Variable quantity custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Variable quantity"
				),
				# Variable Amount
				"variable_amount" => array (
						"FriendlyName" => "Variable Amount Custom Field",
						"Description" => "Variable amount custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Variable amount"
				),
				# Variable Identifier
				"variable_identifier" => array (
						"FriendlyName" => "Variable Identifier Custom Field",
						"Description" => "Variable identifier custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Variable identifier"
				),
				# Variable Attributes
				"variable_attributes" => array (
						"FriendlyName" => "Variable Attributes Custom Field",
						"Description" => "Variable Attributes custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Variable attributes"
				),
				# Contract Start
				"contract_start" => array (
						"FriendlyName" => "Contract Start Custom Field",
						"Description" => "Contract start custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Contract start"
				),
				# Contract Term
				"contract_term" => array (
						"FriendlyName" => "Contract Term Custom Field",
						"Description" => "Contract term custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Contract term"
				),
				# Commitment Value
				"commitment_value" => array (
						"FriendlyName" => "Commitment Value Custom Field",
						"Description" => "Commitment Value custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Commitment value"
				),
				# Commitment Calculation
				"commitment_calculation" => array (
						"FriendlyName" => "Commitment Calculation Custom Field",
						"Description" => "Commitment Calculation custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Commitment calculation"
				),
				# Bulk Discount Calculation
				"bulk_discount_calculation" => array (
						"FriendlyName" => "Bulk Discount Calculation Custom Field",
						"Description" => "Bulk Discount Calculation custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Bulk discount calculation"
				),
			)
	);

	return $configarray;
}

# Addon activation
function awit_ippm_activate() {

	# Create Custom DB Table
	$result = mysql_query("
		CREATE TABLE `mod_awit_ippm` (
			`id` INT( 1 ) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`timestamp` DATETIME NOT NULL,
			`invoice_id` VARCHAR(10) NOT NULL,
			`sales_person` VARCHAR(30) NOT NULL,
			`sales_commission` DECIMAL(8,2) NOT NULL,
			`sales_commission_s` VARCHAR(80) NOT NULL,
			`paid` SMALLINT(1) NOT NULL DEFAULT 0
		)
	");

	# Return Result
	if (!$result) {
		return array("status" => "error", "description"=>"There was a problem activating the module.");
	} else {
		return array("status" => "success","description" =>"Open module configuration for configuration options.");
	}

}


# Deactivate the addon
function awit_ippm_deactivate() {

	# Remove Custom DB Table
	$result = mysql_query("
		DROP TABLE `mod_awit_ippm`
	");

	# Return Result
	if (!$result) {
		return array("status"=>"error","description"=>"There was an error deactivating the module.");
	} else {
		return array("status"=>"success","description"=>"Module has been deactivated.");
	}

}


# Upgrade the addon
function awit_ippm_upgrade($vars) {

	$version = $vars['version'];

	# Run SQL Updates for V1.0 to V1.1
	#if ($version < 1.3) {
	#}

}

# Addon output
function awit_ippm_output($vars) {

	# Check if we have to mark items as paid
	if (isset($_POST['paid']) && is_array($_POST['paid'])) {

		# Query actions
		$table = "mod_awit_ippm";
		$update = array("paid"=>"1");
		$where = array();
		foreach ($_POST['paid'] as $id) {
			$where["id"] = $id;
		}

		# Do query
		update_query($table,$update,$where);
	}

	# Make link to use
	$link = $vars['modulelink'];

	# Default dates
	$dateFrom = new DateTime(date("Y-m"));
	$dateTo = clone $dateFrom;
	$dateTo->add(new DateInterval("P1M"));

	# Dates, formatted
	$dateFrom = $dateFrom->format("Y-m-d");
	$dateTo = $dateTo->format("Y-m-d");

	# Check if we have dates from and to to override defaults
	if (isset($_POST['date_from']) && isset($_POST['date_to'])) {

		# Try and convert to date time
		$postDateFrom = new DateTime($_POST['date_from']);
		$postDateTo = new DateTime($_POST['date_to']);

		# If successful, use them
		if (is_object($postDateFrom)) {
			$dateFrom = $postDateFrom->format("Y-m-d");
		}
		if (is_object($postDateTo)) {
			$dateTo = $postDateTo->format("Y-m-d");
		}
	}

	# Fancy date picker
	echo '<script>
		$(function() {
			$( "#date_from" ).datepicker({
				dateFormat: "yy-mm-dd",
				constrainInput: true
			});
			$( "#date_to" ).datepicker({
				dateFormat: "yy-mm-dd",
				constrainInput: true
			});
		});
	      </script>';

	# Date search fields
	echo "<p>Select a start and end date and hit search.</p>";
	echo "<form action='$link' method='post'>";
	echo "<input id='date_from' type='text' value='$dateFrom' name='date_from' />";
	echo "<input id='date_to' type='text' value='$dateTo' name='date_to' />";
	echo "<input type='submit' value='Search' />";
	echo "</form>";
	echo "<br /><br />";

	# Query the database
	$result = mysql_query("SELECT * FROM mod_awit_ippm WHERE timestamp >= '$dateFrom' AND timestamp <= '$dateTo'");

	# Loop through results and genenrate form
	$includeForm = 0;
	while ($row = mysql_fetch_array($result)) {

		# Open form
		if (!$includeForm) {
			$includeForm = 1;
			echo "<form action='".$link."' method='post' >";
			echo "<table border='1' cellpadding='10'>";
			echo "<tr>";
			echo "<th>Timestamp</th>";
			echo "<th>Invoice ID</th>";
			echo "<th>Sales Person</th>";
			echo "<th>Commission</th>";
			echo "<th>Status</th>";
			echo "</tr>";
		}

		# Nice pay status
		$payStatus = $row['paid'] ? "Paid" : "Unpaid";

		echo "<tr>";
		echo "<td>".$row['timestamp']."</td>";
		echo "<td>".$row['invoice_id']."</td>";
		echo "<td>".$row['sales_person']."</td>";
		echo "<td>".$row['sales_commission']."</td>";
		if ($payStatus == "Unpaid") {
			echo "<td>".$payStatus."  <input type='checkbox' name='paid[]' value='".$row['id']."' /></td>";
		} else {
			echo "<td>".$payStatus."</td>";
		}
		echo "</tr>";
	}
	unset($result);

	# Close form
	if ($includeForm) {
		echo "</table><br>";
		echo "<input type='submit' value='Mark as paid' />";
		echo "</form>";
	} else {
		echo "<p>No logs yet for selected period..</p>";
	}
}

# Addon side bar
#function awit_ippm_sidebar($vars) {
#
#	$modulelink = $vars['modulelink'];
#	$version = $vars['version'];
#	$option1 = $vars['option1'];
#	$option2 = $vars['option2'];
#	$LANG = $vars['_lang'];
#
#	$sidebar = '<span class="header"><img src="images/icons/addonmodules.png" class="absmiddle" width="16" height="16" /> awit_ippm</span>
#		<ul class="menu">
#		<li>'.$version.'</li>
#		</ul>';
#	return $sidebar;
#
#}

?>


