<?php


/**
 * @author Maciej Husak <maciej@modulesgarden.com>
 */
class NewOnApp_VM extends NewOnApp_Connection {

    protected $_id = null;
    protected $_api = null;

    public function __construct($id) {
        $this->_id = $id;
    }

    public function setID($id){
        $this->_id = $id;
    }
    
    public function getAutoScalling(){
        return $this->_api->sendGET('/virtual_machines/' . $this->_id.'/auto_scaling');
    }
    
    public function setAutoScalling($params){
         return $this->_api->sendPOST('/virtual_machines/' .$this->_id . '/auto_scaling' , $params);
    }
    
    public function deleteAutoScalling(){
         return $this->_api->sendDELETE('/virtual_machines/' .$this->_id . '/auto_scaling');
    }

    public function getDetails() {
        $result  = $this->_api->sendGET('/virtual_machines/' . $this->_id);
        $modifed = array();
        foreach($result as $key=>$value){
            foreach($value as $k =>$val){
                if($k=='created_at' || $k=='updated_at'){
                    $result['virtual_machine'][$k] = date('Y-m-d H:i:s',  strtotime($val));
                }
            }
        }

        return $result;
    }

    public function getLists() {
        return $this->_api->sendGET('/virtual_machines');
    }

    public function create($params) {
        return $this->_api->sendPOST('/virtual_machines', $params);
    }

    public function viewPassword() {
        return $this->_api->sendGET('/virtual_machines/' . $this->_id . '/with_decrypted_password', array(), array('initial_root_password_encryption_key' => 'encryptionkey'));
    }

    public function rebuild($params) {
        return $this->_api->sendPOST('/virtual_machines/' . $this->_id . '/build', $params);
    }

    public function modify($params) {
        return $this->_api->sendPUT('/virtual_machines/' . $this->_id, $params);
    }

    public function changeOwner($params) {
        return $this->_api->sendPOST('/virtual_machines/' . $this->_id . '/change_owner', $params);
    }

    public function changePassword($params) {
        return $this->_api->sendPOST('/virtual_machines/' . $this->_id . '/reset_password', $params);
    }

    public function setSSH() {
        return $this->_api->sendPOST('/virtual_machines/' . $this->_id . '/set_ssh_keys');
    }

    public function migrate($params) {
        return $this->_api->sendPOST('/virtual_machines/' . $this->_id . '/migrate', $params);
    }

    public function setVip() {
        return $this->_api->sendPOST('/virtual_machines/' . $this->_id . '/set_vip');
    }

    public function delete() {
        return $this->_api->sendDELETE('/virtual_machines/' . $this->_id);
    }

    public function resize($params) {
        return $this->_api->sendPOST('/virtual_machines/' . $this->_id . '/resize', $params);
    }

    public function start() {
        return $this->_api->sendPOST('/virtual_machines/' . $this->_id . '/startup');
    }

    public function segregate() {
        return $this->_api->sendPOST('/virtual_machines/' . $this->_id . '/strict_vm');
    }

    public function reboot() {
        return $this->_api->sendPOST('/virtual_machines/' . $this->_id . '/reboot');
    }

    public function recovery() {
        return $this->_api->sendPOST('/virtual_machines/' . $this->_id . '/reboot', array(), array('mode' => 'recovery'));
    }

    public function suspend() {
       $status  = $this->getDetails();
       if($status['virtual_machine']['suspended']==1)
            return true;
       return $this->_api->sendPOST('/virtual_machines/' . $this->_id . '/suspend');
    }

    public function unlock() {
        return $this->_api->sendPOST('/virtual_machines/' . $this->_id . '/unlock');
    }

    public function unsuspend() {
       $status  = $this->getDetails();
       if($status['virtual_machine']['suspended']==null)
            return true;
        return $this->_api->sendPOST('/virtual_machines/' . $this->_id . '/suspend');
    }

    public function shutdown() {
        return $this->_api->sendPOST('/virtual_machines/' . $this->_id . '/shutdown');
    }

    public function stop() {
        return $this->_api->sendPOST('/virtual_machines/' . $this->_id . '/stop');
    }

    public function getConsoleKey() {
        return $this->_api->sendGET('/virtual_machines/' . $this->_id . '/console');
    }

    public function getStats($params = array()) 
    {
        return $this->_api->sendGET('/virtual_machines/' . $this->_id . '/vm_stats', $params);
    }

    public function search($params) {
        return $this->_api->sendGET('/virtual_machines', array(), $params);
    }

    public function getUserVMs($user_id){
        return $this->_api->sendGET('/users/' . $user_id . '/virtual_machines');
    }
    
    public function getDisks(){
        return $this->_api->sendGET('/virtual_machines/' . $this->_id . '/disks');
    } 
    
    public function getDiskBackups($disk_id){
        return $this->_api->sendGET('/virtual_machines/' . $this->_id  . '/disks/'.$disk_id.'/backups');
    } 
    
    public function getUsageCPU() {
        return $this->_api->sendGET('/virtual_machines/' . $this->_id . '/cpu_usage');
    }
    
    public function searchLog($key){
        return $this->_api->sendGET('/logs',array('q'=>$key));
    }
    
    public function getUsageChart(){
        $this->_api->_unsetJSON();
        $result = $this->_api->sendGET('/virtual_machines/' . $this->_id . '/auto_scaling.chart');
        $this->_api->_setJSON();
        return $result;
    }

    public function getUsageCPUChart() {
        $this->_api->_unsetJSON();
        return $this->_api->sendGET('/virtual_machines/' . $this->_id . '/cpu_usage.chart');
    }
    
    public function getNetworkInterfaces() {
        return $this->_api->sendGET('/virtual_machines/' . $this->_id . '/network_interfaces');
    }

    public function getNetworkInterface($id) {
        $id = (int)$id;
        return $this->_api->sendGET('/virtual_machines/' . $this->_id . '/network_interfaces/'.$id);
    }
    public function available($user_id){
        $result = $this->getDetails();
        if($this->isSuccess() && $result['virtual_machine']['user_id']==$user_id){
            $this->details = $result;
            return true;
        } else return false;
    }
    
    public function getTransactions($page=false){
        $limit = 20;
        $data = $this->_api->sendGET('/virtual_machines/' . $this->_id . '/transactions');
        $new  = array();
        $i    = 0;
        foreach($data as $k=> $value){
            foreach($value['transaction'] as $key=>$val){
                if($key=='created_at'){
                    
                    $format = date('Y-m-d H:i:s',  strtotime($val));
                }
            }
            $new[$k] = $value;
            $new[$k]['transaction']['created_at']  = $format;
            $i++;
        }

        $list  = array();
        $backups_ids = array();
        $disks = $this->getDisks();
        if($this->isSuccess()){
            foreach ($disks as $val){
                $backups = $this->getDiskBackups($val['disk']['id']);
                if($this->isSuccess()){
                    foreach($backups as $b){
                        $backups_ids[] = $b['backup']['identifier'];
                    }
                }

            }
        }

        foreach($backups_ids as $val){
            $logs = $this->searchLog($val);
            if($this->isSuccess()){
              foreach($logs as $l){
                  $list[$i]['transaction']['action']     = $l['log_item']['action'];
                  $list[$i]['transaction']['created_at'] = $format = date('Y-m-d H:i:s',  strtotime($l['log_item']['created_at']));
                  $list[$i]['transaction']['status']     = strtolower($l['log_item']['status']);
                  $i++;
              }  
            }

        }
               
        $data = array_merge($new,$list);
        usort($data, cmp);
        if($page===false)
            return array ('pages'=>null,'data'=>$data);
        else {
           return array('pages'=>ceil(count($data)/$limit),'data'=>array_slice($data, $page*$limit,$limit));
            
        }
        
        
    }
    

    
    public function assignIP($cn_ip,$network_id,$hypervisor_id = null) {
            $interfaces = $this->getNetworkInterfaces();
            $interface  = $interfaces[0]['network_interface']['id'];
            if (empty($interface))
                return false;
            
            if(!$network_id){
                $hypervisor = new NewOnApp_Hypervisor($hypervisor_id);
                $hypervisor ->setapi($this->_api);
                $joins      = $hypervisor->networkJoins();
                foreach($joins as $key=>$value){
                    if($value['network_join']['id'] == $interfaces[0]['network_interface']['network_join_id']){
                        $network_id = $value['network_join']['network_id'];
                        break;
                    }
                }
            }
            
            $ip_address = new NewOnApp_IPAddress($network_id);
            $ip_address ->setapi($this->_api);
            $ip_pools   = $ip_address->getList();
            $ip_join    = new NewOnApp_IPAddressJoin($this->_id);
            $ip_join    ->setapi($this->_api);
            $ip_joins   = $ip_join->getList();
            $i          = count($ip_joins);
            if ($i == $cn_ip)
                return true;
            else {
                if($i >= $cn_ip){
                    $rem = 1;
                    foreach($ip_joins as $key =>$value){
                      if($rem>$cn_ip){
                           $ip_join->delete($value['ip_address_join']['id']);
                           if(!$ip_join->isSuccess())
                               return $ip_join->error();
                      }
                      $rem++;
                    }
                    return;
            }

            if(isset($ip_pools) && is_array($ip_pools)){
                for($j=1; $j<=$cn_ip; $j++){
                    foreach ($ip_pools as $key => $value) {
                            if ($i == $cn_ip)
                                return;

                            if($i <= $cn_ip){
                                if (isset($value['ip_address']['free']) && $value['ip_address']['free'] && empty($value['ip_address']['user_id'])) {
                                    $data = array('ip_address_join' => array('ip_address_id' => $value['ip_address']['id'], 'network_interface_id' => $interface));
                                    $ip_join->assign($data);
                                    if($ip_join->isSuccess()){
                                        $i++;
                                    } else {
                                        return $ip_join->error();
                                    }
                                }
                                else
                                    continue;
                            } 
                    }
                }
            }    
            return true;
        }
    }
}

    function cmp($a,$b)
    {
        if(strtotime($a['transaction']['created_at']) < strtotime($b['transaction']['created_at']))
            return 1;
        else if(strtotime($a['transaction']['created_at']) == strtotime($b['transaction']['created_at']))
            return 0;
        else
            return -1;
    }
