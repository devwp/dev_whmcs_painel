<?php

global $errormessage,$cfnCPF,$validar,$permitirCPF,$permitirCNPJ;

$cfnCPF							=	'CNPJ ou CPF'	; // Preencha com o nome do CUSTOM FIELD referente ao CPF/CNPJ
$validar 						=	TRUE			; // Preencha com false se n�o deseja efetuar valida��o de CPF/CNPJ
$permitirCPF 					=	TRUE			; // Preencha com false se n�o deseja permitir CPF
$permitirCNPJ 					=	TRUE			; // Preencha com false se n�o deseja permitir CNPJ

if ($validar)		add_hook("ClientDetailsValidation",1,"validaCampo","");
//if ($permitirTroca)	add_hook("ClientEdit",1,"validaTroca","");

function validaCampo($vars) {
	$erro = array();
	global $cfnCPF,$permitirCPF,$permitirCNPJ;
	
	$data = mysql_fetch_array(select_query("tblcustomfields","id",array("type"=>"client","fieldname"=>$cfnCPF)));
	$cfnID = $data["id"];
	$value = $_POST["customfield"][$cfnID];
	if (!Empty($value)) {
		$value = SoNumeros($value);
		if ((strlen($value) == 11) && ($permitirCPF)) {
			if (!ValidaCPF($value)) $erro[] = "O CPF informado &eacute; inv&aacute;lido";
			else $_POST["customfield"][$cfnID] = ValidaCPF($value);
		}
		else if ((strlen($value) == 14) && ($permitirCNPJ)) {
			if (!ValidaCNPJ($value)) $erro[] = "O CNPJ informado &eacute; inv&aacute;lido";
			else $_POST["customfield"][$cfnID] = ValidaCNPJ($value);
		}
		else $erro[] = "O valor informado no campo ".$cfnCPF." &eacute; inv&aacute;lido";
	}
	if (sizeof($erro)>0) return $erro;
	else return;
}

function SoNumeros($num) {
	return ereg_replace('[^0-9]', '', $num);
}

function ValidaCPF($cpf) {
	$cpf = str_pad(ereg_replace('[^0-9]', '', $cpf), 11, '0', STR_PAD_LEFT);
	if (
		strlen($cpf) != 11 
		|| $cpf == '01234567890'
		|| $cpf == '00000000000'
		|| $cpf == '11111111111'
		|| $cpf == '22222222222'
		|| $cpf == '33333333333'
		|| $cpf == '44444444444'
		|| $cpf == '55555555555'
		|| $cpf == '66666666666'
		|| $cpf == '77777777777'
		|| $cpf == '88888888888'
		|| $cpf == '99999999999'
		) return false;
	for ($t = 9; $t < 11; $t++) {
		for ($d = 0, $c = 0; $c < $t; $c++) $d += $cpf{$c} * (($t + 1) - $c);$d = ((10 * $d) % 11) % 10;
		if ($cpf{$c} != $d) return false;
	}
	return substr($cpf,0,3).".".substr($cpf,3,3).".".substr($cpf,6,3)."-".substr($cpf,9,2);
}

function ValidaCNPJ($cnpj) {
	$cnpj = str_pad(ereg_replace('[^0-9]', '', $cnpj), 11, '0', STR_PAD_LEFT);
	if (
		strlen($cnpj) != 14 
		|| $cnpj == '00000000000000'
		|| $cnpj == '11111111111111'
		|| $cnpj == '22222222222222'
		|| $cnpj == '33333333333333'
		|| $cnpj == '44444444444444'
		|| $cnpj == '55555555555555'
		|| $cnpj == '66666666666666'
		|| $cnpj == '77777777777777'
		|| $cnpj == '88888888888888'
		|| $cnpj == '99999999999999'
		) return false;
	$soma = ($cnpj[0]*5)+($cnpj[1] * 4)+($cnpj[2]*3)+($cnpj[3]*2)+($cnpj[4]*9)+($cnpj[5]*8)+($cnpj[6]*7)+($cnpj[7]*6)+($cnpj[8]*5)+($cnpj[9]*4)+($cnpj[10]*3)+($cnpj[11]*2);
	$d1 = ($soma % 11) < 2 ? 0 : 11 - ($soma % 11);
	$soma = ($cnpj[0]*6)+($cnpj[1]*5)+($cnpj[2]*4)+($cnpj[3]*3)+($cnpj[4]*2)+($cnpj[5]*9)+($cnpj[6]*8)+($cnpj[7]*7)+($cnpj[8]*6)+($cnpj[9]*5)+($cnpj[10]*4)+($cnpj[11]*3)+($cnpj[12]*2);
	$d2 = ($soma % 11) < 2 ? 0 : 11 - ($soma % 11);
	if ($cnpj[12] != $d1 || $cnpj[13] != $d2) return false;
	return substr($cnpj,0,2).".".substr($cnpj,2,3).".".substr($cnpj,5,3)."/".substr($cnpj,8,4)."-".substr($cnpj,12,2);
}

?>