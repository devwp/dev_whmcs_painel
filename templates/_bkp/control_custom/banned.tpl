<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">{$LANG.accessdenied}</h3>
			</div>
			<div class="panel-body">
				<div class="alert alert-danger alert-dismissable">
					<p>{$LANG.bannedyourip} {$ip} {$LANG.bannedhasbeenbanned}.</p>
				</div>
				<p>{$LANG.bannedbanreason}: <strong>{$reason}</strong></p>
				<p>{$LANG.bannedbanexpires}: {$expires}</p>
			</div>
		</div>
	</div>
</div>