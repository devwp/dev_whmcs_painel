{if $validDomain}
	{if $searchResults}
		<div class="domain-checker-result-headline">
			{if $searchResults.isRegistered}
				<div class="alert alert-danger">
					<i class="fa fa-2x fa-times"></i> <span class="results-text"><strong>{$searchResults.domainName}</strong> {$LANG.domainunavailable2}</span>
					<span class="pull-right res-center">
						<button type="button" onclick="viewWhois('{$searchResults.domainName}')" class="btn btn-danger btn-sm">WHOIS</button>&nbsp;&nbsp;
						<a href="http://{$searchResults.domainName}" target="_blank" class="btn btn-danger btn-sm">www</a>
					</span>
					<div class="clearfix"></div>
				</div>
			{elseif $searchResults.status == 'reserved'}
				<div class="alert alert-warning domain-result">
					<i class="fa fa-2x fa-exclamation-triangle"></i> <span class="results-text"><strong>{$searchResults.domainName}</strong> {$LANG.domainreserved2}</span>
					{if count($searchResults.pricing) == 1}
						<button type="button" onclick="addToCart2(this, false, 'register', '{$LANG.viewcart}')" class="btn btn-success btn-sm pull-right res-center">
							<span class="fa fa-cart-plus"></span>
							{$LANG.addtocart} ({$searchResults.shortestPeriod.register})
						</button>
					{else}
						<div class="btn-group pull-right res-center">
							<button type="button" class="btn btn-success btn-sm" onclick="addToCart2(this, false, 'register', '{$LANG.viewcart}')">
									<span class="fa fa-cart-plus"></span>
									{$LANG.addtocart} ({$searchResults.shortestPeriod.register})
							</button>
							<button type="button" class="btn btn-success btn-sm dropdown-toggle additional-options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<b class="caret"></b>
									<span class="sr-only">
											{lang key="domainChecker.additionalPricingOptions" domain=$searchResults.domainName}
									</span>
							</button>
							<ul class="dropdown-menu" role="menu">
									{foreach $searchResults.pricing as $years => $price}
											{if $price@iteration eq 1}
													{* Don't output the first as this is default *}
													{continue}
											{/if}
											<li>
													<a onclick="addToCart2(this, false, 'register', '{$LANG.viewcart}', {$years});return false;">
															{$years} {$LANG.orderyears} @ {$price.register}
													</a>
											</li>
									{/foreach}
							</ul>
						</div>
					{/if}
					<div class="clearfix"></div>
				</div>
			{else}
				<div class="alert alert-success domain-result">
					<i class="fa fa-2x fa-check"></i> <span class="results-text"><strong>{$searchResults.domainName}</strong> {$LANG.domainavailable2}</span>
					{if count($searchResults.pricing) == 1}
						<button type="button" onclick="addToCart2(this, false, 'register', '{$LANG.viewcart}')" class="btn btn-success btn-sm pull-right res-center">
							<span class="fa fa-cart-plus"></span>
							{$LANG.addtocart} ({$searchResults.shortestPeriod.register})
						</button>
					{else}
						<div class="btn-group pull-right res-center">
							<button type="button" class="btn btn-success btn-sm" onclick="addToCart2(this, false, 'register', '{$LANG.viewcart}')">
									<span class="fa fa-cart-plus"></span>
									{$LANG.addtocart} ({$searchResults.shortestPeriod.register})
							</button>
							<button type="button" class="btn btn-success btn-sm dropdown-toggle additional-options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<b class="caret"></b>
									<span class="sr-only">
											{lang key="domainChecker.additionalPricingOptions" domain=$searchResults.domainName}
									</span>
							</button>
							<ul class="dropdown-menu" role="menu">
									{foreach $searchResults.pricing as $years => $price}
											{if $price@iteration eq 1}
													{* Don't output the first as this is default *}
													{continue}
											{/if}
											<li>
													<a onclick="addToCart2(this, false, 'register', '{$LANG.viewcart}', {$years});return false;">
															{$years} {$LANG.orderyears} @ {$price.register}
													</a>
											</li>
									{/foreach}
							</ul>
						</div>
					{/if}
					<div class="clearfix"></div>
				</div>
			{/if}
		</div>
	{/if}
	{if $bulkCheckResults}
		<div class="domain-checker-result-headline">{$result.domainName}
			{foreach $bulkCheckResults as $result}
				{if $searchingFor == 'register'}
					{if $result.isAvailable}
						{if $result.status == 'reserved'}
							<div class="alert alert-warning domain-result">
								<i class="fa fa-2x fa-exclamation-triangle"></i> <span class="results-text"><strong>{$result.domainName}</strong> {$LANG.domainreserved2}</span>
						{else}
							<div class="alert alert-success domain-result">
								<i class="fa fa-2x fa-check"></i> <span class="results-text"><strong>{$result.domainName}</strong> {$LANG.domainavailable2}</span>
						{/if}
							{if count($result.pricing) == 1}
								<button type="button" onclick="addToCart2(this, false, 'register', '{$LANG.viewcart}')" class="btn btn-success btn-sm pull-right res-center">
									<span class="fa fa-cart-plus"></span>
									{$LANG.addtocart} ({$result.shortestPeriod.register})
								</button>
							{else}
								<div class="btn-group pull-right res-center">
									<button type="button" class="btn btn-success btn-sm" onclick="addToCart2(this, false, 'register', '{$LANG.viewcart}')">
											<span class="fa fa-cart-plus"></span>
											{$LANG.addtocart} ({$result.shortestPeriod.register})
									</button>
									<button type="button" class="btn btn-success btn-sm dropdown-toggle additional-options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<b class="caret"></b>
											<span class="sr-only">
													{lang key="domainChecker.additionalPricingOptions" domain=$result.domainName}
											</span>
									</button>
									<ul class="dropdown-menu" role="menu">
											{foreach $result.pricing as $years => $price}
													{if $price@iteration eq 1}
															{* Don't output the first as this is default *}
															{continue}
													{/if}
													<li>
															<a onclick="addToCart2(this, false, 'register', '{$LANG.viewcart}', {$years});return false;">
																	{$years} {$LANG.orderyears} @ {$price.register}
															</a>
													</li>
											{/foreach}
									</ul>
								</div>
							{/if}
							<div class="clearfix"></div>
						</div>
					{elseif $result.isRegistered}
						<div class="alert alert-danger">
							<i class="fa fa-2x fa-times"></i> <span class="results-text"><strong>{$result.domainName}</strong> {$LANG.domainunavailable2}</span>
							<span class="pull-right res-center">
								<button type="button" onclick="viewWhois('{$result.domainName}')" class="btn btn-danger btn-sm">WHOIS</button>&nbsp;&nbsp;
								<a href="http://{$result.domainName}" target="_blank" class="btn btn-danger btn-sm">www</a>
							</span>
							<div class="clearfix"></div>
						</div>
					{else}
						<div class="alert alert-default">
							<i class="fa fa-2x fa-question"></i> <span class="results-text"><strong>{$result.domainName}</strong> Status Unknown</span>
						</div>
					{/if}
				{else}
					{if $result.isRegistered}
						<div class="alert alert-success">
							<i class="fa fa-2x fa-check"></i> <span class="results-text"><strong>{$result.domainName}</strong> {$LANG.domaincheckertransferable}</span>
							<button type="button" onclick="addToCart2(this, false, 'transfer', '{$LANG.viewcart}')" class="btn btn-success btn-sm pull-right res-center">
								<span class="fa fa-cart-plus"></span>
								{$LANG.addtocart} ({$result.shortestPeriod.transfer})
							</button>
							<div class="clearfix"></div>
						</div>
					{elseif $result.isAvailable}
						<div class="alert alert-danger">
							<i class="fa fa-2x fa-times"></i> <span class="results-text"><strong>{$result.domainName}</strong> {$LANG.domaintransfer} {$LANG.domainunavailable}</span>
							<div class="clearfix"></div>
						</div>
					{else}
						<div class="alert alert-default">
							<i class="fa fa-2x fa-question"></i> <span class="results-text"><strong>{$result.domainName}</strong> Status Unknown</span>
						</div>
					{/if}
				{/if}
			{/foreach}
		</div>
	{/if}
{/if}
{if $searchResults && count($searchResults.suggestions) > 0}
			<div class="domainresults panel panel-default" id="suggestionSearchResults">
				<div class="panel-heading">
					<h3 class="panel-title">{$LANG.domainssuggestions}</h3>
					<p style="padding-top: 5px;">{$LANG.domainsothersuggestions}</p>
				</div>
				<table class="table table-curved table-hover" id="suggestionResults">
					<tbody>
						{foreach $searchResults.suggestions as $i => $result}
							<tr{if $i >= 10} class="hidden"{/if}>
								<td class="dname">
									<strong>{$result.domainName}</strong>
								</td>
								<td class="text-right">
									{if count($result.pricing) == 1}
										<button type="button" class="btn btn-default btn-sm" onclick="addToCart3(this, true, 'register', '{$LANG.viewcart}')">
											<span class="fa fa-cart-plus"></span>
											{$LANG.addtocart} ({$result.shortestPeriod.register})
										</button>
									{else}
										<div class="btn-group pull-right res-center">
											<button type="button" class="btn btn-default btn-sm" onclick="addToCart3(this, true, 'register', '{$LANG.viewcart}')">
												<span class="fa fa-cart-plus"></span>
												{$LANG.addtocart} ({$result.shortestPeriod.register})
											</button>
											<button type="button" class="btn btn-default btn-sm dropdown-toggle additional-options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<b class="caret"></b>
													<span class="sr-only">
															{lang key="domainChecker.additionalPricingOptions" domain=$searchResults.domainName}
													</span>
											</button>
											<ul class="dropdown-menu" role="menu">
													{foreach $result.pricing as $years => $price}
															{if $price@iteration eq 1}
																	{* Don't output the first as this is default *}
																	{continue}
															{/if}
															<li>
																	<a onclick="addToCart3(this, true, 'register', '{$LANG.viewcart}', {$years});return false;">
																			{$years} {$LANG.orderyears} @ {$price.register}
																	</a>
															</li>
													{/foreach}
											</ul>
										</div>
									{/if}
								</td>
							</tr>
						{/foreach}
						<tr id="trNoMoreSuggestions" class="hidden">
							<td colspan="2" class="text-muted text-center">
								{$LANG.domaincheckernomoresuggestions}
							</td>
						</tr>
					</tbody>
				</table>
			{if $moreSuggestionsAvailable}
				<p class="text-center">
					<a href="#" class="btn btn-link" id="btnMoreSuggestions" onclick="showMoreSuggestions();return false">{$LANG.domainsmoresuggestions}</a>
				</p>
			{/if}
			<div class="panel-footer">
				<p class="text-muted"><strong>{$LANG.disclaimers}</strong><br />{$LANG.domainssuggestionswarnings}</p>
			</div>
		</div>
	</div>
{/if}