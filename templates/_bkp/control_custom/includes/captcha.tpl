{if $captcha}
    {if $filename == 'domainchecker'}
		<br />
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h3 class="panel-title">{lang key="captchatitle"}</h3>
            </div>
            <div class="panel-body text-center"> 
				{if $captcha == "recaptcha"}
					<script src="https://www.google.com/recaptcha/api.js" async defer></script>
					<div id="google-recaptcha-domainchecker" class="g-recaptcha center-block" data-sitekey="{$reCaptchaPublicKey}"></div>
				{else}
			        <p>{lang key="captchaverify"}</p>
					<div class="text-center">
						<div class="col-md-6 col-md-offset-3 captchaimage">
							<img id="inputCaptchaImage" src="includes/verifyimage.php" align="middle" />

							<br /><br />
							<input id="inputCaptcha" type="text" name="code" maxlength="5" class="form-control" />
						</div>
					</div>
				{/if}
			</div>
        </div>
	{elseif $templatefile == 'homepage'}
		<div id="captcha-panel" class="panel panel-warning" style="display: none;">
            <div class="panel-heading">
                <h3 class="panel-title">{lang key="captchatitle"}</h3>
            </div>
            <div class="panel-body text-center"> 
				{if $captcha == "recaptcha"}
					<script src="https://www.google.com/recaptcha/api.js" async defer></script>
					<div id="google-recaptcha-domainchecker" class="g-recaptcha center-block" data-sitekey="{$reCaptchaPublicKey}"></div>
				{else}
			        <p>{lang key="captchaverify"}</p>
					<div class="text-center">
						<div class="col-md-6 col-md-offset-3 captchaimage">
							<img id="inputCaptchaImage" src="includes/verifyimage.php" align="middle" />

							<br /><br />
							<input id="inputCaptcha" type="text" name="code" maxlength="5" class="form-control" />
						</div>
					</div>
				{/if}
			</div>
        </div>
	{else}	
		<hr/>
		<fieldset>
			<div class="form-group">
				<label class="col-sm-3 control-label" for="code">{lang key="captchatitle"}</label>
				<div class="col-sm-6 captchaimage">
					<p style="padding-top: 7px;">{lang key="captchaverify"}</p>
					{if $captcha eq "recaptcha"}
						{$recaptchahtml}
					{else}
						<img src="includes/verifyimage.php" align="middle" />
						<br /><br />
						<input id="inputCaptcha" type="text" name="code" maxlength="5" class="form-control" />
					{/if}
				</div>
			</div>
		</fieldset>
    {/if}
{/if}