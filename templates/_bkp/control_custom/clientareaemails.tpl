<!-- Start Modal: emailModal  -->
<div class="modal fade" id="emailModal" tabindex="-1" role="dialog" aria-labelledby="emailModalLabel">
  <div class="modal-dialog model-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="emailModalLabel">...</h4>
      </div>
      <div id="modal-load" class="modal-body modal-body-touch">
        <img src="templates/{$template}/img/loading.gif" style="display: block; margin: 10px auto;" />
      </div>
      <div class="modal-footer">
	  	<div class="btn-group">
			<button type="button" id="printButton" class="btn btn-default">{$LANG.print}</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">{$LANG.supportticketsclose}</button>
		</div>
      </div>
    </div>
  </div>
</div>
<!-- End Modal: emailModal  -->
{include file="$template/includes/tablelist.tpl" tableName="EmailsList" noSortColumns="-1"}
<script type="text/javascript">
	jQuery(document).ready( function ()
	{
		var table = $('#tableEmailsList').DataTable();
		{if $orderby == 'date'}
			table.order(0, '{$sort}');
		{elseif $orderby == 'subject'}
			table.order(1, '{$sort}');
		{/if}
		table.draw();
	});
</script>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">{$LANG.clientareaemails}</h3>
			</div>
			<div class="panel-body">
				<div class="table-container clearfix">
					<table id="tableEmailsList" class="table table-bordered table-hover table-list" width="100%">
						<thead>
							<tr>
								<th>{$LANG.clientareaemailsdate}</th>
								<th>{$LANG.clientareaemailssubject}</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							{foreach from=$emails item=email}
							<tr>
								<td><span class="hidden">{$email.normalisedDate}</span>{$email.date}</td>
								<td>{$email.subject}</td>
								<td>
								 <button type="button" class="btn btn-primary btn-3d btn-sm btn-block" data-toggle="modal" data-target="#emailModal" data-ref="{$email.id}" data-subj="{$email.subject}" data-date="{$email.date}">
				{$LANG.emailviewmessage}
				</button>
								</td>
							</tr>
							{/foreach}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$('#emailModal').on('show.bs.modal', function (event) {
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var email = button.data('ref') // Extract info from data-* attributes
	  var subject = button.data('subj')
	  var date = button.data('date')
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this)
	  modal.find('.modal-title').html('{$LANG.clientareaemailssubject}: ' + subject + '<br/><small>{$LANG.clientareaemailsdate}: ' + date + '</small>')
	  modal.find('.modal-body').load('viewemail.php?id=' + email + '&internal=yes p')
	});
	$('#emailModal').on('hide.bs.modal', function (event) {
	  var modal = $(this)
	  modal.find('.modal-title').text('...')
	  modal.find('.modal-body').html('<img src="templates/{$template}/img/loading.gif" style="display: block; margin: 10px auto;" />')
	});
	$('#printButton').on('click',function (){
	   //call print method
	   $("#modal-load").printThis({ 
			debug: false,              
			importCSS: true,             
			importStyle: true,         
			printContainer: true,
			removeInline: false,        
			printDelay: 333,            
			header: null,             
			formValues: true          
		}); 
	});
</script>