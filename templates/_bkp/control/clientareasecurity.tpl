{if $twofaavailable}
	{if $twofaactivation}
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">{$LANG.twofactorauth}</h3>
					</div>
					<div class="panel-body">
						<div id="twofaactivation">
							{$twofaactivation}
						</div>
					</div>
				</div>
			</div>
		</div>
	{else}
		{include file="$template/includes/alert.tpl" type="warning" msg="{lang key="clientAreaSecurityTwoFactorAuthRecommendation"}"}
		<form method="post" action="clientarea.php?action=security">
			<input type="hidden" name="2fasetup" value="1" />
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">{$LANG.twofactorauth}</h3>
						</div>
						<div class="panel-body">
							<p>{$LANG.twofaactivationintro}</p>
						</div>
						<div class="panel-footer">
							{if $twofastatus}
								<input type="submit" value="{$LANG.twofadisableclickhere}" class="btn btn-3d res-100 btn-danger" />
							{else}
								<input type="submit" value="{$LANG.twofaenableclickhere}" class="btn btn-3d res-100 btn-success" />
							{/if}
						</div>
					</div>
				</div>
			</div>
		</form>
	{/if}
{/if}
{if $securityquestionsenabled && !$twofaactivation}
	{if $successful}
		{include file="$template/includes/alert.tpl" type="success" msg=$LANG.changessavedsuccessfully textcenter=true}
	{elseif $errormessage}
		{include file="$template/includes/alert.tpl" type="error" errorshtml=$errormessage}
	{elseif $nocurrent}
		{include file="$template/includes/alert.tpl" type="info" msg="{lang key="clientAreaSecurityNoSecurityQuestions"}"}
	{else $nocurrent}
		{include file="$template/includes/alert.tpl" type="info" msg="{lang key="clientAreaSecuritySecurityQuestionOtherError"}"}
	{/if}
	<form class="form-horizontal" method="post" action="{$smarty.server.PHP_SELF}?action=security">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">{$LANG.clientareanavsecurityquestions}</h3>
					</div>
					<div class="panel-body">
						<fieldset>
							{if !$nocurrent}
								<div class="form-group">
									<label class="col-sm-3 control-label" for="currentans">{$currentquestion}</label>
									<div class="col-sm-6">
										<input class="form-control" type="password" name="currentsecurityqans" id="currentans" autocomplete="off" />
									</div>
								</div>
							{/if}
	
							<div class="form-group">
								<label class="col-sm-3 control-label" for="securityqid">{$LANG.clientareasecurityquestion}</label>
								<div class="col-sm-6">
									<select class="form-control" name="securityqid" id="securityqid">
										{foreach key=num item=question from=$securityquestions}
											<option value={$question.id}>{$question.question}</option>
										{/foreach}
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="securityqans">{$LANG.clientareasecurityanswer}</label>
								<div class="col-sm-6">
									<input class="form-control" type="password" name="securityqans" id="securityqans" autocomplete="off" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="securityqans2">{$LANG.clientareasecurityconfanswer}</label>
								<div class="col-sm-6">
									<input class="form-control" type="password" name="securityqans2" id="securityqans2" autocomplete="off" />
								</div>
							</div>
						</fieldset>
					</div>
					<div class="panel-footer">
						<input class="btn res-100 btn-3d btn-primary" type="submit" name="submit" value="{$LANG.clientareasavechanges}" />
						<input class="btn res-100 btn-default pull-right res-left" type="reset" value="{$LANG.cancel}" />
					</div>
				</div>
			</div>
		</div>
	</form>
{/if}
{if $showSsoSetting && !$twofaactivation}
	<form id="frmSingleSignOn">	
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">{$LANG.sso.title}</h3>
					</div>
					<div class="panel-body">
						{include file="$template/includes/alert.tpl" type="success" msg=$LANG.sso.summary}
						<input type="hidden" name="token" value="{$token}" />
						<input type="hidden" name="action" value="security" />
						<input type="hidden" name="toggle_sso" value="1" />
						<div class="margin-10">
							<input type="checkbox" name="allow_sso" class="toggle-switch-success" id="inputAllowSso"{if $isSsoEnabled} checked{/if}>
							&nbsp;
							<span id="ssoStatusTextEnabled"{if !$isSsoEnabled} class="hidden"{/if}>
									{$LANG.sso.enabled}
							</span>
							<span id="ssoStatusTextDisabled"{if $isSsoEnabled} class="hidden"{/if}>
									{$LANG.sso.disabled}
							</span>
						</div>
						<p>{$LANG.sso.disablenotice}</p>
					</div>
				</div>
			</div>
		</div>
  </form>
	<link href="{$BASE_PATH_CSS}/bootstrap-switch.min.css" rel="stylesheet">
	<script src="{$BASE_PATH_JS}/bootstrap-switch.min.js"></script>
{/if}