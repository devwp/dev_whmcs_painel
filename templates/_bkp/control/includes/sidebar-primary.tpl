{$itCount = 0}
{foreach $sidebar as $item}
	{$itCount = $itCount + 1}
	{if $itCount != $itSidebarPrimaryExcludeItem and $itCount != $itSidebarPrimaryExcludeItem2}
		{if $item->getName() eq "Domain Details Management" or $item->getName() eq "My Domains Status Filter" or $item->getName() eq "My Invoices Status Filter" or $item->getName() eq "My Services Status Filter" or $item->getName() eq "My Quotes Status Filter" or $item->getName() eq "Network Status" or $item->getName() eq "Ticket List Status Filter"}
			{assign var=itPanelCollapse value="yes" scope="global"}
		{/if}
		<div menuItemName="{$item->getName()}" class="panel {if $item->getClass()}{$item->getClass()}{else}panel-default{/if}{if $item->getExtra('mobileSelect') and $item->hasChildren()} hidden-sm hidden-xs{/if}"{if $item->getAttribute('id')} id="{$item->getAttribute('id')}"{/if}>
			<div class="panel-heading">
				{if $itPanelCollapse eq "yes"}
					<a href="#sidebarCollapse" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarCollapse" class="collapse-link">
				{/if}
					<h3 class="panel-title">
						{if $item->hasIcon()}<i class="{$item->getIcon()}"></i>&nbsp;{/if}
						{$item->getLabel()}
						{if $item->hasBadge()}&nbsp;<span class="badge">{$item->getBadge()}</span>{/if}
						{if $itPanelCollapse eq "yes"}
							<i class="fa fa-bars pull-right visible-sm visible-xs"></i>
						{/if}
					</h3>
				{if $itPanelCollapse eq "yes"}
					</a>
				{/if}
			</div>
			{if $item->hasBodyHtml()}
				<div class="panel-body">
					{$item->getBodyHtml()}
				</div>
			{/if}
			{if $item->hasChildren()}
				<div {if $itPanelCollapse eq "yes"}id="sidebarCollapse" {/if}class="{if $itPanelCollapse eq "yes"}panel-collapse collapse in {/if}list-group{if $item->getChildrenAttribute('class')} {$item->getChildrenAttribute('class')}{/if}">
					{foreach $item->getChildren() as $childItem}
						{if $childItem->getUri()}
							<a menuItemName="{$childItem->getName()}" href="{$childItem->getUri()}" class="list-group-item{if $childItem->isDisabled()} disabled{/if}{if $childItem->getClass()} {$childItem->getClass()}{/if}{if $childItem->isCurrent()} active{/if}"{if $childItem->getAttribute('dataToggleTab')} data-toggle="tab"{/if}{if $childItem->getAttribute('target')} target="{$childItem->getAttribute('target')}"{/if} id="{$childItem->getId()}">
								{if $childItem->hasIcon()}<i class="{$childItem->getIcon()}"></i>&nbsp;{/if}
								{$childItem->getLabel()}
								{if $childItem->hasBadge()}&nbsp;<span class="badge">{$childItem->getBadge()}</span>{/if}
							</a>
						{else}
							<div menuItemName="{$childItem->getName()}" class="list-group-item{if $childItem->getClass()} {$childItem->getClass()}{/if}" id="{$childItem->getId()}">
								{if $childItem->hasIcon()}<i class="{$childItem->getIcon()}"></i>&nbsp;{/if}
								{$childItem->getLabel()}
								{if $childItem->hasBadge()}&nbsp;<span class="badge">{$childItem->getBadge()}</span>{/if}
							</div>
						{/if}
					{/foreach}
				</div>
				
			{/if}
			{if $item->hasFooterHtml()}
				<div class="panel-footer clearfix">
					{$item->getFooterHtml()}
				</div>
			{/if}
		</div>
		{if $item->getExtra('mobileSelect') and $item->hasChildren()}
			{* Mobile Select only supports dropdown menus *}
			<div menuItemName="Mobile Select {$item->getName()}" class="panel hidden-lg hidden-md {if $item->getClass()}{$item->getClass()}{else}panel-default{/if}"{if $item->getAttribute('id')} id="{$item->getAttribute('id')}"{/if}>
				<div class="panel-heading">
					<h3 class="panel-title">
						{if $item->hasIcon()}<i class="{$item->getIcon()}"></i>&nbsp;{/if}
						{$item->getLabel()}
						{if $item->hasBadge()}&nbsp;<span class="badge">{$item->getBadge()}</span>{/if}
					</h3>
				</div>
				<div class="panel-body">
					<form role="form">
						<select class="form-control" onchange="selectChangeNavigate(this)">
							{foreach $item->getChildren() as $childItem}
								<option menuItemName="{$childItem->getName()}" value="{$childItem->getUri()}" class="list-group-item" {if $childItem->isCurrent()}selected="selected"{/if}>
									{$childItem->getLabel()}
									{if $childItem->hasBadge()}({$childItem->getBadge()}){/if}
								</option>
							{/foreach}
						</select>
					</form>
				</div>
				{if $item->hasFooterHtml()}
					<div class="panel-footer">
						{$item->getFooterHtml()}
					</div>
				{/if}
			</div>
		{/if}
	{/if}
{/foreach}