					<!-- If page isn't shopping cart, close main content layout and display secondary sidebar (if enabled and applicable) -->
					{if !$inShoppingCart}
						<!-- Close main content layout and display secondary sidebar (if enabled and applicable) -->
						{if $itShowSidebarLayout eq "yes"}
								</div>
								{if $itShowSidebarSecondary eq "yes"}
									<div class="col-md-3 pull-md-right whmcs-sidebar sidebar-secondary">
										{include file="$template/includes/sidebar-secondary.tpl" sidebar=$secondarySidebar}
									</div>
								{/if}
							</div>
						{/if}
						<div class="clearfix"></div>
					</section>
				{/if}
				<!-- If theme debug is enabled, display function variables for test and debugging purposes -->
				{if $itThemeDebug eq "enabled"}
					{include file="$template/includes/theme-debug.tpl"}
				{/if}
				<div id="footer" class="panel panel-solid-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-7">
								<span class="footer-text">Copyright &copy; {$date_year} {$companyname}. All Rights Reserved.</span>
							</div>
							<div class="col-sm-5">
								<div class="row">
									<div class="col-xs-10 text-right lang-ft">
										{if $itLanguageIconDisplayFooter eq "enabled" and $languagechangeenabled and count($locales) > 1}
											<a href="#bottom" data-toggle="popover" id="languageChooser3"><i class="fa fa-globe"></i> {$LANG.chooselanguage}</a>
										{/if}
									</div>
									<div class="col-xs-2 text-right">
										<a href="#top"><i class="fa fa-angle-up fa-2x"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="languageChooserContent" class="hidden">
					<ul>
						{foreach from=$locales item=locale}
							<li><a href="{$currentpagelinkback}language={$locale.language}">{$locale.localisedName}</a></li>
						{/foreach}
					</ul>
				</div>
			</section>
		</div>
		<!-- WHMCS core JS -->
		<script src="{$BASE_PATH_JS}/bootstrap.min.js"></script>
		<script src="{$BASE_PATH_JS}/jquery-ui.min.js"></script>
		<script type="text/javascript">
		    var csrfToken = '{$token}',
		        markdownGuide = '{lang key="markdown.title"}',
		        locale = '{if !empty($mdeLocale)}{lang key="locale"}{else}en_GB{/if}',
		        saved = '{lang key="markdown.saved"}',
		        saving = '{lang key="markdown.saving"}';
		</script>
		<script src="{$WEB_ROOT}/templates/{$template}/js/whmcs.js"></script>
		<script src="{$BASE_PATH_JS}/AjaxModal.js"></script>
		<script src="{$WEB_ROOT}/templates/{$template}/js/sidebar.js"></script>
		<!--Theme JS-->
		<script src="{$WEB_ROOT}/templates/{$template}/js/jquery.navgoco.min.js"></script>
		<script src="{$WEB_ROOT}/templates/{$template}/js/application.js"></script>
		<script src="{$WEB_ROOT}/templates/{$template}/js/jquery.countTo.js"></script>
		{literal}
			<script>
				$(document).ready(function() {
					app.timer();
				});
				$('a[href=#top]').click(function(){
					$('html, body').animate({scrollTop:0}, 'slow');
				});
			</script>
		{/literal}
		<div class="modal system-modal fade" id="modalAjax" tabindex="-1" role="dialog" aria-hidden="true">
		    <div class="modal-dialog">
		        <div class="modal-content panel panel-primary">
		            <div class="modal-header panel-heading">
		                <button type="button" class="close" data-dismiss="modal">
		                    <span aria-hidden="true">&times;</span>
		                    <span class="sr-only">Close</span>
		                </button>
		                <h4 class="modal-title">Title</h4>
		            </div>
		            <div class="modal-body panel-body">
		                Loading...
		            </div>
		            <div class="modal-footer panel-footer">
		                <div class="pull-left loader">
		                    <i class="fa fa-circle-o-notch fa-spin"></i> Loading...
		                </div>
		                <button type="button" class="btn btn-default" data-dismiss="modal">
		                    Close
		                </button>
		                <button type="button" class="btn btn-primary modal-submit">
		                    Submit
		                </button>
		            </div>
		        </div>
		    </div>
		</div>
		{$footeroutput}
	</body>
</html>