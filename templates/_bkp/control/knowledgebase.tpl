{if empty($kbcats) }
	{include file="$template/includes/alert.tpl" type="info" msg=$LANG.knowledgebasenoarticles textcenter=true}
{else}
	<form role="form" method="post" action="{$WEB_ROOT}/knowledgebase.php?action=search">
		<div class="input-group">
			<input type="text" name="search" class="form-control input-group" placeholder="{$LANG.kbsearchexplain}" />
			<span class="input-group-btn">
				<input type="submit" class="btn btn-primary btn-input-padded-responsive" value="{$LANG.search}" />
			</span>
		</div>
	</form>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-folder-open"></i> {$LANG.knowledgebasecategories}</h3>
				</div>
				<div class="list-group">
					{foreach from=$kbcats name=kbcats item=kbcat}
						<span class="list-group-item">
							<a href="{if $seofriendlyurls}{$WEB_ROOT}/knowledgebase/{$kbcat.id}/{$kbcat.urlfriendlyname}{else}knowledgebase.php?action=displaycat&amp;catid={$kbcat.id}{/if}">
								<i class="fa fa-folder-open-o"></i>
								<strong>{$kbcat.name}</strong>
							</a>
							({$kbcat.numarticles})
							<br />
							{$kbcat.description}
						</span>
					{foreachelse}
						<span class="list-group-item">
							<p class="text-center fontsize3">{$LANG.knowledgebasenoarticles}</p>
						</span>
					{/foreach}
				</div>
			</div>
		</div>
	</div>
{/if}
{if $kbmostviews}
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-star"></i> {$LANG.knowledgebasepopular}</h3>
				</div>
				<div class="list-group">
					{foreach from=$kbmostviews item=kbarticle}
						<div class="list-group-item">
							<a href="{if $seofriendlyurls}{$WEB_ROOT}/knowledgebase/{$kbarticle.id}/{$kbarticle.urlfriendlytitle}.html{else}knowledgebase.php?action=displayarticle&amp;id={$kbarticle.id}{/if}">
								<i class="fa fa-file-o"></i>
								<strong>{$kbarticle.title}</strong>
							</a><br />
							{$kbarticle.article|truncate:100:"..."}
						</div>
					{/foreach}
				</div>
			</div>
		</div>
	</div>
{/if}