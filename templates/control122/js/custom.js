function mascarar(field){
	try {
		$(field).unmask();
	} catch (e) { }
	var size=$(field).val().length;

	if(size==11){
		$(field).mask("999.999.999-99");
	} else if(size==14) {
		$(field).mask("99.999.999/9999-99");
	} else {
		try {
			$(field).unmask();
		} catch (e) { }
	}
}
function desmascarar(field){
	try {
		$(field).val($(field).val().replace(/[\.]|[\-]|[\/]/g,''));
		$(field).unmask();
	} catch (e) { }
}
$(document).ready(function(){
	$("#carticondesk").hide();
	$('div[menuitemname="Support"]').hide();
	$('div[menuitemname="Network Status"]').hide();
	$('div[menuitemname="Active Products/Services"]').hide();
	
	var $cpfcnpj=jQuery("label:contains('CNPJ ou CPF')").parent().find('input');
	//var $cpfcnpj=$("td.fieldlabel:contains('CNPJ ou CPF')").next().find('input');
	//$cpfcnpj.css('background-color','red');
	$cpfcnpj.blur(function(){mascarar(this);});
	$cpfcnpj.focus(function(){desmascarar(this);});
	$cpfcnpj.keypress(function(event){
		if(event.keyCode==10||event.keyCode==13){
			event.preventDefault();
			mascarar(this);
			$('form[action="clients.php"]').submit();
		}
	});
	$(".panel-title").each(function(){
    	valor = $(this).text();
    	if (valor.indexOf("Affiliate Program") >= 0){
    		  $(this).parent().parent().attr("style", "display:none");
    	}
    	if (valor.indexOf("programa de afiliados") >= 0){
    		  $(this).parent().parent().attr("style", "display:none");
    	}

	});

	$('a').each(function(){
	    if ($(this).attr("href").indexOf("clientarea.php?action=masspay&all=true") >= 0){
	    	$(this).attr("href", "clientarea.php?action=invoices");
	    };
	    
    }); 
	
	
	//lert( $(".dial-usage").parent().children().attr("style", "display:block"));
	//$(".dial-usage").parent().attr("style", "display:block !important;width:100px;height:100px;");
	$(".clientareaproductdetails ul.nav-tabs li:last").each(function(){
		if ($(this).text().indexOf("Upgrade/Downgrade") >= 0){
			$(this).hide();
		}
	});

	$("ul.nav-tabs li").each(function(){
		 if ($(this).text().indexOf("Upgrade/Downgrade") >= 0){
			$(this).hide();
		}
	});
	 $('.breadcrumb').hide();
	var menuserv = $('li[menuitemname="My Services"]').parent();
	//menuserv.remove(); 
	$('li[menuitemname="My Services"]').remove();
	menuserv.append('<li menuitemname="Todos" 			class="" id="Primary_Navbar-Services-E-mail"			><a href="clientarea.php?action=services&cat=Todos"			>Meus Serviços</a></li>');
	menuserv.append('<li menuitemname="E-mail" 			class="" id="Primary_Navbar-Services-E-mail"			><a href="clientarea.php?action=services&cat=E-mail"			>E-mail</a></li>');
	menuserv.append('<li menuitemname="Hospedagem" 		class="" id="Primary_Navbar-Services-Hospedagem"		><a href="clientarea.php?action=services&cat=Hospedagem"		>Hospedagem</a></li>');
	menuserv.append('<li menuitemname="Email Marketing" class="" id="Primary_Navbar-Services-Email_Marketing"	><a href="clientarea.php?action=services&cat=Email_Marketing"	>Email Marketing</a></li>');
	menuserv.append('<li menuitemname="Cloud Server" 	class="" id="Primary_Navbar-Services-Cloud_Server"		><a href="clientarea.php?action=services&cat=Cloud_Server"		>Cloud Server</a></li>');
	menuserv.append('<li menuitemname="CDN" 			class="" id="Primary_Navbar-Services-CDN"				><a href="clientarea.php?action=services&cat=CDN"				>CDN</a></li>');
	menuserv.append('<li menuitemname="Cloud Backup" 	class="" id="Primary_Navbar-Services-Cloud_Backup"		><a href="clientarea.php?action=services&cat=Cloud_Backup"		>Cloud Backup</a></li>');
	menuserv.append('<li menuitemname="Domínios" 		class="" id="Primary_Navbar-Services-Dominios"			><a href="clientarea.php?action=domains"					>Domínios</a></li>');
});   
			

