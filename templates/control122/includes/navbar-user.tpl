{foreach $navbar as $item}
    <li menuItemName="{$item->getName()}"{if $item->hasChildren()} class="dropdown settings"{elseif $item->getClass()} class="{$item->getClass()}"{/if} id="{$item->getId()}">
		<button type="button" class="btn btn-default dropdown-toggle options" id="toggle-user" data-toggle="dropdown">
			<i class="fa fa-user"></i>
		</button>
        <a {if $item->hasChildren()}class="dropdown-toggle hidden-xs" data-toggle="dropdown" href="#"{else}href="{$item->getUri()}"{/if}{if $item->getAttribute('target')} target="{$item->getAttribute('target')}"{/if}>
            {if $item->hasIcon()}<i class="{$item->getIcon()}"></i>&nbsp;{/if}
            {$item->getLabel()}
            {if $item->hasBadge()}&nbsp;<span class="badge">{$item->getBadge()}</span>{/if}
            {if $item->hasChildren()}&nbsp;<b class="caret"></b>{/if}
        </a>
        {if $item->hasChildren()}
            <ul class="dropdown-menu dropdown-menu-right animated fadeInDown">
            {foreach $item->getChildren() as $childItem}
              {if $childItem->getName() neq "Register"}
                  <li menuItemName="{$childItem->getName()}"{if $childItem->getClass()} class="{$childItem->getClass()}"{/if} id="{$childItem->getId()}">
                      <a href="{$childItem->getUri()}"{if $childItem->getAttribute('target')} target="{$childItem->getAttribute('target')}"{/if}>
                          {if $childItem->hasIcon()}<i class="{$childItem->getIcon()}"></i>&nbsp;
  						{elseif $childItem->getName() eq "Edit Account Details"}<i class="fa fa-edit fa-fw"></i>&nbsp;
  						{elseif $childItem->getName() eq "Manage Credit Card"}<i class="fa fa-credit-card fa-fw"></i>&nbsp;
  						{elseif $childItem->getName() eq "Contacts/Sub-Accounts"}<i class="fa fa-book fa-fw"></i>&nbsp;
  						{elseif $childItem->getName() eq "Change Password"}<i class="fa fa-key fa-fw"></i>&nbsp;
  						{elseif $childItem->getName() eq "Email History"}<i class="fa fa-envelope-o fa-fw"></i>&nbsp;
  						{elseif $childItem->getName() eq "Logout"}<i class="fa fa-sign-out fa-fw"></i>&nbsp;
  						{elseif $childItem->getName() eq "Login"}<i class="fa fa-sign-in fa-fw"></i>&nbsp;
  						{elseif $childItem->getName() eq "Register"}<i class="fa fa-level-up fa-fw"></i>&nbsp;
  						{elseif $childItem->getName() eq "Forgot Password?"}<i class="fa fa-question-circle fa-fw"></i>&nbsp;
  						{else}<i class="fa fa-angle-right fa-fw"></i>&nbsp;
  						{/if}
                          {$childItem->getLabel()}
                          {if $childItem->hasBadge()}&nbsp;<span class="badge">{$childItem->getBadge()}</span>{/if}
                      </a>
                  </li>
              {/if}
            {/foreach}
            </ul>
        {/if}
    </li>
{/foreach}
