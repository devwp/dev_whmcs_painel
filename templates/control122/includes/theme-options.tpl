{*
		Impressive Themes
		http://www.impressivethemes.net
		-------------------------------

		Theme Options for Control Theme by Impressive Themes
		----------------------------------------------------

		Please refer to the theme documentation available at the
		following url before making any changes to this file:

		http://www.impressivethemes.net/support/documentation/control-whmcs-theme-documentation


		Should you require any further help or assistance, please
		contact customer support via our online support desk at:

		http://www.impressivethemes.net/support/contact
*}
{* 1. Main Nav Bar and Brand Display Options *}
	{* 1.1.	Enable or Disable Navbar Brand Display - enabled / disabled *}
		{assign var=itBrandDisplay value="enabled" scope="global"}
	{* 1.2.	Specify Navbar Brand Type - text / logo *}
		{assign var=itBrandType value="logo" scope="global"}
	{* 1.3.	Specify Navbar Brand Logo Url (if applicable) - eg. http://www.yoursite.com/logo.png *}
		{assign var=itBrandLogoUrl value="/logo-webplus.png" scope="global"}
	{* 1.4.	Specify Navbar Brand Custom Text (if applicable) - eg. Your Company *}
		{assign var=itBrandText value="Webplus" scope="global"}
	{* 1.5.	Enable or Disable Breadcrumb Dispay - enabled / disabled *}
		{assign var=itBreadcrumbDisplay value="enabled" scope="global"}
	{* 1.6. Enable or Disable Cart Icon Display on Desktop View- enabled / disabled *}
		{if $loggedin}
		{assign var=itCartIconDisplayDesktop value="enabled" scope="global"}
		{else}
		{assign var=itCartIconDisplayDesktop value="disabled" scope="global"}
		{/if}
	{* 1.7. Enable or Disable Cart Icon Display on Mobile View- enabled / disabled *}
		{assign var=itCartIconDisplayMobile value="enabled" scope="global"}
	{* 1.8. Enable or Disable Notifications Icon Display on Desktop View - enabled / disabled *}
		{assign var=itNotificationsIconDisplayDesktop value="enabled" scope="global"}
	{* 1.9. Enable or Disable Notifications Icon Display on Mobile View - enabled / disabled *}
		{assign var=itNotificationsIconDisplayMobile value="enabled" scope="global"}
	{* 1.10. Enable or Disable NavBar Language Icon Display on Desktop View - enabled / disabled *}
		{assign var=itLanguageIconDisplayDesktop value="enabled" scope="global"}
	{* 1.11. Enable or Disable NavBar Language Icon Display on Mobile View - enabled / disabled *}
		{assign var=itLanguageIconDisplayMobile value="disabled" scope="global"}
	{* 1.12. Enable or Disable Footer Language Icon Display - enabled / disabled *}
		{assign var=itLanguageIconDisplayFooter value="enabled" scope="global"}
	{* 1.13. Enable or Disable Main Nav Display for Guests (Non-Logged In Users) - enabled / disabled *}
		{assign var=itGuestNavDisplay value="enabled" scope="global"}
{* 2. Sidebar Options *}
	{* 2.1.	Enable or Disable Primary Sidebar Display - enabled / disabled *}
		{assign var=itPrimarySidebar value="enabled" scope="global"}
	{* 2.2.	Enable or Disable Secondary Sidebar Display - enabled / disabled *}
		{assign var=itSecondarySidebar value="enabled" scope="global"}
{* 3. Color Scheme / Style Options  *}
	{* 3.1	Define color scheme / style to be used - green / green-solid / green-white / blue / blue-solid / blue-white *}
		{assign var=itColorScheme value="blue-solid" scope="global"}
{* 4. Theme Debug Options *}
	{* 4.1	Enable or Disable Theme Debug Mode - enabled / disabled *}
		{assign var=itThemeDebug value="disabled" scope="global"}
