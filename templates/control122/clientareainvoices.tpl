{if $itShowSidebarPrimary neq "yes"}
	{if $totalbalance}
		<div class="alert alert-danger" style="display:none">
			{$LANG.invoicesoutstandingbalance}: {$totalbalance} {if $masspay}&nbsp; <a href="clientarea.php?action=masspay&all=true" class="btn btn-success"><i class="icon-ok-circle icon-white"></i> {$LANG.masspayall}</a>{/if}
		</div>
	{/if}
{/if}

<link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.responsive.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.responsive.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
	$('#tableInvoicesList').DataTable( {
	    initComplete: function () {
	        this.api().columns().every( function () {
	            var column = this.column(4);
	            var select = $('<select class="form-control input-sm" style="width:60%"><option value="">Filtro</option></select>')
	                .appendTo( $("#tableInvoicesList_filter").empty() )
	                .on( 'change', function () {
	                    var val = $.fn.dataTable.util.escapeRegex(
	                        $(this).val()
	                    );

	                    column
	                        .search( val ? '^'+val+'$' : '', true, false )
	                        .draw(); 
	                } );

	            column.data().unique().sort().each( function ( d, j ) {
	                try {
						select.append( '<option value="'+$(d).text()+'">'+$(d).text()+'</option>' );
					}
					catch(err) {
						select.append( '<option value="'+d+'">'+d+'</option>' );
					}

	            } );
	        } );
	    },
	   "dom": '<"listtable"fit>pl', "responsive": true, "oLanguage": { "sEmptyTable": "{$LANG.smglobalNothing}", "sInfo": "", "sInfoEmpty": "", "sInfoFiltered": "({$LANG.smglobalshowing} _MAX_ total)", "sInfoPostFix": "", "sInfoThousands": ",", "sLengthMenu": "{$LANG.smglobalshow} _MENU_", "sLoadingRecords": "{$LANG.smgloballoading}", "sProcessing": "{$LANG.smglobalProcess}", "sSearch": "", "sZeroRecords": "{$LANG.smglobalNothing}", "oPaginate": { "sFirst": "", "sLast": "", "sNext": "{$LANG.smglobalNext}", "sPrevious": "{$LANG.smglobalPrev}" } }, "pageLength": 10, "columnDefs": [{ "targets": 'no-sort', "orderable": false, }], "order": [[0, "asc"]], "stateSave": true }); 
    jQuery(".dataTables_filter input").attr("placeholder", "{$LANG.tableentersearchterm}");

		 jQuery(".dataTables_filter input").attr("placeholder", "Pesquisar");

		
	} );
</script>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">{$LANG.invoices}</h3>
			</div>
			<div class="panel-body">
				<div class="table-container clearfix">
					<div id="tableInvoicesList_filter" class="dataTables_filter" style="position: absolute;right: 20%;width: 20%;z-index: 10000;"></div>
					<table id="tableInvoicesList" class="table table-bordered table-hover table-list" width="100%">
						<thead> 
							<tr>
								<th>{$LANG.invoicestitle}</th>
								<th>{$LANG.invoicesdatecreated}</th>
								<th>{$LANG.invoicesdatedue}</th>
								<th>{$LANG.invoicestotal}</th>
								<th>{$LANG.invoicesstatus}</th>
								<th>&nbsp;</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							{foreach key=num item=invoice from=$invoices}
								<tr onclick="clickableSafeRedirect(event, 'viewinvoice.php?id={$invoice.id}', false)" style="cursor: pointer;">
									<td>{$invoice.invoicenum}</td>
									<td><span class="hidden">{$invoice.normalisedDateCreated}</span>{$invoice.datecreated}</td>
									<td><span class="hidden">{$invoice.normalisedDateDue}</span>{$invoice.datedue}</td>
									<td>{$invoice.total}</td>
									<td><span class="label status status-{$invoice.statusClass}">{$invoice.status}</span></td>
									<td class="text-center">
										<a href="viewinvoice.php?id={$invoice.id}" class="btn btn-sm btn-primary btn-3d btn-block">
											<i class="fa fa-file-text"></i> {$LANG.invoicesview}
										</a>
									</td>
									<td class="text-center">
										<form method="submit" action="dl.php">
											<input type="hidden" name="type" value="i" />
											<input type="hidden" name="id" value="{$invoice.id}" />
											<button type="submit" class="btn btn-default btn-sm btn-3d btn-block"><i class="fa fa-download"></i> {$LANG.quotedownload}</button>
										</form>
									</td>
								</tr>
							{/foreach}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>