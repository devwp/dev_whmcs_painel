
<link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.responsive.css">
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.responsive.min.js"></script>
<script type="text/javascript">
	jQuery(document).ready( function ()
	{

		var table = $('#tableServicesList2').DataTable({ "dom": '<"listtable"fit>pl', "responsive": true, "oLanguage": { "sEmptyTable": "{$LANG.smglobalNothing}", "sInfo": "", "sInfoEmpty": "", "sInfoFiltered": "", "sInfoPostFix": "", "sInfoThousands": ",", "sLengthMenu": "{$LANG.smglobalshow} _MENU_", "sLoadingRecords": "{$LANG.smgloballoading}", "sProcessing": "{$LANG.smglobalProcess}", "sSearch": "", "sZeroRecords": "{$LANG.smglobalNothing}", "oPaginate": { "sFirst": "", "sLast": "", "sNext": "{$LANG.smglobalNext}", "sPrevious": "{$LANG.smglobalPrev}" } }, "pageLength": 10}); 

		{if $orderby == 'product'}
			table.order([0, '{$sort}'], [3, 'asc']);
		{elseif $orderby == 'amount' || $orderby == 'billingcycle'}
			table.order(1, '{$sort}');
		{elseif $orderby == 'nextduedate'}
			table.order(2, '{$sort}');
		{elseif $orderby == 'domainstatus'}
			table.order(3, '{$sort}');
		{/if}
		//table.draw();
		var hash = document.URL.substr(document.URL.indexOf('&cat=')+1) 
		hash = hash.replace("cat=","");
		if(hash == "Todos") {
			  
		} else {
			table.columns(5).search(hash).draw();
		}
		
		
	});
</script>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">{$LANG.clientareaproducts}</h3>
			</div>
			<div class="panel-body">
				<div class="table-container clearfix">
					<table id="tableServicesList2" class="table table-bordered table-hover table-list" width="100%">
						<thead>
							<tr>
								<th>{$LANG.orderproduct}</th>
								<th>{$LANG.clientareaaddonpricing}</th>
								<th>{$LANG.clientareahostingnextduedate}</th>
								<th>{$LANG.clientareastatus}</th>
								<th>&nbsp;</th>
								<th style="display:none">GRUPO</th>
								<!--th>PRODUTO</th-->
							</tr>
						</thead>
						<tbody>
							{foreach key=num item=service from=$services}
								<tr>
									<td><a href="clientarea.php?action=productdetails&amp;id={$service.id}"><strong>{$service.product}</strong>{if $service.domain}<br />{$service.domain}{/if}</a></td>
									<td>{$service.amount} {$service.billingcycle}</td>
									<td><span class="hidden">{$service.normalisedNextDueDate}</span>{$service.nextduedate}</td>
									<td><span class="label status status-{$service.status|strtolower}">{$service.statustext}</span></td>
									<td class="responsive-edit-button">
										<a href="clientarea.php?action=productdetails&amp;id={$service.id}" class="btn btn-primary btn-3d btn-sm btn-block"><i class="fa fa-cog"></i> {$LANG.manageproduct}</a>
									</td>
									<td style="display:none">{$service.group } </td>
									<!--td>{$service.product }</td-->
								</tr>
							{/foreach}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>