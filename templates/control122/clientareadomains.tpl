{if $warnings}
	{include file="$template/includes/alert.tpl" type="warning" msg=$warnings}
{/if}
<div class="tab-content">
	<!-- Tab 1: My Domains List -->
    <div class="tab-pane fade in active" id="tabOverview">
		<div class="row">
			<div class="col-md-12">
				{include file="$template/includes/tablelist.tpl" tableName="DomainsList" noSortColumns="0, 6" startOrderCol="1" filterColumn="5"}
				<script type="text/javascript">
					jQuery(document).ready( function ()
					{
						var table = jQuery('#tableDomainsList').DataTable();
						{if $orderby == 'domain'}
							table.order(1, '{$sort}');
						{elseif $orderby == 'regdate' || $orderby == 'registrationdate'}
							table.order(2, '{$sort}');
						{elseif $orderby == 'nextduedate'}
							table.order(3, '{$sort}');
						{elseif $orderby == 'autorenew'}
							table.order(4, '{$sort}');
						{elseif $orderby == 'status'}
							table.order(5, '{$sort}');
						{/if}
						table.draw();
					});
				</script>
				<form id="domainForm" method="post" action="clientarea.php?action=bulkdomain">
					<input id="bulkaction" name="update" type="hidden" />
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">{$LANG.clientareanavdomains}</h3>
						</div>
						<div class="panel-body">
							<div class="table-container clearfix">
								<table id="tableDomainsList" class="table table-bordered table-hover table-list" width="100%">
									<thead>
										<tr>
											<th></th>
											<th>{$LANG.orderdomain}</th>
											<th>{$LANG.regdate}</th>
											<th>{$LANG.nextdue}</th>
											<th>{$LANG.domainsautorenew}</th>
											<th>{$LANG.domainstatus}</th>
											<th>&nbsp;</th>
										</tr>
									</thead>
									<tbody>
									{foreach key=num item=domain from=$domains}
										<tr>
											<td class="text-right">
												<input type="checkbox" name="domids[]" class="domids stopEventBubble" value="{$domain.id}" />
											</td>
											<td><a href="clientarea.php?action=domaindetails&id={$domain.id}">{$domain.domain}</a></td>
											<td><span class="hidden">{$domain.normalisedRegistrationDate}</span>{$domain.registrationdate}</td>
											<td><span class="hidden">{$domain.normalisedNextDueDate}</span>{$domain.nextduedate}</td>
											<td>
												{if $domain.autorenew}
													<span class="label label-success">{$LANG.domainsautorenewenabled}</span>
												{else}
													<span class="label label-danger">{$LANG.domainsautorenewdisabled}</span>
												{/if}
											</td>
											<td>
												<span class="label status status-{$domain.statusClass}">{$domain.statustext}</span>
												<span class="hidden">
													{if $domain.next30}{$LANG.domainsExpiringInTheNext30Days}<br />{/if}
													{if $domain.next90}{$LANG.domainsExpiringInTheNext90Days}<br />{/if}
													{if $domain.next180}{$LANG.domainsExpiringInTheNext180Days}<br />{/if}
													{if $domain.after180}{$LANG.domainsExpiringInMoreThan180Days}{/if}
												</span>
											</td>
											<td>
												<a href="clientarea.php?action=domaindetails&id={$domain.id}" class="btn btn-primary btn-3d btn-sm btn-block"><i class="fa fa-cog"></i> {$LANG.managedomain}</a>
											</td>
										</tr>
									{/foreach}
									</tbody>
								</table>
							</div>
						</div>
						<div class="panel-footer">
							<div class="btn-group margin-bottom">
								<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
									<span class="fa fa-cog"></span> &nbsp; {$LANG.withselected} <span class="caret"></span>
								</button>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#" id="nameservers" class="setBulkAction"><i class="glyphicon glyphicon-globe"></i> {$LANG.domainmanagens}</a></li>
									<li><a href="#" id="autorenew" class="setBulkAction"><i class="glyphicon glyphicon-refresh"></i> {$LANG.domainautorenewstatus}</a></li>
									<li><a href="#" id="reglock" class="setBulkAction"><i class="glyphicon glyphicon-lock"></i> {$LANG.domainreglockstatus}</a></li>
									<li><a href="#" id="contactinfo" class="setBulkAction"><i class="glyphicon glyphicon-user"></i> {$LANG.domaincontactinfoedit}</a></li>
								</ul>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Tab 2: Renewals List -->
	<div class="tab-pane fade in" id="tabRenew">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">{$LANG.domainrenewals}</h3>
					</div>
					<div class="panel-body">
						{include file="$template/includes/tablelist.tpl" tableName="RenewalsList" noSortColumns="4, 5" startOrderCol="0" filterColumn="1" dontControlActiveClass=true}
						<script type="text/javascript">
								var observer = new MutationObserver(function(mutations) {
										jQuery('#Secondary_Sidebar-My_Domains_Actions-Renew_Domain').toggleClass('active')
								});
								var target = document.querySelector('#tabRenew');
								observer.observe(target, {
										attributes: true
								});
						</script>
						<div class="table-container clearfix">
							<table id="tableRenewalsList" class="table table-bordered table-hover table-list" width="100%">
								<thead>
									<tr>
										<th>{$LANG.orderdomain}</th>
										<th>{$LANG.domainstatus}</th>
										<th>{$LANG.clientareadomainexpirydate}</th>
										<th>{$LANG.domaindaysuntilexpiry}</th>
										<th>&nbsp;</th>
										<th>&nbsp;</th>
									</tr>
								</thead>
								<tbody>
									{foreach $renewals as $id => $renewal}
										<tr id="domainRow{$renewal.id}" {if $selectedIDs && in_array($renewal.id, $selectedIDs)}class="highlight"{/if}>
											<td id="domain{$renewal.id}">{$renewal.domain}</td>
											<td id="status{$renewal.id}">
												<span class="label status status-{$renewal.statusClass}">{$renewal.status}</span>
												<span class="hidden">
													{if $renewal.next30}{$LANG.domainsExpiringInTheNext30Days}<br />{/if}
													{if $renewal.next90}{$LANG.domainsExpiringInTheNext90Days}<br />{/if}
													{if $renewal.next180}{$LANG.domainsExpiringInTheNext180Days}<br />{/if}
													{if $renewal.after180}{$LANG.domainsExpiringInMoreThan180Days}{/if}
												</span>
											</td>
											<td id="expiry{$renewal.id}"><span class="hidden">{$renewal.normalisedExpiryDate}</span>{$renewal.expiryDate}</td>
											<td id="days{$renewal.id}">
												{if $renewal.daysUntilExpiry > 30}
													<span class="text-success">{$renewal.daysUntilExpiry} {$LANG.domainrenewalsdays}</span>
												{elseif $renewal.daysUntilExpiry > 0}
													<span class="text-warning">{$renewal.daysUntilExpiry} {$LANG.domainrenewalsdays}</span>
												{else}
													<span class="text-danger">{$renewal.daysUntilExpiry*-1} {$LANG.domainrenewalsdaysago}</span>
												{/if}
												{if $renewal.inGracePeriod}
													<br />
													<span class="text-danger">{$LANG.domainrenewalsingraceperiod}</span>
												{/if}
											</td>
											<td id="period{$renewal.id}" class="text-right">
												{if $renewal.beforeRenewLimit}
													<span class="text-danger">
														{$LANG.domainrenewalsbeforerenewlimit|sprintf2:$renewal.beforeRenewLimitDays}
													</span>
												{elseif $renewal.pastGracePeriod}
													<span class="textred">{$LANG.domainrenewalspastgraceperiod}</span>
												{else}
													<select id="renewalPeriod{$renewal.id}" name="renewalPeriod[{$renewal.id}]" class="form-control" style="display: block; width: 100%;">
														{foreach $renewal.renewalOptions as $renewalOption}
															<option value="{$renewalOption.period}">
																{$renewalOption.period} {$LANG.orderyears} @ {$renewalOption.price}
															</option>
														{/foreach}
													</select>
												{/if}
											</td>
											<td class="text-center">
												{if !$renewal.beforeRenewLimit && !$renewal.pastGracePeriod}
													<button type="button" class="btn btn-primary btn-3d btn-sm btn-block" id="renewButton{$renewal.id}" onclick="addRenewalToCart2({$renewal.id}, this, '{$LANG.viewcart}')">
														<span class="fa fa-cart-plus"></span> {$LANG.addtocart}
													</button>
												{/if}
											</td>
										</tr>
									{/foreach}
								</tbody>
							</table>
						</div>
					</div>
					<div class="panel-footer">
						<a href="#tabOverview" class="btn btn-default btn-sm" data-toggle="tab" id="back">
							<i class="glyphicon glyphicon-backward"></i> {$LANG.clientareabacklink|replace:'&laquo; ':''}
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	jQuery(document).ready(function() {
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			var target = $(e.target).attr("href");
			if ((target == 'clientarea.php?action=domains#tabRenew')) {
				$('#tableRenewalsList').DataTable()
				.columns.adjust()
				.responsive.recalc();
			}
			if ((target == 'clientarea.php?action=domains#tabOverview')) {
				$('#tableDomainsList').DataTable()
				.columns.adjust()
				.responsive.recalc();
			}
		});
	});
</script>