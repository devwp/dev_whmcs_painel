
jQuery(document).ready(function ($) 
{
		
	var btnclick = $('.btn-click');
	btnclick.on('click', function () 
	{
		activate($(this));
	});
	
	$(document).on('click', '.step-tabs li label', function() {
		$(this).tab('show');
	});
       $(document).delegate("select[name=country]",'change', function(){
           if(typeof statechange == 'function')
              statechange();
       });

});

function activate($this) 
{
	
	var self = $this, 
	activatedClass = 'btn-activated';

	if(!self.hasClass(activatedClass) ) {
		self.addClass(activatedClass);
		setTimeout( function() { self.removeClass( activatedClass ) }, 1000 );
	}
	
}


/* --- ONE STEP SLIDER FUNCTIONS ---- */

function scrollToEl(ele, pid) 
{
    $('#onepage-slider .slider').slider("value", ele-1);
}	

function slidCb(magic, ui, callback) 
{
    $('.slider-labels > p, .slider-features > li, .slider-packages > div').removeClass('current');
    $('.slider-labels > p:eq('+ui+'), .slider-features > li:eq('+ui+'), .slider-packages > div:eq('+ui+')').addClass('current');
		
    $('.slider-packages > div').each(function(i) 
    {				
        if(i <= ui) 
            $(this).addClass('prev');
				else 
					$(this).removeClass('prev');
			
    });
    
    if(typeof callback === 'function')
    {
        callback();
    }
		
}


/*  AJAX CART   */
//Check Domain 
$(function()
{
    $(document).delegate(".check-domain", "click", function(event){
        event.preventDefault();
				
				showHide('.domain-loader','on');
				showHide('#add-domain-result','off');
        
        cart = new MG_Cart();
        
        domainoption    =   $("ul.domainoptions li.active input[type=radio]").val();
        sld             =   $("#"+domainoption+"sld").val();
        tld             =   $("#"+domainoption+"tld").val();
        
        cart.checkDomainOptions(domainoption, sld, tld, function(data){
            showHide('.domain-loader','off', function(){
                $("#add-domain-result").html(data).promise().done(function() { showHide('#add-domain-result','on') });
            });
						
            if($("#domainfrm").length > 0)
            {
                cart.clear(function(){
                    pid = $("#onepage-slider .slider-packages .current").attr("data-pid");
                    $.post("cart.php", "ajax=1&a=add&pid="+pid+"&domainselect=1&"+$("#domainfrm").serialize(), function(data){
                        $("#conf-product").html(data);
                        viewCartSummary();
                        cart.configureDomains(function(data){
                            $("#domains-conf").html(data);
                        });
                    });
                });
            }
        });
    });
});


function showHide(element, status, callback, delay) 
{
	
	if(status == 'on') $(element).addClass('on');
	if(status == 'off') $(element).removeClass('on');
	
	if ((typeof callback === 'function') && delay) 
		setTimeout(callback(),delay);
	else if ((typeof callback === 'function') && !delay) 
		callback();
}

//Add Product To Cart
function addProduct(pid)
{
    showHide(".configuration-loader", "on");
    
    cart = new MG_Cart();
    cart.clear();
    cart.addProduct(pid, 0, 0, 0, 0, function(data){
        if(data)
        {
            $("#add-domain").html(data);
        }
        
        viewCartSummary();
    }); 
}

/*
function completedomain(sld, tld, domainoption)
{
    cart = new MG_Cart();
    cart.addProduct(0, sld, tld, domainoption, 1, function(data)
    {
        alert(data);
    });
}*/


// Update Product Configuration
$(function(){
    $(document).delegate("#product-configuration input[type=text]", "change", updateProductConfiguration);
    $(document).delegate("#product-configuration .amount input[type=text]", "change", updateProductConfiguration);
    $(document).delegate("#product-configuration input[type=password]", "change", updateProductConfiguration);
    $(document).delegate("#product-configuration input[type=checkbox]", "click", updateProductConfiguration);
    $(document).delegate("#product-configuration input[type=radio]", "click", updateProductConfiguration);
    $(document).delegate("#product-configuration select", "change", updateProductConfiguration);
});

function updateProductConfiguration()
{
					
    $.post("cart.php", "ajax=1&a=confproduct&"+$("#product-configuration").serialize(), function(data){
        viewCartSummary();
    });
}


//View Cart
function viewCartSummary(clientDetails)
{

     
    showHide('.cart-loader','on');

    cart = new MG_Cart();
    cart.viewSummary(
            function(data){
                $(".step-sidebar").html(data);
                showHide('.cart-loader','off');
                showHide(".configuration-loader", "off");
                if(typeof statechange == 'function')
                    statechange();
        },clientDetails
            );
}

//Clear Cart
$(function(){
    $(document).delegate('.btn-clearcart', 'click', function(event){
        event.preventDefault();
        cart = new MG_Cart();
        cart.clear(function(data){
            viewCartSummary();
        });
        $("#domains-conf").html("");
        $("#conf-product").html("");
        return false;
    });
});

//Update Domain Configuration
$(function(){
    $(document).delegate("#domains-conf form input", "change", updateDomainConfiguration);
    $(document).delegate("#domains-conf form select", "change", updateDomainConfiguration);
    $(document).delegate("#domains-conf form checkbox", "click", updateDomainConfiguration);
});

function updateDomainConfiguration()
{
    cart = new MG_Cart();
    cart.updateDomainsConfiguration($("#domains-conf form").serialize(), function(data){
        if(data) //Handle Error
        {
                
        }
        viewCartSummary();
    });
}

//Remove item
function removeItem(type, index)
{
    cart = new MG_Cart();
    if(type === 'p')
    {
        cart.clear(function(){
            viewCartSummary();
        });
        $("#domains-conf").html("");
        $("#conf-product").html("");
    }
    else if(type === 'd')
    {
        cart.deleteDomain(index, function(){
            viewCartSummary();
        }); 
        $("#domains-conf").html("");
    }
}

//View Cart
function viewCart()
{
   cart = new MG_Cart();
   cart.view(function(data){
       $("#cart-summary").html(data);
   });
}


//Promo Code
function applypromo()
{
    cart = new MG_Cart();
    cart.setPromoCode($("#promocode_val").val(), function(data){
        if(data) //handle error
        {
            $('#errorPopup .modal-body .alert ul').html(data);
            $('#errorPopup').modal('show');
        }
        else
        {
            viewCartSummary();
        }
    });
}

function removepromo()
{
    
    cart = new MG_Cart();
    cart.removePromoCode(function(data){
        viewCartSummary();
    });
}

//Complete Order
$(function(){
    $("#complete-order").click(function(event){
        event.preventDefault();
        cart = new MG_Cart();
        showHide('.order-loader','on');
        $.post("cart.php", "ajax=1&a=confproduct&configure=1"+$("#product-configuration").serialize(), function(data){
            if(data)
            {
                $('#errorPopup .modal-body .alert ul').html(data);
                $('#errorPopup').modal('show');
                showHide('.order-loader','off');
            }
            else
            {
                cart.fullCheckout($("#form-complete").serialize(), function(data2){
                    if(data2) //Error Handling
                    { 
                        $('#errorPopup .modal-body .alert ul').html(data2);
                        $('#errorPopup').modal('show');
                    }
                    else
                    {
                        window.location = 'cart.php?a=fraudcheck';
                    }
                    showHide('.order-loader','off');
                });
            }
        });
    });
});

//CC
function showCCForm()
{
    jQuery("#ccinputform").slideDown();
}
function hideCCForm()
{
    jQuery("#ccinputform").slideUp();
}
function useExistingCC()
{
    jQuery(".newccinfo").hide();
}
function enterNewCC()
{
    jQuery(".newccinfo").show();
}

/* State change update tax */
$(function(){
    
      //state
      $(document).delegate("#stateselect, #country", "change", function(event){
            event.preventDefault();
            if($("#stateselect").size()){
                var state = $("#stateselect").val();
            }else{
                var state = $("#stateinput").val();
            }
            var clientDetails ={
                "country": $("#country").val(),
                "state": state
            };
            viewCartSummary(clientDetails);
            
      });
});