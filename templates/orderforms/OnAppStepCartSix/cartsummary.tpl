<div class="step-cartsummary">
    <div class="cartsummary-title">
        <h3>{$LANG.ordersummary}</h3>
    </div>
    <div class="cartsummary-content">
        {*<div class="cartsummary-row template-logo">
            <img src="assets/images/template-logo-sample.jpg" alt="template logo" />
        </div>*}
         
        {foreach key=num item=product from=$products}
            <div class="cartsummary-row">
                <h5>{$product.productinfo.groupname} - {$product.productinfo.name}</h5>
                <div class="prod-price">
                        {$product.pricingtext}{if $product.proratadate}<br />({$LANG.orderprorata} {$product.proratadate}){/if}
              </div>
            </div>
            {if $product.configoptions}
                {foreach key=confnum item=configoption from=$product.configoptions}
                    <div class="cartsummary-row">
                        <div class="cartsummary-product">
                            <span>{$configoption.name}</span>
                            {if $configoption.type eq 1 || $configoption.type eq 2}
                                {$configoption.option}
                            {elseif $configoption.type eq 3}
                                {if $configoption.qty}
                                    {$LANG.yes}
                                {else}
                                    {$LANG.no}
                                {/if}
                                    {elseif $configoption.type eq 4}
                                {$configoption.qty} {$configoption.option}
                            {/if}
                        </div>
                        <div class="cartsummary-price">{$configoption.recurring}</div>
                    </div>
                {/foreach}
            {/if}
        {/foreach}
        {foreach key=num item=addon from=$addons}
                <div class="cartsummary-row">
                    <h5>{$addon.productname} </h5>
                    <ul class="cart-features">
                        <li><span>{$addon.productname}{if $addon.domainname} - {$addon.domainname}{/if}</span> {$addon.pricingtext}</li>
                    </ul>  
                    <div class="prod-price">
                        {$addon.pricingtext}
                    </div>
                </div>
            {/foreach}
        {foreach key=num item=domain from=$domains}
                <div class="cartsummary-row">
                    <h5>{if $domain.type eq "register"}{$LANG.orderdomainregistration}{else}{$LANG.orderdomaintransfer}{/if} </h5>
                    <ul class="cart-features">
                      <li><span>{$domain.domain} </span> {$domain.regperiod} {$LANG.orderyears}</li>
                      {if $domain.dnsmanagement}
                          <li><span>{$LANG.domaindnsmanagement} </span> {$LANG.yes}</li>
                      {/if}
                      {if $domain.emailforwarding}
                          <li><span>{$LANG.domainemailforwarding} </span> {$LANG.yes}</li>
                      {/if}
                      {if $domain.idprotection}
                          <li><span>{$LANG.domainidprotection} </span> {$LANG.yes}</li>
                      {/if}
                    </ul>
                    <div class="prod-price">
                        {$domain.price}
                    </div>
                </div>
       {/foreach}
       {foreach key=num item=domain from=$renewals}
                <div class="cartsummary-row">
                    <h5>{$LANG.domainrenewal}</h5>
                    <ul class="cart-features">
                      <li><span>{$domain.domain} </span> {$domain.regperiod} {$LANG.orderyears}</li>
                      {if $domain.dnsmanagement}
                          <li><span>{$LANG.domaindnsmanagement} </span> {$LANG.yes}</li>
                      {/if}
                      {if $domain.emailforwarding}
                          <li><span>{$LANG.domainemailforwarding} </span> {$LANG.yes}</li>
                      {/if}
                      {if $domain.idprotection}
                          <li><span>{$LANG.domainidprotection} </span> {$LANG.yes}</li>
                      {/if}
                    </ul>
                    <div class="prod-price">
                        {$domain.price}
                    </div>
                </div>
      {/foreach}
    </div>
      <div class="cartsummary-row cartsummary-subtotal">
            <ul>
                <li>{$LANG.ordersubtotal} <b class="font-blue">{$subtotal}</b></li>
                {if $taxrate}
                      <li>{$taxname} @ {$taxrate}%: &nbsp;<b class="font-blue">{$taxtotal}</b>
                {/if}
                {if $taxrate2}
                      <li>{$taxname2} @ {$taxrate2}%: &nbsp;<b class="font-blue">{$taxtotal2}</b>
                {/if}    
                {if $totalrecurringmonthly || $totalrecurringquarterly || $totalrecurringsemiannually || $totalrecurringannually || $totalrecurringbiennially || $totalrecurringtriennially}
                    <li><b>{$LANG.cartrecurringcharges}</b></li>
                    {if $totalrecurringmonthly}<li>{$totalrecurringmonthly} {$LANG.orderpaymenttermmonthly}</li>{/if}
                    {if $totalrecurringquarterly}<li>{$totalrecurringquarterly} {$LANG.orderpaymenttermquarterly}</li>{/if}
                    {if $totalrecurringsemiannually}<li>{$totalrecurringsemiannually} {$LANG.orderpaymenttermsemiannually}</li>{/if}
                    {if $totalrecurringannually}<li>{$totalrecurringannually} {$LANG.orderpaymenttermannually}</li>{/if}
                    {if $totalrecurringbiennially}<li>{$totalrecurringbiennially} {$LANG.orderpaymenttermbiennially}</li>{/if}
                    {if $totalrecurringtriennially}<li>{$totalrecurringtriennially} {$LANG.orderpaymenttermtriennially}</li>{/if}
                {/if}
            </ul>                    
        </div><!--/cartsummary-row-->
        <div class="cartsummary-total">
            <span><p>Total price:</p>
            {$total|replace:$currency.suffix:""}<small>{$currency.suffix}</small></span>
            <div id="promocode">
                {if $promotype}
                    {$LANG.cartpromo}: <b>{$promovalue}{if $promotype eq "Percentage"}%{elseif $promotype eq "Fixed Amount"}{/if} {$promorecurring}</b><br />
                    <a href="#" onclick="removepromo();return false"><i class="icon icon-remove"></i> {$LANG.cartremovepromo}</a>
                {else}
                    <input type="text" id="promocode_val" placeholder="{$LANG.cartenterpromo}" />
                    <button class="btn btn-click" onclick="applypromo()"><i class="icon icon-arrow-right"></i></button>
                {/if}
            </div>
            <a href="#" class="btn btn-red btn-clearcart">{$LANG.emptycart}</a>
        </div><!--/cartsummary-row-->
</div>
