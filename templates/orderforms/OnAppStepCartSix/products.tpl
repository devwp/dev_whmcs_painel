{include file="orderforms/OnAppStepCartSix/header.tpl"} 


<h2>{$LANG.choose_product}</h2>   
<div class="accordion" id="prodgroup">
      <div class="accordion-group">
        <div class="accordion-heading">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#prodgroup" href="#collapseOne">
                    <span>{$lang.current_product_group} <b>{$groupname}</b></span>
                    <span class="accordion-btn">
                        {$LANG.change} <i class="icon icon-plus"></i>
                    </span>
            </a>
        </div>
        <div id="collapseOne" class="accordion-body collapse in" style="height: 0px; width: 100%">
          <div class="accordion-inner">
                {foreach from=$productgroups item=group key=key}
                        <div class="template-row" {if $key===0} style="border-top: 1px solid #ebeef0;"{/if}> 
                                <label><input type="radio" name="gid" value="{$group.gid}" onclick="window.location='cart.php?gid={$group.gid}'"{if $group.gid eq $gid} checked{/if} /> {$group.name}</label> 
                        </div><!--/template-row-->
                {/foreach}
          </div>
        </div>
      </div>
    {if !$loggedin && $currencies}        
<div id="currencychooser">
{foreach from=$currencies item=curr}
    <div role="group" class="btn-group">
        <a class="btn btn-default" href="cart.php?gid={$gid}&currency={$curr.id}"><img src="assets/img/flags/{if $curr.code eq "AUD"}au{elseif $curr.code eq "CAD"}ca{elseif $curr.code eq "EUR"}eu{elseif $curr.code eq "GBP"}gb{elseif $curr.code eq "INR"}in{elseif $curr.code eq "JPY"}jp{elseif $curr.code eq "USD"}us{elseif $curr.code eq "ZAR"}za{else}na{/if}.png" border="0" alt="" /> {$curr.code}</a>
        
    </div>
{/foreach}
</div>
{/if}
</div> 

<div class="row">
    {foreach key=num item=product from=$products name=prod}
        {assign var="foreachindex" value=$smarty.foreach.prod.index+1}
        <div class="col-md-5 product-block span6 nomargin {if $smarty.foreach.prod.index % 2 == 0}nomargin{/if}">
            <div class="row-fluid">
                <div class="pb-body product-block-title">
                    <h4>{$product.name}</h4>
                    <ul>
                        {foreach from=$product.custom_description item=desc}
                            <li>{$desc[0]}: <b>{$desc[1]}</b></li>
                        {/foreach}
                    </ul>
                    <p>{$product.description_line}</p>
                </div>
                <div class="pb-right product-block-price">
                			<div class="pb-right-body">  
                    {if $product.paytype eq "free"}
                        <span class="pb-price">{$LANG.orderfree}</span>
                    {elseif $product.paytype eq "onetime"}
                        <span class="pb-price">
                            	{$product.pricing.onetime|replace:$LANG.orderpaymenttermmonthly:""|replace:$currency.suffix:""}
                            	{if $currency.suffix} <small>{$currency.suffix}</small> {/if}
                       </span>
                    {elseif $product.paytype eq "recurring"}
                        {if $product.pricing.monthly}
                            <span class="pb-price">
                            	{$product.pricing.monthly|replace:$LANG.orderpaymenttermmonthly:""|replace:$currency.suffix:""}
                            	{if $currency.suffix} <small>{$currency.suffix}</small> {/if}
                            </span>
                            <span class="pb-cycle">{$LANG.orderpaymenttermmonthly}</span>
                        {elseif $product.pricing.quarterly}
                            <span class="pb-price">
                            	{$product.pricing.quarterly|replace:$LANG.orderpaymenttermquarterly:""|replace:$currency.suffix:""}
                            	{if $currency.suffix} <small>{$currency.suffix}</small> {/if}
                            </span>
                            <span class="pb-cycle">{$LANG.orderpaymenttermquarterly}</span>
                        {elseif $product.pricing.semiannually}
                            <span class="pb-price">{$product.pricing.semiannually|replace:$LANG.orderpaymenttermsemiannually:""|replace:$currency.suffix:""}
                            	{if $currency.suffix} <small>{$currency.suffix}</small> {/if}
                            </span>
                            <span class="pb-cycle">{$LANG.orderpaymenttermsemiannually}</span>
                        {elseif $product.pricing.annually}
                            <span class="pb-price">
                            	{$product.pricing.annually|replace:$LANG.orderpaymenttermannually:""|replace:$currency.suffix:""}
                            	{if $currency.suffix} <small>{$currency.suffix}</small> {/if}
                            </span>
                            <span class="pb-cycle">{$LANG.orderpaymenttermannually}</span>
                        {elseif $product.pricing.biennially}
                            <span class="pb-price">
                            	{$product.pricing.biennially|replace:$LANG.orderpaymenttermbiennially:""|replace:$currency.suffix:""}
                            	{if $currency.suffix} <small>{$currency.suffix}</small> {/if}
                            </span>
                            <span class="pb-cycle">{$LANG.orderpaymenttermbiennially}</span>
                        {elseif $product.pricing.triennially}
                            <span class="pb-price">
                            	{$product.pricing.triennially|replace:$LANG.orderpaymenttermtriennially:""|replace:$currency.suffix:""}
                            	{if $currency.suffix} <small>{$currency.suffix}</small> {/if}
                            </span>
                            <span class="pb-cycle">{$LANG.orderpaymenttermtriennially}</span>
                        {/if}
                    {/if}
                    </div>
                    <a href="cart.php?a=add&pid={$product.pid}" class="btn btn-blue">Order Now <i class="icon icon-shopping-basket"></i></a>
                </div>
            </div>
        </div>
        {if $foreachindex % 2 == 0}<div class="clearfix"></div>{/if}
    {/foreach}
</div>
                
{include file="orderforms/OnAppStepCartSix/footer.tpl"} 