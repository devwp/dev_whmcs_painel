<form action=""  method="post" id="form-complete">
    <div class="widget">
        <div class="widget-row">
            <h3>{$LANG.yourdetails}</h3>
            {if !$loggedin}
            <ul class="step-tabs" id="clientinfo">
                <li class="active"><label data-target="#clientregister"><input type="radio" name="custtype" value="new" /><i class="icon icon-ok"></i>Register</label></li>
                <li><label data-target="#clientlogin" ><input type="radio" name="custtype" value="existing"/><i class="icon icon-ok"></i>Login</label></li>
            </ul>
            {else}
                <input type="hidden" name="custtype" value="existing" />
            {/if}
            
            {if !$loggedin}<div class="tab-content">{/if}
                <div class="tab-pane fade in active" id="clientregister">
                    <div class="row ">
                        <div class="span-half">

                             <div class="control-group">
                                <div class="control-label">{$LANG.clientareafirstname}</div>
                                <div class="controls">
                                    {if $loggedin}
                                        <b>{$clientsdetails.firstname}</b>
                                    {else}
                                        <input type="text" name="firstname" value="{$clientsdetails.firstname}" class="form-control" />
                                    {/if}
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="control-label">{$LANG.clientarealastname}</div>
                                <div class="controls">
                                    {if $loggedin}
                                        <b>{$clientsdetails.lastname}</b>
                                    {else}
                                        <input type="text" name="lastname" value="{$clientsdetails.lastname}"  class="form-control"/>
                                    {/if}
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="control-label">{$LANG.clientareaemail}</div>
                                <div class="controls">
                                    {if $loggedin}
                                        <b>{$clientsdetails.email}</b>
                                    {else}
                                        <input type="text" name="email"  value="{$clientsdetails.email}" class="form-control"/>
                                    {/if}
                                </div>
                            </div>
                                
                            {if !$loggedin}
                            <div class="control-group">
                                <div class="control-label">{$LANG.clientareapassword}</div>
                                <div class="controls">
                                    <input type="password" name="password" id="newpw" value="{$password}" class="form-control"/>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="control-label">{$LANG.clientareaconfirmpassword}</div>
                                <div class="controls">
                                    <input type="password" name="password2" value="{$password2}" class="form-control"/>
                                </div>
                            </div>
                            {/if}
                            
                            {if $securityquestions && !$loggedin}
                                <div class="control-group">
                                    <div class="control-label">{$LANG.clientareasecurityquestion}</div>
                                    <div class="controls">
                                        <select name="securityqid" >
                                            {foreach key=num item=question from=$securityquestions}
                                                <option value={$question.id}>{$question.question}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="control-lable">{$LANG.clientareasecurityanswer}</div>
                                    <div class="controls">
                                        <input type="password" name="securityqans" value="" class="form-control">
                                    </div>
                                </div>
                            {/if}    
                            
                        </div>

                        <div class="span-half">

                            <div class="control-group">
                                <div class="control-label">{$LANG.clientareacompanyname}</div>
                                <div class="controls">
                                    {if $loggedin}
                                        <b>{$clientsdetails.companyname}</b>
                                    {else}
                                        <input type="text" name="companyname" value="{$clientsdetails.companyname}"   class="form-control"/>
                                    {/if}
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="control-label">{$LANG.clientareacountry}</div>
                                <div class="controls">
                                    {if $loggedin}
                                        <b>{$clientsdetails.country}</b>
                                    {else}
                                        {$clientcountrydropdown}
                                    {/if}
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="control-label">{$LANG.clientareastate}</div>
                                <div class="controls">
                                    {if $loggedin}
                                        <b>{$clientsdetails.state}</b>
                                    {else}
                                        <input type="text" name="state" id="stateinput" value="{$clientsdetails.state}"  class="form-control" />
                                    {/if}
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="control-label">{$LANG.clientareacity}</div>
                                <div class="controls">
                                    {if $loggedin}
                                        <b>{$clientsdetails.city}</b>
                                    {else}
                                        <input type="text" name="city" value="{$clientsdetails.city}"  class="form-control" />
                                    {/if}
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="control-label">{$LANG.clientareapostcode}</div>
                                <div class="controls">
                                    {if $loggedin}
                                        <b>{$clientsdetails.postcode}</b>
                                    {else}
                                        <input type="text" name="postcode" value="{$clientsdetails.postcode}"  class="form-control"/>
                                    {/if}
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="control-label">{$LANG.clientareaaddress1}</div>
                                <div class="controls">
                                    {if $loggedin}
                                        <b>{$clientsdetails.address1}</b>
                                    {else}
                                        <input type="text" name="address1" value="{$clientsdetails.address1}"  class="form-control" />
                                    {/if}
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="control-label">{$LANG.clientareaaddress2}</div>
                                <div class="controls">
                                    {if $loggedin}
                                        <b>{$clientsdetails.address2}</b>
                                    {else}
                                        <input type="text" name="address2" value="{$clientsdetails.address2}"   class="form-control"/>
                                    {/if}
                                </div>
                            </div>
                                
                            <div class="control-group">
                                <div class="control-label">{$LANG.clientareaphonenumber}</div>
                                <div class="controls">
                                    {if $loggedin}
                                        <b>{$clientsdetails.phonenumber}</b>
                                    {else}
                                        <input type="text" name="phonenumber" value="{$clientsdetails.phonenumber}"  class="form-control" />
                                    {/if}
                                </div>
                            </div>
                                
                            {if $customfields}
                                {foreach key=num item=customfield from=$customfields}
                                    <div class="control-group">
                                        <div class="control-label">{$customfield.name}</div>
                                        <div class="controls">
                                            {$customfield.input}
                                        </div>
                                    </div>
                                {/foreach}
                            {/if}



        </div>
    </div><!--/row-->
    </div>
    {if !$loggedin}
    <div class="tab-pane fade" id="clientlogin">
        <div class="row">
            <div class="span-half">

                <div class="control-group">
                    <div class="control-label">{$LANG.clientareaemail}</div>
                    <div class="controls">
                        <input type="text" name="loginemail"  class="form-control"/>
                    </div>
                </div>

                <div class="control-group">
                    <div class="control-label">{$LANG.clientareapassword}</div>
                    <div class="controls">
                        <input type="password" name="loginpw"  class="form-control" />
                    </div>
                </div>

            </div>
        </div>

    </div>
    {/if}

    </div>
   {if !$loggedin} </div>{/if}
    </div>
    <div class="widget">
        <div class="widget-row form-horizontal">
            <h3>{$LANG.orderpaymentmethod}</h3>
            <ul class="step-tabs" id="paymenttype">
                {foreach key=num item=gateway from=$gateways}
                    <li  {if $selectedgateway eq $gateway.sysname} class="active"{/if}><label onclick="{if $gateway.type eq "CC"}showCCForm(){else}hideCCForm(){/if}"><input type="radio" name="paymentmethod" value="{$gateway.sysname}" {if $selectedgateway eq $gateway.sysname} checked{/if}/><i class="icon icon-ok"></i>{$gateway.name}</label></li>
                {/foreach}
            </ul>
  
            <div id="ccinputform"{if $selectedgatewaytype neq "CC"} style="display:none;"{/if}>
                                    {if $clientsdetails.cclastfour}
                            <div class="control-group">
                                <label><input type="radio" name="ccinfo" value="useexisting" id="useexisting" onclick="useExistingCC()"{if $clientsdetails.cclastfour} checked{else} disabled{/if} /> {$LANG.creditcarduseexisting}{if $clientsdetails.cclastfour} ({$clientsdetails.cclastfour}){/if}</label><br />
                        <label><input type="radio" name="ccinfo" value="new" id="new" onclick="enterNewCC()"{if !$clientsdetails.cclastfour || $ccinfo eq "new"} checked{/if} /> {$LANG.creditcardenternewcard}</label>
                               
                            </div>
                        {else}
                            <input type="hidden" name="ccinfo" value="new" />
                        {/if}
                        <div class="control-group newccinfo {if $clientsdetails.cclastfour && $ccinfo neq "new"}hidden{/if}">
                            <div class="control-label">{$LANG.creditcardcardtype}</div>
                            <div class="controls">
                                <select name="cctype" class="input-small">
                                {foreach key=num item=cardtype from=$acceptedcctypes}
                                    <option{if $cctype eq $cardtype} selected{/if}>{$cardtype}</option>
                                {/foreach}
                                </select>
                            </div>
                        </div>
                        
                        <div class="control-group newccinfo"{if $clientsdetails.cclastfour && $ccinfo neq "new"} style="display:none;"{/if}>
                            <div class="control-label">{$LANG.creditcardcardnumber}</div>
                            <div class="controls"><input type="text" name="ccnumber" size="30" value="{$ccnumber}" autocomplete="off" /></div>
                        </div>
                        <div class="control-group newccinfo ccrow"{if $clientsdetails.cclastfour && $ccinfo neq "new"} style="display:none;"{/if}>
                            <div class="control-label">{$LANG.creditcardcardexpires}</div>
                            <div class="controls">
                                <select name="ccexpirymonth" id="ccexpirymonth" class="newccinfo input-small">
                                {foreach from=$months item=month}
                                <option{if $ccexpirymonth eq $month} selected{/if}>{$month}</option>
                                {/foreach}
                                </select> 
                                <span>/</span>
                                <select name="ccexpiryyear" class="newccinfo input-small">
                                {foreach from=$expiryyears item=year}
                                    <option{if $ccexpiryyear eq $year} selected{/if}>{$year}</option>
                                {/foreach}
                                </select>
                            </div>
                        </div>
                        {if $showccissuestart}
                            <div class="control-group newccinfo ccrow"{if $clientsdetails.cclastfour && $ccinfo neq "new"} style="display:none;"{/if}>			
                                <div class="control-label">{$LANG.creditcardcardstart}</div>
                                <div class="controls">
                                    <select name="ccstartmonth" id="ccstartmonth" class="newccinfo">
                                    {foreach from=$months item=month}
                                        <option{if $ccstartmonth eq $month} selected{/if}>{$month}</option>
                                    {/foreach}
                                    </select>
                                    <span>/</span>
                                     <select name="ccstartyear" class="newccinfo">
                                    {foreach from=$startyears item=year}
                                        <option{if $ccstartyear eq $year} selected{/if}>{$year}</option>
                                    {/foreach}
                                    </select>
                                </div>
                            </div>
                           <div class="control-group newccinfo"{if $clientsdetails.cclastfour && $ccinfo neq "new"} style="display:none;"{/if}>
                                <div class="control-label">{$LANG.creditcardcardissuenum}</div>
                                <div class="controls">
                                    <input type="text" name="ccissuenum" value="{$ccissuenum}" size="5" maxlength="3" />
                                </div>
                           </div>
                         {/if}
                        <div class="control-group">
                            <div class="control-label">{$LANG.creditcardcvvnumber}</div>
                            <div class="controls">
                                <input type="text" name="cccvv" value="{$cccvv}" size="5" autocomplete="off" class="input-small" />
                                <a href="#" onclick="window.open('images/ccv.gif','','width=280,height=200,scrollbars=no,top=100,left=100');return false">{$LANG.creditcardcvvwhere}</a>
                            </div>
                        </div>
                    </div>

            </div>

        </div>
    </div>

    <div class="widget">
        <div class="widget-row">      
            {if $shownotesfield}
                <h3>{$LANG.ordernotes}</h3>
                <div class="control-group">
                    <textarea name="notes" class="textarea-full" placeholder="{$LANG.ordernotesdescription}"></textarea>
                </div>
            {/if}
            {if $accepttos}
                <div class="control-group">
                    <label><input type="checkbox" name="accepttos" id="accepttos"/> {$LANG.ordertosagreement} <a href="{$tosurl}" target="_blank">{$LANG.ordertos}</a></label> 	
                </div>
            {/if}
        </div>
    </div>
</div>
</form>