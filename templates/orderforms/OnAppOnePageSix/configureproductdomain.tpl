<div class="widget">
    <div class="widget-row">
        <h3>
            Choose Domain Name
            <div class="loader configuration-loader" style="float: right"></div>
        </h3>
        <ul class="step-tabs domainoptions" style="padding-left:0px;">
            <li class="active"><label data-target="#domainregister"><input type="radio" name="domainoption" value="register"/><i class="icon icon-ok"></i>{$LANG.registerdomain}</label></li>
            <li><label data-target="#domaintrasnfer"><input type="radio" name="domainoption" value="transfer" /><i class="icon icon-ok"></i>{$LANG.transferdomain}</label></li>
            <li><label data-target="#owndomain"><input type="radio" name="domainoption" value="owndomain" /><i class="icon icon-ok"></i>Already Have</label></li>
        </ul><!--step-tabs-->
        <div class="tab-content">
            <div class="tab-pane fade in active" id="domainregister">
                <form class="domain-form">
                    <span class="www">www.</span>
                    <input id="registersld" type="text" class="sld" value="{$sld}"/>
                    <select id="registertld" class="tld form-control" style="display:inline;">
                        {foreach key=num item=listtld from=$registertlds}
                        <option value="{$listtld}"{if $listtld eq $tld} selected="selected"{/if}>{$listtld}</option>
                        {/foreach}
                    </select>
                  <button class="btn btn-green check-domain"><i class="icon icon-search"></i>Search</button>
                </form>
            </div><!--domain register -->
            <div class="tab-pane fade" id="domaintrasnfer">
                <form class="domain-form">
                    <span class="www">www.</span>
                        <input id="transfersld" type="text" class="sld"/>
                        <select id="transfertld" class="tld">
                            {foreach key=num item=listtld from=$transfertlds}
                                <option value="{$listtld}"{if $listtld eq $tld} selected="selected"{/if}>{$listtld}</option>
                            {/foreach}
                        </select>
                    <button class="btn btn-green check-domain"><i class="icon icon-search"></i>Search</button>
                </form>
              </div><!-- domaintransfer -->
              <div class="tab-pane fade" id="owndomain">
                  <form class="domain-form">
                      <span class="www">www.</span>
                      <input type="text" class="sld" id="owndomainsld" />
                      <input type="text" class="tld" id="owndomaintld" />
                      <button class="btn btn-green check-domain"><i class="icon icon-search"></i>Search</button>
                  </form>
              </div>
        </div>
        <div class="loader domain-loader"></div> 
        <div id="add-domain-result"></div>               
    </div>
</div>
            