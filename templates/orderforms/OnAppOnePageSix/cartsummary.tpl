<div class="step-cartsummary">
    <div class="cartsummary-title">
        <h3>{$LANG.ordersummary}<div class="loader cart-loader"></div> </h3>
    </div>
    <div class="cartsummary-content">
        {if $cartitems}
            {literal}
                <script type="text/javascript">
                    $(function(){
                        $("#complete-order").show();
                    });
                </script>
            {/literal}
            
            {foreach key=num item=product from=$products}
                <div class="cartsummary-row">
                    <h5>{$product.productinfo.groupname} - {$product.productinfo.name} {if $product.domain}({$product.domain}){/if} <a href="#" onclick="removeItem('p','{$num}');return false;"><i class="icon icon-remove"></i></a></h5>
                    {if $product.configoptions}
                        <ul class="cart-features">
                        {foreach key=confnum item=configoption from=$product.configoptions}
                            <li>
                                <span>{$configoption.name}</span>
                                {if $configoption.type eq 1 || $configoption.type eq 2}
                                    {$configoption.option}
                                {elseif $configoption.type eq 3}
                                    {if $configoption.qty}
                                        {$LANG.yes}
                                    {else}
                                        {$LANG.no}
                                    {/if}
                                {elseif $configoption.type eq 4}
                                    {$configoption.qty} {$configoption.option}
                                {/if}
                            </li>
                        {/foreach}
                        {foreach item=customfields from=$product.customfields}
                            {if $customfields.adminonly == ''}
                                <li>
                                    <span>{$customfields.name}</span>
                                    {$customfields.value}
                                </li>
                            {/if}
                        {/foreach}
                        </ul>
                    {/if}
                    <div class="prod-price">
                        {$product.pricingtext}{if $product.proratadate}<br />({$LANG.orderprorata} {$product.proratadate}){/if}
                    </div>
                </div>
            {/foreach}
            
            
            {foreach key=num item=addon from=$addons}
                <div class="cartsummary-row">
                    <h5>{$addon.productname} <a href="#"><i class="icon icon-remove" href="#" onclick="removeItem('a','{$num}');return false"></i></a></h5>
                    <ul class="cart-features">
                        <li><span>{$addon.productname}{if $addon.domainname} - {$addon.domainname}{/if}</span> {$addon.pricingtext}</li>
                    </ul>  
                    <div class="prod-price">
                        {$addon.pricingtext}
                    </div>
                </div>
            {/foreach}
            
            {foreach key=num item=domain from=$domains}
                <div class="cartsummary-row">
                    <h5>{if $domain.type eq "register"}{$LANG.orderdomainregistration}{else}{$LANG.orderdomaintransfer}{/if} <a href="#" onclick="removeItem('d','{$num}');return false"><i class="icon icon-remove"></i></a></h5>
                    <ul class="cart-features">
                      <li><span>{$domain.domain} </span> {$domain.regperiod} {$LANG.orderyears}</li>
                      {if $domain.dnsmanagement}
                          <li><span>{$LANG.domaindnsmanagement} </span> {$LANG.yes}</li>
                      {/if}
                      {if $domain.emailforwarding}
                          <li><span>{$LANG.domainemailforwarding} </span> {$LANG.yes}</li>
                      {/if}
                      {if $domain.idprotection}
                          <li><span>{$LANG.domainidprotection} </span> {$LANG.yes}</li>
                      {/if}
                    </ul>
                    <div class="prod-price">
                        {$domain.price}
                    </div>
                </div>
            {/foreach}
            
            {foreach key=num item=domain from=$renewals}
                <div class="cartsummary-row">
                    <h5>{$LANG.domainrenewal} <a href="#" href="#" onclick="removeItem('r','{$num}');return false"><i class="icon icon-remove"></i></a></h5>
                    <ul class="cart-features">
                      <li><span>{$domain.domain} </span> {$domain.regperiod} {$LANG.orderyears}</li>
                      {if $domain.dnsmanagement}
                          <li><span>{$LANG.domaindnsmanagement} </span> {$LANG.yes}</li>
                      {/if}
                      {if $domain.emailforwarding}
                          <li><span>{$LANG.domainemailforwarding} </span> {$LANG.yes}</li>
                      {/if}
                      {if $domain.idprotection}
                          <li><span>{$LANG.domainidprotection} </span> {$LANG.yes}</li>
                      {/if}
                    </ul>
                    <div class="prod-price">
                        {$domain.price}
                    </div>
                </div>
            {/foreach}
        {else}
            {literal}
                <script type="text/javascript">
                    $(function(){
                        $("#complete-order").hide();
                    });
                </script>
            {/literal}
        {/if}
        <div class="cartsummary-row cartsummary-subtotal">
            <ul>
                <li>{$LANG.ordersubtotal} <b class="font-blue">{$subtotal}</b></li>
                {if $taxrate}
                      <li>{$taxname} @ {$taxrate}%: &nbsp;<b class="font-blue">{$taxtotal}</b>
                {/if}
                {if $taxrate2}
                      <li>{$taxname2} @ {$taxrate2}%: &nbsp;<b class="font-blue">{$taxtotal2}</b>
                {/if}    
                {if $totalrecurringmonthly || $totalrecurringquarterly || $totalrecurringsemiannually || $totalrecurringannually || $totalrecurringbiennially || $totalrecurringtriennially}
                    <li><b>{$LANG.cartrecurringcharges}</b></li>
                    {if $totalrecurringmonthly}<li>{$totalrecurringmonthly} {$LANG.orderpaymenttermmonthly}</li>{/if}
                    {if $totalrecurringquarterly}<li>{$totalrecurringquarterly} {$LANG.orderpaymenttermquarterly}</li>{/if}
                    {if $totalrecurringsemiannually}<li>{$totalrecurringsemiannually} {$LANG.orderpaymenttermsemiannually}</li>{/if}
                    {if $totalrecurringannually}<li>{$totalrecurringannually} {$LANG.orderpaymenttermannually}</li>{/if}
                    {if $totalrecurringbiennially}<li>{$totalrecurringbiennially} {$LANG.orderpaymenttermbiennially}</li>{/if}
                    {if $totalrecurringtriennially}<li>{$totalrecurringtriennially} {$LANG.orderpaymenttermtriennially}</li>{/if}
                {/if}
            </ul>                    
        </div><!--/cartsummary-row-->
        <div class="cartsummary-total">
            <span><p>{$LANG.ordertotalduetoday}:</p>
            {$total|replace:$currency.suffix:""}<small>{$currency.suffix}</small></span>
            <div id="promocode">
                {if $promotype}
                    {$LANG.cartpromo}: <b>{$promovalue}{if $promotype eq "Percentage"}%{elseif $promotype eq "Fixed Amount"}{/if} {$promorecurring}</b><br />
                    <a href="#" onclick="removepromo();return false"><i class="icon icon-remove"></i> {$LANG.cartremovepromo}</a>
                {else}
                    <input type="text" id="promocode_val" placeholder="{$LANG.cartenterpromo}" />
                    <button class="btn btn-click" onclick="applypromo()"><i class="icon icon-arrow-right"></i></button>
                {/if}
            </div>
            <a href="#" class="btn btn-red btn-clearcart">{$LANG.emptycart}</a>
        </div><!--/cartsummary-row-->
    </div>
</div>

{literal}
<script>
	/* --- RIGHT SIDEBAR--- */

	$('.step-cartsummary').affix({
			offset: {
				top: $('.step-cartsummary').offset().top,
                                bottom: $('body').height() - $('.step-main').offset().top - $('.step-main').outerHeight() + 34
			}
	});

</script>
{/literal}