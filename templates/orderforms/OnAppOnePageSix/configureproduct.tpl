<div class="widget">
    <form class="widget-row form-horizontal" method="post" action="" id="product-configuration">
        <h3>
            Product Configuration 
            <div class="loader configuration-loader" style="float: right"></div>
        </h3>
        <input type="hidden" name="configure" value="true" />
        <input type="hidden" name="i" value="{$i}" />
        
        {if $pricing.type eq "recurring"}
            <div class="control-group">
                <div class="control-label">{$LANG.orderbillingcycle}</div>
                <div class="controls">
                    <select name="billingcycle" class="form-control">
                        {if $pricing.monthly}<option value="monthly"{if $billingcycle eq "monthly"} selected="selected"{/if}>{$pricing.monthly}</option>{/if}
                        {if $pricing.quarterly}<option value="quarterly"{if $billingcycle eq "quarterly"} selected="selected"{/if}>{$pricing.quarterly}</option>{/if}
                        {if $pricing.semiannually}<option value="semiannually"{if $billingcycle eq "semiannually"} selected="selected"{/if}>{$pricing.semiannually}</option>{/if}
                        {if $pricing.annually}<option value="annually"{if $billingcycle eq "annually"} selected="selected"{/if}>{$pricing.annually}</option>{/if}
                        {if $pricing.biennially}<option value="biennially"{if $billingcycle eq "biennially"} selected="selected"{/if}>{$pricing.biennially}</option>{/if}
                        {if $pricing.triennially}<option value="triennially"{if $billingcycle eq "triennially"} selected="selected"{/if}>{$pricing.triennially}</option>{/if}
                    </select>
                </div>
            </div>
        {/if}
        
        {if $productinfo.type eq "server"}
            <div class="control-group">
                <div class="control-label">{$LANG.serverhostname}</div>
                <div class="controls">
                    <input type="text" name="hostname" size="15" value="{$server.hostname}" class="form-control" />
                    <div class="help">eg. server1(.yourdomain.com)</div>
                </div>
            </div>
            <div class="control-group">
                <div class="control-label">{$LANG.serverns1prefix}</div>
                <div class="controls">
                    <input type="text" name="ns1prefix" size="10" value="{$server.ns1prefix}"  class="form-control"/>
                    <div class="help">eg. ns1(.yourdomain.com)</div>
                </div>
            </div>
            <div class="control-group">
                <div class="control-label">{$LANG.serverns2prefix}</div>
                <div class="controls">
                    <input type="text" name="ns2prefix" size="10" value="{$server.ns2prefix}"  class="form-control"/> 
                    <div class="help">eg. ns2(.yourdomain.com)</div>
                </div>
            </div>
            <div class="control-group">
                <div class="control-label">{$LANG.serverrootpw}</div>
                <div class="controls">
                    <input type="password" name="rootpw" size="20" value="{$server.rootpw}" class="form-control" />
                </div>
            </div>
        {/if}
        
        {if $configurableoptions}
            {foreach from=$configurableoptions item=configoption}
                <div class="control-group">
                    <div class="control-label">{$configoption.optionname}</div>
                    <div class="controls">
                        {if $configoption.optiontype eq 1}
                            <select name="configoption[{$configoption.id}]" class="form-control">
                                {foreach key=num2 item=options from=$configoption.options}
                                    <option value="{$options.id}"{if $configoption.selectedvalue eq $options.id} selected="selected"{/if}>{$options.name}</option>
                                {/foreach}
                            </select>
                        {elseif $configoption.optiontype eq 2}
                            {foreach key=num2 item=options from=$configoption.options}
                                <label><input type="radio" name="configoption[{$configoption.id}]" value="{$options.id}" /> {$options.name}</label>
                            {/foreach}
                        {elseif $configoption.optiontype eq 3}
                            <label><input type="checkbox" name="configoption[{$configoption.id}]" value="1"{if $configoption.selectedqty} checked{/if} /> {$configoption.options.0.name}</label>
                        {elseif $configoption.optiontype eq 4}
                            {if $configoption.qtymaximum}
                                <div id="slider_confop{$configoption.id}" class="slider slider-gradient"></div>
                                <div class="amount amount-inline">
                                    <input type="text" name="configoption[{$configoption.id}]" id="confop{$configoption.id}" value="{if $configoption.selectedqty}{$configoption.selectedqty}{else}{$configoption.qtyminimum}{/if}" />
                                    <span style="margin-left: 1px;">{$configoption.selectedoption}</span>
                                </div>
                                <script type="text/javascript">
                                    {literal}
                                        $(function(){
                                            $('#slider_confop{/literal}{$configoption.id}{literal}').slider(
                                            {
                                                range:  "min",
                                                value:  {/literal}{if $configoption.selectedqty}{$configoption.selectedqty}{else}{$configoption.qtyminimum}{/if}{literal},
                                                min:    {/literal}{$configoption.qtyminimum}{literal},
                                                max:    {/literal}{$configoption.qtymaximum}{literal},
                                                slide: function( event, ui )
                                                {
                                                    $(this).parent().find('.amount input').val( ui.value );
                                                },
                                                stop: updateProductConfiguration	
                                            });

                                            $('#slider_confop{/literal}{$configoption.id}{literal}').next( ".amount input" ).change(function() 
                                            {
                                                $(this).closest('.controls').find('.slider').slider( "value", $(this).val() );
                                            });	

                                            $('#slider_confop{/literal}{$configoption.id}{literal}').next( ".amount input" ).each(function() 
                                            {
                                                $(this).val($(this).closest('.controls').find('.slider').slider('value'));
                                            });
                                        });
                                    {/literal}
                                </script>
                        {else}
                            <input type="text" name="configoption[{$configoption.id}]" value="{$configoption.selectedqty}" class="input-small" class="form-control" /> x {$configoption.options.0.name}
                        {/if}
                    {/if}
                </div>
            </div>
        {/foreach}
    {/if}
    {if $addons}
        {foreach from=$addons item=addon}
            <div class="control-group">
                <b><label><input type="checkbox" name="addons[{$addon.id}]" id="a{$addon.id}"{if $addon.status} checked{/if}/>{$addon.name}</label></b>
                {$addon.description} ({$addon.pricing})             
                
                </div>
        {/foreach}
    {/if}
    {if $customfields}
        {foreach key=num item=customfield from=$customfields}
            <div class="control-group">
                <div class="control-label">{$customfield.name}</div>
                <div class="controls">
                    {$customfield.input}
                </div>
            </div>
        {/foreach}
    {/if}
    </form>
</div>