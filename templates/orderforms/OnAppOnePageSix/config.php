<?php

$orderconf = array
(
    'directpidstep1' => true,
);

$langfile = $CONFIG['Language'];
if(file_exists(dirname(__FILE__).DIRECTORY_SEPARATOR.'language'.DIRECTORY_SEPARATOR.$langfile.'.php'))
{
    require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'language'.DIRECTORY_SEPARATOR.$langfile.'.php';
}
else
{
    require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'language'.DIRECTORY_SEPARATOR.'english'.'.php';
}
global $smarty;
$smarty->assign('lang', $lang);