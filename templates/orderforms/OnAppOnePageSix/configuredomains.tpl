<div class="widget">
    <div class="widget-row">
        <h3>{$LANG.cartdomainsconfig}</h3>
        <form action="" method="post">
            {foreach key=num item=domain from=$domains}
                <h4>
                    {$domain.domain}
                    <div class="loader configuration-loader" style="float: right"></div>
                </h4>
                {if $domain.dnsmanagement}
                    <div class="control-group">
                        <label><input type="checkbox" name="dnsmanagement[{$num}]" />{$LANG.domaindnsmanagement}  <span class="font-green">({$domain.dnsmanagementprice})</span></label>
                    </div>
                {/if}
                {if $domain.emailforwarding}
                    <div class="control-group">
                        <label><input type="checkbox" name="emailforwarding[{$num}]" />{$LANG.domainemailforwarding} <span class="font-green">({$domain.emailforwardingprice})</span></label>
                    </div>
                {/if}
                {if $domain.idprotection}
                    <div class="control-group">
                        <label><input type="checkbox" name="idprotection[{$num}]"/>{$LANG.domainidprotection} <span class="font-blue">({$domain.idprotectionprice})</span></label>
                    </div>
                {/if}
                
                {if $domain.eppenabled}
                    <div class="control-group form-horizontal">
                        <div class="control-label">{$LANG.domaineppcode}</div>
                        <div class="controls">
                            <input type="text" name="epp[{$num}]" value="{$domain.eppvalue}" />
                        </div>
                    </div>
                {/if}
                {foreach key=domainfieldname item=domainfield from=$domain.fields}
                    <div class="control-group form-horizontal">
                        <div class="control-label">{$domainfieldname}</div>
                        <div class="controls">
                            {$domainfield}
                        </div>
                    </div>
                {/foreach}
                {if $domain.pricing}
                <div class="control-group form-horizontal">
                    <div class="control-label">Billing Cycle</div>
                    <div class="controls">
                        <select class="input-medium" class="billingcycle">
                            {foreach from=$domain.pricing key=period item=regoption}
                                {if $regoption[$domain.type]}
                                    <option value="{$period}">{$period} {$LANG.orderyears} @ {$regoption[$domain.type]}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </div>
                </div>
                 {/if}
            {/foreach}
            {if $atleastonenohosting}
                <h4>{$LANG.domainnameservers}</h4>
                <table>
                <tr class="rowcolor1"><td class="fieldlabel">{$LANG.cartnameserverchoice}:</td><td class="fieldarea"><label><input type="radio" name="customns" id="usedefaultns" onclick="showcustomns()" checked /> {$LANG.cartnameserverchoicedefault}</label><br /><label><input type="radio" name="customns" id="usecustomns" onclick="showcustomns()" /> {$LANG.cartnameserverchoicecustom}</label></td></tr>
                <tr class="rowcolor2 hiddenns"><td class="fieldlabel">{$LANG.domainnameserver1}:</td><td class="fieldarea"><input type="text" name="domainns1" size="40" value="{$domainns1}" /></td></tr>
                <tr class="rowcolor1 hiddenns"><td class="fieldlabel">{$LANG.domainnameserver2}:</td><td class="fieldarea"><input type="text" name="domainns2" size="40" value="{$domainns2}" /></td></tr>
                <tr class="rowcolor2 hiddenns"><td class="fieldlabel">{$LANG.domainnameserver3}:</td><td class="fieldarea"><input type="text" name="domainns3" size="40" value="{$domainns3}" /></td></tr>
                <tr class="rowcolor1 hiddenns"><td class="fieldlabel">{$LANG.domainnameserver4}:</td><td class="fieldarea"><input type="text" name="domainns4" size="40" value="{$domainns4}" /></td></tr>
                <tr class="rowcolor2 hiddenns"><td class="fieldlabel">{$LANG.domainnameserver5}:</td><td class="fieldarea"><input type="text" name="domainns5" size="40" value="{$domainns5}" /></td></tr>
                </table>
            {/if}
        </form>
    </div>
</div>

