
<link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'></head>

<link type="text/css" rel="stylesheet" href="templates/orderforms/{$carttpl}/assets/css/font-awesome.css" charset="utf-8"/>
<link type="text/css" rel="stylesheet" href="templates/orderforms/{$carttpl}/assets/css/style.css" charset="utf-8"/>
    
<script type="text/jscript" src="templates/orderforms/{$carttpl}/assets/js/application.js"></script>

<script type="text/javascript" src="templates/orderforms/{$carttpl}/MG_Cart/MG_Cart.js"></script>
<script type="text/javascript" src="includes/jscript/statesdropdown.js"></script>


<div class="container">
    

    <div id="onepage" class="step-blue">
    
        <!-- Modal -->
        <div class="modal fade" id="errorPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="font-red" style="margin-top: 1px;"><i class="icon icon-exclamation-sign"></i> Error</h3>
              </div>
              <div class="modal-body">
                    <div class="alert alert-error">
                                        <ul></ul>
                    </div>
              </div>
              <div class="modal-footer">
                <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
              </div>
            </div>
          </div>
        </div>

        <div id="stepcontent" class="content">