{if $itColorScheme eq "blue" or $itColorScheme eq "blue-solid" or $itColorScheme eq "blue-white"}
	<script>
	    jQuery('input.icheck').iCheck({
	        checkboxClass: 'icheckbox_flat-blue',
	        radioClass: 'iradio_flat-blue',
	        increaseArea: '20%'
	    });
	</script>
{else}
	<script>
	    jQuery('input.icheck').iCheck({
	        checkboxClass: 'icheckbox_flat-green',
	        radioClass: 'iradio_flat-green',
	        increaseArea: '20%'
	    });
	</script>
{/if}