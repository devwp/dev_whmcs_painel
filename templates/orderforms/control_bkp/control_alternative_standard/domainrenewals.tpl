{include file="orderforms/control_standard/common.tpl"}
<!--main content start-->
<section id="main-content" class="cart">
	<!-- Display mobile sidebar alternative if applicable -->
	{if $itCartSidebarMobileDisplay eq "enabled"}
		<div class="row cat-col-row visible-xs visible-sm">
			<div class="col-md-12">
				{include file="orderforms/control_standard/sidebar-categories-collapsed.tpl"}
			</div>
		</div>
	{/if}
	<!-- Display page title -->
	<div class="row">
		<div class="col-md-12">
			<!--breadcrumbs start -->
			{if $itCartSidebarDisplay eq "enabled"}
				<div style="position: absolute; right: 15px; margin-top: 22px;">
				    <button type="button" class="btn btn-default btn-sm hidden-xs hidden-sm" id="btnShowSidebar">
				        <i class="fa fa-arrow-circle-left"></i>
				        {$LANG.showMenu}
				    </button>
				</div>
			{/if}
			{include file="$template/includes/breadcrumb.tpl"}
			<!--breadcrumbs end -->
			<h1 class="h1">{$LANG.domainrenewals}</h1>
			<small class="res-left pull-right" style="margin: 12px 12px 20px 12px;">{$LANG.domainrenewdesc}</small>
		</div>
	</div>
	<!-- Display sidebar layout if applicable -->
	{if $itCartSidebarDisplay eq "enabled"}
		<div class="row cart-main-column">
			<div id="internal-content" class="col-md-12 pull-md-left">
				{/if}
				<div class="row">
					<div class="col-md-12">
			            <form method="post" action="cart.php?a=add&renewals=true">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">	
										{$LANG.domainrenewals}
									</h3>
								</div>
				                <table class="table table-hover renewals">
				                    <thead>
				                        <tr>
				                            <th width="20"></th>
				                            <th>{$LANG.orderdomain}</th>
				                            <th>{$LANG.domainstatus}</th>
				                            <th>{$LANG.domaindaysuntilexpiry}</th>
				                            <th></th>
				                        </tr>
				                    </thead>
				                    <tbody>
				                        {foreach from=$renewals item=renewal}
				                            <tr>
				                                <td>
				                                    {if !$renewal.pastgraceperiod && !$renewal.beforerenewlimit}
				                                        <input type="checkbox" class="icheck" name="renewalids[]" value="{$renewal.id}" />
				                                    {/if}
				                                </td>
				                                <td>
				                                    {$renewal.domain}
				                                </td>
				                                <td>
				                                    <span class="label status status-{$renewal.status|lower}">{$renewal.status}</span>
				                                </td>
				                                <td>
				                                    {if $renewal.daysuntilexpiry > 30}
				                                        <span class="text-success">
				                                            {$renewal.daysuntilexpiry} {$LANG.domainrenewalsdays}
				                                        </span>
				                                    {elseif $renewal.daysuntilexpiry > 0}
				                                        <span class="text-warning">
				                                            {$renewal.daysuntilexpiry} {$LANG.domainrenewalsdays}
				                                        </span>
				                                    {else}
				                                        <span class="text-danger">
				                                            {$renewal.daysuntilexpiry*-1} {$LANG.domainrenewalsdaysago}
				                                        </span>
				                                    {/if}
				                                    {if $renewal.ingraceperiod}
				                                        <br />
				                                        <span class="text-danger">
				                                            {$LANG.domainrenewalsingraceperiod}
				                                        </span>
				                                    {/if}
				                                </td>
				                                <td>
				                                    {if $renewal.beforerenewlimit}
				                                        <span class="textred">
				                                            {$LANG.domainrenewalsbeforerenewlimit|sprintf2:$renewal.beforerenewlimitdays}
				                                        </span>
				                                    {elseif $renewal.pastgraceperiod}
				                                        <span class="textred">
				                                            {$LANG.domainrenewalspastgraceperiod}
				                                        </span>
				                                    {else}
				                                        <select name="renewalperiod[{$renewal.id}]" class="form-control">
				                                            {foreach from=$renewal.renewaloptions item=renewaloption}
				                                                <option value="{$renewaloption.period}">
				                                                    {$renewaloption.period} {$LANG.orderyears} @ {$renewaloption.price}
				                                                </option>
				                                            {/foreach}
				                                        </select>
				                                    {/if}
				                                </td>
				                            </tr>
				                        {foreachelse}
				                            <tr class="carttablerow">
				                                <td colspan="5">{$LANG.domainrenewalsnoneavailable}</td>
				                            </tr>
				                        {/foreach}
				                    </tbody>
				                </table>
						        <div class="panel-footer">
							        <button type="submit" class="btn btn-primary btn-3d">
				                        <i class="fa fa-shopping-cart"></i>
				                        {$LANG.ordernowbutton}
				                    </button>
						        </div>
							</div>
			            </form>    
		            </div>
				</div>
				{if $itCartSidebarDisplay eq "enabled"}
			</div>
			<div class="col-md-3 pull-md-right whmcs-sidebar hidden-xs hidden-sm sidebar-secondary cart-sidebar">
				{include file="orderforms/control_standard/sidebar-categories.tpl"}
			</div>
			<div class="clearfix"></div>
		</div>
	{/if}
	<div class="clearfix"></div>
</section>	    
{include file="orderforms/control_standard/icheck.tpl"}