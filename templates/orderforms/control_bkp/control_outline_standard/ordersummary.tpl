<table class="table order-summary">
	<tbody>
		<tr class="no-border">
			<td>
				<span class="item-title">
					<strong>{$producttotals.productinfo.name}</strong> 
				</span>
				({$producttotals.productinfo.groupname})
			</td>
			<td class="text-right">
				<span>{$producttotals.pricing.baseprice}</span>
			</td>
		</tr>
		{foreach $producttotals.configoptions as $configoption}
		    {if $configoption}
				<tr class="no-border">
					<td>
		            	<span>&nbsp;&raquo; {$configoption.name}: {$configoption.optionname}</span>
					</td>
					<td class="text-right">
			            <span>{$configoption.recurring}{if $configoption.setup} + {$configoption.setup} {$LANG.ordersetupfee}{/if}</span>
					</td>
				</tr>
		    {/if}
		{/foreach}
		{foreach $producttotals.addons as $addon}
			<tr class="dashed">
				<td>
	            	<span>+ {$addon.name}</span>
				</td>
				<td class="text-right">
		            <span>{$addon.recurring}</span>
				</td>
		    </tr>
		{/foreach}
		{if $producttotals.pricing.setup || $producttotals.pricing.addons}
	        {if $producttotals.pricing.setup}
				<tr>
		            <td>
		            	<span>{$LANG.cartsetupfees}</span>
		            </td>
		            <td class="text-right">
			            <span>{$producttotals.pricing.setup}</span>
		            </td>
	        	</tr>
	        {/if}
	        
	        {if $producttotals.pricing.tax1}
	           	<tr>
		            <td>
		            	<span>{$carttotals.taxname} @ {$carttotals.taxrate}%</span>
		            </td>
		            <td class="text-right">
			            <span>{$producttotals.pricing.tax1}</span>
		            </td>
	           	</tr>
	        {/if}
	        {if $producttotals.pricing.tax2}
	            <tr>
		            <td>
		            	<span>{$carttotals.taxname2} @ {$carttotals.taxrate2}%</span>
		            </td>
		            <td class="text-right">
			            <span>{$producttotals.pricing.tax2}</span>
		            </td>
	            </tr>
	        {/if}
		{/if}
		<tr class="totals">
			<td>
				<span><strong>{$LANG.ordertotalduetoday}</strong></span>
			</td>
			<td class="text-right">
				<span><strong>{$producttotals.pricing.totaltoday}</strong></span>
			</td>
		</tr>
		{if $producttotals.pricing.recurring}											
			{foreach from=$producttotals.pricing.recurringexcltax key=cycle item=recurring}
	            <tr>
		            <td>
		            	<span>{$cycle} {$LANG.cartrecurringcharges}</span>
		            </td>
		            <td class="text-right">
			            <span>{$recurring}</span>
		            </td>
	            </tr>
	        {/foreach}
	    {/if}	
	</tbody>
</table>








