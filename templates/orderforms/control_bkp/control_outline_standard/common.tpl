{include file="orderforms/$carttpl/cart-options.tpl"}
{if $itColorScheme eq "blue" or $itColorScheme eq "blue-solid" or $itColorScheme eq "blue-white"}
	<link rel="stylesheet" type="text/css" href="templates/orderforms/{$carttpl}/icheck/skins/flat/blue.css" />
{else}
	<link rel="stylesheet" type="text/css" href="templates/orderforms/{$carttpl}/icheck/skins/flat/green.css" />
{/if}
<script type="text/javascript" src="{$BASE_PATH_JS}/icheck.js"></script>
<script type="text/javascript" src="templates/orderforms/{$carttpl}/base.js"></script>
<script>
jQuery(document).ready(function () {
    jQuery('#btnShowSidebar').click(function () {
        if (jQuery('.cart-sidebar').is(":visible")) {
            jQuery('.cart-main-column').css('right','0');
            jQuery('.cart-sidebar').fadeOut();
            jQuery('#btnShowSidebar').html('<i class="fa fa-arrow-circle-left"></i> {$LANG.showMenu}');
        } else {
            jQuery('.cart-sidebar').fadeIn();
            jQuery('.cart-main-column').css('right','300px');
            jQuery('#btnShowSidebar').html('<i class="fa fa-arrow-circle-right"></i> {$LANG.hideMenu}');
        }
    });
});
</script>