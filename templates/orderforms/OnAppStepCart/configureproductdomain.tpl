{include file="orderforms/OnAppStepCart/header.tpl"} 
<h2>{$LANG.cartproductconfig}</h2>

<div class="widget">
    <form method="post" action="{$smarty.server.PHP_SELF}?a=add&pid={$pid}">
        <div class="widget-row">
            <h3>Select Domain Name:</h3>
            
            {foreach from=$passedvariables key=name item=value}
                <input type="hidden" name="{$name}" value="{$value}" />
            {/foreach}
        

            <ul class="step-tabs domainoptions">
                <li {if $smarty.post.domainoption == 'register' || $smarty.post.domainoption == ''}class="active"{/if}><label data-target="#domainregister"><input type="radio" name="domainoption" value="register" {if $smarty.post.domainoption == 'register' || $smarty.post.domainoption == ''}checked="checked"{/if} /><i class="icon icon-ok"></i>{$LANG.registerdomain}</label></li>
                <li {if $smarty.post.domainoption == 'transfer'}class="active"{/if}><label data-target="#domaintrasnfer"><input type="radio" name="domainoption" value="transfer" {if $smarty.post.domainoption == 'transfer'}checked="checked"{/if}/><i class="icon icon-ok"></i>{$LANG.transferdomain}</label></li>
                <li {if $smarty.post.domainoption == 'owndomain'}class="active"{/if}><label data-target="#owndomain"><input type="radio" name="domainoption" value="owndomain" {if $smarty.post.domainoption == 'owndomain'}checked="checked"{/if}/><i class="icon icon-ok"></i>Already Have</label></li>
            </ul><!--step-tabs-->
        
            <div class="tab-content">
                <div class="tab-pane fade {if $smarty.post.domainoption == 'register' || $smarty.post.domainoption == ''}in active{/if} domain-form" id="domainregister">

                        <span class="www">www.</span>
                        <input name="sld[0]" id="registersld" type="text" class="sld" value="{$sld}"/>
                        <select name="tld[0]" id="registertld" class="tld">
                            {foreach key=num item=listtld from=$registertlds}
                            <option value="{$listtld}"{if $listtld eq $tld} selected="selected"{/if}>{$listtld}</option>
                            {/foreach}
                        </select>
                      <button class="btn btn-green check-domain"><i class="icon icon-search"></i>{$LANG.search}</button>

                </div><!--domain register -->

                <div class="tab-pane fade {if $smarty.post.domainoption == 'transfer'}in active {/if} domain-form" id="domaintrasnfer">


                    <span class="www">www.</span>
                        <input name="sld[1]" id="transfersld" type="text" class="sld" value="{$sld}"/>
                        <select name="tld[1]" id="transfertld" class="tld">
                            {foreach key=num item=listtld from=$transfertlds}
                                <option value="{$listtld}"{if $listtld eq $tld} selected="selected"{/if}>{$listtld}</option>
                            {/foreach}
                        </select>
                    <button class="btn btn-green check-domain"><i class="icon icon-search"></i>{$LANG.search}</button>


                  </div><!-- domaintransfer -->

                  <div class="tab-pane fade {if $smarty.post.domainoption == 'owndomain'}in active{/if} domain-form" id="owndomain">

                      <span class="www">www.</span>
                      <input name="sld[2]" type="text" class="sld" id="owndomainsld" value="{$sld}"/>
                      <input name="tld[2]" type="text" class="tld" id="owndomaintld" value="{$tld}"/>
                      <button class="btn btn-green check-domain"><i class="icon icon-search"></i>{$LANG.search}</button>

                  </div>
            </div> 
        
            <div id="add-domain-result">


            </div>    
        </div>
    </form>
</div>

{if $errormessage}
    <div class="alert alert-error">
        {$errormessage|replace:'<li>':' &nbsp;#&nbsp; '}
    </div>
{/if}

{if $availabilityresults}
  <div class="widget">
      <form method="post" action="{$smarty.server.PHP_SELF}?a=add&pid={$pid}" id="onapp_domains">
        <div class="widget-row">

        <h3>{$LANG.choosedomains}</h3>


        <table class="table">
          <thead>
            <tr>
              <th>{$LANG.domainname}</th>
              <th>{$LANG.domainstatus}</th>
              <th>{$LANG.domainmoreinfo}</th>
            </tr>
          </thead>
          <tbody>
            {foreach key=num item=result from=$availabilityresults}
            <tr {if $result.status != $searchvar}class="domain-unavailable"{/if}>
              <td><label><input type="checkbox" name="domains[]" value="{$result.domain}"{if $num eq 0 && $result.status == $searchvar} checked{/if} {if $result.status != $searchvar}disabled{/if}/>{$result.domain}</label></td>
              <td>{if $result.status eq $searchvar}<span class="label label-success">{$LANG.domainavailable}</span>{else}<span class="label label-important">{$LANG.domainunavailable}</span>{/if}</td>
              <td>
                {if $result.regoptions}
                  <select name="domainsregperiod[{$result.domain}]">
                    {foreach key=period item=regoption from=$result.regoptions}
                      {if $regoption.$domainoption}
                        <option value="{$period}">{$period} {$LANG.orderyears} @ {$regoption.$domainoption}</option>
                      {/if}
                    {/foreach}
                  </select>
                {/if}
              </td>
            </tr>
            {/foreach}
          </tbody>
        </table>

        <button class="btn btn-blue btn-large">{$LANG.ordercontinuebutton|replace:">>":""}</button>

        </div>
        <input type="hidden" value="" name="domainoption" id="onapp_domainoption">
        <input type="hidden" value="" name="sld[0]" id="onapp_sld">
        <input type="hidden" value="" name="tld[0]" id="onapp_tld">

    </form>
  </div><!--/widget-->
{/if}

{literal}
      <script type="text/javascript">
            $(document).ready(function() {
                  $("#onapp_domains").submit(function(){
                        var domainoption = $("input[name*='domainoption']:checked").val();
                        $("#onapp_domainoption").val(domainoption);
                        $("#onapp_sld").val($("#"+domainoption+"sld").val());
                        $("#onapp_tld").val($("#"+domainoption+"tld").val());
                  });
            });
      </script>
{/literal}

{include file="orderforms/OnAppStepCart/footer.tpl"} 