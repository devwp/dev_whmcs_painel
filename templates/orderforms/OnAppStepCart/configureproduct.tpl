{include file="orderforms/OnAppStepCart/header.tpl"} 
{literal}
    <script type="text/javascript">
        //Set Custom Template
        MG_Cart.custom_template =   'OnAppStepCart';
    </script>
{/literal}
<div id="steptitle">
    {section name=heading start=1 loop=4}
        <div class="onapp-heading">
            <h1>{$heading_title}</h1>
            <p>{$heading_description}</p>
        </div>
    {/section}
</div><!--/steptitle-->
            
<div id="stepbar">
    <ul class="steps steps{$number_of_steps}">
        {assign var="counter" value=1}
        {foreach from=$steps key=step_key item=step}
            <li {if $step_key == $first_step}class="step-current"{/if}><a onclick="return false;" href="#{$step_key}" data-step=".{$step_key}" ><em class="step-number">{$counter}.</em><em class="step-icon"><i class="icon icon-ok"></i></em><span>{$step.name}</span>{$step.description}</a></li>
          {assign var="counter" value=$counter+1}
        {foreachelse}
            {if $productinfo.type eq "server"}
                <li class="step-current"><a onclick="return false;" href="#onapp-step-server-configuration" data-step=".onapp-step-server-configuration"><em class="step-number">1.</em><em class="step-icon"><i class="icon icon-ok"></i></em><span>Configuration {$LANG.default_configuration}</span>{$LANG.default_description}</a></li>
            {/if}
        {/foreach}
        
        <li {if $first_step == 'onapp-step-confirmation'} class="step-current" {/if}><a onclick="return false;" href="#onapp-step-confirmation" data-step=".onapp-step-confirmation" ><em class="step-number">{$number_of_steps}.</em><em class="step-icon"><i class="icon icon-ok"></i></em><span>Confirmation {$LANG.confirmation}</span></a></li>
        
    </ul>
</div><!--/stepbar-->
            

<div id="stepcontent" class="content" >
    <div class="modal hide fade" id="errorPopup">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 class="font-red"><i class="icon icon-exclamation-sign"></i> Error</h3>
        </div>
        <div class="modal-body">
            <div class="alert alert-error">
                <ul></ul>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
        </div>
    </div><!-- error popup -->
    
    <div class="step-main">
        <form action=""  method="post" class="" id="product-configuration">
            <input type="hidden" name="configure" value="true">
            {foreach from=$steps key=step_key item=step}
                <div class="widget display-step {$step_key}">
                    <div class="widget-row form-horizontal">
                        <h3>{$step.name}</h3>
                            {foreach key=num item=configoption from=$step.configurableoptions}
                                {if $configoption.optionname == 'spacer'}
                                    <hr />
                                {else}
                                    {if $configoption.optiontype eq 1}
                                        <div class="control-group">
                                            <div class="control-label">
                                                {$configoption.optionname}
                                            </div>
                                            <div class="controls">
                                                <select name="configoption[{$configoption.id}]">
                                                    {foreach key=num2 item=options from=$configoption.options}
                                                        <option value="{$options.id}"{if $configoption.selectedvalue eq $options.id} selected="selected"{/if}>{$options.name}</option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                        </div>
                                    {elseif $configoption.optiontype eq 2}
                                        <div class="accordion" id="templates">
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#templates" href="#collapse{$num}">
                                                        {$configoption.optionname}
                                                        <span class="accordion-btn">
                                                            Show <i class="icon icon-plus"></i>
                                                        </span>
                                                    </a>
                                                </div>
                                                <div id="collapse{$num}" class="accordion-body collapse">
                                                        <div class="accordion-inner">
                                                    {foreach key=num2 item=options from=$configoption.options}
                                                            <div class="template-row">
                                                                <label>
                                                                    <input type="radio" name="configoption[{$configoption.id}]" value="{$options.id}"{if $configoption.selectedvalue eq $options.id} checked="checked"{/if}>
                                                                    <span class="template-name">{$options.name}</span>
                                                                    <span class="template-price">{$options.recurring|formatCurrency}</span>
                                                                </label>
                                                            </div><!--/template-row-->
                                                    {/foreach}   
                                                        </div>
                                                </div>
                                            </div><!--/accordion group-->           
                                        </div><!--/accordion-->


                                    {elseif $configoption.optiontype eq 3}
                                        <div class="control-group">
                                            <div class="control-label">
                                                {$configoption.optionname}
                                            </div>
                                            <div class="controls">
                                                <input type="checkbox" name="configoption[{$configoption.id}]" value="1"{if $configoption.selectedqty} checked{/if}>
                                            </div>
                                        </div>
                                    {elseif $configoption.optiontype eq 4}
                                        <div class="control-group">
                                            <div class="control-label">
                                                {$configoption.optionname}
                                            </div>
                                            <div class="controls">
                                                <div id="slider_confop{$configoption.id}" class="slider slider-gradient"></div>
                                                <div class="amount amount-inline">
                                                    <input type="text" name="configoption[{$configoption.id}]" id="confop{$configoption.id}" value="{if $configoption.selectedqty}{$configoption.selectedqty}{else}{$configoption.qtyminimum}{/if}" />
                                                    <span></span>
                                                </div>
                                                <script type="text/javascript">
                                                    {literal}
                                                        $(function(){
                                                            $('#slider_confop{/literal}{$configoption.id}{literal}').slider(
                                                            {
                                                                range:  "min",
                                                                value:  {/literal}{if $configoption.selectedqty}{$configoption.selectedqty}{else}{$configoption.qtyminimum}{/if}{literal},
                                                                min:    {/literal}{$configoption.qtyminimum}{literal},
                                                                max:    {/literal}{$configoption.qtymaximum}{literal},
                                                                slide: function( event, ui )
                                                                {
                                                                    $(this).parent().find('.amount input').val( ui.value );
                                                                },
                                                                stop: updateProductConfiguration	
                                                            });

                                                            $('#slider_confop{/literal}{$configoption.id}{literal}').next( ".amount input" ).change(function() 
                                                            {
                                                                $(this).closest('.controls').find('.slider').slider( "value", $(this).val() );
                                                            });	

                                                            $('#slider_confop{/literal}{$configoption.id}{literal}').next( ".amount input" ).each(function() 
                                                            {
                                                                $(this).val($(this).closest('.controls').find('.slider').slider('value'));
                                                            });
                                                        });
                                                    {/literal}
                                                </script>
                                            </div>	
                                        </div>
                                    {/if}
                                {/if}
                            {/foreach}
                        </div><!--/widget-row-->
                    </div><!--/widget-->
                    {if $step_key == $last_step && $productinfo.type eq "server"}
                        <div class="widget display-step {$step_key}">
                            <div class="widget-row form-horizontal">
                                <h3>{$LANG.cartconfigserver}</h3>
                                <div class="control-group">
                                    <div class="control-label">{$LANG.serverhostname}</div>
                                    <div class="controls">
                                        <input type="text" name="hostname" size="15" value="{$server.hostname}" />
                                        <div class="help">eg. server1(.yourdomain.com)</div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="control-label">{$LANG.serverns1prefix}</div>
                                    <div class="controls">
                                        <input type="text" name="ns1prefix" size="10" value="{$server.ns1prefix}" />
                                        <div class="help">eg. ns1(.yourdomain.com)</div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="control-label">{$LANG.serverns2prefix}</div>
                                    <div class="controls">
                                        <input type="text" name="ns2prefix" size="10" value="{$server.ns2prefix}" /> 
                                        <div class="help">eg. ns2(.yourdomain.com)</div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="control-label">{$LANG.serverrootpw}</div>
                                    <div class="controls">
                                        <input type="password" name="rootpw" size="20" value="{$server.rootpw}" />
                                    </div>
                                </div>
                                {if $customfields}
                                    {foreach key=num item=customfield from=$customfields}
                                        <div class="control-group">
                                            <div class="control-label">{$customfield.name}</div>
                                            <div class="controls">
                                                {$customfield.input}
                                            </div>
                                        </div>
                                    {/foreach}
                                {/if}
                                
                            </div><!--/widget-row-->
                        </div><!--/widget-->
                    {elseif $step_key == $last_step && $customfields}
                        <div class="widget display-step {$step_key} onapp-step-server-configuration">
                            <div class="widget-row form-horizontal">
                                <h3>{$LANG.cartconfigserver}</h3>
                                {if $customfields}
                                    {foreach key=num item=customfield from=$customfields}
                                        <div class="control-group">
                                            <div class="control-label">{$customfield.name}</div>
                                            <div class="controls">
                                                {$customfield.input}
                                            </div>
                                        </div>
                                    {/foreach}
                                {/if}
                            </div>
                        </div><!--/widget-->
                    {/if}
            {foreachelse}
                {if $productinfo.type eq "server"}
                    <div class="widget display-step {$step_key} onapp-step-server-configuration">
                        <div class="widget-row form-horizontal">
                            <h3>{$LANG.cartconfigserver}</h3>
                            <div class="control-group">
                                <div class="control-label">{$LANG.serverhostname}</div>
                                <div class="controls">
                                    <input type="text" name="hostname" size="15" value="{$server.hostname}" />
                                    <div class="help">eg. server1(.yourdomain.com)</div>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="control-label">{$LANG.serverns1prefix}</div>
                                <div class="controls">
                                    <input type="text" name="ns1prefix" size="10" value="{$server.ns1prefix}" />
                                    <div class="help">eg. ns1(.yourdomain.com)</div>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="control-label">{$LANG.serverns2prefix}</div>
                                <div class="controls">
                                    <input type="text" name="ns2prefix" size="10" value="{$server.ns2prefix}" /> 
                                    <div class="help">eg. ns2(.yourdomain.com)</div>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="control-label">{$LANG.serverrootpw}</div>
                                <div class="controls">
                                    <input type="password" name="rootpw" size="20" value="{$server.rootpw}" />
                                </div>
                            </div>
                         {* Configurable options*}
                                        {if $configurableoptions}
            {foreach from=$configurableoptions item=configoption}
                <div class="control-group">
                    <div class="control-label">{$configoption.optionname}</div>
                    <div class="controls">
                        {if $configoption.optiontype eq 1}
                            <select name="configoption[{$configoption.id}]">
                                {foreach key=num2 item=options from=$configoption.options}
                                    <option value="{$options.id}"{if $configoption.selectedvalue eq $options.id} selected="selected"{/if}>{$options.name}</option>
                                {/foreach}
                            </select>
                        {elseif $configoption.optiontype eq 2}
                            {foreach key=num2 item=options from=$configoption.options}
                                <label><input type="radio" name="configoption[{$configoption.id}]" value="{$options.id}" /> {$options.name}</label>
                            {/foreach}
                        {elseif $configoption.optiontype eq 3}
                            <label><input type="checkbox" name="configoption[{$configoption.id}]" value="1"{if $configoption.selectedqty} checked{/if}/> {$configoption.options.0.name}</label>
                        {elseif $configoption.optiontype eq 4}
                            {if $configoption.qtymaximum}
                                <div id="slider_confop{$configoption.id}" class="slider slider-gradient"></div>
                                <div class="amount amount-inline">
                                    <input type="text" name="configoption[{$configoption.id}]" id="confop{$configoption.id}" value="{if $configoption.selectedqty}{$configoption.selectedqty}{else}{$configoption.qtyminimum}{/if}" />
                                    <span></span>
                                </div>
                                <script type="text/javascript">
                                    {literal}
                                        $(function(){
                                            $('#slider_confop{/literal}{$configoption.id}{literal}').slider(
                                            {
                                                range:  "min",
                                                value:  {/literal}{if $configoption.selectedqty}{$configoption.selectedqty}{else}{$configoption.qtyminimum}{/if}{literal},
                                                min:    {/literal}{$configoption.qtyminimum}{literal},
                                                max:    {/literal}{$configoption.qtymaximum}{literal},
                                                slide: function( event, ui )
                                                {
                                                    $(this).parent().find('.amount input').val( ui.value );
                                                },
                                                stop: updateProductConfiguration	
                                            });

                                            $('#slider_confop{/literal}{$configoption.id}{literal}').next( ".amount input" ).change(function() 
                                            {
                                                $(this).closest('.controls').find('.slider').slider( "value", $(this).val() );
                                            });	

                                            $('#slider_confop{/literal}{$configoption.id}{literal}').next( ".amount input" ).each(function() 
                                            {
                                                $(this).val($(this).closest('.controls').find('.slider').slider('value'));
                                            });
                                        });
                                    {/literal}
                                </script>
                        {else}
                            <input type="text" name="configoption[{$configoption.id}]" value="{$configoption.selectedqty}" class="input-small" /> x {$configoption.options.0.name}
                        {/if}
                    {/if}
                </div>
            </div>
        {/foreach}
    {/if}
    {if $addons}
        {foreach from=$addons item=addon}
            <div class="control-group">
                <b><label><input type="checkbox" name="addons[{$addon.id}]" id="a{$addon.id}"{if $addon.status} checked{/if}/>{$addon.name}</label></b>
                {$addon.description} ({$addon.pricing})             
                
                </div>
        {/foreach}
    {/if}

                        {if $customfields}
                            {foreach key=num item=customfield from=$customfields}
                                <div class="control-group">
                                    <div class="control-label">{$customfield.name}</div>
                                    <div class="controls">
                                        {$customfield.input}
                                    </div>
                                </div>
                            {/foreach}
                        {/if}
                        {* Configurable options*}
                        
                        </div><!--/widget-row-->
                    </div><!--/widget-->
                {elseif $customfields}
                    <div class="widget display-step {$step_key} onapp-step-server-configuration">
                        <div class="widget-row form-horizontal">
                            <h3>{$LANG.cartconfigserver}</h3>
                            {if $customfields}
                                {foreach key=num item=customfield from=$customfields}
                                    <div class="control-group">
                                        <div class="control-label">{$customfield.name}</div>
                                        <div class="controls">
                                            {$customfield.input}
                                        </div>
                                    </div>
                                {/foreach}
                            {/if}
                        </div>
                    </div><!--/widget-->
                {/if}
            {/foreach}
        </form>
        <div id="cart-summary"></div><!--  cart summary -->
    </div><!--/step-main-->
    
    <div class="step-sidebar">
    </div><!--/step-sidebar-->
 
    <div class="clearfix"></div>
                
    <div class="widget step-actions">
        <div class="widget-row">
              <div class="loader order-loader pull-right" style="margin-left:10px; margin-right:-20px; display:none;"></div>
              
            <a style="display: none; float:left" href="#" class="btn btn-prevstep"><i class="icon  icon-chevron-left"></i> Previous Step</a>
            <a href="#" class="btn btn-nextstep" {if $first_step == 'onapp-step-confirmation'} style="display: none" {/if}>Next Step<i class="icon  icon-chevron-right"></i></a>
            <p>{$LANG.ordersecure} (<strong>{$ipaddress}</strong>) {$LANG.ordersecure2}</p>
            <a id="complete-order" style="display: none" href="#" class="btn btn-green btn-complete btn-click pull-right">{$LANG.completeorder} <i class="icon icon-ok"></i>
            </a>
           
        </div>
    </div>
</div>
           
    
        
{literal}
    <script type="text/javascript">
      jQuery(function(){
         viewCartSummary();
        // showStep({/literal}{$first_step}{literal});
      });
  </script>
{/literal}
{if $first_step == 'onapp-step-confirmation'}
    {literal}
        <script type="text/javascript">
            $(function(){
                confirmationStep();
            });
        </script>
    {/literal}
{/if}

{include file="orderforms/OnAppStepCart/footer.tpl"}