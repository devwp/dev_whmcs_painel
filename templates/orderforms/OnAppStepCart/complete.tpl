{include file="orderforms/OnAppStepCart/header.tpl"} 

<h2>{$LANG.orderconfirmation}</h2>

<div class="widget">
	<div class="widget-row">
  
    <p>{$LANG.orderreceived}</p>
    
    <br/>
    
    <div class="alert alert-info">
      <p><strong>{$LANG.ordernumberis} {$ordernumber}</strong></p>
    </div>
    
    <p>{$LANG.orderfinalinstructions}</p>
    
    {if $invoiceid && !$ispaid}
    <div class="alert alert-important">{$LANG.ordercompletebutnotpaid}</div>
    <p align="center"><a href="viewinvoice.php?id={$invoiceid}" target="_blank">{$LANG.invoicenumber}{$invoiceid}</a></p>
    {/if}
    
    {foreach from=$addons_html item=addon_html}
    <div style="margin:15px 0 15px 0;">{$addon_html}</div>
    {/foreach}
    
    {if $ispaid}
    <!-- Enter any HTML code which needs to be displayed once a user has completed the checkout of their order here - for example conversion tracking and affiliate tracking scripts -->
    {/if}
    
    <br/>
    
    <p align="center"><a href="clientarea.php" class="btn btn-green">{$LANG.ordergotoclientarea}</a></p>
	</div>
</div>
{include file="orderforms/OnAppStepCart/footer.tpl"} 