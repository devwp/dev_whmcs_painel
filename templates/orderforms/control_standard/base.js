if (typeof localTrans === 'undefined') {
    localTrans = function (phraseId, fallback)
    {
        if (typeof _localLang !== 'undefined') {
            if (typeof _localLang[phraseId] !== 'undefined') {
                if (_localLang[phraseId].length > 0) {
                    return _localLang[phraseId];
                }
            }
        }

        return fallback;
    }
}

jQuery(document).ready(function(){

    if (jQuery('#inputCardNumber').length) {
        jQuery('#inputCardNumber').payment('formatCardNumber');
        jQuery('#inputCardCVV').payment('formatCardCVC');
        jQuery('#inputCardStart').payment('formatCardExpiry');
        jQuery('#inputCardExpiry').payment('formatCardExpiry');
    }

    jQuery("#btnCompleteProductConfig").click(function() {
        var btnOriginalText = jQuery(this).html();
        jQuery(this).find('i').removeClass('fa-arrow-circle-right').addClass('fa-spinner fa-spin');
        jQuery.post("cart.php", 'ajax=1&a=confproduct&' + jQuery("#frmConfigureProduct").serialize(),
            function(data) {
                if (data) {
                    jQuery("#btnCompleteProductConfig").html(btnOriginalText);
                    jQuery("#containerProductValidationErrorsList").html(data);
                    jQuery("#containerProductValidationErrors").removeClass('hidden').show();
                    // scroll to error container if below it
                    if (jQuery(window).scrollTop() > jQuery("#containerProductValidationErrors").offset().top) {
                        jQuery('html, body').scrollTop(jQuery("#containerProductValidationErrors").offset().top - 15);
                    }
                } else {
                    window.location = 'cart.php?a=confdomains';
                }
            }
        );
    });

    jQuery("#productConfigurableOptions").on('ifChecked', 'input', function() {
        recalctotals();
    });
    jQuery("#productConfigurableOptions").on('ifUnchecked', 'input', function() {
        recalctotals();
    });
    jQuery("#productConfigurableOptions").on('change', 'select', function() {
        recalctotals();
    });
    
    
    jQuery("#ProductAddons").on('ifChecked', 'input', function() {
        recalctotals();
    });
    jQuery("#ProductAddons").on('ifUnchecked', 'input', function() {
        recalctotals();
    });
    jQuery("#ProductAddons").on('change', 'select', function() {
        recalctotals();
    });

    jQuery(".addon-products").on('click', '.panel-addon', function(e) {
        e.preventDefault();
        var $activeAddon = jQuery(this);
        if ($activeAddon.hasClass('panel-addon-selected')) {
            $activeAddon.find('input[type="checkbox"]').iCheck('uncheck');
        } else {
            $activeAddon.find('input[type="checkbox"]').iCheck('check');
        }
    });
    jQuery(".addon-products").on('ifChecked', '.panel-addon input', function(event) {
        var $activeAddon = jQuery(this).parents('.panel-addon');
        $activeAddon.addClass('panel-addon-selected');
        $activeAddon.find('input[type="checkbox"]').iCheck('check');
        $activeAddon.find('.panel-add').html('<i class="fa fa-shopping-cart"></i> '+localTrans('addedToCartRemove', 'Added to Cart (Remove)'));
        recalctotals();
    });
    jQuery(".addon-products").on('ifUnchecked', '.panel-addon input', function(event) {
        var $activeAddon = jQuery(this).parents('.panel-addon');
        $activeAddon.removeClass('panel-addon-selected');
        $activeAddon.find('input[type="checkbox"]').iCheck('uncheck');
        $activeAddon.find('.panel-add').html('<i class="fa fa-plus"></i> '+localTrans('addToCart', 'Add to Cart'));
        recalctotals();
    });

    jQuery(".domain-selection-options input:first").iCheck('check');
    jQuery(".domain-selection-options input:first").parents('.option').addClass('option-selected');
    jQuery("#domain" + jQuery(".domain-selection-options input:first").val()).show();
    jQuery(".domain-selection-options input").on('ifChecked', function(event){
        jQuery(".domain-selection-options .option").removeClass('option-selected');
        jQuery(this).parents('.option').addClass('option-selected');
        jQuery(".domain-input-group").hide();
        jQuery("#domain" + jQuery(this).val()).show();
    });

    jQuery(".domain-selection-options .option").click(function(e) {
        jQuery(this).find('input').iCheck('check');
    });

    jQuery('#frmProductDomain button[type="submit"]').click(function(e) {
        e.preventDefault();
        var btnSearchObj = jQuery(this);
        var preSearchText = btnSearchObj.html();
        jQuery(this).html('<i class="fa fa-spinner fa-spin"></i> ' + preSearchText);
        jQuery("#domainSearchResults").hide();
        jQuery("#frmProductDomain").hide();
        jQuery("#domainLoadingSpinner").show();
        var domainoption = jQuery(".domain-selection-options input:checked").val();
        var sld = jQuery("#"+domainoption+"sld").val();
        var tld = '';
        if (domainoption=='incart') {
            var sld = jQuery("#"+domainoption+"sld option:selected").text();
        } else if (domainoption=='subdomain') {
            var tld = jQuery("#"+domainoption+"tld option:selected").text();
        } else {
            var tld = jQuery("#"+domainoption+"tld").val();
        }
        jQuery.post("cart.php", { ajax: 1, a: "domainoptions", sld: sld, tld: tld, checktype: domainoption, cancelbtn: "yes" },
            function(data) {
                jQuery("#domainLoadingSpinner").hide();
                jQuery("#domainSearchResults").html(data);
                jQuery("#domainSearchResults").slideDown();
                btnSearchObj.html(preSearchText);
            }
        );
    });


	jQuery("#cancelbtn").click(function() {
        e.preventDefault();
		jQuery("#domainSearchResults").hide();
		jQuery("#frmProductDomain").show();
    });
    
    
    
    jQuery("#btnAlreadyRegistered").click(function() {
        jQuery("#containerNewUserSignup").slideUp('', function() {
            jQuery("#containerExistingUserSignin").hide().removeClass('hidden').slideDown('', function() {
                jQuery("#inputCustType").val('existing');
                jQuery("#btnAlreadyRegistered").fadeOut('', function() {
                    jQuery("#btnNewUserSignup").removeClass('hidden').fadeIn();
                });
            });
        });
        jQuery("#containerNewUserSecurity").hide();
        if (jQuery("#stateselect").attr('required')) {
            jQuery("#stateselect").removeAttr('required').addClass('requiredAttributeRemoved');
        }
    });

    jQuery("#btnNewUserSignup").click(function() {
        jQuery("#containerExistingUserSignin").slideUp('', function() {
            jQuery("#containerNewUserSignup").hide().removeClass('hidden').slideDown('', function() {
                jQuery("#inputCustType").val('new');
                jQuery("#containerNewUserSecurity").show();
                jQuery("#btnNewUserSignup").fadeOut('', function() {
                    jQuery("#btnAlreadyRegistered").removeClass('hidden').fadeIn();
                });
            });
        });
        if (jQuery("#stateselect").hasClass('requiredAttributeRemoved')) {
            jQuery("#stateselect").attr('required', 'required').removeClass('requiredAttributeRemoved');
        }
    });

    jQuery(".payment-methods").on('ifChecked', function(event) {
        if (jQuery(this).hasClass('is-credit-card')) {
            if (!jQuery("#creditCardInputFields").is(":visible")) {
                jQuery("#creditCardInputFields").hide().removeClass('hidden').slideDown();
                            console.log('slidedown');
            }
        } else {
            jQuery("#creditCardInputFields").slideUp();
            console.log('slideup');
        }
    });

    jQuery("input[name='ccinfo']").on('ifChecked', function(event) {
        if (jQuery(this).val() == 'new') {
            jQuery("#existingCardInfo").slideUp('', function() {
                jQuery("#newCardInfo").hide().removeClass('hidden').slideDown();
            });
        } else {
            jQuery("#newCardInfo").slideUp('', function() {
                jQuery("#existingCardInfo").hide().removeClass('hidden').slideDown();
            });
        }
    });

    jQuery("#inputDomainContact").on('change', function() {
        if (this.value == "addingnew") {
            jQuery("#domainRegistrantInputFields").hide().removeClass('hidden').slideDown();
        } else {
            jQuery("#domainRegistrantInputFields").slideUp();
        }
    });

   

    jQuery("#btnCheckAvailability").click(function(e) {
        e.preventDefault();
        var buttonContent = jQuery(this).html();
        jQuery(this).html('<i class="fa fa-spinner fa-spin"></i> ' + buttonContent);
        jQuery("#domainSearchResults").hide();
        jQuery("#domainLoadingSpinner").show();
        jQuery.post("cart.php", jQuery("#frmDomainSearch").serialize(),
                function(data) {
                    jQuery("#domainLoadingSpinner").hide();
                    jQuery("#domainSearchResults").html(data).show();
                    jQuery("#btnCheckAvailability").html(buttonContent);
                }
            );
    });

    jQuery("#btnEmptyCart").click(function() {
        jQuery('#modalEmptyCart').modal('show');
    });
    
     jQuery("#btnPromoCodeApply").click(function() {
        jQuery('#modalPromoCodeApply').modal('show');
    });
  
    jQuery("#btnPromoCodeRemove").click(function() {
        jQuery('#modalPromoCodeRemove').modal('show');
    });
  
    jQuery("#btnEstimateTaxes").click(function() {
        jQuery('#modalEstimateTaxes').modal('show');
    });

    jQuery("#cardType li a").click(function (e) {
        e.preventDefault();
        jQuery("#selectedCardType").html(jQuery(this).html());
        jQuery("#cctype").val(jQuery('span.type', this).html());
    });
    
    jQuery("#tlddropdown li a").click(function (e) {
        e.preventDefault();
        jQuery("#tldbtn").html(jQuery(this).html());
    });
    
    jQuery("#tlddropdown2 li a").click(function (e) {
        e.preventDefault();
        jQuery("#tldbtn2").html(jQuery(this).html());
    });

    jQuery("#tlddropdown3 li a").click(function (e) {
        e.preventDefault();
        jQuery("#tldbtn3").html(jQuery(this).html());
    });
    
    jQuery("#tlddropdown4 li a").click(function (e) {
        e.preventDefault();
        jQuery("#tldbtn4").html(jQuery(this).html());
    });

});

function domainGotoNextStep() {
    jQuery("#domainLoadingSpinner").show();
    jQuery("#frmProductDomainSelections").submit();
}

function cancelcheck() {
		jQuery("#domainSearchResults").hide();
		jQuery("#frmProductDomain").show();
		window.scrollTo(0, 200);
}

function removeItem(type, num) {
    jQuery('#inputRemoveItemType').val(type);
    jQuery('#inputRemoveItemRef').val(num);
    jQuery('#modalRemoveItem').modal('show');
}

function updateConfigurableOptions(i, billingCycle) {

    jQuery.post("cart.php", 'a=cyclechange&ajax=1&i='+i+'&billingcycle='+billingCycle,
        function(data) {
            jQuery("#productConfigurableOptions").html(jQuery(data).find('#productConfigurableOptions').html());
        }
    );
    recalctotals();

}

function recalctotals() {
    if (!jQuery("#orderSummaryLoader").is(":visible")) {
        jQuery("#orderSummaryLoader").fadeIn('fast');
    }
    
    thisRequestId = Math.floor((Math.random() * 1000000) + 1);
    window.lastSliderUpdateRequestId = thisRequestId;
    
    var post = jQuery.post("cart.php", 'ajax=1&a=confproduct&calctotal=true&'+jQuery("#frmConfigureProduct").serialize());
    post.done(
        function(data) {
            if (thisRequestId == window.lastSliderUpdateRequestId) {
                jQuery("#producttotal").html(data);
            }
        }
    );
    post.always(
        function() {
            jQuery("#orderSummaryLoader").delay(500).fadeOut('slow');
        }
    );
}

function selectDomainPricing(domainName, price, period, yearsString, suggestionNumber) {
    jQuery("#domainSuggestion" + suggestionNumber).iCheck('check');
    jQuery("[name='domainsregperiod[" + domainName + "]']").val(period);
    jQuery("[name='" + domainName + "-selected-price']").html('<b class="glyphicon glyphicon-shopping-cart"></b>'
        + ' ' + period + ' ' + yearsString + ' @ ' + price);
}

function catchEnter(e) {
    if (e) {
        addtocart();
        e.returnValue=false;
    }
}

function sameHeights(selector) {
    var selector = selector || '[data-key="sameHeights"]',
        query = document.querySelectorAll(selector),
        elements = query.length,
        max = 0;
    if (elements) {
        while (elements--) {
            var element = query[elements];
            if (element.clientHeight > max) {
                max = element.clientHeight;
            }
        }
        elements = query.length;
        while (elements--) {
            var element = query[elements];
            element.style.height = max + 'px';
        }
    }
}

function sameHeights2(selector) {
    var selector = selector || '[data-key="sameHeights2"]',
        query = document.querySelectorAll(selector),
        elements = query.length,
        max = 0;
    if (elements) {
        while (elements--) {
            var element = query[elements];
            if (element.clientHeight > max) {
                max = element.clientHeight;
            }
        }
        elements = query.length;
        while (elements--) {
            var element = query[elements];
            element.style.height = max + 'px';
        }
    }
}

function sameHeights3(selector) {
    var selector = selector || '[data-key="sameHeights3"]',
        query = document.querySelectorAll(selector),
        elements = query.length,
        max = 0;
    if (elements) {
        while (elements--) {
            var element = query[elements];
            if (element.clientHeight > max) {
                max = element.clientHeight;
            }
        }
        elements = query.length;
        while (elements--) {
            var element = query[elements];
            element.style.height = max + 'px';
        }
    }
}	

function sameHeights4(selector) {
    var selector = selector || '[data-key="sameHeights4"]',
        query = document.querySelectorAll(selector),
        elements = query.length,
        max = 0;
    if (elements) {
        while (elements--) {
            var element = query[elements];
            if (element.clientHeight > max) {
                max = element.clientHeight;
            }
        }
        elements = query.length;
        while (elements--) {
            var element = query[elements];
            element.style.height = max + 'px';
        }
    }
}	

function sameHeights5(selector) {
    var selector = selector || '[data-key="sameHeights5"]',
        query = document.querySelectorAll(selector),
        elements = query.length,
        max = 0;
    if (elements) {
        while (elements--) {
            var element = query[elements];
            if (element.clientHeight > max) {
                max = element.clientHeight;
            }
        }
        elements = query.length;
        while (elements--) {
            var element = query[elements];
            element.style.height = max + 'px';
        }
    }
}	