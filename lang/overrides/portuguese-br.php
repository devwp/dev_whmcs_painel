<?php
$_LANG['navservicesorder'] = "Contratar Serviços";
$_LANG['tablepagesnext'] = "Próximo";
$_LANG['clientareaterminated'] = "Finalizado";
$_LANG['orderForm']['domainAvailabilityCached'] = "Os resultados de disponibilidade de domínio estão armazenados em memória, o que pode fazer com que domínios registrados recentemente sejam exibidos como disponíveis.";
$_LANG['emailAddressVerified'] = "Obrigado por confirmar seu endereço de e-mail.";
$_LANG['verifyEmailAddress'] = "Por favor verifique seu e-mail e siga as instruções do link para validar seu endereço de e-mail.";
$_LANG['resendEmail'] = "Reenviar E-mail de Verificação";
$_LANG['orderForm']['use'] = "Usar";
$_LANG['orderForm']['check'] = "Verificar";
$_LANG['quotedownload'] = "Baixar PDF";

////////// End of portuguese-br language file.  Do not place any translation strings below this line!
////////// End of english language file.  Do not place any translation strings below this line!
$_LANG['smglobalcancel'] = "Cancelar";
$_LANG['smglobalsave'] = "Salvar";
$_LANG['smglobalreflesh'] = "Atualizar";
$_LANG['smglobalDelete'] = "Excluir";
$_LANG['smglobalback'] = "Voltar";
$_LANG['smglobalshow'] = "Exibir";
$_LANG['smglobalsearch'] = "Pesquisar";
$_LANG['smglobalshowing'] = "Mostrando...";
$_LANG['smgloballoading'] = "Carregando...";
$_LANG['smglobalto'] = "de";
$_LANG['smglobalPrev'] = "Ant.";
$_LANG['smglobalNext'] = "Prox.";

$_LANG['smglobalNothing'] = "Nenhum dado encontrado";
$_LANG['smmanageusers'] = "Gerenciar Usuários";
$_LANG['smaddusers'] = "Adicionar Usuário";
$_LANG['smeditusers'] = "Editar Usuário";
$_LANG['smusersemail'] = "E-mail";
$_LANG['smusersname'] = "Nome de Exibição";
$_LANG['smuserspass'] = "Senha";
$_LANG['smusersconfirmpass'] = "Confirmar Senha";
$_LANG['smmanageusers'] = "Gerenciar Usuários";
$_LANG['smuserssize'] = "Tamanho";

$_LANG['smmanagemailinglist'] = "Gerenciar Listas de E-mail";
$_LANG['smaddmailinglist'] = "Adicionar Lista de E-mail";
$_LANG['smeditmailinglist'] = "Editar Lista de E-mail";
$_LANG['smmailinglistsModerator'] = "Moderador";
$_LANG['smmailinglistsname'] = "Nome da Lista";
$_LANG['smmailinglistssize'] = "Tamanho";
$_LANG['smmailinglistsperm'] = "Permissão";
$_LANG['smmailinglistsmod'] = "Moderador";
$_LANG['smmailinglistsass'] = "Assinante";
$_LANG['smmailinglistsother'] = "Qualquer Um";

$_LANG['smmanagemailinglistASS'] = "Gerenciar Assinantes";
$_LANG['smaddmailinglistASS'] = "Adicionar Assinante";
$_LANG['smeditmailinglistASS'] = "Editar Assinante";
$_LANG['smmailinglistsASS'] = "Assinantes";

$_LANG['smmanageAlias'] = "Gerenciar Apelidos";
$_LANG['smAliasEmail'] = "E-mail";
$_LANG['smaddAlias'] = "Adicionar Apelido";
$_LANG['smeditAlias'] = "Editar Apelido";
$_LANG['smAlias'] = "Apelido";

$_LANG['smmanageDomain'] = "Gerenciar Domínios";
$_LANG['smaddDomain'] = "Adicionar Domínio";
$_LANG['smeditDomain'] = "Editar Domínio";
$_LANG['smDomain'] = "Domínio";


////// SMARTER TRACK
$_LANG['priority'] = "Prioridade";

